/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DAGGER_H
#define DAGGER_H

#include "Object/ObjectItem.h"

/** \addtogroup Object
  * \{
  * \class Dagger
  *
  * \brief The dagger object.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class Dagger : public ObjectItem
{
    Q_OBJECT

public:
    explicit Dagger(const QString &name = tr("Dagger"), const QString &description = tr("A short pointy blade to cut and stab with."), bool inside = false, QObject *parent = 0);
    explicit Dagger(const Dagger &other);

protected:
    void initDagger();

public:
    virtual NumberMod *refAttackModifier();
    virtual int getCritRange() const;
    virtual Dice baseDamageDice();

    virtual Dagger *copy() const;
    virtual void update(const ObjectBase &object);

    // Get-Methods
    virtual ObjectID::ItemID getObjectID() const;
    virtual QList<CommandTree *> getCommandTreeList(ObjectBase *player);
    virtual QImage getSilhouette() const;
    virtual QPoint getSlotOffset() const;

    // Serialize
    virtual void serialize(QDataStream &dataStream) const;
    virtual void serialize(QIODevice *device) const;
    virtual void deserialize(QDataStream &dataStream);
    virtual void deserialize(QIODevice *device);
};

#endif // DAGGER_H
