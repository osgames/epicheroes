/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Corpse.h"

#include <QPainter>
#include <QDebug>

#include "Common/Common.h"

Corpse::Corpse(ObjectCharacter *character, const ItemContainer &container, bool inside, QObject *parent)
// TODO: Weight based on character and derive rest in ObjectContainerItem class in a "getWeight" virtual.
    : ObjectContainerItem(container, Object::NONE, false, tr("Corpse"), tr("A lifeless body of an unknown being."), ":/objects/items/corpse", 1, Object::MEDIUM_SIZE, true, inside, parent)
{
    bool isGeneric = true;

    if(character)
    {
        this->setObjectName(tr("%1's Corpse").arg(character->getObjectName()));
        this->setDescription(tr("The lifeless body of the deceased %1.").arg(character->getObjectName()));
        this->setSizeIncrement(character->getSizeIncrement());
        this->setPosition(character->getX(), character->getY(), character->getZ());
        this->loadImage(character->getCurrentImagePath());
        isGeneric = false;
    }
    this->initCorpse(isGeneric);
}

Corpse::Corpse(const Corpse &corpse)
    : ObjectContainerItem(corpse)
{
    this->initCorpse(corpse.isGeneric());
}

void Corpse::initCorpse(bool isGeneric)
{
    this->setGeneric(isGeneric);

    return;
}

Corpse *Corpse::copy() const
{
    return new Corpse(*this);
}

void Corpse::update(const ObjectBase &object)
{
    if(this->getType() != object.getType() || this->getObjectIDNumber() != object.getObjectIDNumber())
    {
        qDebug("Object has not been updatet since the given object was not of that type.");
        return;
    }

    const Corpse &corpse = (const Corpse &) object;

    this->initCorpse(corpse.isGeneric());

    this->updateContainerItem(corpse);
    return;
}

ObjectID::ItemID Corpse::getObjectID() const
{
    return ObjectID::CORPSE;
}

QList<CommandTree *> Corpse::getCommandTreeList(ObjectBase *player)
{
    QList<CommandTree *> list;

    list.append(this->ObjectContainerItem::getCommandTreeList(player));

    return list;
}

QImage Corpse::getSilhouette() const
{
    return QImage(":/objects/items/corpse_s");
}

QPoint Corpse::getSlotOffset() const
{
    return QPoint(1,7);
}

bool Corpse::isGeneric() const
{
    return this->generic;
}

QImage Corpse::getImage() const
{
    return this->ObjectContainerItem::getImage();
}

QImage Corpse::getImage(int index) const
{
    QImage image = this->ObjectContainerItem::getImage(index);

    if(!this->isGeneric())
    {
        QPainter painter(&image);
        painter.drawImage(TEoH::CONDITION_X_POSITION_ON_SQUARE, 1, QImage(":/objects/characters/conditions/dead"));
    }

    return image;
}

void Corpse::setGeneric(bool isGeneric)
{
    this->generic = isGeneric;
    return;
}

void Corpse::serialize(QDataStream &dataStream) const
{
    this->ObjectContainerItem::serialize(dataStream);

    dataStream << this->isGeneric();
    return;
}

void Corpse::serialize(QIODevice *device) const
{
    QDataStream stream(device);
    this->serialize(stream);
    return;
}

void Corpse::deserialize(QDataStream &dataStream)
{
    this->ObjectContainerItem::deserialize(dataStream);

    bool isGeneric;
    dataStream >> isGeneric;
    this->initCorpse(isGeneric);
    return;
}

void Corpse::deserialize(QIODevice *device)
{
    QDataStream stream(device);
    this->deserialize(stream);
    return;
}
