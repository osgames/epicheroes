/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CORPSE_H_
#define _CORPSE_H_

#include "Object/Item/ObjectContainerItem.h"
#include "Object/ObjectCharacter.h"

/** \addtogroup Object
  * \{
  * \class Corpse
  *
  * \brief A corpse of a character.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class Corpse : public ObjectContainerItem
{
    Q_OBJECT

    bool generic;

public:
    explicit Corpse(ObjectCharacter *character = 0, const ItemContainer &container = ItemContainer(), bool inside = false, QObject *parent = 0);
    explicit Corpse(const Corpse &corpse);

protected:
    void initCorpse(bool isGeneric = true);

public:
    virtual Corpse *copy() const;
    virtual void update(const ObjectBase &object);

    // Get-Methods
    virtual ObjectID::ItemID getObjectID() const;
    virtual QList<CommandTree *> getCommandTreeList(ObjectBase *player);
    virtual QImage getSilhouette() const;
    virtual QPoint getSlotOffset() const;
    bool isGeneric() const;

    virtual QImage getImage() const;
    virtual QImage getImage(int index) const;

    // Set-Methods
    void setGeneric(bool isGeneric);

    // Serialize
    virtual void serialize(QDataStream &dataStream) const;
    virtual void serialize(QIODevice *device) const;
    virtual void deserialize(QDataStream &dataStream);
    virtual void deserialize(QIODevice *device);
};

#endif // _CORPSE_H_
