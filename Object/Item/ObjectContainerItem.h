/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef OBJECTCONTAINERITEM_H
#define OBJECTCONTAINERITEM_H

#include "Object/ObjectItem.h"
#include "Object/Item/ItemContainer.h"

/** \addtogroup Object
  * \{
  * \class ObjectContainerItem
  *
  * \brief An container class to represent items holding a container.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class ObjectContainerItem : public ObjectItem
{
    Q_OBJECT

protected:
    ItemContainer container;

public:
    explicit ObjectContainerItem(const ItemContainer &container,
                                 Object::EquipmentSlotType equipmentSlot = Object::NONE,
                                 bool wieldable = true,
                                 const QString &name = "",
                                 const QString &description = "",
                                 const QString &imagePath = "",
                                 int weight = 100,
                                 Object::SizeIncrement size = Object::MEDIUM_SIZE,
                                 bool isVisible = true,
                                 bool inside = false,
                                 QObject *parent = 0);
    explicit ObjectContainerItem(const ObjectContainerItem &other);

public:
    virtual ObjectBase *lookupObjectInside(ObjectBase::ObjectType type, int objectIDNumber, unsigned int spawnID) const;

    void updateContainerItem(const ObjectContainerItem &other);

    // Get-Methods
    virtual QList<CommandTree *> getCommandTreeList(ObjectBase *player);
    virtual QImage getImage(int index) const;
    virtual QImage getImage() const;
    virtual bool containsItems() const;

    // Ref-Methods
    ItemContainer *refContainer();

public:
    // Serialize
    virtual void serialize(QDataStream &dataStream) const;
    virtual void deserialize(QDataStream &dataStream);
};

#endif // OBJECTCONTAINERITEM_H
