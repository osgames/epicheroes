/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ITEM_CONTAINER_H_
#define _ITEM_CONTAINER_H_

#include <QMap>
#include <QObject>
#include <QList>
#include <QDataStream>

#include "Object/ObjectItem.h"
#include "Command/Backend/BaseCommand.h"

/** \addtogroup Commands
  * \{
  * \class ItemContainer
  *
  * \brief Holds items and manages them.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class ItemContainer : public QObject
{
    Q_OBJECT

signals:
    void itemContainerUpdated();

public:
    /**
     * @brief The default container space.
     */
    static const int DEFAULT_CONTAINER_SPACE = 512;

private:
    QList<ObjectItem *> items;
    int containerSpace;

public:
    ItemContainer(int containerSpace = DEFAULT_CONTAINER_SPACE, const QList<ObjectItem *> &items = QList<ObjectItem *>(), QObject *parent = 0);
    ItemContainer(const ItemContainer &other);

    ItemContainer &operator=(const ItemContainer &other);

    ObjectItem *lookup(ObjectBase::ObjectType type, int objectIDNumber, unsigned int spawnID) const;

    void addItem(ObjectItem *item);
    void addItems(const QList<ObjectItem *> &items);
    void insertItem(int index, ObjectItem *item);

    ObjectItem *takeItem(ObjectItem *item);
    ObjectItem *takeItem(int index);
    QList<ObjectItem *> takeItems(const QList<ObjectItem *> &items);
    QList<ObjectItem *> takeItems(const QList<int> &indexes);
    QList<ObjectItem *> takeAllItems();

    int removeItem(ObjectItem *item);
    int removeItem(int index);
    void removeItems(const QList<ObjectItem *> &items);
    void removeItems(const QList<int> &indexes);

    void update(const ItemContainer &other);

    // Get-Methods
    int getContainerSpaceTotal() const;
    bool hasItem(ObjectItem *item) const;
    ObjectItem *getItem(int index) const;
    QList<ObjectItem *> getItems(const QList<int> &indexes) const;
    QList<ObjectItem *> getItems() const;

    // Set-Methods
    void setContainerSpace(int containerSpace);

    // Ref-Methods
    ObjectItem *refItem(int index) const;

    // Serialize
    void serialize(QDataStream &dataStream) const;
    void deserialize(QDataStream &dataStream);

private:
    void copy(const ItemContainer &other);
};

#endif // _ITEM_CONTAINER_H_
