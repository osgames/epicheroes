/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ObjectContainerItem.h"

#include "Command/Backend/Object/Item/SearchContainer.h"

#include "Command/Backend/CommandLeaf.h"

ObjectContainerItem::ObjectContainerItem(const ItemContainer &container, Object::EquipmentSlotType equipmentSlot, bool wieldable, const QString &name, const QString &description, const QString &imagePath, int weight, Object::SizeIncrement size, bool isVisible, bool inside, QObject *parent)
    : ObjectItem(equipmentSlot, wieldable, name, description, imagePath, weight, size, isVisible, inside, parent), container(container)
{}

ObjectContainerItem::ObjectContainerItem(const ObjectContainerItem &other)
    : ObjectItem(other)
{
    this->container = other.container;
}

ObjectBase *ObjectContainerItem::lookupObjectInside(ObjectBase::ObjectType type, int objectIDNumber, unsigned int spawnID) const
{
    return this->container.lookup(type, objectIDNumber, spawnID);
}

void ObjectContainerItem::updateContainerItem(const ObjectContainerItem &other)
{
    this->container.update(other.container);

    this->updateItem(other);
    return;
}

QList<CommandTree *> ObjectContainerItem::getCommandTreeList(ObjectBase *player)
{
    QList<CommandTree *> list;

    list.append(new CommandLeaf(new SearchContainerItem(this), tr("Search (Standard)")));

    list.append(this->ObjectItem::getCommandTreeList(player));

    return list;
}

QImage ObjectContainerItem::getImage(int index) const
{
    return this->ObjectItem::getImage(index);
}

QImage ObjectContainerItem::getImage() const
{
    return this->ObjectItem::getImage();
}

bool ObjectContainerItem::containsItems() const
{
    return true;
}

ItemContainer *ObjectContainerItem::refContainer()
{
    return &this->container;
}

void ObjectContainerItem::serialize(QDataStream &dataStream) const
{
    this->ObjectItem::serialize(dataStream);
    this->container.serialize(dataStream);

    return;
}

void ObjectContainerItem::deserialize(QDataStream &dataStream)
{
    this->ObjectItem::deserialize(dataStream);
    this->container.deserialize(dataStream);

    return;
}
