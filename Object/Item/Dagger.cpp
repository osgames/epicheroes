/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Dagger.h"

#include <QGridLayout>
#include <QLabel>

#include "Command/Backend/Object/ShowDescription.h"

Dagger::Dagger(const QString &name, const QString &description, bool inInventory, QObject *parent)
    : ObjectItem(Object::NONE, true, name, description, ":/objects/items/dagger", 50, Object::SMALL_SIZE, true, inInventory, parent)
{
    this->initDagger();
}

Dagger::Dagger(const Dagger &other)
    : ObjectItem(other)
{
    this->initDagger();
}

void Dagger::initDagger()
{
    return;
}

NumberMod *Dagger::refAttackModifier()
{
    return new NumberMod(0,tr("%1").arg(this->getObjectName()), true, this);
}

int Dagger::getCritRange() const
{
    return 19;
}

Dice Dagger::baseDamageDice()
{
    return Dice(tr("Dagger Base Damage"), 1, 4);
}

Dagger *Dagger::copy() const
{
    return new Dagger(*this);
}

void Dagger::update(const ObjectBase &object)
{
    if(this->getType() != object.getType() || this->getObjectIDNumber() != object.getObjectIDNumber())
    {
        qDebug("Object has not been updatet since the given object was not of that type.");
        return;
    }

    const Dagger &dagger = (const Dagger &) object;

    this->updateItem(dagger);
    return;
}

ObjectID::ItemID Dagger::getObjectID() const
{
    return ObjectID::DAGGER;
}

QList<CommandTree *> Dagger::getCommandTreeList(ObjectBase *player)
{
    QList<CommandTree *> list;

    list.append(this->ObjectItem::getCommandTreeList(player));

    return list;
}

QImage Dagger::getSilhouette() const
{
    return QImage(":/objects/items/dagger_s");
}

QPoint Dagger::getSlotOffset() const
{
    return QPoint(1,5);
}

void Dagger::serialize(QDataStream &dataStream) const
{
    this->ObjectItem::serialize(dataStream);
    return;
}

void Dagger::serialize(QIODevice *device) const
{
    QDataStream stream(device);
    this->serialize(stream);
    return;
}

void Dagger::deserialize(QDataStream &dataStream)
{
    this->ObjectItem::deserialize(dataStream);
    return;
}

void Dagger::deserialize(QIODevice *device)
{
    QDataStream stream(device);
    this->deserialize(stream);
    return;
}


