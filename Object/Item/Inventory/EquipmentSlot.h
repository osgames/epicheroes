/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _EQUIPMENT_SLOT_H_
#define _EQUIPMENT_SLOT_H_

#include <QPoint>

#include "Object/ObjectCommon.h"
#include "Object/ObjectItem.h"
#include "Common/Serialize.h"

/** \addtogroup Object
  * \{
  * \class EquipmentSlot
  *
  * \brief Represents a place where you can equipt items in the inventory.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class EquipmentSlot : public QObject
{
    Q_OBJECT

    QPoint                      slotPosition;
    Object::EquipmentSlotType   type;
    ObjectItem *                item;

public:
    EquipmentSlot(const QPoint &slotPosition = QPoint(), Object::EquipmentSlotType type = Object::NONE, ObjectItem *item = 0);
    EquipmentSlot(const EquipmentSlot &other);

    EquipmentSlot &operator=(const EquipmentSlot &other);

    virtual ~EquipmentSlot();

private:
    void init(const EquipmentSlot &other);
    void init(QPoint slotPosition = QPoint(),
              Object::EquipmentSlotType type = Object::NONE,
              ObjectItem *item = 0);

public:
    void removeItem();
    ObjectItem *takeItem();

    // Get-Methods
    QPoint getSlotPosition() const;
    QPoint getItemPosition() const;
    Object::EquipmentSlotType getEquipmentSlotType() const;
    bool hasItemEquiped() const;
    // Set-Methods
    void setSlotPosition(const QPoint &slotPosition);
    void setEquipmentSlotType(Object::EquipmentSlotType type);
    void setItem(ObjectItem *item);
    // Ref-Methods
    ObjectItem *refItem() const;

    // Serialize
    virtual void serialize(QDataStream &dataStream) const;
    virtual void deserialize(QDataStream &dataStream);
};

#endif // EQUIPMENTSLOT_H
