/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SELECTED_ITEM_MODEL_H_
#define _SELECTED_ITEM_MODEL_H_

#include <QObject>
#include <QPair>

#include "Command/Processor.h"
#include "Object/ObjectCharacter.h"
#include "Object/ObjectItem.h"

/** \addtogroup Object
  * \{
  * \class SelectedItemModel
  *
  * \brief Represents the item that is currently selected that may be moved in the inventory.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class SelectedItemModel : public QObject
{
    Q_OBJECT
    Processor *                 processor;
    ObjectCharacter *           character;
    QPair<int, ObjectItem *>    selectedItem;
    int                         originalIndex;
    InventoryModel::Type        originalInventoryType;

public:
    SelectedItemModel(Processor *processor, ObjectCharacter *character, QObject *parent = 0);

    void selectBackpackItem(int index);
    void unselectBackpackItem();
    void selectEquipmentItem(int index);
    void unselectEquipmentItem();
    void changeSelectedItemIndex(int index);

    bool isEquipable(int index);

    // Ref-Methods
    ObjectItem *refSelectedItem();

private:
    bool selectItem(int index, InventoryModel::Type inventoryType, ObjectItem *item);
    bool unselectItem();
};

#endif // _SELECTED_ITEM_MODEL_H_
