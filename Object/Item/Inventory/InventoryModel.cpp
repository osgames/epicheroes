/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "InventoryModel.h"

#include <stdint.h>

#include <QDebug>

#include "Object/ObjectFromID.h"

InventoryModel::InventoryModel(QList<EquipmentSlot> equipment, int backpackSpaceTotal, QObject *parent)
    : QObject(parent)
{
    this->init(equipment, ItemContainer(backpackSpaceTotal));
}

InventoryModel::InventoryModel(const InventoryModel &inventoryModel)
    : QObject(inventoryModel.parent())
{
    this->init(inventoryModel.getEquipment(),
               inventoryModel.getBackpack(),
               inventoryModel.getPlatinumPieces(),
               inventoryModel.getGoldPieces(),
               inventoryModel.getSilverPieces(),
               inventoryModel.getCopperPieces());
}

void InventoryModel::init(QList<EquipmentSlot> equipment, const ItemContainer &backpack, int platinumPieces, int goldPieces, int silverPieces, int copperPieces)
{
    this->equipment = equipment;
    this->backpack = backpack;
    this->setPlatinumPieces(platinumPieces);
    this->setGoldPieces(goldPieces);
    this->setSilverPieces(silverPieces);
    this->setCopperPieces(copperPieces);

    connect(&this->backpack, SIGNAL(itemContainerUpdated()), this, SLOT(sendBackpackUpdated()));

    return;
}

void InventoryModel::update(const InventoryModel &newInventory)
{
    this->updateBackpack(newInventory.getBackpack());
    this->updateEquipment(newInventory.getEquipment());
    return;
}

void InventoryModel::updateBackpack(const ItemContainer &newBackpack)
{
    this->backpack.update(newBackpack);
    return;
}

void InventoryModel::updateEquipment(const QList<EquipmentSlot> &newEquipment)
{
    QList<EquipmentSlot> updatedEquipment;

    int i;

    EquipmentSlot slot;
    EquipmentSlot newSlot;
    ObjectItem *item;

    for(i = 0; i < this->equipment.size() && i < newEquipment.size(); ++i)
    {
        slot = this->equipment.at(i);
        newSlot = newEquipment.at(i);

        if(slot.getEquipmentSlotType() == newSlot.getEquipmentSlotType() &&
           (slot.refItem() == newSlot.refItem() ||
           (slot.refItem() && newSlot.refItem() && slot.refItem()->equals(newSlot.refItem()))))
        {
            updatedEquipment.append(slot);
            if(slot.refItem())
            {
                slot.refItem()->setParent(this);
            }
        }
        else
        {
            if(slot.refItem())
            {
                slot.removeItem();
            }

            if(newSlot.refItem())
            {
                item = newSlot.refItem()->copy();
                item->setParent(this);
                item->setInside(true);
            }
            else
            {
                item = 0;
            }

            newSlot.setItem(item);
            updatedEquipment.append(newSlot);
        }
    }

    for(;i < this->equipment.size(); ++i)
    {
        if(this->equipment.at(i).refItem())
        {
            this->equipment[i].removeItem();
        }
    }

    for(;i < newEquipment.size(); ++i)
    {
        slot = newEquipment.at(i);
        item = slot.refItem();

        if(item)
        {
            item = item->copy();
            item->setParent(this);
            item->setInside(true);
        }

        slot.setItem(item);
        updatedEquipment.append(slot);
    }

    this->equipment = updatedEquipment;

    emit this->equippedItemsUpdated();
    return;
}

ObjectItem *InventoryModel::lookup(ObjectBase::ObjectType type, int objectIDNumber, unsigned int spawnID) const
{
    foreach(EquipmentSlot slot, this->equipment)
    {
        ObjectItem *item = slot.refItem();

        if(item)
        {
            if(item->equals(type, objectIDNumber, spawnID))
            {
                return item;
            }
            else
            {
                ObjectBase *insideObject = item->lookupObjectInside(type, objectIDNumber, spawnID);

                if(insideObject)
                {
                   return qobject_cast<ObjectItem *>(insideObject);
                }
            }
        }
    }

    return this->backpack.lookup(type, objectIDNumber, spawnID);
}

bool InventoryModel::addToBackpack(ObjectItem *item)
{
    if(!item)
    {
        return false;
    }

    this->backpack.addItem(item);
    return true;
}

bool InventoryModel::insertToBackpack(int index, ObjectItem *item)
{
    if(!item)
    {
        return false;
    }

    this->backpack.insertItem(index,item);

    return true;
}

ObjectItem *InventoryModel::removeFromBackpack(int index)
{
    return this->backpack.takeItem(index);;
}

int InventoryModel::removeFromBackpack(ObjectItem *item)
{
    return this->backpack.removeItem(item);;
}

bool InventoryModel::insertToEquipment(int index, ObjectItem *item)
{
    if(!item || index < 0 || index >= this->equipment.size() || !this->isEquipable(index, item))
    {
        return false;
    }

    this->equipment[index].setItem(item);
    item->setInside(true);

    return true;
}

ObjectItem *InventoryModel::removeFromEquipment(int index)
{
    ObjectItem *item = 0;

    if(index >= 0 && index < this->equipment.size() && this->equipment[index].refItem())
    {
        item = this->equipment[index].refItem();
        this->equipment[index].removeItem();
        item->setInside(false);
    }

    return item;
}

int InventoryModel::removeFromEquipment(ObjectItem *item)
{
    if(!item)
    {
        return -1;
    }

    ObjectItem *foundItem;
    for(int i = 0; i < this->equipment.size(); ++i)
    {
        foundItem = this->equipment.at(i).refItem();

        if(foundItem && foundItem->equals(item))
        {
            this->equipment[i].removeItem();
            item->setInside(false);
            emit this->equippedItemsUpdated();
            return i;
        }
    }

    return -1;
}

int InventoryModel::removeFromInventory(ObjectItem *item)
{
    int indexResult = this->removeFromBackpack(item);
    if(indexResult == -1) // If item was not found in backpack, try the equipment.
    {
        indexResult = this->removeFromEquipment(item);
    }

    return indexResult;
}

int InventoryModel::removeFromInventory(ObjectItem *item, InventoryModel::Type inventoryType)
{
    switch(inventoryType)
    {
    case InventoryModel::BACKPACK:
    {
        return this->removeFromBackpack(item);
        break;
    }
    case InventoryModel::EQUIPMENT:
    {
        return this->removeFromEquipment(item);
        break;
    }
    default: return -1; break;
    }

    return -1;
}

ObjectItem *InventoryModel::removeFromInventory(int index, InventoryModel::Type inventoryType)
{
    switch(inventoryType)
    {
    case InventoryModel::BACKPACK:
    {
        return this->removeFromBackpack(index);
        break;
    }
    case InventoryModel::EQUIPMENT:
    {
        return this->removeFromEquipment(index);
        break;
    }
    default: return 0; break;
    }

    return 0;
}

void InventoryModel::insertToInventory(int index, ObjectItem *item, InventoryModel::Type inventoryType)
{
    switch(inventoryType)
    {
    case InventoryModel::BACKPACK:
    {
        this->insertToBackpack(index, item);
        break;
    }
    case InventoryModel::EQUIPMENT:
    {
        this->insertToEquipment(index, item);
        break;
    }
    default: break;
    }

    return;
}

QList<ObjectItem *> InventoryModel::removeAllItems()
{
    QList<ObjectItem *> items = this->removeAllFromBackpack();
    items.append(removeAllFromEquipment());
    return items;
}

QList<ObjectItem *> InventoryModel::removeAllFromBackpack()
{
    return this->backpack.takeAllItems();
}

QList<ObjectItem *> InventoryModel::removeAllFromEquipment()
{
    QList<ObjectItem *> items;
    ObjectItem *item;

    for(int i = 0; i < this->equipment.size(); ++i)
    {
        item = this->equipment[i].takeItem();
        if(item)
        {
            item->setInside(false);
            items.append(item);
        }
    }

    return items;
}

bool InventoryModel::isEquipable(int index, ObjectItem *item)
{
    if(!item || index < 0 || index >= this->equipment.size())
    {
        return false;
    }

    EquipmentSlot equipmentSlot = this->equipment[index];

    return (!equipmentSlot.refItem()) && // Slot is free.
            (item->getEquipmentSlot() == equipmentSlot.getEquipmentSlotType() || // Slot is correct type or...
             ((equipmentSlot.getEquipmentSlotType() == Object::MAIN_HAND || equipmentSlot.getEquipmentSlotType() == Object::OFF_HAND)));
}

int InventoryModel::getBackpackSpaceTotal() const
{
    return this->backpack.getContainerSpaceTotal();
}

const ItemContainer &InventoryModel::getBackpack() const
{
    return this->backpack;
}

const QList<EquipmentSlot> &InventoryModel::getEquipment() const
{
    return this->equipment;
}

int InventoryModel::getPlatinumPieces() const
{
    return this->platinumPieces;
}

int InventoryModel::getGoldPieces() const
{
    return this->goldPieces;
}

int InventoryModel::getSilverPieces() const
{
    return this->silverPieces;
}

int InventoryModel::getCopperPieces() const
{
    return this->copperPieces;
}

bool InventoryModel::hasItemInInventory(ObjectItem *item) const
{
    if(!item)
    {
        return false;
    }

    if(this->backpack.hasItem(item))
    {
        return true;
    }

    ObjectItem *foundItem;

    for(int i = 0; i < this->equipment.size(); ++i)
    {
        foundItem = this->equipment.at(i).refItem();

        if(foundItem && foundItem->equals(item))
        {
            return true;
        }
    }

    return false;
}

bool InventoryModel::hasItemEquipped(int index) const
{
    return this->equipment.at(index).hasItemEquiped();
}

bool InventoryModel::hasImprovisedItemEquipped(int index) const
{
    return this->hasItemEquipped(index) && !this->refEquipmentItem(index)->isWieldable();
}

void InventoryModel::setBackpackSpaceTotal(int backpackSpaceTotal)
{
    this->backpack.setContainerSpace(backpackSpaceTotal);
    return;
}

void InventoryModel::setPlatinumPieces(int platinumPieces)
{
    this->platinumPieces = platinumPieces;
    return;
}

void InventoryModel::setGoldPieces(int goldPieces)
{
    this->goldPieces = goldPieces;
    return;
}

void InventoryModel::setSilverPieces(int silverPieces)
{
    this->silverPieces = silverPieces;
    return;
}

void InventoryModel::setCopperPieces(int copperPieces)
{
    this->copperPieces = copperPieces;
    return;
}

ItemContainer *InventoryModel::refBackpack()
{
    return &this->backpack;
}

ObjectItem *InventoryModel::refBackpackItem(int index) const
{
    return this->backpack.refItem(index);
}

ObjectItem *InventoryModel::refEquipmentItem(int index) const
{
    if(index < 0 || index >= this->equipment.size())
    {
        return 0;
    }

    return this->equipment.at(index).refItem();
}

void InventoryModel::serialize(QDataStream &dataStream) const
{
    this->backpack.serialize(dataStream);

    dataStream << this->equipment.size();
    for(int i = 0; i < this->equipment.size(); ++i)
    {
        this->equipment.at(i).serialize(dataStream);
    }

    dataStream << this->platinumPieces;
    dataStream << this->goldPieces;
    dataStream << this->silverPieces;
    dataStream << this->copperPieces;

    return;
}

void InventoryModel::deserialize(QDataStream &dataStream)
{
    this->backpack.deserialize(dataStream);

    int equipmentSize;
    dataStream >> equipmentSize;

    this->equipment.clear();
    for(int i = 0; i < equipmentSize; ++i)
    {
        this->equipment.append(EquipmentSlot());
        this->equipment.last().deserialize(dataStream);
    }

    int platinumPieces, goldPieces, silverPieces, copperPieces;

    dataStream >> platinumPieces;
    dataStream >> goldPieces;
    dataStream >> silverPieces;
    dataStream >> copperPieces;

    this->setPlatinumPieces(platinumPieces);
    this->setGoldPieces(goldPieces);
    this->setSilverPieces(silverPieces);
    this->setCopperPieces(copperPieces);

    return;
}

void InventoryModel::sendBackpackUpdated()
{
    emit this->backpackUpdated();
    return;
}
