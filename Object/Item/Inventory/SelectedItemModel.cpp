/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SelectedItemModel.h"

#include "Command/Backend/Game/Ingame/PlaceItemToInventory.h"
#include "Command/Backend/Game/Ingame/RemoveItemFromInventory.h"

SelectedItemModel::SelectedItemModel(Processor *processor, ObjectCharacter *character, QObject *parent)
    : QObject(parent), processor(processor), character(character)
{
    this->unselectItem();
}

void SelectedItemModel::selectBackpackItem(int index)
{
    if(this->selectItem(index, InventoryModel::BACKPACK, this->character->refInventoryModel()->refBackpackItem(index)))
    {
        this->processor->execute(new RemoveItemFromInventory(this->character, index, InventoryModel::BACKPACK));
    }

    return;
}

void SelectedItemModel::unselectBackpackItem()
{
    if(this->selectedItem.second)
    {
        this->processor->execute(new PlaceItemToInventory(this->originalIndex, this->originalInventoryType, this->selectedItem.second, this->character, this->selectedItem.first+1, InventoryModel::BACKPACK));
        this->unselectItem();
    }

    return;
}

void SelectedItemModel::selectEquipmentItem(int index)
{
    if(this->selectItem(index, InventoryModel::EQUIPMENT, this->character->refInventoryModel()->refEquipmentItem(index)))
    {
        this->processor->execute(new RemoveItemFromInventory(this->character, index, InventoryModel::EQUIPMENT));
    }

    return;
}

void SelectedItemModel::unselectEquipmentItem()
{
    if(this->selectedItem.second)
    {
        this->processor->execute(new PlaceItemToInventory(this->originalIndex, this->originalInventoryType, this->selectedItem.second, this->character, this->selectedItem.first, InventoryModel::EQUIPMENT));
        this->unselectItem();
    }

    return;
}

void SelectedItemModel::changeSelectedItemIndex(int index)
{
    this->selectedItem.first = index;
    return;
}

bool SelectedItemModel::isEquipable(int index)
{
    return character->refInventoryModel()->isEquipable(index, this->selectedItem.second);
}

ObjectItem *SelectedItemModel::refSelectedItem()
{
    return this->selectedItem.second;
}

bool SelectedItemModel::selectItem(int index, InventoryModel::Type inventoryType, ObjectItem *item)
{
    if(!item)
    {
        return false;
    }

    this->selectedItem = QPair<int, ObjectItem *>(index, item);
    this->originalIndex = index;
    this->originalInventoryType = inventoryType;
    return true;
}

bool SelectedItemModel::unselectItem()
{
    this->selectedItem = QPair<int, ObjectItem *>(-1,0);
    this->originalIndex = -1;
    this->originalInventoryType = InventoryModel::BACKPACK;
    return true;
}

