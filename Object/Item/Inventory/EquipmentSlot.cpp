/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "EquipmentSlot.h"

#include "Object/ObjectFromID.h"

EquipmentSlot::EquipmentSlot(const QPoint &slotPosition, Object::EquipmentSlotType type, ObjectItem *item)
{
    this->init(slotPosition, type, item);
}

EquipmentSlot::EquipmentSlot(const EquipmentSlot &other)
{
    this->init(other);
}

EquipmentSlot &EquipmentSlot::operator=(const EquipmentSlot &other)
{
    this->init(other);
    return *this;
}

EquipmentSlot::~EquipmentSlot()
{
}

void EquipmentSlot::init(const EquipmentSlot &other)
{
    this->init(other.getSlotPosition(), other.getEquipmentSlotType(), other.refItem());
    return;
}

void EquipmentSlot::init(QPoint handPosition, Object::EquipmentSlotType type, ObjectItem *item)
{
    this->setSlotPosition(handPosition);
    this->setEquipmentSlotType(type);
    this->setItem(item);
    return;
}

void EquipmentSlot::removeItem()
{
    this->setItem(0);
    return;
}

ObjectItem *EquipmentSlot::takeItem()
{
    ObjectItem *item = this->refItem();
    this->removeItem();
    return item;
}

QPoint EquipmentSlot::getSlotPosition() const
{
    return this->slotPosition;
}

QPoint EquipmentSlot::getItemPosition() const
{
    QPoint result = this->slotPosition;

    if(this->item)
    {
        result -= this->item->getSlotOffset();
    }

    return result;
}

Object::EquipmentSlotType EquipmentSlot::getEquipmentSlotType() const
{
    return this->type;
}

bool EquipmentSlot::hasItemEquiped() const
{
    return this->item != 0;
}

void EquipmentSlot::setSlotPosition(const QPoint &slotPosition)
{
    this->slotPosition = slotPosition;
    return;
}

void EquipmentSlot::setEquipmentSlotType(Object::EquipmentSlotType type)
{
    this->type = type;
    return;
}

void EquipmentSlot::setItem(ObjectItem *item)
{
    this->item = item;
    return;
}

ObjectItem *EquipmentSlot::refItem() const
{
    return this->item;
}

void EquipmentSlot::serialize(QDataStream &dataStream) const
{
    dataStream << this->slotPosition;
    dataStream << this->type;
    bool hasItemEquiped = this->hasItemEquiped();
    dataStream << hasItemEquiped;
    if(hasItemEquiped)
    {
        dataStream << this->item->getObjectID();
        this->item->serialize(dataStream);
    }
    return;
}

void EquipmentSlot::deserialize(QDataStream &dataStream)
{
    QPoint slotPosition;
    int type;
    bool hasItemEquiped;

    dataStream >> slotPosition;
    this->setSlotPosition(slotPosition);
    dataStream >> type;
    this->setEquipmentSlotType(Object::EquipmentSlotType(type));

    dataStream >> hasItemEquiped;
    if(hasItemEquiped)
    {
        int objectID;
        dataStream >> objectID;
        this->item = qobject_cast<ObjectItem *>(ObjectFromID::objectFrom(ObjectBase::ITEM, objectID, this));
        this->item->deserialize(dataStream);
    }
    else
    {
        this->item = 0;
    }
    return;
}
