/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INVENTORY_MODEL_H
#define INVENTORY_MODEL_H

#include <QObject>
#include <QList>
#include <QMap>
#include <QPair>

#include "Object/ObjectItem.h"
#include "Object/ObjectCommon.h"
#include "Object/Item/Inventory/EquipmentSlot.h"
#include "Object/Item/ItemContainer.h"

/** \addtogroup Object
  * \{
  * \class InventoryModel
  *
  * \brief The model representing the inventory of a character.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class InventoryModel : public QObject
{
    Q_OBJECT

signals:
    void backpackUpdated();
    void equippedItemsUpdated();

public:

    /**
     * @brief The default backpack space.
     */
    static const int DEFAULT_BACKPACK_SPACE = 512;

    enum Type {BACKPACK = 0, EQUIPMENT};

    QList<EquipmentSlot>    equipment;
    ItemContainer           backpack;
    int                     platinumPieces;
    int                     goldPieces;
    int                     silverPieces;
    int                     copperPieces;

public:
    InventoryModel(QList<EquipmentSlot> equipment = QList<EquipmentSlot>(),
                   int backpackSpaceTotal = InventoryModel::DEFAULT_BACKPACK_SPACE,
                   QObject *parent = 0);
    InventoryModel(const InventoryModel &inventoryModel);

private:
    void init(QList<EquipmentSlot>equipment = QList<EquipmentSlot>(),
              const ItemContainer &backpack = ItemContainer(InventoryModel::DEFAULT_BACKPACK_SPACE),
              int platinumPieces = 0,
              int goldPieces = 0,
              int silverPieces = 0,
              int copperPieces = 0);

public:
    /**
      * @brief update The inventory in comparison to a newer given inventory.
      * @param inventory The new inventory to be put in place of the current one.
      * @return Return the list of obselete items that are not used in the inventory anymore.
      */
    void update(const InventoryModel &inventory);
    void updateBackpack(const ItemContainer &newBackpack);
    void updateEquipment(const QList<EquipmentSlot> &equipment);

    ObjectItem *lookup(ObjectBase::ObjectType type, int objectIDNumber, unsigned int spawnID) const;

    bool addToBackpack(ObjectItem *item);
    bool insertToBackpack(int index, ObjectItem *item);
    ObjectItem *removeFromBackpack(int index);
    int removeFromBackpack(ObjectItem *item);
    bool insertToEquipment(int index, ObjectItem* item);
    ObjectItem *removeFromEquipment(int index);
    int removeFromEquipment(ObjectItem *item);

    /**
      * @brief Search the inventory for a given item, remove it and then return the index.
      * @param item The item to be found and removed.
      * @return The removed item index. Is -1 if no item was found.
      */
    int removeFromInventory(ObjectItem *item);

    /**
      * @brief Search the inventory for a given item, remove it and then return the index.
      * @param item The item to be found and removed.
      * @return The removed item index. Is -1 if no item was found.
      */
    int removeFromInventory(ObjectItem *item, InventoryModel::Type inventoryType);

    /**
      * @brief Search the inventory for a given item, remove and then return it.
      * @param item The item to be found and removed.
      * @param inventoryType The type of the inventory the item is to be added.
      * @return The removed item. Is 0, if the item wasn't found or item was 0.
      */
    ObjectItem *removeFromInventory(int index, InventoryModel::Type inventoryType);

    /**
      * @brief Insert an item to the inventory.
      * @param index The index where to add the item to.
      * @param item The item to be inserted.
      * @param inventoryType The type of the inventory the item is to be added.
      */
    void insertToInventory(int index, ObjectItem *item, InventoryModel::Type inventoryType);

    QList<ObjectItem *> removeAllItems();
    QList<ObjectItem *> removeAllFromBackpack();
    QList<ObjectItem *> removeAllFromEquipment();

    bool isEquipable(int index, ObjectItem *item);

    void addPlatinumPieces(int platinumPieces);
    int takePlatninumPieces(int platinumPieces);

    void addGoldPieces(int goldPieces);
    int takeGoldPieces(int goldPieces);

    void addSilverPieces(int silverPieces);
    int takeSilverPieces(int silverPieces);

    void addCopperPieces(int copperPieces);
    int takeCopperPieces(int copperPieces);

public:
    // Get-Methods
    int getBackpackSpaceTotal() const;
    const ItemContainer &getBackpack() const;
    const QList<EquipmentSlot> &getEquipment() const;
    int getPlatinumPieces() const;
    int getGoldPieces() const;
    int getSilverPieces() const;
    int getCopperPieces() const;
    bool hasItemInInventory(ObjectItem *item) const;
    bool hasItemEquipped(int index) const;
    bool hasImprovisedItemEquipped(int index) const;

    // Set-Methods
    void setBackpackSpaceTotal(int backpackSpaceTotal);

private:
    void setPlatinumPieces(int platinumPieces);
    void setGoldPieces(int goldPieces);
    void setSilverPieces(int silverPieces);
    void setCopperPieces(int copperPieces);

public:
    // Ref-Methods
    ItemContainer *refBackpack();
    ObjectItem *refBackpackItem(int index) const;
    ObjectItem *refEquipmentItem(int index) const;

    // Serialize
    virtual void serialize(QDataStream &dataStream) const;
    virtual void deserialize(QDataStream &dataStream);

private slots:
    void sendBackpackUpdated();
};

#endif // INVENTORY_MODEL_H
