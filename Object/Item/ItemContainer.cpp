/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "ItemContainer.h"

#include "Object/ObjectFromID.h"

ItemContainer::ItemContainer(int containerSpace, const QList<ObjectItem *> &items, QObject *parent)
    : QObject(parent), items(items), containerSpace(containerSpace)
{}

ItemContainer::ItemContainer(const ItemContainer &other)
    : QObject(other.parent())
{
    this->copy(other);
}

ItemContainer &ItemContainer::operator=(const ItemContainer &other)
{
    this->copy(other);
    return (*this);
}

ObjectItem *ItemContainer::lookup(ObjectBase::ObjectType type, int objectIDNumber, unsigned int spawnID) const
{
    foreach(ObjectItem *item, this->items)
    {
        if(item->equals(type, objectIDNumber, spawnID))
        {
            return item;
        }
        else
        {
            ObjectBase *insideObject = item->lookupObjectInside(type, objectIDNumber, spawnID);

            if(insideObject)
            {
                return qobject_cast<ObjectItem *>(insideObject);
            }
        }
    }

    return 0;
}

void ItemContainer::addItem(ObjectItem *item)
{
    item->setParent(this);
    this->items.append(item);

    emit this->itemContainerUpdated();

    return;
}

void ItemContainer::addItems(const QList<ObjectItem *> &items)
{
    this->items.append(items);

    foreach(ObjectItem *item, this->items)
    {
        item->setParent(this);
    }

    emit this->itemContainerUpdated();

    return;
}

void ItemContainer::insertItem(int index, ObjectItem *item)
{
    if(index < 0 || index > this->items.size())
    {
        index = this->items.size();
    }

    this->items.insert(index, item);
    item->setParent(this);
    item->setInside(true);

    emit this->itemContainerUpdated();

    return;
}

ObjectItem *ItemContainer::takeItem(ObjectItem *item)
{
    return this->takeItem(this->items.indexOf(item));
}

ObjectItem *ItemContainer::takeItem(int index)
{
    ObjectItem *item = this->items.takeAt(index);
    item->setInside(false);

    emit this->itemContainerUpdated();
    return item;
}

QList<ObjectItem *> ItemContainer::takeItems(const QList<ObjectItem *> &items)
{
    QList<int> indexes;

    foreach(ObjectItem *item, items)
    {
        indexes.append(this->items.indexOf(item));
    }

    return this->takeItems(indexes);
}

QList<ObjectItem *> ItemContainer::takeItems(const QList<int> &indexes)
{
    QList<ObjectItem *> result;

    foreach(int index, indexes)
    {
        ObjectItem *item = this->items.takeAt(index);
        item->setInside(false);
        result.append(item);
    }

    emit this->itemContainerUpdated();

    return result;
}

QList<ObjectItem *> ItemContainer::takeAllItems()
{
    QList<ObjectItem *> items;

    while(!this->items.isEmpty())
    {
        items.append(this->items.takeFirst());
    }

    emit this->itemContainerUpdated();

    return items;
}

int ItemContainer::removeItem(ObjectItem *item)
{
    int index = this->items.indexOf(item);
    this->items.removeAt(index);
    return index;
}

int ItemContainer::removeItem(int index)
{
    this->takeItem(index);
    return index;
}

void ItemContainer::removeItems(const QList<ObjectItem *> &items)
{
    this->takeItems(items);
    return;
}

void ItemContainer::removeItems(const QList<int> &indexes)
{
    this->takeItems(indexes);
    return;
}

void ItemContainer::update(const ItemContainer &other)
{
    QList<ObjectItem *> newItems;

    int i;

    ObjectItem *item;
    ObjectItem *newItem;

    for(i = 0; i < this->items.size() && i < other.items.size(); ++i)
    {
        item = this->items.at(i);
        newItem = other.items.at(i);

        if(item->equals(newItem))
        {
            newItems.append(item);
            if(item)
            {
                item->setParent(this);
            }
        }
        else
        {
            newItem = newItem->copy();
            newItems.append(newItem);
            newItem->setParent(this);
            newItem->setInside(true);
        }
    }

    for(;i < this->items.size(); ++i)
    {
        this->items.removeLast();
    }

    for(;i < other.items.size(); ++i)
    {
        item = other.items.at(i)->copy();
        newItems.append(item);
        item->setParent(this);
        item->setInside(true);
    }

    this->items = newItems;

    emit this->itemContainerUpdated();

    return;
}

int ItemContainer::getContainerSpaceTotal() const
{
    return this->containerSpace;
}

bool ItemContainer::hasItem(ObjectItem *item) const
{
    for(int i = 0; i < this->items.size(); ++i)
    {
        if(this->items.at(i)->equals(item))
        {
            return true;
        }
    }

    return false;
}

ObjectItem *ItemContainer::getItem(int index) const
{
    return this->items.at(index);
}

QList<ObjectItem *> ItemContainer::getItems(const QList<int> &indexes) const
{
    QList<ObjectItem *> result;

    foreach(int index, indexes)
    {
        result.append(this->items.at(index));
    }

    return result;
}

QList<ObjectItem *> ItemContainer::getItems() const
{
    return this->items;
}

void ItemContainer::setContainerSpace(int containerSpace)
{
    this->containerSpace = containerSpace;
    return;
}

ObjectItem *ItemContainer::refItem(int index) const
{
    if(index < 0 || index >= this->items.size())
    {
        return 0;
    }

    return this->items[index];
}

void ItemContainer::serialize(QDataStream &dataStream) const
{
    dataStream << this->items.size();
    ObjectItem *item;

    for(int i = 0; i < this->items.size(); ++i)
    {
        item = this->items.at(i);
        dataStream << item->getObjectID();
        item->serialize(dataStream);
    }

    dataStream << this->containerSpace;

    return;
}

void ItemContainer::deserialize(QDataStream &dataStream)
{
    int size, id;
    dataStream >> size;
    ObjectItem *item;

    for(int i = 0; i < this->items.size(); ++i)
    {
        this->items[i]->deleteLater();
    }
    this->items.clear();

    for(int i = 0; i < size; ++i)
    {
        dataStream >> id;
        item = qobject_cast<ObjectItem *>(ObjectFromID::objectFrom(ObjectBase::ITEM, id, this));
        item->deserialize(dataStream);

        this->items.append(item);
    }

    dataStream >> (this->containerSpace);

    emit this->itemContainerUpdated();
    return;
}

void ItemContainer::copy(const ItemContainer &other)
{
    this->items = other.items;
    return;
}

