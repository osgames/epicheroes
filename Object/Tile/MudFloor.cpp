/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "MudFloor.h"

#include <QGridLayout>
#include <QLabel>

MudFloor::MudFloor(QObject *parent)
    : ObjectTile(tr("Mud Floor"),
                 tr("Muddy, dry and stable looking ground."),
                 ObjectTile::FLOOR, ":/objects/floors/mud_floor", true, parent)
{}

MudFloor::MudFloor(const MudFloor &other)
    : ObjectTile(other)
{}

QWidget *MudFloor::createInformationWidget(QWidget *parent) const
{
    QWidget *widget = new QWidget(parent);
    QGridLayout *layout = new QGridLayout(widget);
    QLabel *description = new QLabel(this->getDescription(), widget);

    layout->addWidget(description, 1,0);

    widget->setLayout(layout);
    return widget;
}

ObjectTile *MudFloor::copy() const
{
    return new MudFloor(*this);
}

void MudFloor::update(const ObjectBase &object)
{
    if(this->getType() != object.getType() || this->getObjectIDNumber() != object.getObjectIDNumber())
    {
        qDebug("Object has not been updatet since the given object was not of that type.");
        return;
    }

    const MudFloor &mudFloor = (const MudFloor &) object;

    this->updateTile(mudFloor);
    return;
}

ObjectID::TileID MudFloor::getObjectID() const
{
    return ObjectID::BRICK_WALL;
}

QList<CommandTree *> MudFloor::getCommandTreeList(ObjectBase *player)
{
    QList<CommandTree *> list;

    list.append(this->ObjectTile::getCommandTreeList(player));

    return list;
}

void MudFloor::serialize(QDataStream &dataStream) const
{
    ObjectTile::serialize(dataStream);
    return;
}

void MudFloor::serialize(QIODevice *device) const
{
    QDataStream stream(device);
    this->serialize(stream);
    return;
}

void MudFloor::deserialize(QDataStream &dataStream)
{
    ObjectTile::deserialize(dataStream);
    return;
}

void MudFloor::deserialize(QIODevice *device)
{
    QDataStream stream(device);
    this->deserialize(stream);
    return;
}
