/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Object/ObjectBase.h"

#include <QGridLayout>
#include <QLabel>
#include <QImage>
#include <QPainter>

#include <iostream>
#include <climits>

#include "Command/Backend/Object/ShowDescription.h"
#include "Command/Backend/CommandNode.h"
#include "Command/Backend/CommandLeaf.h"
#include "Command/Backend/CommandSeperator.h"
#include "Common/Common.h"

ObjectBase::ObjectBase(const ObjectBase& objectBase) :
    QObject(objectBase.parent())
{
    this->initObjectBase(objectBase.getObjectName(), objectBase.getDescription(), objectBase.getUsedImageIndex(), objectBase.getIsVisible());
    this->setPosition(objectBase.getX(), objectBase.getY(), objectBase.getZ());
    this->setSpawnID(objectBase.getSpawnID());
    this->loadImages(objectBase.getImagePathList());
}

ObjectBase::ObjectBase(const QString &name, const QString &description, const QString &imagePath, bool isVisible, QObject *parent)
    : QObject(parent)
{
    this->initObjectBase(name, description, -1, isVisible);

    if(!imagePath.isEmpty())
    {
        this->appendImage(imagePath);
    }
}

void ObjectBase::initObjectBase(const QString &name, const QString &description, int usedImageIndex, bool isVisible)
{
    this->unsetPosition();
    this->setObjectName(name);
    this->setDescription(description);
    this->spawnID = UINT_MAX;
    this->setUsedImageIndex(usedImageIndex);
    this->setIsVisible(isVisible);

    return;
}

bool ObjectBase::equals(ObjectBase::ObjectType type,
                        int objectIDNumber,
                        unsigned int spawnID) const
{
    return this->getType() == type &&
           this->getObjectIDNumber() == objectIDNumber &&
           this->getSpawnID() == spawnID;
}

bool ObjectBase::equals(const ObjectBase &other) const
{
    return this->equals(other.getType(), other.getObjectIDNumber(), other.getSpawnID());
}

bool ObjectBase::equals(ObjectBase *other) const
{
    return this->equals(*other);
}

ObjectBase *ObjectBase::lookupObjectInside(ObjectBase::ObjectType type, int objectIDNumber, unsigned int spawnID) const
{
    Q_UNUSED(type);
    Q_UNUSED(objectIDNumber);
    Q_UNUSED(spawnID);
    return 0;
}

ObjectBase *ObjectBase::lookupObjectInside(const ObjectBase &other) const
{
    return this->lookupObjectInside(other.getType(), other.getObjectIDNumber(), other.getSpawnID());
}

ObjectBase *ObjectBase::lookupObjectInside(ObjectBase *other) const
{
    return this->lookupObjectInside(*other);
}

void ObjectBase::clearImageList()
{
    this->imageList.clear();
    this->usedImageIndex = -1;

    return;
}

void ObjectBase::appendImage(const QString &imagePath)
{
    this->imageList.append(ObjectImage(imagePath));
    this->usedImageIndex = 0;

    return;
}

void ObjectBase::appendImages(const QVector<QString> &imagePathList)
{
    for(int i = 0; i < imagePathList.size(); ++i)
    {
        this->appendImage(imagePathList[i]);
    }

    this->usedImageIndex = 0;

    return;
}

void ObjectBase::loadImage(const QString &imagePath)
{
    this->clearImageList();
    this->appendImage(imagePath);

    return;
}

void ObjectBase::loadImages(const QVector<QString> &imagePathList)
{
    this->clearImageList();
    this->appendImages(imagePathList);

    return;
}

void ObjectBase::toggleVisibility()
{
    this->isVisible = !this->isVisible;
    return;
}

bool ObjectBase::isSet() const
{
    return this->x >= 0 && this->y >= 0 && this->z >= 0;
}

void ObjectBase::unsetPosition()
{
    this->x = -1;
    this->y = -1;
    this->z = -1;
    return;
}

void ObjectBase::updateBase(const ObjectBase &object)
{
    this->setObjectName(object.getObjectName());
    this->setDescription(object.getDescription());
    this->setPosition(object.getX(), object.getY(), object.getZ());
    this->loadImages(object.getImagePathList());
    this->setIsVisible(object.getIsVisible());

    return;
}

QWidget *ObjectBase::createInformationWidget(QWidget *parent) const
{
    QWidget *widget = new QWidget(parent);
    QGridLayout *layout = new QGridLayout(widget);
    QLabel *name = new QLabel(QString("Name: <b>%1</b>").arg(this->getObjectName()), widget);
    name->setTextFormat(Qt::RichText);
    QLabel *description = new QLabel(this->getDescription(), widget);

    layout->addWidget(name,0,0);
    layout->addWidget(description, 1,0);

    widget->setLayout(layout);
    return widget;
}

QVector<QString> ObjectBase::getImagePathList() const
{
    QVector<QString> imagePathList;

    for(int i = 0; i < this->imageList.size(); ++i)
    {
        imagePathList.append(this->imageList[i].getPath());
    }

    return imagePathList;
}

QString ObjectBase::getCurrentImagePath() const
{
    if(this->usedImageIndex < 0 || this->usedImageIndex > this->imageList.size())
    {
        qDebug("Loading the current image path failed. The current image was not valid.");
        return QString();
    }

    return this->imageList[this->usedImageIndex].getPath();
}

int ObjectBase::getUsedImageIndex() const
{
    return this->usedImageIndex;
}

QString ObjectBase::getObjectName() const
{
    return this->objectName();
}

QString ObjectBase::getDescriptiveName() const
{
    return this->getObjectName();
}

bool ObjectBase::getIsVisible() const
{
    return this->isVisible;
}

unsigned int ObjectBase::getSpawnID() const
{
    return this->spawnID;
}

int ObjectBase::getX() const
{
    return this->x;
}

int ObjectBase::getY() const
{
    return this->y;
}

int ObjectBase::getZ() const
{
    return this->z;
}

bool ObjectBase::inReachOf(const ObjectBase &other, int reach)
{
    if(reach <= 0 ||
       this->x == -1 || this->y == -1 || this->z == -1 ||
       other.x == -1 || other.y == -1 || other.z == -1)
    {
        return false;
    }

    int distance = 0;
    bool secondDiagonal = false;

    int ox = other.x-this->x, oy = other.y-this->y, oz = other.z-this->z;

    while(ox != 0 && oy != 0 && oz != 0)
    {
        ox > 0 ? --ox : ++ox;
        oy > 0 ? --oy : ++oy;
        oz > 0 ? --oz : ++oz;

        distance += 10;
    }

    int la, lb;

    if(ox == 0)
    {
        la = oy;
        lb = oz;
    }
    else if(oy == 0)
    {
        la = ox;
        lb = oz;
    }
    else //(oz == 0)
    {
        la = ox;
        lb = oy;
    }

    while(la != 0 && lb != 0)
    {
        la > 0 ? --la : ++la;
        lb > 0 ? --lb : ++lb;

        secondDiagonal ? distance += 10 : distance += 5;

        secondDiagonal = !secondDiagonal;
    }

    distance += 5*qAbs(la);
    distance += 5*qAbs(lb);

    return distance <= reach;
}

bool ObjectBase::inReachOf(ObjectBase *other, int reach)
{
    return !other || this->inReachOf(*other, reach);
}

QList<CommandTree *> ObjectBase::getCommandTreeList(ObjectBase *player)
{
    Q_UNUSED(player);

    QList<CommandTree *> result;
    result.append(new CommandSeperator());
    result.append(new CommandLeaf(new ShowDescription(this), tr("Description...")));
    return result;
}

QString ObjectBase::getDescription() const
{
    return this->description;
}

void ObjectBase::setUsedImageIndex(int usedImageIndex)
{
    this->usedImageIndex = usedImageIndex;
    return;
}

void ObjectBase::setIsVisible(bool isVisible)
{
    this->isVisible = isVisible;
    return;
}

void ObjectBase::setSpawnID(unsigned int spawnID)
{
    this->spawnID = spawnID;
    return;
}

void ObjectBase::setPosition(int x, int y, int z)
{
    if((x < 0 && y >= 0 && z >= 0) ||
       (x >= 0 && y < 0 && z >= 0) ||
       (x >= 0 && y >= 0 && z < 0))
    {
        qDebug() << QString("Given position is invalid: (%1,%2,%3). Unsetting position.").arg(QString::number(x), QString::number(y), QString::number(z));
        this->unsetPosition();
        return;
    }

    this->x = x;
    this->y = y;
    this->z = z;

    return;
}

void ObjectBase::setDescription(const QString &description)
{
    this->description = description;
    return;
}

QImage ObjectBase::getImage() const
{
    return this->getImage(this->usedImageIndex);
}

QImage ObjectBase::getImage(int index) const
{
    if(index < 0 || index >= this->imageList.size())
    {
        std::cout << "Couldn't reference the image that's used." << std::endl;
        std::cout << "    Index was: " << index << std::endl;
        std::cout << "    Size was: " << this->imageList.size() << std::endl;
        return QImage(0);
    }

    if(!this->isVisible)
    {
        return QImage(0);
    }

    return this->imageList.at(index);
}

void ObjectBase::serialize(QDataStream &dataStream) const
{
    dataStream << this->getObjectName();
    dataStream << this->description;

    dataStream << this->getImagePathList();
    dataStream << this->getIsVisible();

    dataStream << this->spawnID;

    dataStream << this->x;
    dataStream << this->y;
    dataStream << this->z;

    return;
}

void ObjectBase::deserialize(QDataStream &dataStream)
{
    QString name;
    dataStream >> name;
    this->setObjectName(name);

    QString description;
    dataStream >> description;
    this->setDescription(description);

    QVector<QString> imagePathList;
    dataStream >> imagePathList;
    this->loadImages(imagePathList);

    bool isVisible;
    dataStream >> isVisible;
    this->setIsVisible(isVisible);

    unsigned int spawnID;
    dataStream >> spawnID;
    this->setSpawnID(spawnID);

    int x,y,z;
    dataStream >> x;
    dataStream >> y;
    dataStream >> z;
    this->setPosition(x,y,z);

    return;
}
