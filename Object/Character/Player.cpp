/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Player.h"

#include "Command/Backend/Object/ShowDescription.h"
#include "Command/Backend/Object/Character/ShowInventory.h"
#include "Command/Backend/CommandLeaf.h"

#include "Object/Character/Race/Humanoid/Human.h"

Player::Player(const QString &name, const QString &description, const Abilities &abilities, QObject *parent)
    : ObjectCharacter(new Human(), abilities, name, description, ":/objects/characters/human", true, parent)
{
    this->initPlayer();
}

Player::Player(const Player &player)
    : ObjectCharacter(player)
{
    this->initPlayer();
}

void Player::initPlayer()
{
    return;
}

Player *Player::copy() const
{
    return new Player(*this);
}

void Player::update(const ObjectBase &object)
{
    if(this->getType() != object.getType() || this->getObjectIDNumber() != object.getObjectIDNumber())
    {
        qDebug("Object has not been updatet since the given object was not of that type.");
        qDebug("Given: Player");
        return;
    }

    const Player &player = (const Player &) object;

    this->updateCharacter(player);
    return;
}

void Player::endTurn()
{
    this->fullRoundActionUsed();
    this->standardActionUsed();
    this->moveActionUsed();
}

ObjectID::CharacterID Player::getObjectID() const
{
    return ObjectID::PLAYER;
}

QList<CommandTree *> Player::getCommandTreeList(ObjectBase *player)
{
    QList<CommandTree *> list;

    list.append(new CommandLeaf(new ShowInventory(this), tr("Inventory...")));

    list.append(this->ObjectCharacter::getCommandTreeList(player));

    return list;
}

void Player::serialize(QDataStream &dataStream) const
{
    this->ObjectCharacter::serialize(dataStream);
    return;
}

void Player::serialize(QIODevice *device) const
{
    QDataStream stream(device);
    this->serialize(stream);
    return;
}

void Player::deserialize(QDataStream &dataStream)
{
    this->ObjectCharacter::deserialize(dataStream);
    return;
}

void Player::deserialize(QIODevice *device)
{
    QDataStream stream(device);
    this->deserialize(stream);
    return;
}


