/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Abilities.h"

#include <QDataStream>
#include <qglobal.h>
#include <qmath.h>

#include <Common/Common.h>

Abilities::Abilities(int strength, int dexterity, int constitution, int intelligence, int wisdom, int charisma)
    : strength(strength), dexterity(dexterity), constitution(constitution), intelligence(intelligence), wisdom(wisdom), charisma(charisma)
{}

Abilities::Abilities(const QList<int> &list)
{
    this->strength = list.at(0);
    this->dexterity = list.at(1);
    this->constitution = list.at(2);
    this->intelligence = list.at(3);
    this->wisdom = list.at(4);
    this->charisma = list.at(5);
}

Abilities::Abilities()
{
    this->strength = 10;
    this->dexterity = 10;
    this->constitution = 10;
    this->intelligence = 10;
    this->wisdom = 10;
    this->charisma = 10;
}

void Abilities::update(const Abilities &abilities)
{
    this->setStrength(abilities.getStrength());
    this->setDexterity(abilities.getDexterity());
    this->setConstitution(abilities.getConstitution());
    this->setIntelligence(abilities.getIntelligence());
    this->setWisdom(abilities.getWisdom());
    this->setCharisma(abilities.getCharisma());

    return;
}

int Abilities::getStrength() const
{
    return this->strength;
}

NumberMod Abilities::getStrengthMod() const
{
    return NumberMod(this->toMod(this->strength), QObject::tr("STR"));
}

int Abilities::getDexterity() const
{
    return this->dexterity;
}

NumberMod Abilities::getDexterityMod() const
{
    return NumberMod(this->toMod(this->dexterity), QObject::tr("DEX"));
}

int Abilities::getConstitution() const
{
    return this->constitution;
}

NumberMod Abilities::getConstitutionMod() const
{
    return NumberMod(this->toMod(this->constitution), QObject::tr("CON"));
}

int Abilities::getIntelligence() const
{
    return this->intelligence;
}

NumberMod Abilities::getIntelligenceMod() const
{
    return NumberMod(this->toMod(this->intelligence), QObject::tr("INT"));
}

int Abilities::getWisdom() const
{
    return this->wisdom;
}

NumberMod Abilities::getWisdomMod() const
{
    return NumberMod(this->toMod(this->wisdom), QObject::tr("WIS"));
}

int Abilities::getCharisma() const
{
    return this->charisma;
}

NumberMod Abilities::getCharismaMod() const
{
    return NumberMod(this->toMod(this->charisma), QObject::tr("CHA"));
}

QString Abilities::getAbilityString() const
{
    QString result;

    result = result.append(QObject::tr("STR: %1 (%2)\n").arg(TEoH::showWithWhitespacePrefix(this->getStrength(),2)).arg(this->getStrengthMod().showMod(false)));
    result = result.append(QObject::tr("DEX: %1 (%2)\n").arg(TEoH::showWithWhitespacePrefix(this->getDexterity(),2)).arg(this->getDexterityMod().showMod(false)));
    result = result.append(QObject::tr("CON: %1 (%2)\n").arg(TEoH::showWithWhitespacePrefix(this->getConstitution(),2)).arg(this->getConstitutionMod().showMod(false)));
    result = result.append(QObject::tr("INT: %1 (%2)\n").arg(TEoH::showWithWhitespacePrefix(this->getIntelligence(),2)).arg(this->getIntelligenceMod().showMod(false)));
    result = result.append(QObject::tr("WIS: %1 (%2)\n").arg(TEoH::showWithWhitespacePrefix(this->getWisdom(),2)).arg(this->getWisdomMod().showMod(false)));
    result = result.append(QObject::tr("CHA: %1 (%2)").arg(TEoH::showWithWhitespacePrefix(this->getCharisma(),2)).arg(this->getCharismaMod().showMod(false)));

    return result;
}

void Abilities::setStrength(int strength)
{
    this->strength = strength;
    return;
}

void Abilities::setDexterity(int dexterity)
{
    this->dexterity = dexterity;
    return;
}

void Abilities::setConstitution(int constitution)
{
    this->constitution = constitution;
    return;
}

void Abilities::setIntelligence(int intelligence)
{
    this->intelligence = intelligence;
    return;
}

void Abilities::setWisdom(int wisdom)
{
    this->wisdom = wisdom;
    return;
}

void Abilities::setCharisma(int charisma)
{
    this->charisma = charisma;
    return;
}

void Abilities::serialize(QDataStream &dataStream) const
{
    dataStream << this->strength;
    dataStream << this->dexterity;
    dataStream << this->constitution;
    dataStream << this->intelligence;
    dataStream << this->wisdom;
    dataStream << this->charisma;
    return;
}

void Abilities::deserialize(QDataStream &dataStream)
{
    int strength, dexterity, constitution, intelligence, wisdom, charisma;

    dataStream >> strength;
    dataStream >> dexterity;
    dataStream >> constitution;
    dataStream >> intelligence;
    dataStream >> wisdom;
    dataStream >> charisma;

    this->setStrength(strength);
    this->setDexterity(dexterity);
    this->setConstitution(constitution);
    this->setIntelligence(intelligence);
    this->setWisdom(wisdom);
    this->setCharisma(charisma);
    return;
}

int Abilities::toMod(int abilityScore)
{
    return qFloor(((double)(abilityScore-10.0)/2.0));
}
