/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ABILITIES_H
#define ABILITIES_H

#include <QString>

#include "Common/NumberMod.h"

/** \addtogroup Object
  * \{
  * \class Abilities
  *
  * \brief Represent the ability scores in the game.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class Abilities
{
    int strength;
    int dexterity;
    int constitution;
    int intelligence;
    int wisdom;
    int charisma;

public:
    Abilities(int strength, int dexterity, int constitution, int intelligence, int wisdom, int charisma);
    Abilities(const QList<int> &list);
    Abilities();

    void update(const Abilities &abilities);

    // Get-Methods
    int getStrength() const;
    NumberMod getStrengthMod() const;
    int getDexterity() const;
    NumberMod getDexterityMod() const;
    int getConstitution() const;
    NumberMod getConstitutionMod() const;
    int getIntelligence() const;
    NumberMod getIntelligenceMod() const;
    int getWisdom() const;
    NumberMod getWisdomMod() const;
    int getCharisma() const;
    NumberMod getCharismaMod() const;

    QString getAbilityString() const;

    // Set-Methods
    void setStrength(int strength);
    void setDexterity(int dexterity);
    void setConstitution(int constitution);
    void setIntelligence(int intelligence);
    void setWisdom(int wisdom);
    void setCharisma(int charisma);

    virtual void serialize(QDataStream &dataStream) const;
    virtual void deserialize(QDataStream &dataStream);

    static int toMod(int abilityScore);
};

#endif // ABILITIES_H
