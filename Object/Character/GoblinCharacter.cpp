/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "GoblinCharacter.h"

#include "Object/Character/Race/Humanoid/Goblin.h"

GoblinCharacter::GoblinCharacter(const QString &name, const QString &description, QObject *parent)
    : ObjectCharacter(new Goblin(), Abilities(11,15,12,10,9,6), name, description, ":/objects/characters/goblin", true, parent)
{
    this->initGoblinCharacter();
}

GoblinCharacter::GoblinCharacter(const GoblinCharacter &GoblinCharacter)
    : ObjectCharacter(GoblinCharacter)
{
    this->initGoblinCharacter();
}

void GoblinCharacter::initGoblinCharacter()
{
    return;
}

GoblinCharacter *GoblinCharacter::copy() const
{
    return new GoblinCharacter(*this);
}

void GoblinCharacter::update(const ObjectBase &object)
{
    if(this->getType() != object.getType() || this->getObjectIDNumber() != object.getObjectIDNumber())
    {
        qDebug("Object has not been updated since the given object was not of that type.");
        qDebug("Given: GoblinCharacter");
        return;
    }

    const GoblinCharacter &goblinCharacter = (const GoblinCharacter &) object;

    this->updateCharacter(goblinCharacter);
    return;
}

ObjectID::CharacterID GoblinCharacter::getObjectID() const
{
    return ObjectID::GOBLIN;
}

QList<CommandTree *> GoblinCharacter::getCommandTreeList(ObjectBase *player)
{
    QList<CommandTree *> list;

    list.append(this->ObjectCharacter::getCommandTreeList(player));

    return list;
}

void GoblinCharacter::serialize(QDataStream &dataStream) const
{
    this->ObjectCharacter::serialize(dataStream);
    return;
}

void GoblinCharacter::serialize(QIODevice *device) const
{
    QDataStream stream(device);
    this->serialize(stream);
    return;
}

void GoblinCharacter::deserialize(QDataStream &dataStream)
{
    this->ObjectCharacter::deserialize(dataStream);
    return;
}

void GoblinCharacter::deserialize(QIODevice *device)
{
    QDataStream stream(device);
    this->deserialize(stream);
    return;
}

