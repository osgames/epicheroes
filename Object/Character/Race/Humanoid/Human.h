/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HUMAN_H
#define HUMAN_H

#include "Object/Character/Race/Humanoid.h"

/** \addtogroup Object
  * \{
  * \class Human
  *
  * \brief Represents the race of humans.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class Human : public Humanoid
{
    Q_OBJECT
public:
    Human(QObject *parent = 0);
    Human(const Human &human);

private:
    void init();

public:
    virtual Race *copy() const;

    virtual int getRaceID() const;

    virtual QList<EquipmentSlot> getEquipmentSlots() const;
};

#endif // HUMAN_H
