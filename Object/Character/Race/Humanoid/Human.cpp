/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Human.h"

#include "Object/Character/Race/RaceID.h"

Human::Human(QObject *parent)
    : Humanoid(parent)
{
    this->init();
}

Human::Human(const Human &human)
    : Humanoid(human)
{
    this->init();
}

void Human::init()
{
    this->setObjectName(tr("Human"));
    return;
}

Race *Human::copy() const
{
    return new Human(*this);
}

int Human::getRaceID() const
{
    return RaceID::HUMAN;
}

QList<EquipmentSlot> Human::getEquipmentSlots() const
{
    QList<EquipmentSlot > equipmentSlots;

    equipmentSlots.append(EquipmentSlot(QPoint(20,16), Object::MAIN_HAND));
    equipmentSlots.append(EquipmentSlot(QPoint(9,16), Object::OFF_HAND));
    equipmentSlots.append(EquipmentSlot(QPoint(1,1), Object::HEAD));
    equipmentSlots.append(EquipmentSlot(QPoint(1,1), Object::HEADBAND));
    equipmentSlots.append(EquipmentSlot(QPoint(1,1), Object::EYES));
    equipmentSlots.append(EquipmentSlot(QPoint(1,1), Object::SHOULDERS));
    equipmentSlots.append(EquipmentSlot(QPoint(1,1), Object::NECK));
    equipmentSlots.append(EquipmentSlot(QPoint(1,1), Object::CHEST));
    equipmentSlots.append(EquipmentSlot(QPoint(1,1), Object::BODY));
    equipmentSlots.append(EquipmentSlot(QPoint(1,1), Object::ARMOR));
    equipmentSlots.append(EquipmentSlot(QPoint(1,1), Object::BELT));
    equipmentSlots.append(EquipmentSlot(QPoint(1,1), Object::HANDS));
    equipmentSlots.append(EquipmentSlot(QPoint(1,1), Object::BELT));
    equipmentSlots.append(EquipmentSlot(QPoint(1,1), Object::RING));
    equipmentSlots.append(EquipmentSlot(QPoint(1,1), Object::RING));
    equipmentSlots.append(EquipmentSlot(QPoint(1,1), Object::FEET));

    return equipmentSlots;
}
