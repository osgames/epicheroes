/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GOBLIN_H
#define GOBLIN_H

#include "Object/Character/Race/Humanoid.h"

/** \addtogroup Object
  * \{
  * \class Goblin
  *
  * \brief Represents the race of Goblins.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class Goblin: public Humanoid
{
    Q_OBJECT
public:
    Goblin(QObject *parent = 0);
    Goblin(const Goblin &goblin);

private:
    void init();

public:
    virtual Race *copy() const;

    virtual int getRaceID() const;

    virtual QList<EquipmentSlot> getEquipmentSlots() const;
};

#endif // GOBLIN_H
