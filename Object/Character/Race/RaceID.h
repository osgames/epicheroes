/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _RACE_ID_H_
#define _RACE_ID_H_

#include <QObject>

#include "Object/Character/Race/Race.h"

#include "Object/Character/Race/Humanoid/Human.h"
#include "Object/Character/Race/Humanoid/Goblin.h"

/** \addtogroup Object
  * \{
  * \namespace RaceID
  *
  * \brief The namespace representing the Race ID.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
namespace RaceID
{
    enum ID
    {
        HUMAN = 0,
        GOBLIN
    };

    inline Race *fromID(RaceID::ID id, QObject *parent = 0)
    {
        Race *race = 0;

        switch(id)
        {
        case HUMAN:
        {
            race = new Human(parent);
            break;
        }
        case GOBLIN:
        {
            race = new Goblin(parent);
            break;
        }
        default: break;
        }

        return race;
    }
}

#endif // _RACE_ID_H_
