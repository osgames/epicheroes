/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Race.h"

#include "Common/Common.h"

Race::Race(QObject *parent)
    : QObject(parent)
{
}

Race::Race(const Race &race)
    : QObject(race.parent())
{
}

int Race::getBaseSpeed() const
{
    return TEoH::STANDARD_BASE_SPEED;
}

void Race::update(const Race &race)
{
    Q_UNUSED(race);

    return;
}
