/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RACE_H
#define RACE_H

#include <QObject>
#include <QPair>

#include "Object/ObjectCommon.h"
#include "Object/ObjectItem.h"
#include "Object/Item/Inventory/EquipmentSlot.h"

/** \addtogroup Object
  * \{
  * \class Race
  *
  * \brief Base class for specific races.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class Race : public QObject
{
    Q_OBJECT

public:
    explicit Race(QObject *parent = 0);
    explicit Race(const Race &race);

    /**
     * @brief The base speed of the current race.
     * @return The base speed.
     */
    virtual int getBaseSpeed() const;

    /**
      * @brief Generate equipment slots for a race.
      * @return Returns the initial equipment slots for this race.
      */
    virtual QList< EquipmentSlot > getEquipmentSlots() const = 0;

    /**
      * @brief Creates a copy of a race.
      * @return Returns the copy of a race.
      */
    virtual Race *copy() const = 0;

    /**
      * @brief Get the Race IF for a specific race.
      * @return Returns the Race ID for the specific race.
      */
    virtual int getRaceID() const = 0;

    /**
      * @brief Update a race with the information of a new race.
      * @param race
      */
    virtual void update(const Race &race);
};

#endif // RACE_H
