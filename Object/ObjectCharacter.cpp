/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Object/ObjectCharacter.h"

#include <QPainter>
#include <QImage>
#include <QRgb>
#include <qmath.h>

#include "Object/Character/Race/RaceID.h"
#include "Common/Common.h"
#include "Object/ObjectCommon.h"
#include "Command/Backend/CommandLeaf.h"
#include "Command/Backend/CommandNode.h"
#include "Command/Backend/Object/Character/AttackCharacter.h"
#include "Command/Backend/Object/Character/TwoWeaponAttack.h"

ObjectCharacter::ObjectCharacter(Race *race, const Abilities &abilities, const QString &name, const QString &description, const QString &imagePath, bool isVisible, QObject *parent)
    : ObjectBase(name, description, imagePath, isVisible, parent), race(race), abilities(abilities), inventory(race->getEquipmentSlots())
{
    this->initObjectCharacter(10,10);
}

ObjectCharacter::ObjectCharacter(const ObjectCharacter &other)
    : ObjectBase(other),
      race(other.refRace()->copy()),
      abilities(other.abilities),
      inventory(other.inventory),
      currentHP(other.currentHP),
      maxHP(other.maxHP),
      sizeIncrement(other.sizeIncrement),
      movement(other.movement),
      moveActionLeft(other.moveActionLeft),
      standardActionLeft(other.standardActionLeft)
{}

ObjectCharacter::~ObjectCharacter()
{
    if(this->race)
    {
        this->race->deleteLater();
    }
}

void ObjectCharacter::initObjectCharacter(int currentHP, int maxHP)
{
    this->currentHP = currentHP;
    this->maxHP = maxHP;
    this->sizeIncrement = Object::MEDIUM_SIZE;
    this->prepareTurn();
    return;
}

ObjectBase *ObjectCharacter::lookupObjectInside(ObjectBase::ObjectType type, int objectIDNumber, unsigned int spawnID) const
{
    return this->inventory.lookup(type, objectIDNumber, spawnID);
}

DiceAttack ObjectCharacter::getAttackDice(int weaponSlotIndex, const QString &ACName, int AC) const
{
    QString weaponName = QString("");

    DiceAttack diceAttack(tr("Melee"), weaponName, ACName, AC);
    if(!this->inventory.hasItemEquipped(weaponSlotIndex))
    {
        weaponName = tr("Unarmed");
        diceAttack.addModifier(new NumberMod(4, weaponName, false));
    }
    else if(this->inventory.hasImprovisedItemEquipped(weaponSlotIndex))
    {
        weaponName = tr("Improvised (%1)").arg(this->inventory.refEquipmentItem(weaponSlotIndex)->getObjectName());
        diceAttack.addModifier(new NumberMod(4, weaponName, false));
    }
    else
    {
        ObjectItem *item = this->inventory.refEquipmentItem(weaponSlotIndex);
        weaponName = item->getObjectName();
        diceAttack.setCritRoll(item->getCritRange());
        diceAttack.setCritMultiplier(item->getCritMultiplier());
        diceAttack.addModifier(item->refAttackModifier());
    }

    diceAttack.setWeaponName(weaponName);
    if(this->inventory.getEquipment().at(weaponSlotIndex).getEquipmentSlotType() == Object::OFF_HAND)
    {
        diceAttack.addModifier(new NumberMod(2,tr("Off-Hand"), false));
    }
    diceAttack.addModifier(new NumberMod(this->abilities.getStrengthMod()));
    return diceAttack;
}

int ObjectCharacter::getAC() const
{
    return 10+this->abilities.getDexterityMod().getMod();
}

Dice ObjectCharacter::getDamageDice(int equippedWeaponIndex, bool isCritical, int critMultiplier) const
{
    Dice damage(tr("Damage Roll"), 1,3);
    EquipmentSlot slot = this->inventory.getEquipment().at(equippedWeaponIndex);
    ObjectItem *weapon = slot.refItem();

    if(weapon)
    {
        Dice dice(weapon->baseDamageDice());
        damage.setRoll(dice.getAmount(), dice.getSides());
    }

    NumberMod *damageMod;

    if(slot.getEquipmentSlotType() == Object::MAIN_HAND)
    {
        damageMod = new NumberMod(this->getAbilities().getStrengthMod());
    }
    else
    {
        damageMod = new NumberMod(qFloor(this->getAbilities().getStrengthMod().getMod()/2), "Off-Hand (STR/2)");
    }

    damage.addModifier(damageMod);
    if(isCritical)
    {
        damage.setRollName(tr("Critical Damage Roll"));
        damage.setRoll(damage.getAmount()*critMultiplier,damage.getSides());
        damage.addModifier(damageMod);
    }

    return damage;
}

int ObjectCharacter::getCurrentHP() const
{
    return this->currentHP;
}

int ObjectCharacter::getMaxHP() const
{
    return this->maxHP;
}

double ObjectCharacter::getHPRatio() const
{
    return (this->currentHP*1.0)/(this->maxHP*1.0);
}

double ObjectCharacter::getDyingRatio() const
{
    return (qAbs(this->currentHP)*1.0)/(this->abilities.getConstitution()*1.0);
}

bool ObjectCharacter::isUnconcious() const
{
    return this->currentHP <= 0;
}

bool ObjectCharacter::isDead() const
{
    return qAbs(this->currentHP) >= this->abilities.getConstitution();
}

ObjectBase *ObjectCharacter::copyBase() const
{
    return this->copy();
}

void ObjectCharacter::updateCharacter(const ObjectCharacter &other)
{
    if(this->race)
    {
        this->race->deleteLater();
    }

    this->race = other.refRace()->copy();
    this->race->setParent(this);
    this->abilities.update(other.getAbilities());
    this->inventory.update(other.getInventoryModel());

    this->setCurrentHP(other.getCurrentHP());
    this->setMaxHP(other.getMaxHP());
    this->movement = other.movement;
    this->moveActionLeft = other.moveActionLeft;
    this->standardActionLeft = other.standardActionLeft;

    this->updateBase(other);
    return;
}

void ObjectCharacter::dealDamage(int damage)
{
    if(damage < 1)
    {
        damage = 1;
    }

    this->currentHP -= damage;

    return;
}

QImage ObjectCharacter::drawStatus(const QImage &character) const
{
    QImage original = character;

    if(this->getCurrentHP() == this->getMaxHP() || this->isDead())
    {
        return original;
    }

    QPainter statusPainter(&original);
    int healthBarLength = 0;

    if(this->getCurrentHP() > 0)
    {
        double HPRatio = this->getHPRatio();
        if(HPRatio > 0.25 && HPRatio <= 0.5)
        {
            statusPainter.setPen(QColor(255,255,0));
        }
        else if(HPRatio <= 0.25)
        {
            statusPainter.setPen(QColor(255,0,0));
        }
        else
        {
            statusPainter.setPen(QColor(0,255,0));
        }

        healthBarLength = ((int) ((original.size().width()-1)*HPRatio)) + 1;
    }
    else if(this->isUnconcious() && !this->isDead())
    {
        double dyingRatio = 1-this->getDyingRatio();

        statusPainter.setPen(QColor(0,0,0));
        healthBarLength = ((int) ((original.size().width()-1)*dyingRatio)) + 1;

        statusPainter.drawImage(TEoH::CONDITION_X_POSITION_ON_SQUARE, 1, QImage(":/objects/characters/conditions/disabled"));
    }

    statusPainter.drawLine(0,
                           original.size().height()-2,
                           healthBarLength,
                           original.size().height()-2);
    statusPainter.drawLine(0,
                           original.size().height()-1,
                           healthBarLength,
                           original.size().height()-1);

    return original;
}

void ObjectCharacter::characterMoved(int movedBy)
{
    if(movedBy > this->getMovementSpeed())
    {
        this->standardActionUsed();
    }

    this->moveActionUsed();
    this->movement = false;
    return;
}

void ObjectCharacter::moveActionUsed()
{
    if(this->moveActionLeft)
    {
        this->moveActionLeft = false;
    }
    else
    {
        this->standardActionUsed();
    }

    return;
}

void ObjectCharacter::standardActionUsed()
{
    this->setStandardActionLeft(false);
    return;
}

void ObjectCharacter::fullRoundActionUsed()
{
    this->moveActionUsed();
    this->standardActionUsed();
    return;
}

void ObjectCharacter::prepareTurn()
{
    this->movement = true;
    this->moveActionLeft = true;
    this->standardActionLeft = true;
    return;
}

int ObjectCharacter::getMovementSpeed() const
{
    return this->race->getBaseSpeed();
}

int ObjectCharacter::getHustleSpeed() const
{
    return this->race->getBaseSpeed();
}

int ObjectCharacter::getRunningSpeed() const
{
    return qMax(10,this->abilities.getDexterityMod().getMod()*2*5);
}

const Abilities &ObjectCharacter::getAbilities() const
{
    return this->abilities;
}

int ObjectCharacter::getTotalSpeed() const
{
    return this->getMovementSpeed() + this->getHustleSpeed() + this->getRunningSpeed();
}

bool ObjectCharacter::canMove() const
{
    return this->movement && this->hasMoveActionLeft();
}

bool ObjectCharacter::canHustle() const
{
    return this->hasFullRoundActionLeft() && this->canMove();
}

bool ObjectCharacter::canRun() const
{
    return this->canHustle();
}

bool ObjectCharacter::hasMoveActionLeft() const
{
    return this->moveActionLeft || this->hasStandardActionLeft();
}

bool ObjectCharacter::hasStandardActionLeft() const
{
    return this->standardActionLeft;
}

bool ObjectCharacter::hasFullRoundActionLeft() const
{
    return this->hasStandardActionLeft() && this->moveActionLeft;
}

const InventoryModel &ObjectCharacter::getInventoryModel() const
{
    return this->inventory;
}

Object::SizeIncrement ObjectCharacter::getSizeIncrement() const
{
    return this->sizeIncrement;
}

ObjectBase::ObjectType ObjectCharacter::getType() const
{
    return ObjectBase::CHARACTER;
}

int ObjectCharacter::getObjectIDNumber() const
{
    return this->getObjectID();
}

QImage ObjectCharacter::getImage() const
{
    return this->getImage(this->usedImageIndex);
}

QImage ObjectCharacter::getImage(int index) const
{
    if(!this->isVisible)
    {
        return QImage(0);
    }

    QImage image = this->ObjectBase::getImage(index);

    QPainter painter(&image);

    QList<EquipmentSlot> equipment = this->inventory.getEquipment();
    ObjectItem *item;

    for(int i = 0; i < equipment.size(); ++i)
    {
        item = equipment.at(i).refItem();

        if(item)
        {
            painter.drawImage(equipment.at(i).getItemPosition(),item->getSilhouette());
        }
    }

    return this->drawStatus(TEoH::drawGlowOnImage(image));
}

QList<CommandTree *> ObjectCharacter::getCommandTreeList(ObjectBase *player)
{
    QList<CommandTree *> list;
    CommandNode *standardAttackNode = new CommandNode(tr("Attack (Standard)"));

    if(player && player->getType() == ObjectBase::CHARACTER && player->getObjectIDNumber() == ObjectID::PLAYER)
    {
        ObjectCharacter *character = qobject_cast<ObjectCharacter *>(player);
        QList<EquipmentSlot> equipment = character->refInventoryModel()->getEquipment();

        for(int i = 0; i < equipment.size(); ++i)
        {
            Object::EquipmentSlotType type = equipment[i].getEquipmentSlotType();

            if(type == Object::MAIN_HAND || type == Object::OFF_HAND)
            {
                QString weaponName = tr("Unarmed");
                ObjectItem *weapon = equipment[i].refItem();
                if(weapon)
                {
                    weaponName = weapon->getObjectName();
                    if(!weapon->isWieldable())
                    {
                        weaponName = weaponName.append(tr(" (Improvised)"));
                    }
                }

                standardAttackNode->append(new CommandLeaf(new AttackCharacter(character, this, i), tr("%1: %2")
                                                          .arg(type == Object::MAIN_HAND ? tr("Main-Hand") : tr("Off-Hand"))
                                                          .arg(weaponName)));
            }
        }
    }

    list.append(standardAttackNode);

    if(player && player->getType() == ObjectBase::CHARACTER && player->getObjectIDNumber() == ObjectID::PLAYER)
    {
        ObjectCharacter *character = qobject_cast<ObjectCharacter *>(player);
        QList<EquipmentSlot> equipment = character->refInventoryModel()->getEquipment();

        // Find first main-hand weapon index.
        int mainHandWeaponIndex = -1;
        for(int i = 0; i < equipment.size(); ++i)
        {
            if(equipment[i].getEquipmentSlotType() == Object::MAIN_HAND && mainHandWeaponIndex < 0)
            {
                mainHandWeaponIndex = i;
            }
        }

        int offHandWeaponIndex = -1;
        for(int i = 0; i < equipment.size(); ++i)
        {
            if(equipment[i].getEquipmentSlotType() == Object::OFF_HAND && offHandWeaponIndex < 0)
            {
                offHandWeaponIndex = i;
            }
        }

        if(mainHandWeaponIndex >= 0 && offHandWeaponIndex >= 0)
        {
            list.append(new CommandLeaf(new TwoWeaponAttack(character, this, mainHandWeaponIndex, offHandWeaponIndex), tr("Two-Weapon Attack (Full-Round)")));
        }
    }

    list.append(this->ObjectBase::getCommandTreeList(player));

    return list;
}

QString ObjectCharacter::getDescriptiveName() const
{
    QString status;

    if(this->isUnconcious())
    {
        status.append(tr("Unconcious"));
    }

    if(status.isEmpty())
    {
        return this->ObjectBase::getDescriptiveName();
    }
    else
    {
        return QString("%1 (%2)").arg(this->ObjectBase::getDescriptiveName(), status);
    }
}

QList<QPair<QString,QString> > ObjectCharacter::getOverviewInformation() const
{
    QList<QPair<QString,QString> > result;

    result.append(QPair<QString,QString>(tr("COMBAT"),""));
    result.append(QPair<QString,QString>(tr("Hit Points   : %1/%2").arg(this->getCurrentHP()).arg(this->getMaxHP()),tr("Non-Lethal Damage: 0")));

    QList<EquipmentSlot> equipmentSlots = this->inventory.getEquipment();
    for(int i = 0; i < equipmentSlots.size(); ++i)
    {
        QString hand = "";

        if(equipmentSlots[i].getEquipmentSlotType() == Object::MAIN_HAND)
        {
            hand = "Main-Hand";
        }
        else if(equipmentSlots[i].getEquipmentSlotType() == Object::OFF_HAND)
        {
            hand = "Off-Hand ";
        }

        if(!hand.isEmpty())
        {
            DiceAttack attackDice = this->getAttackDice(i);
            Dice damageDice = this->getDamageDice(i, false, attackDice.getCritMultiplier());

            result.append(QPair<QString,QString>(tr("%1    : %2")
                                                 .arg(hand)
                                                 .arg(attackDice.getWeaponName()),
                                                 tr("%1 (%2) [%3 | %4/%5 | S]")
                                                 .arg(attackDice.getAttackName())
                                                 .arg(TEoH::showSign(attackDice.getAttackModsTotal()))
                                                 .arg(damageDice.showDiceRoll())
                                                 .arg(attackDice.showCritRange())
                                                 .arg(attackDice.showCritMultiplier())
                                                 ));
        }
    }

    result.append(QPair<QString,QString>(tr("Armor Class  : %1").arg(this->getAC()),
                                         tr("Touch AC      : %1\nFlat-Footed AC: %2").arg(this->getAC()).arg(this->getAC())));
    result.append(QPair<QString,QString>(" ",""));
    result.append(QPair<QString,QString>(tr("ACTIONS"),""));
    result.append(QPair<QString,QString>(tr("Standard Action  : %1").arg(this->hasStandardActionLeft() ? tr("Yes") : tr("No")),""));
    result.append(QPair<QString,QString>(tr("Move Action      : %1").arg(this->moveActionLeft ? tr("Yes") : tr("No")),""));
    result.append(QPair<QString,QString>(tr("Full-Round Action: %1").arg(this->hasFullRoundActionLeft() ? tr("Yes") : tr("No")),""));
    result.append(QPair<QString,QString>(" ",""));
    result.append(QPair<QString,QString>(tr("SPEED"),""));
    result.append(QPair<QString,QString>(tr("Movement: %1ft.").arg(this->getMovementSpeed()),
                                         tr("Hustle: %1ft.\nRun   : %2ft.").arg(this->getHustleSpeed()+this->getMovementSpeed()).arg(this->getRunningSpeed()+this->getHustleSpeed()+this->getMovementSpeed())));
    return result;
}

void ObjectCharacter::setCurrentHP(int currentHP)
{
    this->currentHP = currentHP;
    return;
}

void ObjectCharacter::setMaxHP(int maxHP)
{
    this->maxHP = maxHP;
    return;
}

void ObjectCharacter::setStandardActionLeft(bool standardActionLeft)
{
    this->standardActionLeft = standardActionLeft;
    return;
}

Race *ObjectCharacter::refRace() const
{
    return this->race;
}

InventoryModel *ObjectCharacter::refInventoryModel()
{
    return &this->inventory;
}

void ObjectCharacter::serialize(QDataStream &dataStream) const
{
    ObjectBase::serialize(dataStream);

    dataStream << this->currentHP;
    dataStream << this->maxHP;
    dataStream << (int) this->sizeIncrement;
    dataStream << this->movement;
    dataStream << this->moveActionLeft;
    dataStream << this->standardActionLeft;

    dataStream << this->race->getRaceID();
    this->abilities.serialize(dataStream);
    this->inventory.serialize(dataStream);

    return;
}

void ObjectCharacter::deserialize(QDataStream &dataStream)
{
    ObjectBase::deserialize(dataStream);

    int sizeIncrement;

    dataStream >> this->currentHP;
    dataStream >> this->maxHP;
    dataStream >> sizeIncrement;
    this->sizeIncrement = Object::SizeIncrement(sizeIncrement);
    dataStream >> this->movement;
    dataStream >> this->moveActionLeft;
    dataStream >> this->standardActionLeft;

    int raceID;

    dataStream >> raceID;

    if(this->race)
    {
        this->race->deleteLater();
    }

    this->race = RaceID::fromID(RaceID::ID(raceID));
    this->race->setParent(this);

    this->abilities.deserialize(dataStream);
    this->inventory.deserialize(dataStream);

    return;
}
