/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>

#include "ObjectFromID.h"

#include "Object/Character/Player.h"
#include "Object/Character/GoblinCharacter.h"

#include "Object/Item/Dagger.h"
#include "Object/Item/Corpse.h"

#include "Object/Special/StartPosition.h"

#include "Object/Tile/BrickWall.h"

#include "Object/Tile/MudFloor.h"

ObjectCharacter *ObjectFromID::objectFrom(ObjectID::CharacterID id, QObject *parent)
{
    ObjectCharacter *character = 0;

    switch(id)
    {
        case ObjectID::PLAYER: character = new Player(); break;
        case ObjectID::GOBLIN: character = new GoblinCharacter(); break;
        default: break;
    }

    if(character)
    {
        character->setParent(parent);
    }

    return character;
}

ObjectItem *ObjectFromID::objectFrom(ObjectID::ItemID id, QObject *parent)
{
    ObjectItem *item = 0;

    switch(id)
    {
        case ObjectID::DAGGER: item = new Dagger(); break;
        case ObjectID::CORPSE: item = new Corpse(); break;
        default: break;
    }

    if(item)
    {
        item->setParent(parent);
    }

    return item;
}

ObjectSpecial *ObjectFromID::objectFrom(ObjectID::SpecialID id, QObject *parent)
{
    ObjectSpecial *special = 0;

    switch(id)
    {
        case ObjectID::START_POSITION: special = new StartPosition(); break;
        default: break;
    }

    if(special)
    {
        special->setParent(parent);
    }

    return special;
}

ObjectTile *ObjectFromID::objectFrom(ObjectID::TileID id, QObject *parent)
{
    ObjectTile *tile = 0;

    switch(id)
    {
        case ObjectID::BRICK_WALL: tile = new BrickWall(); break;
        case ObjectID::MUD_FLOOR: tile = new MudFloor(); break;
        default: break;
    }

    if(tile)
    {
        tile->setParent(parent);
    }

    return tile;
}


ObjectBase *ObjectFromID::objectFrom(ObjectBase::ObjectType type, int id, QObject *parent)
{
    ObjectBase *object = 0;

    switch(type)
    {
    case ObjectBase::CHARACTER: object = ObjectFromID::objectFrom(ObjectID::CharacterID(id), parent); break;
    case ObjectBase::ITEM: object = ObjectFromID::objectFrom(ObjectID::ItemID(id), parent); break;
    case ObjectBase::TILE: object = ObjectFromID::objectFrom(ObjectID::TileID(id), parent); break;
    case ObjectBase::SPECIAL: object = ObjectFromID::objectFrom(ObjectID::SpecialID(id), parent); break;
    default: qDebug() << "Non-Existing tool type given when refering. Tool Type was: " << type; break;
    }

    return object;
}
