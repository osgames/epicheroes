/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OBJECT_ITEM_H
#define OBJECT_ITEM_H

#include "Common/Dice.h"
#include "Common/DiceAttack.h"
#include "Common/NumberMod.h"

#include "Object/ObjectBase.h"
#include "Object/ObjectID.h"
#include "Object/ObjectCommon.h"

/** \addtogroup Object
  * \{
  * \class ObjectItem
  *
  * \brief The baseclass for the Items to be placed into the game's mainView.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class ObjectItem : public ObjectBase
{
    Q_OBJECT

private:
    Object::SizeIncrement       sizeIncrement;
    Object::EquipmentSlotType   equipmentSlot;
    bool                        wieldable;
    bool                        inside;
    int                         weight;        ///< Represents decimal weight as integer (I.e.: weight = 100 => 1.00 lbs)

public:
    explicit ObjectItem(Object::EquipmentSlotType equipmentSlot = Object::NONE,
                        bool wieldable = true,
                        const QString &name = "",
                        const QString &description = "",
                        const QString &imagePath = "",
                        int weight = 100,
                        Object::SizeIncrement size = Object::MEDIUM_SIZE,
                        bool isVisible = true,
                        bool inside = false,
                        QObject *parent = 0);
    explicit ObjectItem(const ObjectItem &objectItem);

protected:
    /**
      * \brief Initialize Attributes
      */
    void initObjectItem(Object::SizeIncrement sizeIncrement = Object::MEDIUM_SIZE,
                        Object::EquipmentSlotType equipmentSlot = Object::NONE,
                        bool wieldable = true,
                        bool inside = false,
                        int weight = 1);

public:
    virtual NumberMod *refAttackModifier();
    virtual int getCritRange() const;
    virtual int getCritMultiplier() const;
    virtual Dice damageDiceByWeight();
    virtual Dice baseDamageDice();

    virtual ObjectItem *copy() const = 0;
    virtual ObjectBase *copyBase() const;

    virtual void update(const ObjectBase &object) = 0;
    void updateItem(const ObjectItem &item);

    // Get-Methods
    Object::SizeIncrement getSizeIncrement() const;
    Object::EquipmentSlotType getEquipmentSlot() const;
    bool isWieldable() const;
    bool isInside() const;
    int getWeight() const;
    virtual ObjectBase::ObjectType getType() const;
    virtual ObjectID::ItemID getObjectID() const = 0;
    virtual int getObjectIDNumber() const;
    virtual QList<CommandTree *> getCommandTreeList(ObjectBase *player);
    virtual QImage getSilhouette() const = 0;
    virtual QPoint getSlotOffset() const = 0;
    virtual QImage getImage() const;
    virtual QImage getImage(int index) const;
    virtual bool containsItems() const;

    // Set-Methods
    void setSizeIncrement(Object::SizeIncrement sizeIncrement);
    void setEquipmentSlot(Object::EquipmentSlotType equipmentSlot);
    void setWieldable(bool wieldable);
    void setInside(bool inside);
    void setWeight(int weight);

public:
    // Serialize
    virtual void serialize(QDataStream &dataStream) const;
    virtual void serialize(QIODevice *device) const = 0;
    virtual void deserialize(QDataStream &dataStream);
    virtual void deserialize(QIODevice *device) = 0;
};

#endif // OBJECT_ITEM_H
