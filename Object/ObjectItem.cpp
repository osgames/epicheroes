/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Object/ObjectItem.h"

#include "Command/Backend/Object/Item/PickupItem.h"
#include "Command/Backend/Object/Item/DropItem.h"
#include "Command/Backend/CommandLeaf.h"

ObjectItem::ObjectItem(Object::EquipmentSlotType equipmentSlot, bool wieldable, const QString &name, const QString &description, const QString &imagePath, int weight, Object::SizeIncrement sizeIncrement, bool isVisible, bool inInventory, QObject *parent)
    : ObjectBase(name, description, imagePath, isVisible, parent)
{
    this->initObjectItem(sizeIncrement, equipmentSlot, wieldable, inInventory, weight);
}

ObjectItem::ObjectItem(const ObjectItem &objectItem)
    : ObjectBase(objectItem)
{
    this->initObjectItem(objectItem.getSizeIncrement(), objectItem.getEquipmentSlot(), objectItem.isWieldable(), objectItem.isInside(), objectItem.getWeight());
}

void ObjectItem::initObjectItem(Object::SizeIncrement sizeIncrement, Object::EquipmentSlotType equipmentSlot, bool wieldable, bool inside, int weight)
{
    this->setSizeIncrement(sizeIncrement);
    this->setEquipmentSlot(equipmentSlot);
    this->setWieldable(wieldable);
    this->setInside(inside);
    this->setWeight(weight);
    return;
}

NumberMod *ObjectItem::refAttackModifier()
{
    return new NumberMod(4,tr("Improvised(%1)").arg(this->getObjectName()), false, this);
}

int ObjectItem::getCritRange() const
{
    return 20;
}

int ObjectItem::getCritMultiplier() const
{
    return 2;
}

Dice ObjectItem::damageDiceByWeight()
{
    return Dice(QString("Weight Damage"), 1 + ((int) (this->weight/10000)), 2 + 2*((int) (((this->weight%10000)/2) / 1000)));
}

Dice ObjectItem::baseDamageDice()
{
    return this->damageDiceByWeight();
}

ObjectBase *ObjectItem::copyBase() const
{
    return this->copy();
}

void ObjectItem::updateItem(const ObjectItem &item)
{
    this->updateBase(item);
    return;
}

Object::SizeIncrement ObjectItem::getSizeIncrement() const
{
    return this->sizeIncrement;
}

Object::EquipmentSlotType ObjectItem::getEquipmentSlot() const
{
    return this->equipmentSlot;
}

bool ObjectItem::isWieldable() const
{
    return this->wieldable;
}

bool ObjectItem::isInside() const
{
    return this->inside;
}

int ObjectItem::getWeight() const
{
    return this->weight;
}

ObjectBase::ObjectType ObjectItem::getType() const
{
    return ObjectBase::ITEM;
}

int ObjectItem::getObjectIDNumber() const
{
    return this->getObjectID();
}

QList<CommandTree *> ObjectItem::getCommandTreeList(ObjectBase *player)
{
    QList<CommandTree *> list;
    if(player && player->getType() == ObjectBase::CHARACTER && player->getObjectIDNumber() == ObjectID::PLAYER)
    {
        ObjectCharacter *character = qobject_cast<ObjectCharacter *>(player);

        if(this->isInside())
        {
            list.append(new CommandLeaf(new DropItem(this, character), tr("Drop Item (Move)")));
        }
        else
        {
            list.append(new CommandLeaf(new PickupItem(this, character), tr("Pick Up (Move)")));
        }
    }

    list.append(this->ObjectBase::getCommandTreeList(player));

    return list;
}

QImage ObjectItem::getImage() const
{
    return this->ObjectBase::getImage();
}

QImage ObjectItem::getImage(int index) const
{
    return this->ObjectBase::getImage(index);
}

bool ObjectItem::containsItems() const
{
    return false;
}

void ObjectItem::setSizeIncrement(Object::SizeIncrement sizeIncrement)
{
    this->sizeIncrement = sizeIncrement;
    return;
}

void ObjectItem::setEquipmentSlot(Object::EquipmentSlotType equipmentSlot)
{
    this->equipmentSlot = equipmentSlot;
    return;
}

void ObjectItem::setWieldable(bool wieldable)
{
    this->wieldable = wieldable;
    return;
}

void ObjectItem::setInside(bool inside)
{
    if(inside)
    {
        this->unsetPosition();
    }

    this->inside = inside;
    return;
}

void ObjectItem::setWeight(int weight)
{
    this->weight = weight;
    return;
}

void ObjectItem::serialize(QDataStream &dataStream) const
{
    ObjectBase::serialize(dataStream);

    dataStream << this->sizeIncrement;
    dataStream << this->equipmentSlot;
    dataStream << this->wieldable;
    dataStream << this->inside;
    dataStream << this->weight;

    return;
}

void ObjectItem::deserialize(QDataStream &dataStream)
{
    ObjectBase::deserialize(dataStream);

    int sizeIncrement;
    int equipmentSlot;
    bool wieldable;
    bool inside;
    int weight;

    dataStream >> sizeIncrement >> equipmentSlot >> wieldable >> inside >> weight;

    this->initObjectItem(Object::SizeIncrement(sizeIncrement), Object::EquipmentSlotType(equipmentSlot), wieldable, inside, weight);

    return;
}
