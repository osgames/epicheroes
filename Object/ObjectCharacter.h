/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OBJECT_CHARACTER_H
#define OBJECT_CHARACTER_H

#include "Common/DiceAttack.h"
#include "Object/ObjectBase.h"
#include "Object/ObjectID.h"
#include "Object/Item/Inventory/InventoryModel.h"
#include "Object/Character/Race/Race.h"
#include "Object/Character/Abilities/Abilities.h"
#include "Object/ObjectCommon.h"

/** \addtogroup Object
  * \{
  * \class ObjectCharacter
  *
  * \brief The baseclass for the Characters to be placed into the game's mainView.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class ObjectCharacter : public ObjectBase
{
    Q_OBJECT
protected:
    Race *                  race;
    Abilities               abilities;
    InventoryModel          inventory;
    int                     currentHP;
    int                     maxHP;
    Object::SizeIncrement   sizeIncrement;
    bool                    movement;
    bool                    moveActionLeft;
    bool                    standardActionLeft;

public:
    explicit ObjectCharacter(Race *race,
                             const Abilities &abilities,
                             const QString &name = "",
                             const QString &description = "",
                             const QString &imagePath = "",
                             bool isVisible = true,
                             QObject *parent = 0);
    explicit ObjectCharacter(const ObjectCharacter &other);
    ~ObjectCharacter();

protected:
    /**
      * \brief Initialize attributes.
      */
    void initObjectCharacter(int currentHP, int maxHP);

public:
    virtual ObjectBase *lookupObjectInside(ObjectBase::ObjectType type, int objectIDNumber, unsigned int spawnID) const;

    virtual ObjectCharacter *copy() const = 0;
    virtual ObjectBase *copyBase() const;

    void updateCharacter(const ObjectCharacter &other);
    virtual void update(const ObjectBase &object) = 0;

    /**
      * @brief Deal damage to character. Damage dealt is always at least 1.
      * @param damage The amount of damage dealt.
      */
    void dealDamage(int damage);

    /**
      * @brief Draw an healthbar and other status informations onto an character.
      * @param character The image of the finished character, to get status information.
      * @return An image with the status information drawn on.
      */
    QImage drawStatus(const QImage &character) const;

public:
    void characterMoved(int movedBy);
    void moveActionUsed();
    void standardActionUsed();
    void fullRoundActionUsed();

    /**
     * @brief Ready up for a new round.
     */
    void prepareTurn();

    // Get-Methods
    int getMovementSpeed() const;
    int getHustleSpeed() const;
    int getRunningSpeed() const;
    int getTotalSpeed() const;
    bool canMove() const;
    bool canHustle() const;
    bool canRun() const;
    bool hasMoveActionLeft() const;
    bool hasStandardActionLeft() const;
    bool hasFullRoundActionLeft() const;
    DiceAttack getAttackDice(int weaponSlotIndex, const QString &ACName = "AC", int AC = 10) const;
    int getAC() const;
    Dice getDamageDice(int equippedWeaponIndex, bool isCritical, int critMultiplier) const;
    int getCurrentHP() const;
    int getMaxHP() const;
    double getHPRatio() const;
    double getDyingRatio() const;
    bool isUnconcious() const;
    bool isDead() const;
    const Abilities &getAbilities() const;
    const InventoryModel &getInventoryModel() const;
    Object::SizeIncrement getSizeIncrement() const;
    virtual ObjectBase::ObjectType getType() const;
    virtual ObjectID::CharacterID getObjectID() const = 0;
    virtual int getObjectIDNumber() const;
    virtual QImage getImage() const;
    virtual QImage getImage(int index) const;
    virtual QList<CommandTree *> getCommandTreeList(ObjectBase *player);
    virtual QString getDescriptiveName() const;
    virtual QList<QPair<QString,QString> > getOverviewInformation() const;

    // Set-Methods
    void setCurrentHP(int currentHP);
    void setMaxHP(int maxHP);
    void setStandardActionLeft(bool standardActionLeft);

    // Ref-Methods
    Race *refRace() const;
    InventoryModel *refInventoryModel();

    // Serialize
    virtual void serialize(QDataStream &dataStream) const;
    virtual void serialize(QIODevice *device) const = 0;
    virtual void deserialize(QDataStream &dataStream);
    virtual void deserialize(QIODevice *device) = 0;
};

#endif // OBJECT_CHARACTER_H
