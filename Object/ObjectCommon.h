/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OBJECT_COMMON_H
#define OBJECT_COMMON_H

#include <QString>
#include <QTranslator>

/**
  * \brief Object namespace.
  */
namespace Object
{
const QString ITEM_MIME_DATA = "object/x-item";

/**
  * @enum EquipmentSlotType
  * @brief The different equipment slots.
  */
enum EquipmentSlotType
{
    MAIN_HAND = 0,
    OFF_HAND,
    HEAD,
    HEADBAND,
    EYES,
    SHOULDERS,
    NECK,
    CHEST,
    BODY,
    ARMOR,
    BELT,
    HANDS,
    RING,
    FEET,
    EQUIPMENT_SLOT_TYPE_COUNT,
    NONE
};

/**
  * @enum SizeIncrement
  * @brief Represents the different size increments in the game.
  */
enum SizeIncrement {FINE_SIZE=0, DIMINUTIVE_SIZE, TINY_SIZE, SMALL_SIZE, MEDIUM_SIZE, LARGE_SIZE, HUGE_SIZE, GARGANTUAN_SIZE, COLOSSAL_SIZE};

/**
  * @brief Converts an Equipment Slot Type Enum to a representive string.
  * @param equipmentSlot The Equipment Slot Type Enum to be converted.
  * @return The converted string.
  */
inline QString equipmentSlotTypeToString(Object::EquipmentSlotType equipmentSlotType)
{
    QString equipmentTypeString = QObject::tr("Not a slot.");

    switch(equipmentSlotType)
    {
        case Object::MAIN_HAND: equipmentTypeString = QObject::tr("Main-Hand"); break;
        case Object::OFF_HAND: equipmentTypeString = QObject::tr("Off-Hand"); break;
        case Object::HEAD: equipmentTypeString = QObject::tr("Head"); break;
        case Object::HEADBAND: equipmentTypeString = QObject::tr("Headband"); break;
        case Object::EYES: equipmentTypeString = QObject::tr("Eyes"); break;
        case Object::SHOULDERS: equipmentTypeString = QObject::tr("Shoulders"); break;
        case Object::NECK: equipmentTypeString = QObject::tr("Neck"); break;
        case Object::CHEST: equipmentTypeString = QObject::tr("Chest"); break;
        case Object::BODY: equipmentTypeString = QObject::tr("Body"); break;
        case Object::ARMOR: equipmentTypeString = QObject::tr("Armor"); break;
        case Object::BELT: equipmentTypeString = QObject::tr("Belt"); break;
        case Object::HANDS: equipmentTypeString = QObject::tr("Hands"); break;
        case Object::RING: equipmentTypeString = QObject::tr("Ring"); break;
        case Object::FEET: equipmentTypeString = QObject::tr("Feet"); break;
        default: break;
    }

    return equipmentTypeString;
}

}

#endif // OBJECT_COMMON_H
