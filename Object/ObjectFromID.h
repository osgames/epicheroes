/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OBJECT_FROM_ID_H
#define OBJECT_FROM_ID_H

#include "Object/ObjectID.h"
#include "Object/ObjectBase.h"
#include "Object/ObjectCharacter.h"
#include "Object/ObjectItem.h"
#include "Object/ObjectTile.h"
#include "Object/ObjectSpecial.h"

/**
  * \brief ObjectFromID namespace.
  */
namespace ObjectFromID
{
    /**
     * @brief Create specific object from id.
     * @param id The id for a specific object of a certain type.
     * @param parent The parent to be set for that object.
     * @return Returns the created object.
     */
    ObjectCharacter *objectFrom(ObjectID::CharacterID id, QObject *parent = 0);
    ObjectItem *objectFrom(ObjectID::ItemID id, QObject *parent = 0);
    ObjectSpecial *objectFrom(ObjectID::SpecialID id, QObject *parent = 0);
    ObjectTile *objectFrom(ObjectID::TileID id, QObject *parent = 0);

    /**
      * @brief Create object from type and id.
      * @param type The type of object.
      * @param id The id for a specific object of the given type.
      * @param parent The parent to be set for that object.
      * @return Returns the created object.
      */
    ObjectBase *objectFrom(ObjectBase::ObjectType type, int id, QObject *parent = 0);
}

#endif // OBJECT_FROM_ID_H
