/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Editor/EditorTools.h"
#include "Editor/EditorCommon.h"
#include "Command/Backend/Editor/Tools/ChooseMainTool.h"
#include "Object/ObjectFromID.h"

#include <QPoint>

EditorTools::EditorTools(Processor *processor, EditorModel *editorModel, QWidget *parent)
    : QWidget(parent)
{
    this->processor = processor;
    this->editorModel = editorModel;

    // Set Tooldialog settings.
    this->setWindowFlags(Qt::Tool);
    this->setFixedSize(150,170);

    // Initialize button vector.
    this->addButton(":/objects/characters/hero");
    this->buttonVector.last()->setToolTip(tr("Characters"));
    this->addButton(":/objects/items/dagger");
    this->buttonVector.last()->setToolTip(tr("Items"));
    this->addButton(":/objects/walls/brick_wall");
    this->buttonVector.last()->setToolTip(tr("Tiles"));
    this->addButton(":/objects/specials/special_button");
    this->buttonVector.last()->setToolTip(tr("Special"));

    // Set up the grid layout
    this->gridLayout = new QGridLayout(this);
    this->gridLayout->setAlignment(Qt::AlignTop | Qt::AlignCenter);

    int i = 0;
    int y = 0;
    for(y = 0; i < this->buttonVector.size(); ++y)
    {
        for(int x = 0; x < TEoH::TOOL_BUTTON_COUNT_IN_ROW && i < this->buttonVector.size(); ++x)
        {
            QAbstractButton *button = this->buttonVector[i];
            button->setParent(this);
            this->toolButtons.addButton(button, i);
            this->gridLayout->addWidget(button,y,x);
            i++;
        }
    }

    this->chosenToolButton = new QToolButton(this);
    this->gridLayout->addWidget(chosenToolButton,y+1,0,1,4,Qt::AlignCenter);

    // Finish it up, by making the tools usable.
    this->setEnabled(true);
    this->setVisible(true);

    this->updateChosenTool();

    connect(&this->toolButtons, SIGNAL(buttonClicked(int)), this, SLOT(chooseTool(int)));
}

void EditorTools::addButton(const QString &iconPath)
{
    QToolButton *button = new QToolButton(this);
    button->setIcon(QIcon(QPixmap(iconPath)));
    this->buttonVector.append(button);
    return;
}

void EditorTools::chooseTool(int button)
{
    this->processor->execute(new ChooseMainTool(button));
    this->updateChosenTool();
    return;
}

void EditorTools::updateChosenTool()
{
    ObjectBase *object = this->editorModel->copyChosenObject();
    QPixmap p = QPixmap(object->getCurrentImagePath());
    this->chosenToolButton->setIcon(QIcon(p.scaledToHeight(120)));
    this->chosenToolButton->setIconSize(QSize(120,90));
    this->chosenToolButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    this->chosenToolButton->setText(object->getObjectName());
    this->chosenToolButton->setToolTip(object->getDescription());
    this->chosenToolButton->resize(150,150);
    object->deleteLater();
    return;
}
