/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ChooseToolDialog.h"

#include <QPainter>
#include <QScrollBar>

#include "Object/ObjectID.h"
#include "Object/ObjectFromID.h"


ChooseToolDialog::ChooseToolDialog(ObjectBase::ObjectType type, QWidget *parent)
    : QDialog(parent)
{
    this->type = type;
    this->chosenObjectID = 0;

    this->gridLayout = new QGridLayout(this);
    this->setLayout(this->gridLayout);

    this->list = new QListWidget(this);

    this->list->setDragEnabled(false);
    this->list->setViewMode(QListView::IconMode);
    this->list->setResizeMode(QListView::Adjust);
    this->list->setMovement(QListView::Static);
    this->list->setDragDropMode(QAbstractItemView::NoDragDrop);
    this->list->setIconSize(QSize(30,30));
    this->list->setSpacing(5);
    this->list->setAcceptDrops(false);
    this->list->setDropIndicatorShown(false);
    this->list->setSelectionMode(QAbstractItemView::SingleSelection);

    this->createSelection(this->type);

    connect(this->list->verticalScrollBar(), SIGNAL(sliderReleased()), this, SLOT(snapSlider()));
    connect(this->list, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(itemSelected(QListWidgetItem*)));

    this->ok = new QPushButton(tr("Ok"),this);
    connect(this->ok, SIGNAL(clicked()), this, SLOT(accept()));

    this->cancel = new QPushButton(tr("Cancel"),this);
    connect(this->cancel, SIGNAL(clicked()), this, SLOT(reject()));

    this->gridLayout->addWidget(this->list,0,0,1,2);
    this->gridLayout->addWidget(this->ok,1,0);
    this->gridLayout->addWidget(this->cancel,1,1);

}

int ChooseToolDialog::getChosenObjectID() const
{
    return this->chosenObjectID;
}

void ChooseToolDialog::createSelection(ObjectBase::ObjectType type)
{
    int count = 0;
    int i = 0;

    switch(type)
    {
    case(ObjectBase::CHARACTER):
    {
        count = ObjectID::CHARACTER_COUNT;
        break;
    }
    case(ObjectBase::ITEM):
    {
        count = ObjectID::ITEM_COUNT;
        break;
    }
    case(ObjectBase::TILE):
    {
        count = ObjectID::TILE_COUNT;
        break;
    }
    case(ObjectBase::SPECIAL):
    {
        count = ObjectID::SPECIAL_COUNT;
        break;
    }
    default: break;
    }

    this->chosenObjectID = i;

    QListWidgetItem *item;
    ObjectBase *object;

    for(; i < count; ++i)
    {
        object = ObjectFromID::objectFrom(type, i);

        item = new QListWidgetItem(this->list);
        item->setSizeHint(QSize(32,32));

        QPixmap itemPixmap(":/misc/inventory/slot");
        QPainter painter(&itemPixmap);
        painter.drawPixmap(0,0,30,30,QPixmap(object->getCurrentImagePath()));
        item->setIcon(QIcon(itemPixmap));
        item->setToolTip(QString("%1 - %2").arg(object->objectName(), object->getDescription()));
        item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
        this->list->insertItem(i, item);
        object->deleteLater();
    }

    this->list->verticalScrollBar()->setPageStep(this->list->verticalScrollBar()->singleStep()*
                                                 ((int) this->list->verticalScrollBar()->maximum()/4));

    return;
}

void ChooseToolDialog::snapSlider()
{
    int value = qRound(this->list->verticalScrollBar()->value()/ (double) this->list->verticalScrollBar()->singleStep());
    this->list->verticalScrollBar()->setValue(this->list->verticalScrollBar()->singleStep()*value);
    return;
}

void ChooseToolDialog::itemSelected(QListWidgetItem *item)
{
    this->chosenObjectID = this->list->row(item);
    return;
}
