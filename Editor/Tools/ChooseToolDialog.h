/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CHOOSE_TOOL_DIALOG_H_
#define _CHOOSE_TOOL_DIALOG_H_

#include <QDialog>
#include <QGridLayout>
#include <QListWidget>
#include <QPushButton>

#include "Object/ObjectBase.h"

/** \addtogroup GUI
  * \{
  * \class ChooseToolDialog
  *
  * \brief Choose one of the given tools.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class ChooseToolDialog : public QDialog
{
    Q_OBJECT

private:

    QGridLayout *           gridLayout;
    ObjectBase::ObjectType  type;

    QListWidget *           list;
    QPushButton *           ok;
    QPushButton *           cancel;

    int                     chosenObjectID;

public:
    ChooseToolDialog(ObjectBase::ObjectType type, QWidget *parent = 0);

    int getChosenObjectID() const;

private:
    void createSelection(ObjectBase::ObjectType type);

private slots:
    void snapSlider();
    void itemSelected(QListWidgetItem *item);
};

#endif //_CHOOSE_TOOL_DIALOG_H_
