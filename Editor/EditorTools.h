/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EDITOR_TOOLS_H
#define EDITOR_TOOLS_H

#include <QObject>
#include <QVector>
#include <QWidget>
#include <QGridLayout>
#include <QButtonGroup>
#include <QToolButton>

#include "Command/Processor.h"
#include "Editor/Model/EditorModel.h"
#include "Editor/EditorCommon.h"

/** \addtogroup GUI
  * \{
  * \class EditorTools
  *
  * \brief The tools to be used in the editor.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class EditorTools : public QWidget
{
    Q_OBJECT

private:
    Processor *             processor;
    EditorModel *           editorModel;

    QButtonGroup            toolButtons;        ///< All the possible objects to be chosen.
    QToolButton *           chosenToolButton;
    QVector<QToolButton *>  buttonVector;       ///< The list of buttons to click on to choose tools.

    // Layout
    QGridLayout *           gridLayout;         ///< Grid Layout for the buttons to choose a tool.

public:
    explicit EditorTools(Processor *processor, EditorModel *editorModel, QWidget *parent = 0);

private:
    /**
      * \brief Append a new Button showing a given icon.
      * \param iconPath The path to the icon to be used as button.
      */
    void addButton(const QString &iconPath);

    void updateChosenTool();

private slots:
    void chooseTool(int chosenToolButton);

};

#endif // EDITOR_TOOLS_H
