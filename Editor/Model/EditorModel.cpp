/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Editor/Model/EditorModel.h"

#include <QToolButton>
#include <QDebug>

#include "Object/ObjectFromID.h"

EditorModel::EditorModel(bool editor, QObject *parent) :
    QObject(parent)
{
    this->editor = editor;
    this->chosenObject = 0;

    if(this->editor)
    {
        this->setChosenObject(ObjectBase::SPECIAL, 0);
    }
}

bool EditorModel::isEditor() const
{
    return this->editor;
}

ObjectBase::ObjectType EditorModel::chosenObjectType() const
{
    return chosenObject->getType();
}

void EditorModel::setChosenObject(ObjectBase::ObjectType toolType, int objectID)
{
    if(this->chosenObject)
    {
        this->chosenObject->deleteLater();
    }
    this->chosenObject = ObjectFromID::objectFrom(toolType, objectID, this);
    return;
}

ObjectBase *EditorModel::copyChosenObject()
{
    return this->chosenObject->copyBase();
}
