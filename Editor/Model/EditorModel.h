/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EDITOR_MODEL_H
#define EDITOR_MODEL_H

#include <QObject>

#include "Object/ObjectBase.h"

/** \addtogroup Model
  * \{
  * \class EditorModel
  *
  * \brief Represents the editor state.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class EditorModel : public QObject
{
private:
    Q_OBJECT

    bool                    editor;         ///< Is true, if the programm ist used as a editor.
    ObjectBase *            chosenObject;   ///< Holds the chosen object to be placed in the editor.

public:
    explicit EditorModel(bool editor, QObject *parent = 0);

    // Get-Methods
    bool isEditor() const;
    ObjectBase::ObjectType chosenObjectType() const;

    // Set-Methods
    void setChosenObject(ObjectBase::ObjectType toolType, int objectID);

    // Ref-Methods
    ObjectBase *copyChosenObject();
};

#endif // EDITOR_MODEL_H
