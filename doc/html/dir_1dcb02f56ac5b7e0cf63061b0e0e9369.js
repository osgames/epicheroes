var dir_1dcb02f56ac5b7e0cf63061b0e0e9369 =
[
    [ "ClientModel.cpp", "_client_model_8cpp.html", null ],
    [ "ClientModel.h", "_client_model_8h.html", null ],
    [ "ClientSocket.cpp", "_client_socket_8cpp.html", null ],
    [ "ClientSocket.h", "_client_socket_8h.html", [
      [ "ClientSocket", "class_client_socket.html", "class_client_socket" ]
    ] ],
    [ "NetworkModel.cpp", "_network_model_8cpp.html", null ],
    [ "NetworkModel.h", "_network_model_8h.html", "_network_model_8h" ],
    [ "ServerModel.cpp", "_server_model_8cpp.html", null ],
    [ "ServerModel.h", "_server_model_8h.html", null ],
    [ "WaitingForDataDialog.cpp", "_waiting_for_data_dialog_8cpp.html", null ],
    [ "WaitingForDataDialog.h", "_waiting_for_data_dialog_8h.html", null ]
];