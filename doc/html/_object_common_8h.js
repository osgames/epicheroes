var _object_common_8h =
[
    [ "EquipmentSlotType", "_object_common_8h.html#ac937d53d5b7665b42d738cc464ac274a", [
      [ "MAIN_HAND", "_object_common_8h.html#ac937d53d5b7665b42d738cc464ac274aa299f3b23c7bf1e434c7a5361ddd592df", null ],
      [ "OFF_HAND", "_object_common_8h.html#ac937d53d5b7665b42d738cc464ac274aa0c5720eda1ca553a507f1b1b8f711934", null ],
      [ "HEAD", "_object_common_8h.html#ac937d53d5b7665b42d738cc464ac274aa0030dfe658b34e0ab1b893a86fb496d0", null ],
      [ "HEADBAND", "_object_common_8h.html#ac937d53d5b7665b42d738cc464ac274aae5a67ee8d327ba553cc27557d97fd6ee", null ],
      [ "EYES", "_object_common_8h.html#ac937d53d5b7665b42d738cc464ac274aac562376b630b5b66dafbf095cedf1d47", null ],
      [ "SHOULDERS", "_object_common_8h.html#ac937d53d5b7665b42d738cc464ac274aaf03c1fa3207f6a31780a3836e8d854bd", null ],
      [ "NECK", "_object_common_8h.html#ac937d53d5b7665b42d738cc464ac274aaf3e3c11a77f50ae2529b005f2bb73973", null ],
      [ "CHEST", "_object_common_8h.html#ac937d53d5b7665b42d738cc464ac274aaa0661e0530401b51b2dc8ed30385a849", null ],
      [ "BODY", "_object_common_8h.html#ac937d53d5b7665b42d738cc464ac274aa63a991e5118e324d3289bcf7601800af", null ],
      [ "ARMOR", "_object_common_8h.html#ac937d53d5b7665b42d738cc464ac274aadeca8e50235eb126fe025d153f37fd24", null ],
      [ "BELT", "_object_common_8h.html#ac937d53d5b7665b42d738cc464ac274aa7bc7e82c4a518723af5d43e769bab350", null ],
      [ "HANDS", "_object_common_8h.html#ac937d53d5b7665b42d738cc464ac274aac090d3a8341e06adb19b018d59c8c707", null ],
      [ "RING", "_object_common_8h.html#ac937d53d5b7665b42d738cc464ac274aa3697217a0509d031a8beff6b6a71ec45", null ],
      [ "FEET", "_object_common_8h.html#ac937d53d5b7665b42d738cc464ac274aa2ec60fa5725d9ab9170ff9c365d25cc9", null ],
      [ "EQUIPMENT_SLOT_TYPE_COUNT", "_object_common_8h.html#ac937d53d5b7665b42d738cc464ac274aad6582d1322a7ed852fbb65da28e789a9", null ],
      [ "NONE", "_object_common_8h.html#ac937d53d5b7665b42d738cc464ac274aa4874a336a560cf294bb4e43e3a78cce7", null ]
    ] ],
    [ "SizeIncrement", "_object_common_8h.html#a05cff40568e87f2a1abf15814e4f3300", [
      [ "FINE_SIZE", "_object_common_8h.html#a05cff40568e87f2a1abf15814e4f3300afa88e47a797fdcbdc4d5ac485d27e900", null ],
      [ "DIMINUTIVE_SIZE", "_object_common_8h.html#a05cff40568e87f2a1abf15814e4f3300ad9d6cff647c5013b00ce5804520f8897", null ],
      [ "TINY_SIZE", "_object_common_8h.html#a05cff40568e87f2a1abf15814e4f3300a95b69ab2cb1bb58827e3468cc4ac8e0d", null ],
      [ "SMALL_SIZE", "_object_common_8h.html#a05cff40568e87f2a1abf15814e4f3300aa63baefded97f70979bc39974fd5c28e", null ],
      [ "MEDIUM_SIZE", "_object_common_8h.html#a05cff40568e87f2a1abf15814e4f3300ac3ba73b7ab211d971f683bfed0a48c63", null ],
      [ "LARGE_SIZE", "_object_common_8h.html#a05cff40568e87f2a1abf15814e4f3300a9279af90b648c411748c7de738072bfe", null ],
      [ "HUGE_SIZE", "_object_common_8h.html#a05cff40568e87f2a1abf15814e4f3300a9a951d4a5ac8a700bf86d9bc1a42222c", null ],
      [ "GARGANTUAN_SIZE", "_object_common_8h.html#a05cff40568e87f2a1abf15814e4f3300a19aac0e06bdca2d88ac6eb187e1f637e", null ],
      [ "COLOSSAL_SIZE", "_object_common_8h.html#a05cff40568e87f2a1abf15814e4f3300a3d6a2ce48169ba6661b4f63cae3c6b1a", null ]
    ] ],
    [ "equipmentSlotTypeToString", "_object_common_8h.html#ac086d69356745fba78652e31f7e1e871", null ],
    [ "ITEM_MIME_DATA", "_object_common_8h.html#a19122f274251e7b2eb5a041b037a8154", null ]
];