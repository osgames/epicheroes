var class_dice_base =
[
    [ "DiceBase", "class_dice_base.html#af0d12ff8d5cf7733a4bd228f57a1288d", null ],
    [ "DiceBase", "class_dice_base.html#a282f43765b5a71f8b52c8e4f6312b8e4", null ],
    [ "getAmount", "class_dice_base.html#aa9bb2d2a09f56b4dc18217a0029c8532", null ],
    [ "getNaturalRoll", "class_dice_base.html#a6274be740b34214bb145b3aa5acfcc47", null ],
    [ "getSides", "class_dice_base.html#a8fec9e7d4aaf873a8c1f066d83ba4ad6", null ],
    [ "init", "class_dice_base.html#ac8fb705a6c6b6af4c73223740cb45b62", null ],
    [ "isRolled", "class_dice_base.html#aef08d6609b4f42b44d529069782be638", null ],
    [ "roll", "class_dice_base.html#a265802ded81bce53370c352e8291d638", null ],
    [ "rollOne", "class_dice_base.html#a7c36c7cffe92f5d9b93436646002c26a", null ],
    [ "setRoll", "class_dice_base.html#aab99ffcf336a39c02f38671e4e26cd0c", null ],
    [ "showDiceRoll", "class_dice_base.html#a1948feeaac482f1768d87a044a868865", null ],
    [ "amount", "class_dice_base.html#aae5c57309b4355ec3cc0dd582f1ed54b", null ],
    [ "naturalRoll", "class_dice_base.html#a381c2bbd3da572b80b3f6d134160b8d5", null ],
    [ "rolled", "class_dice_base.html#a5ec77804cf76be16060a411556437c5e", null ],
    [ "sides", "class_dice_base.html#a97eed1a9c146ec16a88eb674f09f3c8c", null ]
];