var class_dice_attribute =
[
    [ "AttributeRollType", "class_dice_attribute.html#a0ae551796670a19bf4c9b0151b215751", [
      [ "KEEP_HIGHEST_THREE", "class_dice_attribute.html#a0ae551796670a19bf4c9b0151b215751ae821e478da110a667dd835aac6e7000a", null ],
      [ "NORMAL", "class_dice_attribute.html#a0ae551796670a19bf4c9b0151b215751adb7c990be5b447516b9770368769a733", null ],
      [ "KEEP_LOWEST_THREE", "class_dice_attribute.html#a0ae551796670a19bf4c9b0151b215751aea3d9a7fddac9654fb5135b5f5ee3cbe", null ]
    ] ],
    [ "DiceAttribute", "class_dice_attribute.html#a0db58a822189fa41bcc0ad7f73bd6c7d", null ],
    [ "roll", "class_dice_attribute.html#a3a7dc8ba3f0d66a70ea133c992f1fb23", null ],
    [ "die1", "class_dice_attribute.html#a1c049bf18cfc543eac91a522b1389092", null ],
    [ "die2", "class_dice_attribute.html#aef4d4b0dff0251069ce1044e999eea33", null ],
    [ "die3", "class_dice_attribute.html#a7847623e0a5a2bcf146653d27465ec1e", null ],
    [ "die4", "class_dice_attribute.html#a91e997a5a5bd1656dec777b6e160bafe", null ],
    [ "result", "class_dice_attribute.html#a789dc1345c049429258508f7cdadf1f2", null ],
    [ "type", "class_dice_attribute.html#a17690e1f366c3bede31fe36b78c3640a", null ]
];