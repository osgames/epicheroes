var class_dagger =
[
    [ "Dagger", "class_dagger.html#a7a29e73f4e0986b7f508e219ce3f0a14", null ],
    [ "Dagger", "class_dagger.html#ab6730027f3fc2de89ffee75178d4747e", null ],
    [ "baseDamageDice", "class_dagger.html#a32bf832d592971c713c35c2a808191ef", null ],
    [ "copy", "class_dagger.html#a79f57d4ab8477bad794736389e388ee3", null ],
    [ "deserialize", "class_dagger.html#ab2c3e71aac187f9a614cf8aebf027caa", null ],
    [ "deserialize", "class_dagger.html#a39f6d81f56138040debc022a7f2a9515", null ],
    [ "getCommandTreeList", "class_dagger.html#a62f024283ae95da78f407dc7f8ae8df3", null ],
    [ "getCritRange", "class_dagger.html#aa197c51d33d1a17d3804ab7dae7ac81f", null ],
    [ "getObjectID", "class_dagger.html#aa8cbf348166c1786afce3dfc02b5148a", null ],
    [ "getSilhouette", "class_dagger.html#a2acf4ec5b706e9277ba7f7e622e98f19", null ],
    [ "getSlotOffset", "class_dagger.html#ac338299c3d07afae9727d579350a87ef", null ],
    [ "initDagger", "class_dagger.html#a1f79399e94e12eec6ba4e0a7321c208c", null ],
    [ "refAttackModifier", "class_dagger.html#a60f8f95c01259c2e70ca180431699bdc", null ],
    [ "serialize", "class_dagger.html#a8d7628bef5ad3cdc50f8b2a74a9d59b8", null ],
    [ "serialize", "class_dagger.html#a12c6380247601c403d377b89d2f3f792", null ],
    [ "update", "class_dagger.html#a4e5529bd84df4c002e1f5b874dfebb15", null ]
];