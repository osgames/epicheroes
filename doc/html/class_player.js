var class_player =
[
    [ "Player", "class_player.html#aa4c301b636f568f38d7f599f9192b266", null ],
    [ "Player", "class_player.html#a3e443ef3b46fced68aa9145d45f87eeb", null ],
    [ "copy", "class_player.html#a291226f65ca793df40cb9b967e3ce2b6", null ],
    [ "deserialize", "class_player.html#a64e0e9adfef7f14de28cf9b2810ba6ab", null ],
    [ "deserialize", "class_player.html#a81e6390dcd72783ddcb330f9ed91710c", null ],
    [ "endTurn", "class_player.html#a8a6677e3a5b23a764ee224336fc61dba", null ],
    [ "getCommandTreeList", "class_player.html#a443ed824ce98a63cc20275ccc94a9297", null ],
    [ "getObjectID", "class_player.html#a09274fca824cd51158356897a1d08677", null ],
    [ "initPlayer", "class_player.html#a2273be994c99f1c86fa25147b6551e8b", null ],
    [ "serialize", "class_player.html#a3fe86b07d39db45e4c8f328729a8a251", null ],
    [ "serialize", "class_player.html#a2634874c3d7f7cf6f61c2b9611ed3d12", null ],
    [ "update", "class_player.html#ac560fcf55610e35e34a012b57b129100", null ]
];