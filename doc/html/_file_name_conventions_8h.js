var _file_name_conventions_8h =
[
    [ "EMAIL_FOLDER_NAME", "_file_name_conventions_8h.html#af048897dc8eb2942ef1d747fe2c8565d", null ],
    [ "EMAIL_SUFFIX", "_file_name_conventions_8h.html#adbebb1eb46a361e4bd4755c3d64d81cd", null ],
    [ "GAME_INFO_SUFFIX", "_file_name_conventions_8h.html#a34a28dfb76b2d0ff2a80188a3e3ec00d", null ],
    [ "MAP_SUFFIX", "_file_name_conventions_8h.html#acd429b9acd81fd10cb741a6142fccee6", null ],
    [ "RSA_FOLDER_NAME", "_file_name_conventions_8h.html#aa55765efccd4db00819eea1438ba12c8", null ],
    [ "RSA_PLAINTEXT_FILE_NAME", "_file_name_conventions_8h.html#a8233e8fb622ffda83399c54353a627ba", null ],
    [ "RSA_PRIVATE_KEY_FILE_NAME", "_file_name_conventions_8h.html#a9fbcb8f373e95afc8c331f5dc0ba2654", null ],
    [ "RSA_PUBLIC_KEY_FILE_NAME", "_file_name_conventions_8h.html#a85ac56552df5f259180d23e19ac6f3ae", null ],
    [ "SAVE_FOLDER_NAME", "_file_name_conventions_8h.html#a38d7830a58890cdad54286a42105331e", null ],
    [ "SAVE_SUFFIX", "_file_name_conventions_8h.html#aa0126dc34bd99587d4b8755036b49f4f", null ],
    [ "WORLD_FOLDER_NAME", "_file_name_conventions_8h.html#a2bf3f75cbd18cb2ebf33506660ce074d", null ],
    [ "WORLD_SUFFIX", "_file_name_conventions_8h.html#a5b6c260cf90eca350edcfe44aaa2939b", null ]
];