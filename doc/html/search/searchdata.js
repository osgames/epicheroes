var indexSectionsWithContent =
{
  0: "abcdefghiklmnopqrstuvwxyz~",
  1: "abcdefghiklmnopqrstuwy",
  2: "ort",
  3: "abcdefghiklmnopqrstuwy",
  4: "abcdefghiklmnopqrstuvwyz~",
  5: "abcdefghilmnopqrstuvwxyz",
  6: "abcefgimopst",
  7: "abcdefghiklmnoprstwy",
  8: "acgmnors"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "groups"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Modules"
};

