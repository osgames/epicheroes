var class_editor_model =
[
    [ "EditorModel", "class_editor_model.html#af02445960d4b6b7e5826bc74e4bf9abc", null ],
    [ "chosenObjectType", "class_editor_model.html#a6b9692dabc738432b9b707080c9231c8", null ],
    [ "copyChosenObject", "class_editor_model.html#aa76e802e002c8b8e5c1e306fb246d898", null ],
    [ "isEditor", "class_editor_model.html#a74a7b3db4e99bbf96b84fc86eba98186", null ],
    [ "setChosenObject", "class_editor_model.html#a210eef4e351b8399ca60da9c4ef53653", null ],
    [ "chosenObject", "class_editor_model.html#a37b65b1aed7d12d274ff39c09b428dae", null ],
    [ "editor", "class_editor_model.html#a17c60b128a9fd18357a94e642d7bc352", null ]
];