var dir_ec97d5d99c0b7b1cb03884f02d4287fa =
[
    [ "Model", "dir_98e03d1e203cd4c0378981e5f6a81f43.html", "dir_98e03d1e203cd4c0378981e5f6a81f43" ],
    [ "Tools", "dir_f0606c35cf69449defeb9905012471f0.html", "dir_f0606c35cf69449defeb9905012471f0" ],
    [ "EditorCommon.h", "_editor_common_8h.html", "_editor_common_8h" ],
    [ "EditorMenu.cpp", "_editor_menu_8cpp.html", null ],
    [ "EditorMenu.h", "_editor_menu_8h.html", null ],
    [ "EditorTools.cpp", "_editor_tools_8cpp.html", null ],
    [ "EditorTools.h", "_editor_tools_8h.html", null ],
    [ "MapPropertiesDialog.cpp", "_map_properties_dialog_8cpp.html", null ],
    [ "MapPropertiesDialog.h", "_map_properties_dialog_8h.html", [
      [ "MapPropertiesDialog", "class_map_properties_dialog.html", "class_map_properties_dialog" ]
    ] ],
    [ "WorldFileTree.cpp", "_world_file_tree_8cpp.html", null ],
    [ "WorldFileTree.h", "_world_file_tree_8h.html", null ],
    [ "WorldPropertiesDialog.cpp", "_world_properties_dialog_8cpp.html", null ],
    [ "WorldPropertiesDialog.h", "_world_properties_dialog_8h.html", null ],
    [ "WorldView.cpp", "_world_view_8cpp.html", null ],
    [ "WorldView.h", "_world_view_8h.html", null ]
];