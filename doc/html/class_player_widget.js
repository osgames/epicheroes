var class_player_widget =
[
    [ "PlayerWidget", "class_player_widget.html#a7a7c23b90281ee373f818463440e9cec", null ],
    [ "endTurn", "class_player_widget.html#a767c511cd2032de6a451d68943723370", null ],
    [ "openInventory", "class_player_widget.html#aa82abb622489e6b2749481503492fd1e", null ],
    [ "showItemToolTip", "class_player_widget.html#a06e4bce40dd4400f731beef0ced35069", null ],
    [ "updatePlayerWidget", "class_player_widget.html#ad16d42532dc53748ee9da8dfd902003e", null ],
    [ "attributeLabel", "class_player_widget.html#a6ae78f74fb9a6ce1d6c83815771a1bb2", null ],
    [ "character", "class_player_widget.html#a2e8bdc24a3595266fc28f373c528f9d0", null ],
    [ "characterSheetButton", "class_player_widget.html#ac94c3628bb41ebfd2ca09c39a68c7bce", null ],
    [ "endTurnButton", "class_player_widget.html#ad6aae9c67000dc58e20d394eb097a417", null ],
    [ "inventoryButton", "class_player_widget.html#a27089bbde8455e8aad301f171ab914ac", null ],
    [ "playerLayout", "class_player_widget.html#a9aa140b54e66dec88ce1d5b04af3b1d7", null ],
    [ "playerOverviewList", "class_player_widget.html#a227d775561984d60a0201edf94f63371", null ],
    [ "playerPortrait", "class_player_widget.html#a6371c5c3ae562921a1c5d9d6ae332493", null ],
    [ "processor", "class_player_widget.html#a9a5c2b4eab37f10911b8c856fec19427", null ]
];