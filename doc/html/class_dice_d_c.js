var class_dice_d_c =
[
    [ "DiceDC", "class_dice_d_c.html#a4e18f7cebc8ffda33bb91fe41cf58bce", null ],
    [ "isSuccessful", "class_dice_d_c.html#ade9f8f0262eb66adeb670acaef8e69c2", null ],
    [ "setDC", "class_dice_d_c.html#a60f8a112f2b5112237f63a030d72009b", null ],
    [ "showDiceRoll", "class_dice_d_c.html#ae9480f04ee10db0fead5ca765b3b6a76", null ],
    [ "DC", "class_dice_d_c.html#ab1859fa32a4c201e15f34cdf8075cdbd", null ],
    [ "DCName", "class_dice_d_c.html#a1b412b3a5e461ef9ba2f745200bc9bfe", null ],
    [ "failure", "class_dice_d_c.html#a14940a9c39e59188173c8fed83149668", null ],
    [ "success", "class_dice_d_c.html#a8019ea1485f85f4700171b27ab4e453e", null ]
];