var class_race =
[
    [ "Race", "class_race.html#a382c215c331c349903e9c044e8f037f2", null ],
    [ "Race", "class_race.html#a15c07546da99688966da2e94ebe00f60", null ],
    [ "copy", "class_race.html#a9cb63191723b14725e352c31ecc874d2", null ],
    [ "getBaseSpeed", "class_race.html#a5bc99de9a218a502a2e8685f2725ad93", null ],
    [ "getEquipmentSlots", "class_race.html#a6369ec5a019d1189b5f58d8ee0333290", null ],
    [ "getRaceID", "class_race.html#a493d7888188135b2f3dfb4422976b03e", null ],
    [ "update", "class_race.html#a3dc800208b0c319ecc54ea8925b894eb", null ]
];