var class_cut_item =
[
    [ "CutItem", "class_cut_item.html#a5705161aac96eeec5e5697e41837da15", null ],
    [ "execute", "class_cut_item.html#a587f0e0baa1d6157a6c41383d54ab4d0", null ],
    [ "redo", "class_cut_item.html#a327898e06b40feb4aa8f1567541ae4e3", null ],
    [ "undo", "class_cut_item.html#a979d6f4ea62cbfed9e2ed9ba6144652f", null ],
    [ "chosenItem", "class_cut_item.html#a87567b9f4755c41308cdefc5bddd74b5", null ],
    [ "oldChosenItem", "class_cut_item.html#ae8336064f7e1d948a76fa091911d1aa6", null ],
    [ "oldItems", "class_cut_item.html#a0f381c65407facf997b733cc0e585e65", null ],
    [ "stackModel", "class_cut_item.html#a148b15fb1a7cfa41f5a654d42bbcbdc1", null ]
];