var class_modifier =
[
    [ "Modifier", "class_modifier.html#ad3a6d038793b6be35d5f8a8725c65d1d", null ],
    [ "Modifier", "class_modifier.html#a8837bc679669905e623618f3da556a5b", null ],
    [ "applyMod", "class_modifier.html#a224a60b8267cb8afa29bfc4d265557dd", null ],
    [ "getMod", "class_modifier.html#a6e6d6b512c26660d75ecccbeed447858", null ],
    [ "getModifierName", "class_modifier.html#a54de82e65726af8738b5c2e914ea8170", null ],
    [ "getModifierOutput", "class_modifier.html#ac872525778fd3d4adeea2039c8f54b56", null ],
    [ "getSign", "class_modifier.html#a5a8393c023ece3df40eaa5dec089b814", null ],
    [ "init", "class_modifier.html#a93e09b1ec5b43ca2076e97c6bd6741a4", null ],
    [ "isBonus", "class_modifier.html#ac30e7072823b9b473a42a2f60ef4cebf", null ],
    [ "operator=", "class_modifier.html#a0d21baaee07969cadefc37a69573ec97", null ],
    [ "showMod", "class_modifier.html#a7e53ba0e7e0ac7e0bb9baeced3bf7dc7", null ],
    [ "showSign", "class_modifier.html#a9a0dda578665ed5e0dd9ab703d158fcd", null ],
    [ "bonus", "class_modifier.html#a4d07d882864cf260eff999fd0b9363db", null ],
    [ "modifierName", "class_modifier.html#a957b580d3a77bce85f52bd3dc85a2ae0", null ]
];