var dir_0ef713cb88e2d43193908ecb94ba837e =
[
    [ "Broadcast", "dir_b577636fc43c8b113c55237168a9e53a.html", "dir_b577636fc43c8b113c55237168a9e53a" ],
    [ "ClientDisconnected.cpp", "_client_disconnected_8cpp.html", null ],
    [ "ClientDisconnected.h", "_client_disconnected_8h.html", null ],
    [ "ReceiveChangePlayerID.cpp", "_receive_change_player_i_d_8cpp.html", null ],
    [ "ReceiveChangePlayerID.h", "_receive_change_player_i_d_8h.html", null ],
    [ "ReplyToRequestForMap.cpp", "_reply_to_request_for_map_8cpp.html", null ],
    [ "ReplyToRequestForMap.h", "_reply_to_request_for_map_8h.html", null ],
    [ "ReplyToRequestForPlayer.cpp", "_reply_to_request_for_player_8cpp.html", null ],
    [ "ReplyToRequestForPlayer.h", "_reply_to_request_for_player_8h.html", null ],
    [ "ReplyToRequestForPlayerModels.cpp", "_reply_to_request_for_player_models_8cpp.html", null ],
    [ "ReplyToRequestForPlayerModels.h", "_reply_to_request_for_player_models_8h.html", null ],
    [ "SendInitialPlayerInformation.cpp", "_send_initial_player_information_8cpp.html", null ],
    [ "SendInitialPlayerInformation.h", "_send_initial_player_information_8h.html", null ],
    [ "SendToClientNextTurn.cpp", "_send_to_client_next_turn_8cpp.html", null ],
    [ "SendToClientNextTurn.h", "_send_to_client_next_turn_8h.html", null ],
    [ "ServerCommand.cpp", "_server_command_8cpp.html", null ],
    [ "ServerCommand.h", "_server_command_8h.html", null ]
];