var NAVTREE =
[
  [ "The Epic of Heroes", "index.html", [
    [ "Modules", "modules.html", "modules" ],
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Classes", null, [
      [ "Class List", "annotated.html", "annotated" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_a_star_map_8cpp.html",
"_drop_item_8cpp.html",
"_object_common_8h.html#a19122f274251e7b2eb5a041b037a8154",
"_send_initial_player_information_8h_source.html",
"class_attribute_roller_widget.html#a0d724b1fc5b720f8ea05ce319c7ddfa8",
"class_command_node.html#a0f803bd5267daa93c73688a494f52494",
"class_dice_d_c.html",
"class_game_model.html#ac2ccfac20a9e274cb6960cc3e8a196aa",
"class_mail_game_info.html#acf527f09cbe9bd51f47192904bc59e0c",
"class_mouse_editor_input.html#a293918cac487de34334f24e035e19713",
"class_object_character.html#a2f3e1dd13df2297a1018e854330b4ab0",
"class_place_item.html#a9287518d1c7bc15711437768990ad734",
"class_remove_item_inventory.html",
"class_server_model.html#ad7f7095acfc3c3988b0a0114c583112a",
"class_undo_redo.html#ab307d917b6b72a704569078d518f55c5",
"group___a_star.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';