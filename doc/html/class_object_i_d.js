var class_object_i_d =
[
    [ "CharacterID", "class_object_i_d.html#a16c21caa6a3e9be2bf5dc5f13eacd261", [
      [ "PLAYER", "class_object_i_d.html#a16c21caa6a3e9be2bf5dc5f13eacd261a2d308f4b7c70fc5cc790c17c2f56976a", null ],
      [ "GOBLIN", "class_object_i_d.html#a16c21caa6a3e9be2bf5dc5f13eacd261a88f808e0f7006bc834c92f4a540587da", null ],
      [ "CHARACTER_COUNT", "class_object_i_d.html#a16c21caa6a3e9be2bf5dc5f13eacd261ad4281a6bdc9ac00df7bb283527392548", null ]
    ] ],
    [ "ItemID", "class_object_i_d.html#a0af71670c986be9e5ad3b39bf50f3c85", [
      [ "DAGGER", "class_object_i_d.html#a0af71670c986be9e5ad3b39bf50f3c85aa56aaa73d5e60da50014129d5d25cfa5", null ],
      [ "CORPSE", "class_object_i_d.html#a0af71670c986be9e5ad3b39bf50f3c85a5243a5dca96380e87a58d0dc62b41965", null ],
      [ "ITEM_COUNT", "class_object_i_d.html#a0af71670c986be9e5ad3b39bf50f3c85acfd96de76bde51f3190e802034555ea2", null ]
    ] ],
    [ "SpecialID", "class_object_i_d.html#af89c829d91487996356925b270ed7c79", [
      [ "START_POSITION", "class_object_i_d.html#af89c829d91487996356925b270ed7c79ad91606b890d30afe0f861c1b84d86fc6", null ],
      [ "SPECIAL_COUNT", "class_object_i_d.html#af89c829d91487996356925b270ed7c79a1f3d58261222b1961bc03fd420e547fd", null ]
    ] ],
    [ "TileID", "class_object_i_d.html#a8c375129c96bdd8b8160849b1f7dbb67", [
      [ "BRICK_WALL", "class_object_i_d.html#a8c375129c96bdd8b8160849b1f7dbb67a1f9c094a6551219dbc14f04f07638cd1", null ],
      [ "MUD_FLOOR", "class_object_i_d.html#a8c375129c96bdd8b8160849b1f7dbb67ad8cf77bf36f6b7cccf5eb7179636e9e7", null ],
      [ "TILE_COUNT", "class_object_i_d.html#a8c375129c96bdd8b8160849b1f7dbb67a435a0f7b991329423b10b8592592624f", null ]
    ] ],
    [ "ObjectID", "class_object_i_d.html#ae9f034430dc135b08cb1ebfdd3d37bbc", null ],
    [ "deserialize", "class_object_i_d.html#a5339ec683828be7e8423caffd0dcd152", null ],
    [ "deserialize", "class_object_i_d.html#a37b8465bc5db084a95ffaeac9fe91569", null ],
    [ "getNewSpawnIDFor", "class_object_i_d.html#a57bccf27a2d6819ab15a6e50e7d9059b", null ],
    [ "getNewSpawnIDFor", "class_object_i_d.html#a19f8791214b115571dd143d6cedc66dd", null ],
    [ "getNewSpawnIDFor", "class_object_i_d.html#a340a6141e6cc2bcb706dbe8798cdf52c", null ],
    [ "getNewSpawnIDFor", "class_object_i_d.html#a110537a514b2fbc32bc93de303b48e0a", null ],
    [ "getNewSpawnIDFromMap", "class_object_i_d.html#aec4e34faf83d607381e8aee5c41379f3", null ],
    [ "serialize", "class_object_i_d.html#a0ca732dc9faac6defe70c6598d34922c", null ],
    [ "serialize", "class_object_i_d.html#a5f9646fe83c2c0b3651fbf1b93b994a1", null ],
    [ "characterSpawnIDMap", "class_object_i_d.html#a974c50371a92e0a07af46f850db27318", null ],
    [ "itemSpawnIDMap", "class_object_i_d.html#aa19a3a1bf32c437baf12609cfeba17b4", null ],
    [ "specialSpawnIDMap", "class_object_i_d.html#abf50fc367d10a7b24171e3e6d56667e5", null ],
    [ "tileSpawnIDMap", "class_object_i_d.html#a1ea7ad41783c6c9b046e5c7c66b1aa04", null ]
];