var hierarchy =
[
    [ "Abilities", "class_abilities.html", null ],
    [ "AplyWorldPropertiesChanges", "class_aply_world_properties_changes.html", null ],
    [ "AStarMap", "class_a_star_map.html", null ],
    [ "AStarMapEntry", "class_a_star_map_entry.html", null ],
    [ "BroadcastManage", "class_broadcast_manage.html", null ],
    [ "MapModel::ConvertedPath", "class_map_model_1_1_converted_path.html", null ],
    [ "MapModel::ConvertedPath::ConvertedPathEntry", "class_map_model_1_1_converted_path_1_1_converted_path_entry.html", null ],
    [ "DiceAttack", "class_dice_attack.html", null ],
    [ "DiceAttribute", "class_dice_attribute.html", null ],
    [ "DiceBase", "class_dice_base.html", [
      [ "Dice", "class_dice.html", [
        [ "DiceDC", "class_dice_d_c.html", null ]
      ] ],
      [ "DiceMod", "class_dice_mod.html", null ]
    ] ],
    [ "DropPlayerItem", "class_drop_player_item.html", null ],
    [ "MapInfos", "class_map_infos.html", null ],
    [ "MouseState", "class_mouse_state.html", [
      [ "ContextEditorMenuState", "class_context_editor_menu_state.html", null ],
      [ "ContextGameMenuState", "class_context_game_menu_state.html", null ],
      [ "IdleEditorMouseState", "class_idle_editor_mouse_state.html", null ],
      [ "IdleGameMouseState", "class_idle_game_mouse_state.html", null ],
      [ "PlaceObjectsState", "class_place_objects_state.html", null ],
      [ "RemoveObjectsState", "class_remove_objects_state.html", null ],
      [ "TargetChosenState", "class_target_chosen_state.html", null ],
      [ "TargetConfirmedState", "class_target_confirmed_state.html", null ],
      [ "TargetMoveState", "class_target_move_state.html", null ]
    ] ],
    [ "ObjectID", "class_object_i_d.html", null ],
    [ "PlaceObject", "class_place_object.html", null ],
    [ "QAbstractItemModel", null, [
      [ "MessageTreeModel", "class_message_tree_model.html", null ]
    ] ],
    [ "QAction", null, [
      [ "CommandAction", "class_command_action.html", null ]
    ] ],
    [ "QDialog", null, [
      [ "ChooseObjectDialog", "class_choose_object_dialog.html", null ],
      [ "ChooseToolDialog", "class_choose_tool_dialog.html", null ],
      [ "MapPropertiesDialog", "class_map_properties_dialog.html", null ],
      [ "PlayerInventoryDialog", "class_player_inventory_dialog.html", null ],
      [ "SearchDialog", "class_search_dialog.html", null ],
      [ "WorldPropertiesDialog", "class_world_properties_dialog.html", null ]
    ] ],
    [ "QFileSystemModel", null, [
      [ "WorldModel", "class_world_model.html", null ]
    ] ],
    [ "QGridLayout", null, [
      [ "OptionsLayout", "class_options_layout.html", null ]
    ] ],
    [ "QImage", null, [
      [ "ObjectImage", "class_object_image.html", null ]
    ] ],
    [ "QListWidget", null, [
      [ "BackpackSlots", "class_backpack_slots.html", null ],
      [ "EquipmentSlots", "class_equipment_slots.html", null ],
      [ "List", "class_list.html", null ],
      [ "OverviewListWidget", "class_overview_list_widget.html", null ]
    ] ],
    [ "QMainWindow", null, [
      [ "MainWindow", "class_main_window.html", null ]
    ] ],
    [ "QMenu", null, [
      [ "ObjectMenu", "class_object_menu.html", null ]
    ] ],
    [ "QMenuBar", null, [
      [ "EditorMenu", "class_editor_menu.html", null ]
    ] ],
    [ "QMessageBox", null, [
      [ "YesNoCancelDialog", "class_yes_no_cancel_dialog.html", null ]
    ] ],
    [ "QObject", null, [
      [ "BaseCommand", "class_base_command.html", [
        [ "Command", "class_command.html", [
          [ "NotUndoable", "class_not_undoable.html", [
            [ "AddToChosenList", "class_add_to_chosen_list.html", null ],
            [ "ApplyMapGeometryChanges", "class_apply_map_geometry_changes.html", null ],
            [ "ApplyWorldPropertiesChanges", "class_apply_world_properties_changes.html", null ],
            [ "AskForSave", "class_ask_for_save.html", null ],
            [ "AskForSaveMap", "class_ask_for_save_map.html", null ],
            [ "AskForSaveWorld", "class_ask_for_save_world.html", null ],
            [ "ChangeZoomFactor", "class_change_zoom_factor.html", null ],
            [ "ChooseMainTool", "class_choose_main_tool.html", null ],
            [ "CommonCommand", "class_common_command.html", [
              [ "CopyFolder", "class_copy_folder.html", null ],
              [ "Quit", "class_quit.html", null ],
              [ "RemoveFolder", "class_remove_folder.html", null ]
            ] ],
            [ "CopyCharacter", "class_copy_character.html", null ],
            [ "CopyItem", "class_copy_item.html", null ],
            [ "CopyItemInventory", "class_copy_item_inventory.html", null ],
            [ "CopySpecial", "class_copy_special.html", null ],
            [ "CopyTile", "class_copy_tile.html", null ],
            [ "CutCharacter", "class_cut_character.html", null ],
            [ "CutItemInventory", "class_cut_item_inventory.html", null ],
            [ "CutTile", "class_cut_tile.html", null ],
            [ "DuplicateWorldEntry", "class_duplicate_world_entry.html", null ],
            [ "GameCommand", "class_game_command.html", [
              [ "ClearPath", "class_clear_path.html", null ],
              [ "ConnectTo", "class_connect_to.html", null ],
              [ "CreateCharacter", "class_create_character.html", null ],
              [ "DealDamageTo", "class_deal_damage_to.html", null ],
              [ "EndTurn", "class_end_turn.html", null ],
              [ "FindPath", "class_find_path.html", null ],
              [ "FirstTurn", "class_first_turn.html", null ],
              [ "KillCharacter", "class_kill_character.html", null ],
              [ "LoadGame", "class_load_game.html", null ],
              [ "LoadStartMapFromIndex", "class_load_start_map_from_index.html", null ],
              [ "MoveCharacter", "class_move_character.html", null ],
              [ "NextTurn", "class_next_turn.html", null ],
              [ "PlaceItemToInventory", "class_place_item_to_inventory.html", null ],
              [ "PreloadAllMaps", "class_preload_all_maps.html", null ],
              [ "PreloadWorld", "class_preload_world.html", null ],
              [ "RemoveItemFromInventory", "class_remove_item_from_inventory.html", null ],
              [ "ResumeEMailGame", "class_resume_e_mail_game.html", null ],
              [ "SaveEMailGame", "class_save_e_mail_game.html", null ],
              [ "SaveGame", "class_save_game.html", null ],
              [ "SetupNewGame", "class_setup_new_game.html", null ],
              [ "ShowGameOptions", "class_show_game_options.html", null ],
              [ "SpawnPlayerOnCurrentMap", "class_spawn_player_on_current_map.html", null ],
              [ "YourTurnInformation", "class_your_turn_information.html", null ]
            ] ],
            [ "LoadMap", "class_load_map.html", null ],
            [ "LoadMapByIndex", "class_load_map_by_index.html", null ],
            [ "LoadWorld", "class_load_world.html", null ],
            [ "MoveFile", "class_move_file.html", null ],
            [ "NetworkCommand", "class_network_command.html", [
              [ "ClientCommand", "class_client_command.html", [
                [ "ChangePlayerIDOnServer", "class_change_player_i_d_on_server.html", null ],
                [ "ReceiveInitialPlayerInformation", "class_receive_initial_player_information.html", null ],
                [ "ReceiveMap", "class_receive_map.html", null ],
                [ "ReceivePlayer", "class_receive_player.html", null ],
                [ "ReceivePlayerModels", "class_receive_player_models.html", null ],
                [ "RequestMapFromServer", "class_request_map_from_server.html", null ],
                [ "RequestPlayerFromServer", "class_request_player_from_server.html", null ],
                [ "RequestPlayerModelsFromServer", "class_request_player_models_from_server.html", null ],
                [ "SendRemoveCharacterToServer", "class_send_remove_character_to_server.html", null ],
                [ "SendRemoveItemToServer", "class_send_remove_item_to_server.html", null ],
                [ "SendSpawnCharacterToServer", "class_send_spawn_character_to_server.html", null ],
                [ "SendSpawnItemToServer", "class_send_spawn_item_to_server.html", null ],
                [ "SendToServerEndTurn", "class_send_to_server_end_turn.html", null ],
                [ "SendToServerMessage", "class_send_to_server_message.html", null ],
                [ "SendUpdateCharacterToServer", "class_send_update_character_to_server.html", null ],
                [ "SendUpdateItemToServer", "class_send_update_item_to_server.html", null ]
              ] ],
              [ "ReceiveMessage", "class_receive_message.html", null ],
              [ "ReceiveRemoveCharacterFrom", "class_receive_remove_character_from.html", null ],
              [ "ReceiveRemoveItemFrom", "class_receive_remove_item_from.html", null ],
              [ "ReceiveSpawnCharacterFrom", "class_receive_spawn_character_from.html", null ],
              [ "ReceiveSpawnItemFrom", "class_receive_spawn_item_from.html", null ],
              [ "ReceiveUpdateCharacterFrom", "class_receive_update_character_from.html", null ],
              [ "ReceiveUpdateItemFrom", "class_receive_update_item_from.html", null ],
              [ "ServerCommand", "class_server_command.html", [
                [ "BroadcastCommand", "class_broadcast_command.html", [
                  [ "BroadcastMessage", "class_broadcast_message.html", null ],
                  [ "BroadcastRemoveCharacter", "class_broadcast_remove_character.html", null ],
                  [ "BroadcastRemoveItem", "class_broadcast_remove_item.html", null ],
                  [ "BroadcastSpawnCharacter", "class_broadcast_spawn_character.html", null ],
                  [ "BroadcastSpawnItem", "class_broadcast_spawn_item.html", null ],
                  [ "BroadcastUpdateCharacter", "class_broadcast_update_character.html", null ],
                  [ "BroadcastUpdateItem", "class_broadcast_update_item.html", null ]
                ] ],
                [ "ClientDisconnected", "class_client_disconnected.html", null ],
                [ "ReceiveChangePlayerID", "class_receive_change_player_i_d.html", null ],
                [ "ReplyToRequestForMap", "class_reply_to_request_for_map.html", null ],
                [ "ReplyToRequestForPlayer", "class_reply_to_request_for_player.html", null ],
                [ "ReplyToRequestForPlayerModels", "class_reply_to_request_for_player_models.html", null ],
                [ "SendInitialPlayerInformation", "class_send_initial_player_information.html", null ],
                [ "SendToClientNextTurn", "class_send_to_client_next_turn.html", null ]
              ] ]
            ] ],
            [ "NewFolder", "class_new_folder.html", null ],
            [ "NewMapFile", "class_new_map_file.html", null ],
            [ "NewWorld", "class_new_world.html", null ],
            [ "ObjectCommand", "class_object_command.html", [
              [ "AttackCharacter", "class_attack_character.html", null ],
              [ "DropAllEquipmentItems", "class_drop_all_equipment_items.html", null ],
              [ "DropItem", "class_drop_item.html", null ],
              [ "PickupItem", "class_pickup_item.html", null ],
              [ "SearchContainerItem", "class_search_container_item.html", null ],
              [ "ShowDescription", "class_show_description.html", null ],
              [ "ShowInventory", "class_show_inventory.html", null ],
              [ "TwoWeaponAttack", "class_two_weapon_attack.html", null ]
            ] ],
            [ "PasteObject", "class_paste_object.html", null ],
            [ "PlaceEditorObject", "class_place_editor_object.html", null ],
            [ "RemoveFromChosenList", "class_remove_from_chosen_list.html", null ],
            [ "RemoveWorldEntry", "class_remove_world_entry.html", null ],
            [ "RenameWorldEntry", "class_rename_world_entry.html", null ],
            [ "SaveMap", "class_save_map.html", null ],
            [ "SaveMapAs", "class_save_map_as.html", null ],
            [ "SaveWorld", "class_save_world.html", null ],
            [ "SaveWorldAs", "class_save_world_as.html", null ],
            [ "ShowAbout", "class_show_about.html", null ],
            [ "ShowInventoryContextMenuEditor", "class_show_inventory_context_menu_editor.html", null ],
            [ "ShowInventoryEditor", "class_show_inventory_editor.html", null ],
            [ "UseRedo", "class_use_redo.html", null ],
            [ "UseUndo", "class_use_undo.html", null ]
          ] ],
          [ "Undoable", "class_undoable.html", [
            [ "AddItemToCharacter", "class_add_item_to_character.html", null ],
            [ "CutItem", "class_cut_item.html", null ],
            [ "CutSpecial", "class_cut_special.html", null ],
            [ "PlaceCharacter", "class_place_character.html", null ],
            [ "PlaceFloor", "class_place_floor.html", null ],
            [ "PlaceItem", "class_place_item.html", null ],
            [ "PlaceItemInInventoryEditor", "class_place_item_in_inventory_editor.html", null ],
            [ "PlaceSpecial", "class_place_special.html", null ],
            [ "PlaceWall", "class_place_wall.html", null ],
            [ "RemoveAllObjects", "class_remove_all_objects.html", null ],
            [ "RemoveCharacter", "class_remove_character.html", null ],
            [ "RemoveItem", "class_remove_item.html", null ],
            [ "RemoveItemInventory", "class_remove_item_inventory.html", null ],
            [ "RemoveSpecial", "class_remove_special.html", null ],
            [ "RemoveTile", "class_remove_tile.html", null ]
          ] ]
        ] ]
      ] ],
      [ "ClientSocket", "class_client_socket.html", null ],
      [ "CommandTree", "class_command_tree.html", [
        [ "CommandLeaf", "class_command_leaf.html", null ],
        [ "CommandNode", "class_command_node.html", null ],
        [ "CommandSeperator", "class_command_seperator.html", null ]
      ] ],
      [ "Dice", "class_dice.html", null ],
      [ "EditorModel", "class_editor_model.html", null ],
      [ "EquipmentSlot", "class_equipment_slot.html", null ],
      [ "FileManager", "class_file_manager.html", null ],
      [ "GameModel", "class_game_model.html", [
        [ "ClientGameModel", "class_client_game_model.html", null ],
        [ "LocalGameModel", "class_local_game_model.html", [
          [ "EMailGameModel", "class_e_mail_game_model.html", null ],
          [ "HotseatGameModel", "class_hotseat_game_model.html", null ],
          [ "ServerGameModel", "class_server_game_model.html", null ]
        ] ]
      ] ],
      [ "InventoryModel", "class_inventory_model.html", null ],
      [ "ItemContainer", "class_item_container.html", null ],
      [ "MailGameInfo", "class_mail_game_info.html", null ],
      [ "MapModel", "class_map_model.html", null ],
      [ "MessageTreeItem", "class_message_tree_item.html", null ],
      [ "Modifier", "class_modifier.html", [
        [ "DiceMod", "class_dice_mod.html", null ],
        [ "NumberMod", "class_number_mod.html", null ]
      ] ],
      [ "MouseInput", "class_mouse_input.html", [
        [ "MouseEditorInput", "class_mouse_editor_input.html", null ],
        [ "MouseGameInput", "class_mouse_game_input.html", null ]
      ] ],
      [ "NetworkModel", "class_network_model.html", [
        [ "ClientModel", "class_client_model.html", null ],
        [ "ServerModel", "class_server_model.html", null ]
      ] ],
      [ "ObjectBase", "class_object_base.html", [
        [ "ObjectCharacter", "class_object_character.html", [
          [ "GoblinCharacter", "class_goblin_character.html", null ],
          [ "Player", "class_player.html", null ]
        ] ],
        [ "ObjectItem", "class_object_item.html", [
          [ "Dagger", "class_dagger.html", null ],
          [ "ObjectContainerItem", "class_object_container_item.html", [
            [ "Corpse", "class_corpse.html", null ]
          ] ]
        ] ],
        [ "ObjectSpecial", "class_object_special.html", [
          [ "StartPosition", "class_start_position.html", null ]
        ] ],
        [ "ObjectTile", "class_object_tile.html", [
          [ "BrickWall", "class_brick_wall.html", null ],
          [ "MudFloor", "class_mud_floor.html", null ]
        ] ]
      ] ],
      [ "PlayerModel", "class_player_model.html", null ],
      [ "Processor", "class_processor.html", null ],
      [ "Race", "class_race.html", [
        [ "Humanoid", "class_humanoid.html", [
          [ "Goblin", "class_goblin.html", null ],
          [ "Human", "class_human.html", null ]
        ] ]
      ] ],
      [ "SaveGameFile", "class_save_game_file.html", null ],
      [ "SelectedItemModel", "class_selected_item_model.html", null ],
      [ "StackModel", "class_stack_model.html", null ],
      [ "UndoRedo", "class_undo_redo.html", null ],
      [ "WaitingForDataDialog", "class_waiting_for_data_dialog.html", null ]
    ] ],
    [ "QTreeView", null, [
      [ "MessageWidget", "class_message_widget.html", null ],
      [ "WorldFileTree", "class_world_file_tree.html", null ]
    ] ],
    [ "QVector", null, [
      [ "LocalFilePaths", "class_local_file_paths.html", null ]
    ] ],
    [ "QWidget", null, [
      [ "AttributeRollerWidget", "class_attribute_roller_widget.html", null ],
      [ "EditorTools", "class_editor_tools.html", null ],
      [ "EMailSetupWidget", "class_e_mail_setup_widget.html", null ],
      [ "GeneralSetupWidget", "class_general_setup_widget.html", null ],
      [ "MainViewMap", "class_main_view_map.html", null ],
      [ "MainViewStack", "class_main_view_stack.html", null ],
      [ "NetworkSetupWidget", "class_network_setup_widget.html", null ],
      [ "PlayerWidget", "class_player_widget.html", null ],
      [ "SoloHotseatSetupWidget", "class_solo_hotseat_setup_widget.html", null ],
      [ "WorldView", "class_world_view.html", null ]
    ] ],
    [ "RSAMethod", "class_r_s_a_method.html", null ],
    [ "SearchContainer", "class_search_container.html", null ],
    [ "Serialize", "class_serialize.html", [
      [ "GameModel", "class_game_model.html", null ],
      [ "MailGameInfo", "class_mail_game_info.html", null ],
      [ "MapModel", "class_map_model.html", null ],
      [ "ObjectBase", "class_object_base.html", null ],
      [ "PlayerModel", "class_player_model.html", null ],
      [ "SaveGameFile", "class_save_game_file.html", null ],
      [ "WorldModel", "class_world_model.html", null ]
    ] ],
    [ "SpawnPlayer", "class_spawn_player.html", null ]
];