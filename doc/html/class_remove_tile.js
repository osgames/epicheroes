var class_remove_tile =
[
    [ "RemoveTile", "class_remove_tile.html#a7df39566684aaf03209a6cfc3be0511a", null ],
    [ "execute", "class_remove_tile.html#ad6da89b0bb33d036b82e1044428fb72b", null ],
    [ "redo", "class_remove_tile.html#af16c94ab44618387dc339173ee420da8", null ],
    [ "undo", "class_remove_tile.html#a2f86c607ba413229fd5dd47b453ea2ff", null ],
    [ "oldTile", "class_remove_tile.html#a2241b0bc9cb2c1c0c8afabc191054dca", null ],
    [ "stackModel", "class_remove_tile.html#a75c21eeda7b3b869f6b9d29f036b1e37", null ],
    [ "tileType", "class_remove_tile.html#ac32614ba5153a9b4d2642951a604ae6d", null ]
];