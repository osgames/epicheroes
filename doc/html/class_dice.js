var class_dice =
[
    [ "Dice", "class_dice.html#af2d1e1e59ec1b0ecd4558ee23e027931", null ],
    [ "Dice", "class_dice.html#a400bb8c3003e1ee991cb79fa290c7ac9", null ],
    [ "addModifier", "class_dice.html#abbf57f0fc756f0922704f25de3849b18", null ],
    [ "getModifierTotal", "class_dice.html#a93c794efe24375c143afa94f0f95e606", null ],
    [ "getRoll", "class_dice.html#a1fbe5fbd5ee25a28904a330ddaebb35a", null ],
    [ "getRollName", "class_dice.html#a891063d2d8d01d31f4a2364a76543124", null ],
    [ "roll", "class_dice.html#a612a16c434160895563d19099ca0b33b", null ],
    [ "setRollName", "class_dice.html#a41d2d27faa73827987b765ef8731895f", null ],
    [ "showDiceRoll", "class_dice.html#a32aa74e631f6cb438811b9aad2d53da8", null ],
    [ "showJSONRoll", "class_dice.html#ad5f15ad88955aaefdbcdc1371557d5bb", null ],
    [ "modifiers", "class_dice.html#a95bd9eecfc800a255c590f9902b758a2", null ],
    [ "rollName", "class_dice.html#aad90d490d267b2ceddaf66ea87d21dad", null ]
];