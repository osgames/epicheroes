var class_message_widget =
[
    [ "MessageWidget", "class_message_widget.html#a54d256a9576eeab0e32b04836953b9b2", null ],
    [ "addNewMessage", "class_message_widget.html#ab890b0a86e22e82efcd24529945c3b0a", null ],
    [ "adjustWidth", "class_message_widget.html#aac6128d131253eec9082368354d77ae2", null ],
    [ "clearMessages", "class_message_widget.html#afaf5f5ab4751204783c2b5e06eb608c3", null ],
    [ "gameModel", "class_message_widget.html#a6d4c0350c901a3d302c634c9a7272c17", null ],
    [ "horizontalSizeAdjust", "class_message_widget.html#a7565b40d0728de4b3afd26bcb00ba319", null ],
    [ "treeModel", "class_message_widget.html#a76cc2d14b7c64c839fc2569c440922ac", null ]
];