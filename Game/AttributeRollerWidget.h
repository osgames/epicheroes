/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ATTRIBUTEROLLERWIDGET_H
#define ATTRIBUTEROLLERWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QGridLayout>

/** \addtogroup GUI
  * \{
  * \class AttributeRollerWidget
  *
  * \brief The widget allowing you to roll and assign attributes.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class AttributeRollerWidget : public QWidget
{
    static const int ATTRIBUTE_ENTRY_SQUARE_SIZE = 20;

    Q_OBJECT
    QList<int> attributes;

    QGridLayout *attributeLayout;

    QPushButton *   rollButton;
    QList<QLabel *> assignedLabel;
    QList<QLabel *> modLabel;

public:
    AttributeRollerWidget(QWidget *parent = 0, Qt::WindowFlags f = 0);

    QList<int> getAssignedAttributes() const;

private:
    QPixmap attributeSlot(int value);

protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);
    void dropEvent(QDropEvent *event);
    void mousePressEvent(QMouseEvent *event);

public slots:
    void rollAttributes();

private slots:
    void updateRoller();
};

#endif // ATTRIBUTEROLLERWIDGET_H
