/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PlayerInventoryDialog.h"

#include "Object/Item/Inventory/SelectedItemModel.h"

PlayerInventoryDialog::PlayerInventoryDialog(Processor *processor, ObjectCharacter *character, QDialog *parent)
    : QDialog(parent)
{
    this->setWindowTitle(tr("Inventory"));
    SelectedItemModel *selectedItemModel = new SelectedItemModel(processor, character, this);

    this->gridLayout = new QGridLayout(this);
    this->setLayout(this->gridLayout);

    this->inventoryWidget = new QWidget(this);
    this->inventoryLayout = new QGridLayout(this->inventoryWidget);
    this->inventoryWidget->setLayout(this->inventoryLayout);

    this->backpackList = new BackpackSlots(processor, character, selectedItemModel, this->inventoryWidget);

    this->informationWidget = new QWidget(this->inventoryWidget);
    this->informationLayout = new QHBoxLayout(this->informationWidget);
    this->informationWidget->setLayout(this->informationLayout);

    this->weightLabel = new QLabel(tr("Weight: 0/150.00 lbs.   Space: 0/512"),this->informationWidget);
    this->currencyLabel = new QLabel(tr("Currency: 0 CP 0 SP 0 GP 0 PP"),this->informationWidget);

    this->equipmentList = new EquipmentSlots(processor, character, selectedItemModel, this->inventoryWidget);

    this->gridLayout->addWidget(this->inventoryWidget, 0,0);
    this->gridLayout->addWidget(this->equipmentList, 0,1);

    this->inventoryLayout->addWidget(this->backpackList, 0,0);
    this->inventoryLayout->addWidget(this->informationWidget, 1,0);

    this->informationLayout->addWidget(this->weightLabel,0, Qt::AlignLeft);
    this->informationLayout->addWidget(this->currencyLabel,0, Qt::AlignRight);
}
