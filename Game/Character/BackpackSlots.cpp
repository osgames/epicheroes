/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "BackpackSlots.h"

#include <QDebug>
#include <QDrag>
#include <QListWidgetItem>
#include <QMimeData>
#include <QIcon>
#include <QPixmap>
#include <QPainter>
#include <QDropEvent>
#include <QDragEnterEvent>
#include <QDragMoveEvent>
#include <QScrollBar>
#include <QMenu>
#include <QAction>
#include <QWidget>

#include "Object/ObjectCommon.h"

#include "Command/Backend/Editor/Inventory/ShowInventoryContextMenuEditor.h"

BackpackSlots::BackpackSlots(Processor* processor, ObjectCharacter *character, SelectedItemModel *selectedItemModel, QWidget *parent)
    : QListWidget(parent), processor(processor), character(character), selectedItemModel(selectedItemModel)
{
    this->setDragEnabled(true);
    this->setViewMode(QListView::IconMode);
    this->setResizeMode(QListView::Adjust);
    this->setMovement(QListView::Snap);
    this->setSelectionMode(QAbstractItemView::SingleSelection);
    this->setDragDropMode(QAbstractItemView::DragDrop);
    this->setIconSize(QSize(60,60));
    this->setSpacing(10);
    this->setAcceptDrops(true);
    this->setDropIndicatorShown(true);

    this->verticalScrollBar()->setSingleStep(72);
    this->updateBackpack();

    connect(this->verticalScrollBar(), SIGNAL(sliderReleased()), this, SLOT(snapSlider()));
    connect(this->character->refInventoryModel(), SIGNAL(backpackUpdated()), this, SLOT(updateBackpack()));

    this->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(contextMenu(const QPoint &)));

    this->setFixedSize(BackpackSlots::BACKPACK_SLOTS_WIDTH, BackpackSlots::BACKPACK_SLOTS_HEIGHT);
}

void BackpackSlots::startDrag(Qt::DropActions supportedActions)
{
    Q_UNUSED(supportedActions);

    int index = this->currentRow();

    QByteArray itemData;
    QMimeData *mimeData = new QMimeData();
    mimeData->setData(Object::ITEM_MIME_DATA, itemData);

    QDrag *drag = new QDrag(this);
    drag->setMimeData(mimeData);

    this->selectedItemModel->selectBackpackItem(index);

    QPixmap dragPixmap(60,60);
    int alpha;
    // NOTE: Change this back for every OS when it's fixed for Unix Systems.
#ifdef Q_OS_UNIX
    alpha = 255;
#else
    alpha = 0;
#endif
    dragPixmap.fill(QColor(60,60,60,alpha));
    QPainter painter(&dragPixmap);
    painter.drawPixmap(0,0,60,60,QPixmap::fromImage(this->selectedItemModel->refSelectedItem()->getImage()).scaledToHeight(60));

    drag->setHotSpot(QPoint(-5,-5));
    drag->setPixmap(dragPixmap);

    this->selectionModel()->clear();

    drag->exec(Qt::MoveAction);
    this->selectedItemModel->unselectBackpackItem();

    this->updateBackpack();
    return;
}

void BackpackSlots::dragEnterEvent(QDragEnterEvent *event)
{
    if(event->mimeData()->hasFormat(Object::ITEM_MIME_DATA))
    {
        if(event->pos().y() < 15)
        {
            this->verticalScrollBar()->setValue(this->verticalScrollBar()->value()-10);
        }
        else if(event->pos().y() > BackpackSlots::BACKPACK_SLOTS_HEIGHT-15)
        {
            this->verticalScrollBar()->setValue(this->verticalScrollBar()->value()+10);
        }
        else
        {
            this->snapSlider();
        }
        event->accept();
    }
    else
    {
        event->ignore();
    }
    return;
}

void BackpackSlots::dragMoveEvent(QDragMoveEvent *event)
{
    if(event->mimeData()->hasFormat(Object::ITEM_MIME_DATA))
    {
        if(event->pos().y() < 15)
        {
            this->verticalScrollBar()->setValue(this->verticalScrollBar()->value()-10);
        }
        else if(event->pos().y() > BackpackSlots::BACKPACK_SLOTS_HEIGHT-15)
        {
            this->verticalScrollBar()->setValue(this->verticalScrollBar()->value()+10);
        }
        else
        {
            this->snapSlider();
        }
        event->accept();
    }
    else
    {
        event->ignore();
    }
    return;
}

void BackpackSlots::dragLeaveEvent(QDragLeaveEvent *event)
{
    this->snapSlider();
    event->accept();
    return;
}

void BackpackSlots::dropEvent(QDropEvent *event)
{
    if(event->mimeData()->hasFormat(Object::ITEM_MIME_DATA))
    {
        int index = this->row(this->itemFromIndex(this->indexAt(event->pos())));

        if(index < 0)
        {
            this->selectedItemModel->changeSelectedItemIndex(this->character->refInventoryModel()->refBackpack()->getItems().size() - 1);
        }
        else
        {
            this->selectedItemModel->changeSelectedItemIndex(index);
        }

        this->selectedItemModel->unselectBackpackItem();
        this->updateBackpack();
        event->setDropAction(Qt::MoveAction);
        event->accept();
    }
    else
    {
        event->ignore();
    }
    return;
}

void BackpackSlots::mouseMoveEvent(QMouseEvent *event)
{
    if(!this->item(this->currentRow()))
    {
        event->ignore();
        return;
    }
    QListWidget::mouseMoveEvent(event);
    return;
}

void BackpackSlots::mouseReleaseEvent(QMouseEvent *)
{
    if(this->currentItem())
    {
        this->currentItem()->setSelected(false);
    }
    return;
}

void BackpackSlots::updateBackpack()
{
    while(this->count() > 0)
    {
        delete this->takeItem(0);
    }

    QList<ObjectItem *> backpack = this->character->refInventoryModel()->refBackpack()->getItems();
    QListWidgetItem *item;

    for(int i = 0; i < backpack.size(); ++i)
    {
        item = new QListWidgetItem(this);
        item->setSizeHint(QSize(62,62));

        QPixmap itemPixmap(":/misc/inventory/slot");
        QPainter painter(&itemPixmap);
        painter.drawPixmap(0,0,30,30,QPixmap::fromImage(backpack[i]->getImage()));
        item->setIcon(QIcon(itemPixmap.scaledToHeight(60)));
        item->setToolTip(backpack[i]->objectName());
        item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled);
        this->insertItem(i, item);
    }

    this->verticalScrollBar()->setPageStep(this->verticalScrollBar()->singleStep()*
                                           ((int) this->verticalScrollBar()->maximum()/4));
    return;
}

void BackpackSlots::snapSlider()
{
    int value = qRound(this->verticalScrollBar()->value()/ (double) this->verticalScrollBar()->singleStep());
    this->verticalScrollBar()->setValue(this->verticalScrollBar()->singleStep()*value);
    return;
}

void BackpackSlots::contextMenu(const QPoint &position)
{
    int index = this->currentRow();
    if(this->currentItem() && this->currentItem()->isSelected())
    {
        this->currentItem()->setSelected(false);
    }
    ObjectItem *item = this->character->refInventoryModel()->refBackpackItem(index);

    if(item && this->character)
    {
        this->processor->execute(new ShowInventoryContextMenuEditor(QWidget::mapToGlobal(position), item, InventoryModel::BACKPACK, this->character->refInventoryModel()));
    }

    return;
}
