/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLAYER_INVENTORY_DIALOG_H
#define PLAYER_INVENTORY_DIALOG_H

#include <QDialog>
#include <QGridLayout>
#include <QLabel>
#include <QHBoxLayout>

#include "Object/ObjectCharacter.h"
#include "Game/Character/BackpackSlots.h"
#include "Game/Character/EquipmentSlots.h"

/** \addtogroup GUI
  * \{
  * \class PlayerInventoryDialog
  *
  * \brief The Player Inventory Dialog.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class PlayerInventoryDialog : public QDialog
{
    Q_OBJECT

    QGridLayout *   gridLayout;

    QWidget *       inventoryWidget;
    QGridLayout *   inventoryLayout;

    BackpackSlots * backpackList;

    EquipmentSlots *equipmentList;

    QWidget *       informationWidget;
    QHBoxLayout *   informationLayout;
    QLabel *        weightLabel;
    QLabel *        currencyLabel;

public:
    PlayerInventoryDialog(Processor *processor, ObjectCharacter *character, QDialog *parent = 0);
};

#endif // PLAYER_INVENTORY_DIALOG_H
