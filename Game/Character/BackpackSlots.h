/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BACKPACK_SLOTS_H
#define BACKPACK_SLOTS_H

#include <QListWidget>

#include "Object/Item/Inventory/SelectedItemModel.h"

/** \addtogroup GUI
  * \{
  * \class BackpackSlots
  *
  * \brief The widget representing the list of backpack items.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class BackpackSlots : public QListWidget
{
    Q_OBJECT

public:
    static const int BACKPACK_SLOTS_WIDTH = 535;
    static const int BACKPACK_SLOTS_HEIGHT = 300;

private:
    Processor *         processor;
    ObjectCharacter *   character;
    SelectedItemModel * selectedItemModel;

public:
    BackpackSlots(Processor* processor, ObjectCharacter *character, SelectedItemModel *selectedItemModel, QWidget *parent = 0);

protected:
    void startDrag(Qt::DropActions supportedActions);
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);
    void dragLeaveEvent(QDragLeaveEvent *event);
    void dropEvent(QDropEvent *event);

    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *);

private slots:
    void updateBackpack();
    void snapSlider();
    void contextMenu(const QPoint &position);
};

#endif // BACKPACK_SLOTS_H
