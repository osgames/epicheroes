/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EQUIPMENT_SLOTS_H
#define EQUIPMENT_SLOTS_H

#include <QListWidget>

#include "Object/ObjectCharacter.h"
#include "Object/Item/Inventory/SelectedItemModel.h"

/** \addtogroup GUI
  * \{
  * \class EquipmentSlots
  *
  * \brief The widget representing the list of equiped items.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class EquipmentSlots : public QListWidget
{
    Q_OBJECT

public:
    static const int EQUIPMENT_SLOTS_WIDTH = 200;
    static const int EQUIPMENT_SLOTS_HEIGHT = 372;

private:
    Processor *         processor;
    ObjectCharacter *   character;
    SelectedItemModel * selectedItemModel;

public:
    EquipmentSlots(Processor* processor, ObjectCharacter *objectCharacter, SelectedItemModel *selectedItemModel, QWidget *parent = 0);

protected:
    void startDrag(Qt::DropActions supportedActions);
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);
    void dragLeaveEvent(QDragLeaveEvent *event);
    void dropEvent(QDropEvent *event);

    void mouseReleaseEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *event);

private slots:
    void updateEquipment();
    void snapSlider();
    void contextMenu(const QPoint &position);
};

#endif // EQUIPMENT_SLOTS_H
