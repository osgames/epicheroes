/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "EquipmentSlots.h"

#include <QDebug>
#include <QDrag>
#include <QListWidgetItem>
#include <QMimeData>
#include <QIcon>
#include <QPixmap>
#include <QPainter>
#include <QDragLeaveEvent>
#include <QScrollBar>
#include <QWheelEvent>

#include "Object/ObjectCommon.h"

#include "Command/Backend/Editor/Inventory/ShowInventoryContextMenuEditor.h"

EquipmentSlots::EquipmentSlots(Processor *processor, ObjectCharacter *character, SelectedItemModel *selectedItemModel, QWidget *parent)
    : QListWidget(parent), processor(processor), character(character), selectedItemModel(selectedItemModel)
{
    this->setDragEnabled(true);
    this->setViewMode(QListView::IconMode);
    this->setResizeMode(QListView::Adjust);
    this->setMovement(QListView::Snap);
    this->setDragDropMode(QAbstractItemView::DragDrop);
    this->setIconSize(QSize(60,60));
    this->setSpacing(10);
    this->setAcceptDrops(true);
    this->setDropIndicatorShown(true);

    this->verticalScrollBar()->setSingleStep(72);
    this->updateEquipment();

    connect(this->character->refInventoryModel(), SIGNAL(equippedItemsUpdated()), this, SLOT(updateEquipment()));
    connect(this->verticalScrollBar(), SIGNAL(sliderReleased()), this, SLOT(snapSlider()));

    this->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(contextMenu(const QPoint &)));

    this->setFixedSize(EquipmentSlots::EQUIPMENT_SLOTS_WIDTH,EquipmentSlots::EQUIPMENT_SLOTS_HEIGHT);
}

void EquipmentSlots::startDrag(Qt::DropActions supportedActions)
{
    Q_UNUSED(supportedActions);

    int index = this->currentRow()/2;

    QByteArray itemData;
    QMimeData *mimeData = new QMimeData();
    mimeData->setData(Object::ITEM_MIME_DATA, itemData);

    QDrag *drag = new QDrag(this);
    drag->setMimeData(mimeData);

    this->selectedItemModel->selectEquipmentItem(index);

    QPixmap dragPixmap(60,60);
    int alpha;
    // NOTE: Change this back for every OS when it's fixed for Unix Systems.
#ifdef Q_OS_UNIX
    alpha = 255;
#else
    alpha = 0;
#endif
    dragPixmap.fill(QColor(60,60,60,alpha));
    QPainter painter(&dragPixmap);
    painter.drawPixmap(0,0,60,60,QPixmap::fromImage(this->selectedItemModel->refSelectedItem()->getImage()).scaledToHeight(60));

    drag->setHotSpot(QPoint(-5,-5));
    drag->setPixmap(dragPixmap);

    this->selectionModel()->clear();
    drag->exec(Qt::MoveAction);

    this->selectedItemModel->unselectEquipmentItem();
    this->updateEquipment();

    return;
}

void EquipmentSlots::dragEnterEvent(QDragEnterEvent *event)
{
    if(event->mimeData()->hasFormat(Object::ITEM_MIME_DATA))
    {
        if(event->pos().y() < 15)
        {
            this->verticalScrollBar()->setValue(this->verticalScrollBar()->value()-10);
        }
        else if(event->pos().y() > EquipmentSlots::EQUIPMENT_SLOTS_HEIGHT-15)
        {
            this->verticalScrollBar()->setValue(this->verticalScrollBar()->value()+10);
        }
        else
        {
            this->snapSlider();
        }
        event->accept();
    }
    else
    {
        event->ignore();
    }
    return;
}

void EquipmentSlots::dragMoveEvent(QDragMoveEvent *event)
{
    if(event->mimeData()->hasFormat(Object::ITEM_MIME_DATA))
    {
        if(event->pos().y() < 15)
        {
            this->verticalScrollBar()->setValue(this->verticalScrollBar()->value()-10);
        }
        else if(event->pos().y() > EquipmentSlots::EQUIPMENT_SLOTS_HEIGHT-15)
        {
            this->verticalScrollBar()->setValue(this->verticalScrollBar()->value()+10);
        }
        else
        {
            this->snapSlider();
        }

        int index = this->row(this->itemFromIndex(this->indexAt(event->pos())));

        if(index == -1)
        {
            event->ignore();
            return;
        }

        if(this->selectedItemModel->isEquipable(index/2))
        {
            event->accept();
        }
        else
        {
            event->ignore();
        }
    }
    else
    {
        event->ignore();
    }
    return;
}

void EquipmentSlots::dragLeaveEvent(QDragLeaveEvent *event)
{
    this->snapSlider();
    event->accept();
    return;
}

void EquipmentSlots::dropEvent(QDropEvent *event)
{
    if(event->mimeData()->hasFormat(Object::ITEM_MIME_DATA))
    {
        int index = this->row(this->itemFromIndex(this->indexAt(event->pos())));

        if(index < 0)
        {
            event->ignore();
            return;
        }

        this->selectedItemModel->changeSelectedItemIndex(index/2);
        this->selectedItemModel->unselectEquipmentItem();
        this->updateEquipment();
        event->setDropAction(Qt::MoveAction);
        event->accept();
    }
    else
    {
        event->ignore();
    }
    return;
}

void EquipmentSlots::mouseReleaseEvent(QMouseEvent *)
{
    if(this->currentItem())
    {
        this->currentItem()->setSelected(false);
    }
    return;
}

void EquipmentSlots::mouseMoveEvent(QMouseEvent *event)
{
    if(!this->item(this->currentRow()))
    {
        event->ignore();
        return;
    }
    QListWidget::mouseMoveEvent(event);
    return;
}

void EquipmentSlots::updateEquipment()
{
    while(this->count() > 0)
    {
        delete this->takeItem(0);
    }

    QList<EquipmentSlot> equipment = this->character->refInventoryModel()->getEquipment();
    QListWidgetItem *widgetItem;

    for(int i = 0; i < equipment.size(); ++i)
    {
        widgetItem = new QListWidgetItem(this);
        widgetItem->setSizeHint(QSize(62,62));
        widgetItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsDropEnabled);

        QPixmap itemPixmap(":/misc/equipment/slot");
        ObjectItem *item = equipment[i].refItem();
        if(item)
        {
            QPainter painter(&itemPixmap);
            painter.drawPixmap(0,0,60,60,QPixmap::fromImage(item->getImage()).scaledToHeight(60));
            widgetItem->setToolTip(item->objectName());
            widgetItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled);
        }
        widgetItem->setIcon(QIcon(itemPixmap.scaledToHeight(60)));
        QListWidgetItem *type = new QListWidgetItem(Object::equipmentSlotTypeToString(equipment[i].getEquipmentSlotType()), this);
        type->setFlags(Qt::ItemIsEnabled);
        QPixmap typeFill = QPixmap(30,17);
        typeFill.fill(QColor(60,60,60,0));
        type->setIcon(QIcon(typeFill));
        type->setTextAlignment(Qt::AlignCenter);
        this->insertItem(i*2, widgetItem);
        this->insertItem(i*2+1, type);
    }

    this->verticalScrollBar()->setPageStep(this->verticalScrollBar()->singleStep()*
                                           ((int) this->verticalScrollBar()->maximum()/4));
    return;
}

void EquipmentSlots::snapSlider()
{
    int value = qRound(this->verticalScrollBar()->value()/ (double) this->verticalScrollBar()->singleStep());
    this->verticalScrollBar()->setValue(this->verticalScrollBar()->singleStep()*value);
    return;
}

void EquipmentSlots::contextMenu(const QPoint &position)
{
    if((this->currentRow() % 2) != 0)
    {
        return;
    }

    int index = this->currentRow()/2;
    if(this->currentItem() && this->currentItem()->isSelected())
    {
        this->currentItem()->setSelected(false);
    }
    ObjectItem *item = this->character->refInventoryModel()->refEquipmentItem(index);

    if(item && this->character)
    {
        this->processor->execute(new ShowInventoryContextMenuEditor(QWidget::mapToGlobal(position), item, InventoryModel::EQUIPMENT, this->character->refInventoryModel()));
    }

    return;
}


