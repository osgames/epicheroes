/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SearchDialog.h"

#include <QScrollBar>
#include <QListWidgetItem>
#include <QPainter>

SearchDialog::SearchDialog(Processor *processor, ItemContainer *itemContainer, ObjectCharacter *character, ObjectBase *containerBase, QWidget *parent)
    : QDialog(parent)
{
    this->processor = processor;
    this->itemContainer = itemContainer;
    this->character = character;
    this->containerBase = containerBase;

    this->setLayout(&this->layout);

    this->containerListWidget = new QListWidget(this);
    this->containerListWidget->setViewMode(QListView::IconMode);
    this->containerListWidget->setResizeMode(QListView::Adjust);
    this->containerListWidget->setMovement(QListView::Snap);
    this->containerListWidget->setSelectionMode(QAbstractItemView::MultiSelection);
    this->containerListWidget->setIconSize(QSize(60,60));
    this->containerListWidget->setSpacing(10);
    this->containerListWidget->verticalScrollBar()->setSingleStep(72);
    this->containerListWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    this->containerListWidget->setDragDropMode(QAbstractItemView::NoDragDrop);
    this->containerListWidget->setDragEnabled(false);

    this->characterListWidget = new QListWidget(this);
    this->characterListWidget->setViewMode(QListView::IconMode);
    this->characterListWidget->setResizeMode(QListView::Adjust);
    this->characterListWidget->setMovement(QListView::Snap);
    this->characterListWidget->setSelectionMode(QAbstractItemView::MultiSelection);
    this->characterListWidget->setIconSize(QSize(60,60));
    this->characterListWidget->setSpacing(10);
    this->characterListWidget->verticalScrollBar()->setSingleStep(72);
    this->characterListWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    this->characterListWidget->setDragDropMode(QAbstractItemView::NoDragDrop);
    this->characterListWidget->setDragEnabled(false);

    //connect(this->listWidget, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(contextMenu(const QPoint &)));

    this->takeItemsButton = new QPushButton(tr("Take >>"), this);
    connect(this->takeItemsButton, SIGNAL(clicked()),this, SLOT(takeItems()));
    this->takeAllItemsButton = new QPushButton(tr("Take All >>"));
    connect(this->takeAllItemsButton, SIGNAL(clicked()),this, SLOT(takeAllItems()));
    this->putItemsButton = new QPushButton(tr("<< Put"), this);
    connect(this->putItemsButton, SIGNAL(clicked()),this, SLOT(putItems()));

    this->doneButton = new QPushButton(tr("Done"), this);
    connect(this->doneButton, SIGNAL(clicked()),this, SLOT(accept()));
    this->cancelButton = new QPushButton(tr("Cancel"), this);
    connect(this->cancelButton, SIGNAL(clicked()),this, SLOT(reject()));

    layout.addWidget(this->containerListWidget,0,0,4,1);
    layout.addWidget(this->takeItemsButton,0,1,Qt::AlignBottom);
    layout.addWidget(this->takeAllItemsButton,1,1,Qt::AlignTop);
    layout.addWidget(this->putItemsButton,1,1,Qt::AlignCenter);
    layout.addWidget(this->doneButton,2,1,Qt::AlignBottom);
    layout.addWidget(this->cancelButton,3,1,Qt::AlignTop);
    layout.addWidget(this->characterListWidget,0,2,4,1);

    //this->setFixedSize(SearchDialog::SEARCH_SLOTS_WIDTH, SearchDialog::SEARCH_SLOTS_HEIGHT);

    this->updateSearch();
}

void SearchDialog::updateSearch()
{    
    this->updateContainerItems();
    this->updateCharacterItems();

    this->containerListWidget->setFixedSize(SearchDialog::SEARCH_SLOTS_WIDTH, SearchDialog::SEARCH_SLOTS_HEIGHT);
    this->characterListWidget->setFixedSize(SearchDialog::SEARCH_SLOTS_WIDTH, SearchDialog::SEARCH_SLOTS_HEIGHT);

    this->setFixedSize(this->layout.sizeHint());

    return;
}

void SearchDialog::updateContainerItems()
{
    while(this->containerListWidget->count() > 0)
    {
        delete this->containerListWidget->takeItem(0);
    }

    QList<ObjectItem *> items = this->itemContainer->getItems();
    QListWidgetItem *item;

    for(int i = 0; i < items.size(); ++i)
    {
        item = new QListWidgetItem(this->containerListWidget);
        item->setSizeHint(QSize(62,62));

        QPixmap itemPixmap(":/misc/inventory/slot");
        QPainter painter(&itemPixmap);
        painter.drawPixmap(0,0,30,30,QPixmap::fromImage(items[i]->getImage()));
        item->setIcon(QIcon(itemPixmap.scaledToHeight(60)));
        item->setToolTip(items[i]->objectName());
        item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled);
        this->containerListWidget->insertItem(i, item);

        if(this->character->equals(items[i]))
        {
            item->setHidden(true);
        }
    }

    this->containerListWidget->verticalScrollBar()->setPageStep(this->containerListWidget->verticalScrollBar()->singleStep()*
                                           ((int) this->containerListWidget->verticalScrollBar()->maximum()/4));

    return;
}

void SearchDialog::updateCharacterItems()
{
    while(this->characterListWidget->count() > 0)
    {
        delete this->characterListWidget->takeItem(0);
    }

    QList<ObjectItem *> items = this->character->refInventoryModel()->refBackpack()->getItems();
    QListWidgetItem *item;

    for(int i = 0; i < items.size(); ++i)
    {
        item = new QListWidgetItem(this->characterListWidget);
        item->setSizeHint(QSize(62,62));

        QPixmap itemPixmap(":/misc/inventory/slot");
        QPainter painter(&itemPixmap);
        painter.drawPixmap(0,0,30,30,QPixmap::fromImage(items[i]->getImage()));
        item->setIcon(QIcon(itemPixmap.scaledToHeight(60)));
        item->setToolTip(items[i]->objectName());
        item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled);
        this->characterListWidget->insertItem(i, item);

        if(this->containerBase->equals(items[i]))
        {
            item->setHidden(true);
        }
    }

    this->characterListWidget->verticalScrollBar()->setPageStep(this->characterListWidget->verticalScrollBar()->singleStep()*
                                           ((int) this->characterListWidget->verticalScrollBar()->maximum()/4));

    return;
}

void SearchDialog::takeItems()
{
    QList<QListWidgetItem *> list = this->containerListWidget->selectedItems();

    foreach(QListWidgetItem *listItem, list)
    {
        int index = this->containerListWidget->row(listItem);
        this->character->refInventoryModel()->addToBackpack(this->itemContainer->takeItem(index));
    }

    this->updateSearch();
    return;
}

void SearchDialog::takeAllItems()
{
    this->containerListWidget->selectAll();
    QList<QListWidgetItem *> list = this->containerListWidget->selectedItems();

    QList<int> indexes;

    foreach(QListWidgetItem *listItem, list)
    {
        if(listItem->isHidden())
        {
            continue;
        }

        int index = this->containerListWidget->row(listItem);
        this->character->refInventoryModel()->addToBackpack(this->itemContainer->getItem(index));
        indexes.append(index);
    }

    this->itemContainer->takeItems(indexes);

    this->updateSearch();
    return;
}

void SearchDialog::putItems()
{
    QList<QListWidgetItem *> list = this->characterListWidget->selectedItems();

    foreach(QListWidgetItem *listItem, list)
    {
        int index = this->characterListWidget->row(listItem);
        this->itemContainer->addItem(this->character->refInventoryModel()->removeFromBackpack(index));
    }

    this->updateSearch();
    return;
}
