/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SEARCH_DIALOG_H_
#define _SEARCH_DIALOG_H_

#include <QDialog>
#include <QListWidget>
#include <QGridLayout>
#include <QPushButton>

#include "Command/Processor.h"
#include "Object/ObjectCharacter.h"
#include "Object/Item/ItemContainer.h"

/** \addtogroup GUI
  * \{
  * \class SearchDialog
  *
  * \brief Dialog to search containers.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class SearchDialog : public QDialog
{
    Q_OBJECT

public:
    static const int SEARCH_SLOTS_WIDTH = 318;
    static const int SEARCH_SLOTS_HEIGHT = 372;

private:
    Processor *         processor;
    ItemContainer *     itemContainer;
    ObjectCharacter *   character;
    ObjectBase *        containerBase;

    QGridLayout         layout;
    QListWidget *       containerListWidget;
    QPushButton *       takeAllItemsButton;
    QPushButton *       takeItemsButton;
    QPushButton *       putItemsButton;
    QPushButton *       doneButton;
    QPushButton *       cancelButton;
    QListWidget *       characterListWidget;

public:
    SearchDialog(Processor *processor, ItemContainer *itemContainer, ObjectCharacter *character, ObjectBase *containerBase = 0, QWidget *parent = 0);

private slots:
    void updateSearch();
    void updateContainerItems();
    void updateCharacterItems();

    void takeItems();
    void takeAllItems();
    void putItems();
};

#endif // _SEARCH_DIALOG_H_
