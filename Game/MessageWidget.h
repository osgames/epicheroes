/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MESSAGE_WIDGET_H_
#define _MESSAGE_WIDGET_H_

#include <QTreeView>

#include "Game/Model/GameModel.h"
#include "Game/Model/MessageTreeModel.h"

/** \addtogroup GUI
  * \{
  * \class MessageWidget
  *
  * \brief The message widget holding all the information for messages to be shown.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class MessageWidget : public QTreeView
{
    Q_OBJECT

    GameModel *         gameModel;
    MessageTreeModel *  treeModel;
    int horizontalSizeAdjust;
public:
    explicit MessageWidget(GameModel *gameModel, QWidget *parent);

public slots:
    void addNewMessage(const QString &JSONMessage);
    void clearMessages();

private:
    void adjustWidth();
};

#endif // _MESSAGE_WIDGET_H_
