/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "MessageTreeItem.h"

#include <QVariant>
#include <QDebug>

MessageTreeItem::MessageTreeItem(const QString &message, MessageTreeItem *parentItem, QObject *parent)
    : QObject(parent), message(message), parentItem(parentItem)
{}

void MessageTreeItem::appendChild(MessageTreeItem *child)
{
    this->childItems.append(child);
    return;
}

//bool MessageTreeItem::insertChild(int position, MessageTreeItem *message)
//{
//    if (position < 0 || position > this->childItems.size())
//        return false;

//    this->childItems.insert(position, message);

//    return true;
//}

int MessageTreeItem::childCount() const
{
    return this->childItems.size();
}

int MessageTreeItem::row() const
{
    if (this->parentItem)
        return this->parentItem->childItems.indexOf(const_cast<MessageTreeItem *>(this));

    return 0;
}

QString MessageTreeItem::getMessage() const
{
    return this->message;
}

QVariant MessageTreeItem::data() const
{
    return QVariant(this->getMessage());
}

//void MessageTreeItem::setMessage(const QString &message)
//{
//    this->message = message;
//    return;
//}

MessageTreeItem *MessageTreeItem::refParentItem()
{
    return this->parentItem;
}

MessageTreeItem *MessageTreeItem::refChild(int row)
{
    return this->childItems.value(row);
}
