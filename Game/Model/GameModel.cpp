/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "GameModel.h"

#include <QDebug>
#include <QMessageBox>

#include "Object/ObjectFromID.h"
#include "FOV/FOV.h"

GameModel::GameModel(QObject *parent)
    : QObject(parent)
{
    this->playerChoosesStartLocation = false;
    this->playerID = 0;
    this->requestedData = 0;
    this->requestedObject = 0;
    this->initialized = false;
}

void GameModel::showNewMessage(unsigned int playerID, const QString &message, const QString &publicMessage, const QList<unsigned int> &playerIDs)
{
    Q_UNUSED(publicMessage);
    Q_UNUSED(playerID)
    if(playerIDs.contains(this->playerID) && this->playerModels.value(this->playerID) && this->playerModels.value(this->playerID)->isActivePlayer())
    {
        emit newMessage(message);
    }

    return;
}

void GameModel::showNewMessage(unsigned int playerID, const QString &message, const QList<unsigned int> &playerIDs)
{
    this->showNewMessage(playerID, message, message, playerIDs);
    return;
}

void GameModel::showNewMessage(const QString &message)
{
    QList<unsigned int> playersIDs = QList<unsigned int>();
    playersIDs.append(this->playerID);
    this->showNewMessage(this->playerID, message, playersIDs);
    return;
}

void GameModel::showNewMessageToMe(const QString &message)
{
    emit newMessage(message);
}

void GameModel::clearMessages()
{
    emit this->clearMessagesRequest();
}

bool GameModel::loadMap(MapModel *map)
{
    if(!map)
    {
        return false;
    }

    emit this->setNewMapSignal(map);

    return true;
}

bool GameModel::spawnCharacter(unsigned int playerID, MapModel *map, const ObjectCharacter &newCharacter)
{
    Q_UNUSED(playerID);

    if(!map)
    {
        qDebug("Spawning character failed because:\nNo map was given.");
        return false;
    }

    ObjectCharacter *character = ObjectFromID::objectFrom(newCharacter.getObjectID());
    character->update(newCharacter);
    character->setSpawnID(newCharacter.getSpawnID());

    map->refStackModel(character->getX(), character->getY(), character->getZ())->setCharacter(character);

    if(character->getObjectID() == ObjectID::PLAYER)
    {
        PlayerModel *playerModel = this->refPlayerModel(character->getSpawnID());
        if(playerModel)
        {
            playerModel->setCharacterSpwawned(true);
        }
    }

    this->checkPlayerChanged(character);

    map->updateFOV(character);

    return true;
}

bool GameModel::updateCharacter(unsigned int playerID, MapModel *map, const ObjectCharacter &oldCharacter, const ObjectCharacter &newCharacter)
{
    Q_UNUSED(playerID);

    if(!map)
    {
        qDebug("Updating character failed because:\nNo map was given.");
        return false;
    }

    ObjectCharacter *character = map->refStackModel(oldCharacter.getX(), oldCharacter.getY(), oldCharacter.getZ())->takeCharacter();

    if(!character)
    {
        qDebug("Updating character failed because:\nThe actual character was not found on the given map.");
        return false;
    }

    character->update(newCharacter);

    map->refStackModel(character->getX(), character->getY(), character->getZ())->setCharacter(character);

    this->checkPlayerChanged(character);

    map->updateFOV(character);

    return true;
}

ObjectCharacter *GameModel::removeCharacter(unsigned int playerID, MapModel *map, const ObjectCharacter &character)
{
    Q_UNUSED(playerID)

    if(!map)
    {
        qDebug("Removing item failed because:\nNo map was given.");
        return 0;
    }

    if(character.getObjectID() == ObjectID::PLAYER)
    {
        PlayerModel *playerModel = this->refPlayerModel(character.getSpawnID());
        if(playerModel)
        {
            playerModel->setCharacterSpwawned(false);
        }

        if(character.getSpawnID() == this->getPlayerID())
        {
            QMessageBox::information(0, tr("%1 is no more...").arg(character.getObjectName()), tr("Your character died."), QMessageBox::Ok);
        }
    }

    return map->refStackModel(character.getX(), character.getY(), character.getZ())->takeCharacter();
}

bool GameModel::spawnItem(unsigned int playerID, MapModel *map, const ObjectItem &newItem)
{
    Q_UNUSED(playerID);

    if(!map)
    {
        qDebug("Spawning item failed because:\nNo map was given.");
        return false;
    }

    ObjectItem *item = ObjectFromID::objectFrom(newItem.getObjectID());
    item->update(newItem);
    item->setSpawnID(newItem.getSpawnID());

    map->refStackModel(item->getX(), item->getY(), item->getZ())->prependItem(item);

    return true;
}

bool GameModel::updateItem(unsigned int playerID, MapModel *map, const ObjectItem &oldItem, const ObjectItem &newItem)
{
    Q_UNUSED(playerID);

    if(!map)
    {
        qDebug("Updating item failed because:\nNo map was given.");
        return false;
    }

    ObjectItem *item;

    if(!oldItem.isInside())
    {
        item = map->refStackModel(oldItem.getX(), oldItem.getY(), oldItem.getZ())->takeItem(oldItem.getObjectID(), oldItem.getSpawnID());
    }
    else
    {
        item = qobject_cast<ObjectItem *>(map->lookupInsideObject(oldItem.getType(), oldItem.getObjectIDNumber(), oldItem.getSpawnID()));
    }

    if(!item)
    {
        qDebug("Updating item failed because:\nThe actual item was not found on the given map.");
        return false;
    }

    item->update(newItem);

    if(!item->isInside())
    {
        map->refStackModel(item->getX(), item->getY(), item->getZ())->prependItem(item);
    }

    return true;
}

ObjectItem *GameModel::removeItem(unsigned int playerID, MapModel *map, const ObjectItem &item)
{
    Q_UNUSED(playerID)

    if(!map)
    {
        qDebug("Removing item failed because:\nNo map was given.");
        return 0;
    }

    return map->refStackModel(item.getX(), item.getY(), item.getZ())->takeItem(item.getObjectID(), item.getSpawnID());
}

void GameModel::deletePlayerModels()
{
    QList<PlayerModel *> playerModelList = this->playerModels.values();

    for(int i = 0; i < playerModelList.size(); ++i)
    {
        playerModelList[i]->deleteLater();
    }

    playerModelList.clear();

    return;
}

void GameModel::convertFromDifferentGameModel(GameModel *gameModel)
{
    QList<PlayerModel *> playerModelList = this->playerModels.values();
    for(int i = 0; i < playerModelList.size(); ++i)
    {
        playerModelList[i]->deleteLater();
    }

    this->playerModels.clear();

    this->playerModels = gameModel->getPlayerModels();
    playerModelList = this->playerModels.values();

    for(int i = 0; i < playerModelList.size(); ++i)
    {
        playerModelList[i]->setParent(this);
    }

    this->setPlayerChoosesStartLocation(gameModel->getPlayerChoosesStartLocation());
    this->setPlayerID(gameModel->getPlayerID());

    return;
}

void GameModel::checkPlayerChanged(ObjectCharacter *player)
{
    if(player->getObjectID() == ObjectID::PLAYER && this->getPlayerID() == player->getSpawnID())
    {
        emit this->playerCharacterUpdated(player);
    }
    return;
}

bool GameModel::getPlayerChoosesStartLocation() const
{
    return this->playerChoosesStartLocation;
}

unsigned int GameModel::getPlayerID() const
{
    return this->playerID;
}

bool GameModel::isInitialized() const
{
    return this->initialized;
}

unsigned int GameModel::getCurrentPlayerID() const
{
    return this->getPlayerID();
}

QMap<unsigned int, PlayerModel *> GameModel::getPlayerModels() const
{
    return this->playerModels;
}

void GameModel::setPlayerChoosesStartLocation(bool playerChoosesStartLocation)
{
    this->playerChoosesStartLocation = playerChoosesStartLocation;
    return;
}

void GameModel::setPlayerID(unsigned int playerID)
{
    this->playerID = playerID;
    return;
}

void GameModel::setPlayerModels(const QMap<unsigned int, PlayerModel *> playerModels)
{
    this->deletePlayerModels();
    this->playerModels = playerModels;
    return;
}

void GameModel::setRequestedData(QVariant data)
{
    this->requestedData = data;
    return;
}

void GameModel::setRequestedObject(QObject *object)
{
    if(this->requestedObject)
    {
        this->requestedObject->deleteLater();
    }

    object->setParent(this);
    this->requestedObject = object;
    return;
}

void GameModel::setInitialized(bool initialized)
{
    this->initialized = initialized;
    return;
}

Player *GameModel::refYourPlayer() const
{
    return this->refPlayer(this->getPlayerID());
}

void GameModel::serialize(QDataStream &dataStream) const
{
    dataStream << this->playerID;
    dataStream << this->getPlayerChoosesStartLocation();

    QList<PlayerModel *> players = this->playerModels.values();

    dataStream << players.size();

    for(int i = 0; i < players.size(); ++i)
    {
        players[i]->serialize(dataStream);
    }

    return;
}

void GameModel::deserialize(QDataStream &dataStream)
{
    unsigned int playerID;
    dataStream >> playerID;
    this->setPlayerID(playerID);

    bool playerChoosesStartLocation;

    dataStream >> playerChoosesStartLocation;

    this->setPlayerChoosesStartLocation(playerChoosesStartLocation);

    PlayerModel *player;
    int playerCount;

    dataStream >> playerCount;

    for(int i = 0; i < playerCount; ++i)
    {
        player = new PlayerModel(this);
        player->deserialize(dataStream);
        this->playerModels.insert(player->getID(), player);
    }
}
