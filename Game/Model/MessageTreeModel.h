/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MESSAGE_TREE_MODEL_H_
#define _MESSAGE_TREE_MODEL_H_

#include <QAbstractItemModel>
#include <QList>
#include <QJsonArray>

#include "Game/Model/MessageTreeItem.h"

/** \addtogroup GUI
  * \{
  * \class MessageTreeModel
  *
  * \brief The message tree model representing actual messages
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class MessageTreeModel : public QAbstractItemModel
{
    Q_OBJECT
private:
    MessageTreeItem *           root;
    QList<MessageTreeItem *>    items;

public:
    MessageTreeModel(QObject *parent = 0);
    ~MessageTreeModel();

    /**
      * @brief Add a JSON message according to a given format.
      * @param json The format this json object is posted is: {<name> : [JSON Object and Array alternating until a value is found, which is a string]}
      *             Examples: QString("{\"Roll\" : []}") or QString("{\"A\" : [{\"SubA1\" : []},{\"SubA2\" : []}]}").
      */
    void addJSONMessage(const QString &json, const QModelIndex &index);
    void addSubEntries(const QJsonArray &array, MessageTreeItem *current);

    void clearItems();

    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &index) const;
    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
//    bool setData(const QModelIndex &index, const QString &value, int role = Qt::DisplayRole);
//    bool insertRows(int position, int rows, const QModelIndex &parent = QModelIndex());

//    MessageTreeItem *getItem(const QModelIndex &index) const;
};

#endif // _MESSAGE_TREE_MODEL_H_
