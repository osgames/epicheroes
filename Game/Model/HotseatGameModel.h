/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HOTSEAT_GAME_MODEL_H
#define HOTSEAT_GAME_MODEL_H

#include "Game/Model/LocalGameModel.h"

/** \addtogroup Game
  * \{
  * \class HotseatGameModel
  *
  * \brief The model representing a hotseat game with one or more players.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class HotseatGameModel : public LocalGameModel
{
    Q_OBJECT

public:
    HotseatGameModel(Processor *processor, QObject *parent);

private:
    bool nextPlayerID();

public:
    virtual bool nextTurn();
    virtual bool beginTurn();
    virtual bool firstTurn();
    virtual bool endTurn();

    virtual void convertFromDifferentGameModel(GameModel *gameModel);

    // Get-Methods
    virtual TEoH::GameType getGameType() const;
    bool isSinglePlayer() const;

    // Serialize
    virtual void serialize(QIODevice *device) const;
    virtual void deserialize(QIODevice *device);
};

#endif // HOTSEAT_GAME_MODEL_H
