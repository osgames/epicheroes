/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "HotseatGameModel.h"

#include <QMessageBox>

#include "Command/Backend/Game/Ingame/FirstTurn.h"
#include "Command/Backend/Game/Ingame/YourTurnInformation.h"
#include "Command/Backend/Common/Quit.h"

#include "Game/Model/ServerGameModel.h"

HotseatGameModel::HotseatGameModel(Processor *processor, QObject *parent)
    : LocalGameModel(processor, parent)
{
}

bool HotseatGameModel::nextPlayerID()
{
    unsigned int oldPlayerID = this->playerID;
    QList<unsigned int> keys = this->playerModels.keys();
    unsigned int nextPlayerID;
    int currentIndex = keys.indexOf(this->playerID);

    do
    {
        currentIndex++;

        if(currentIndex == keys.size())
        {
            currentIndex = 0;
        }

        nextPlayerID = keys[currentIndex];
    }
    while(!this->playerModels[nextPlayerID]->isActivePlayer() && nextPlayerID != oldPlayerID);

    if(nextPlayerID == oldPlayerID && !this->playerModels[nextPlayerID]->isActivePlayer())
    {
        qDebug("No more active players around. Going to quit the game...");

        QMessageBox::information(0,
                                 tr("No more players available..."),
                                 tr("No more players are available, therefore I'll quit the game."),
                                 QMessageBox::Ok);

        this->processor->execute(new Quit());
        return false;
    }

    this->setPlayerID(nextPlayerID);
    return true;
}

bool HotseatGameModel::nextTurn()
{
    if(!this->nextPlayerID())
    {
        return false;
    }

    if(!this->isSinglePlayer())
    {
        this->clearMessages();
    }

    return this->beginTurn();
}

bool HotseatGameModel::beginTurn()
{
    if(!this->isSinglePlayer())
    {
        this->processor->execute(new YourTurnInformation());
    }

    if(!this->refPlayerModel(this->playerID)->hasCharacterSpawned())
    {
        // TODO: Instead of just creating a character, ask if he wants to continue or quit.
        if(!this->processor->execute(new FirstTurn()))
        {
            this->refPlayerModel(this->playerID)->setActivePlayer(false);
            if(this->hasActivePlayers())
            {
                this->endTurn();
            }
            else
            {
                this->processor->execute(new Quit());
            }
            return false;
        }
    }
    else
    {
        if(!this->loadMap(this->playerModels[this->playerID]->getLocalMapPath()))
        {
            QMessageBox::information(0,
                                     tr("The map couldn't be loaded..."),
                                     tr("The map for the current player %1 could not be loaded so something went wrong.\nI'll remove the player for now.").arg(this->refPlayer(this->playerID)->getObjectName()),
                                     QMessageBox::Ok);

            PlayerModel *playerModel = this->playerModels[this->playerID];
            this->playerModels.remove(this->playerID);
            playerModel->deleteLater();
            if(this->hasActivePlayers())
            {
                this->endTurn();
            }
            else
            {
                this->processor->execute(new Quit());
            }
            return false;
        }
    }

    QList<QString> pendingMessages = this->refPlayerModel(this->playerID)->takePendingMessages();

    for(int i = 0; i < pendingMessages.size(); ++i)
    {
        this->GameModel::showNewMessage(pendingMessages.at(i));
    }

    if(this->isInitialized())
    {
        ObjectCharacter *player = this->refCurrentMap()->refPlayerStack(this->getCurrentPlayerID())->refCharacter();
        player->prepareTurn();
        this->checkPlayerChanged(player);
    }

    if(!this->isSinglePlayer())
    {
        emit this->changeGameVisiblityRequest(true);
    }

    return true;
}

bool HotseatGameModel::firstTurn()
{
    return this->beginTurn();
}

bool HotseatGameModel::endTurn()
{
    if(this->playerModels.size() > 1)
    {
        emit this->changeGameVisiblityRequest(false);
    }
    else if(this->isInitialized())
    {
        this->showNewMessageToMe( tr("{\"~ %1's turn ended ~\" : []}").arg(this->refPlayer(this->getPlayerID())->getObjectName()));
    }
    return this->nextTurn();
}

void HotseatGameModel::convertFromDifferentGameModel(GameModel *gameModel)
{
    if(!gameModel)
    {
        qDebug() << "Given Game Model was 0.";
        return;
    }

    if(gameModel->getGameType() != TEoH::NETWORK)
    {
        qDebug() << "Not a compatible game model used to update. Type was" << gameModel->getGameType();
        return;
    }

    this->LocalGameModel::convertFromDifferentGameModel(gameModel);
    this->setPlayerID(gameModel->getCurrentPlayerID());

    return;
}

TEoH::GameType HotseatGameModel::getGameType() const
{
    return TEoH::SOLO_HOTSEAT;
}

bool HotseatGameModel::isSinglePlayer() const
{
    return this->getActivePlayerCount() == 1;
}

void HotseatGameModel::serialize(QIODevice *device) const
{
    QDataStream dataStream(device);

    LocalGameModel::serialize(dataStream);
}

void HotseatGameModel::deserialize(QIODevice *device)
{
    QDataStream dataStream(device);

    LocalGameModel::deserialize(dataStream);
}
