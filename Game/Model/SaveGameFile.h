/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SAVE_GAME_FILE_H
#define SAVE_GAME_FILE_H

#include <QObject>

#include "Common/Serialize.h"
#include "Command/Processor.h"
#include "Editor/Model/WorldModel.h"
#include "Game/Model/GameModel.h"

/** \addtogroup Game
  * \{
  * \class SaveGameFile
  *
  * \brief Represents everything needed to Serialize and Deserialize a complete game save file.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class SaveGameFile : public QObject, public Serialize
{
    Q_OBJECT

    Processor * processor;
    WorldModel *worldModel;
    GameModel * gameModel;

public:
    SaveGameFile(Processor *processor, WorldModel *worldModel, GameModel *gameModel, QObject *parent = 0);

    // Serialize
    virtual void serialize(QDataStream &dataStream) const;
    virtual void serialize(QIODevice *device) const;
    virtual void deserialize(QDataStream &dataStream);
    virtual void deserialize(QIODevice *device);

private:
    /**
      * @brief Creates a GameModel with the correct gametype.
      * @param type The game type.
      * @return A certain game model.
      */
    GameModel *fromGameType(TEoH::GameType type);
};

#endif // SAVE_GAME_FILE_H
