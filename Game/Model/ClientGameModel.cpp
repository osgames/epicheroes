/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ClientGameModel.h"

#include "Common/Common.h"

#include "Command/Backend/Common/Quit.h"
#include "Command/Backend/Game/Ingame/FirstTurn.h"
#include "Command/Backend/Game/Ingame/YourTurnInformation.h"
#include "Command/Backend/Network/Client/RequestMapFromServer.h"
#include "Command/Backend/Network/Client/SendSpawnCharacterToServer.h"
#include "Command/Backend/Network/Client/SendUpdateCharacterToServer.h"
#include "Command/Backend/Network/Client/SendSpawnItemToServer.h"
#include "Command/Backend/Network/Client/SendUpdateItemToServer.h"
#include "Command/Backend/Network/Client/SendRemoveItemToServer.h"
#include "Command/Backend/Network/Client/SendToServerEndTurn.h"
#include "Command/Backend/Network/Client/RequestPlayerModelsFromServer.h"
#include "Command/Backend/Network/Client/RequestPlayerFromServer.h"
#include "Command/Backend/Network/Client/ChangePlayerIDOnServer.h"
#include "Command/Backend/Network/Client/SendToServerMessage.h"
#include "Command/Backend/Network/Client/SendRemoveCharacterToServer.h"

ClientGameModel::ClientGameModel(Processor *processor, QObject *parent)
    : GameModel(parent)
{
    this->currentMap = 0;
    this->processor = processor;
    this->yourTurn = false;
}

void ClientGameModel::showNewMessage(unsigned int playerID, const QString &message, const QString &publicMessage, const QList<unsigned int> &playerIDs)
{
    GameModel::showNewMessage(playerID, message, publicMessage, playerIDs);

    if(playerID != TEoH::SERVER_PLAYER_ID)
    {
        QList<unsigned int> otherPlayerIDs = playerIDs;
        otherPlayerIDs.removeOne(this->playerID);
        this->processor->execute(new SendToServerMessage(publicMessage, playerIDs));
    }

    return;
}

void ClientGameModel::showNewMessage(unsigned int playerID, const QString &message, const QList<unsigned int> &playerIDs)
{
    this->showNewMessage(playerID, message, message, playerIDs);
    return;
}

void ClientGameModel::showNewMessage(const QString &message)
{
    this->processor->execute(new SendToServerMessage(message));
    return;
}

bool ClientGameModel::addNewPlayer(unsigned int playerID, PlayerModel *playerModel)
{
    Q_UNUSED(playerID);
    Q_UNUSED(playerModel);
    qDebug("A Client cannot add a new player.");
    return false;
}

bool ClientGameModel::nextTurn()
{
    return this->beginTurn();
}

bool ClientGameModel::beginTurn()
{
    MapModel *mapModel = this->refCurrentMap();

    // TODO: Instead of just quitting, ask if he wants to continue the game or quit.
    if(mapModel && this->isInitialized())
    {
        StackModel *stackModel = mapModel->refPlayerStack(this->getPlayerID());

        if(stackModel)
        {
            ObjectCharacter *player = stackModel->refCharacter();

            if(player)
            {
                this->setYourTurn(true);
                this->processor->execute(new YourTurnInformation());
                this->GameModel::showNewMessage(tr("{\"%1's turn...\" : []}").arg(player->getObjectName()));
                player->prepareTurn();
                this->checkPlayerChanged(player);
                return true;
            }
            else
            {
                if(!this->processor->execute(new Quit(false)))
                {
                    return false;
                }
            }
        }
        else
        {
            if(!this->processor->execute(new Quit(false)))
            {
                return false;
            }
        }
    }
    else
    {
        this->setYourTurn(true);
        this->processor->execute(new YourTurnInformation());
    }

    return true;
}

bool ClientGameModel::firstTurn()
{
    if(!this->processor->execute(new FirstTurn()))
    {
        this->processor->execute(new Quit());
        return false;
    }

    return true;
}

bool ClientGameModel::endTurn()
{
    this->setYourTurn(false);
    Player *player = this->refPlayer(this->getPlayerID());
    player->endTurn();
    this->checkPlayerChanged(player);
    return this->processor->execute(new SendToServerEndTurn());
}

void ClientGameModel::deavtivatePlayer(unsigned int playerID)
{
    Q_UNUSED(playerID);
    qDebug("Client cannot deactivate player.");
    return;
}

TEoH::GameType ClientGameModel::getGameType() const
{
    return TEoH::NETWORK;
}

void ClientGameModel::addtoPreloadedMaps(const QString &localMapPath, MapModel *map)
{
    Q_UNUSED(localMapPath);
    Q_UNUSED(map);
    qDebug("Client can not preload maps.");
    return;
}

void ClientGameModel::setCurrentMap(MapModel *map)
{
    this->currentMap = map;
    return;
}

bool ClientGameModel::loadMap(const QString &localMapPath)
{
    return this->processor->execute(new RequestMapFromServer(localMapPath));
}

bool ClientGameModel::loadMap(MapModel *map)
{
    this->setCurrentMap(map);
    return this->GameModel::loadMap(map);
}

bool ClientGameModel::spawnCharacter(unsigned int playerID, MapModel *map, const ObjectCharacter &newCharacter)
{
    if(!this->GameModel::spawnCharacter(playerID, map, newCharacter))
    {
        return false;
    }

    if(playerID != TEoH::SERVER_PLAYER_ID)
    {
        return this->processor->execute(new SendSpawnCharacterToServer(newCharacter));
    }

    return true;
}

bool ClientGameModel::updateCharacter(unsigned int playerID, MapModel *map, const ObjectCharacter &oldCharacter, const ObjectCharacter &newCharacter)
{
    if(!GameModel::updateCharacter(playerID, map, oldCharacter, newCharacter))
    {
        return false;
    }

    if(playerID != TEoH::SERVER_PLAYER_ID)
    {
        return this->processor->execute(new SendUpdateCharacterToServer(oldCharacter, newCharacter));
    }

    return true;
}

ObjectCharacter *ClientGameModel::removeCharacter(unsigned int playerID, MapModel *map, const ObjectCharacter &character)
{
    ObjectCharacter *removedCharacter = GameModel::removeCharacter(playerID, map, character);

    if(!removedCharacter)
    {
        return 0;
    }

    if(playerID != TEoH::SERVER_PLAYER_ID)
    {
        this->processor->execute(new SendRemoveCharacterToServer(character));
    }

    return removedCharacter;
}

bool ClientGameModel::spawnItem(unsigned int playerID, MapModel *map, const ObjectItem &newItem)
{
    if(!this->GameModel::spawnItem(playerID, map, newItem))
    {
        return false;
    }

    if(playerID != TEoH::SERVER_PLAYER_ID)
    {
        return this->processor->execute(new SendSpawnItemToServer(newItem));
    }

    return true;
}

bool ClientGameModel::updateItem(unsigned int playerID, MapModel *map, const ObjectItem &oldItem, const ObjectItem &newItem)
{
    if(!GameModel::updateItem(playerID, map, oldItem, newItem))
    {
        return false;
    }

    if(playerID != TEoH::SERVER_PLAYER_ID)
    {
        return this->processor->execute(new SendUpdateItemToServer(oldItem, newItem));
    }

    return true;
}

ObjectItem *ClientGameModel::removeItem(unsigned int playerID, MapModel *map, const ObjectItem &item)
{
    ObjectItem *removedItem = GameModel::removeItem(playerID, map, item);

    if(!removedItem)
    {
        return 0;
    }

    if(playerID != TEoH::SERVER_PLAYER_ID)
    {
        this->processor->execute(new SendRemoveItemToServer(item));
    }

    return removedItem;
}

void ClientGameModel::changePlayerID(unsigned int oldID)
{
    this->processor->execute(new ChangePlayerIDOnServer(oldID));
    return;
}

void ClientGameModel::convertFromDifferentGameModel(GameModel *gameModel)
{
    Q_UNUSED(gameModel);
    qDebug("Cannot convert a Game Model with a different Game Model as EMail Game.");
    return;
}

void ClientGameModel::setYourTurn(bool yourTurn)
{
    this->yourTurn = yourTurn;
    return;
}

bool ClientGameModel::isYourTurn() const
{
    return this->yourTurn;
}

QMap<unsigned int, PlayerModel *> ClientGameModel::getPlayerModels() const
{
    if(!this->processor->execute(new RequestPlayerModelsFromServer()))
    {
        return QMap<unsigned int, PlayerModel *>();
    }

    return this->GameModel::getPlayerModels();
}

QMap<QString, MapModel *> ClientGameModel::getPreloadedMaps() const
{
    qDebug("Clients do not hold any preloaded Maps.");
    return QMap<QString, MapModel *>();
}


PlayerModel *ClientGameModel::refPlayerModel(unsigned int playerID) const
{
    Q_UNUSED(playerID);
    qDebug("Cannot ref player model as client yet.");
    // this->processor->execute(new RequestPlayerModel(unsigned int playerID));

    return 0;
}

MapModel *ClientGameModel::refPreloadedMap(const QString &localMapPath) const
{
    Q_UNUSED(localMapPath);
    qDebug("Clients do not hold any preloaded maps to be referenced.");
    return 0;
}

MapModel *ClientGameModel::refCurrentMap() const
{
    return this->currentMap;
}

Player *ClientGameModel::refPlayer(unsigned int playerID) const
{
    if(!this->processor->execute(new RequestPlayerFromServer(playerID)))
    {
        return 0;
    }

    return qobject_cast<Player *>(this->requestedObject);
}

void ClientGameModel::serialize(QIODevice *device) const
{
    Q_UNUSED(device);
    qDebug("No need to serialize a client game model.");
    return;
}

void ClientGameModel::deserialize(QIODevice *device)
{
    Q_UNUSED(device);
    qDebug("No need to deserialize a client game model.");
    return;
}
