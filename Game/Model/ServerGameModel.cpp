/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ServerGameModel.h"

#include <QMessageBox>

#include "Command/Backend/Game/Ingame/FirstTurn.h"
#include "Command/Backend/Game/Ingame/YourTurnInformation.h"
#include "Command/Backend/Network/Server/SendToClientNextTurn.h"
#include "Command/Backend/Network/Server/Broadcast/BroadcastSpawnCharacter.h"
#include "Command/Backend/Network/Server/Broadcast/BroadcastUpdateCharacter.h"
#include "Command/Backend/Network/Server/Broadcast/BroadcastRemoveCharacter.h"
#include "Command/Backend/Network/Server/Broadcast/BroadcastSpawnItem.h"
#include "Command/Backend/Network/Server/Broadcast/BroadcastUpdateItem.h"
#include "Command/Backend/Network/Server/Broadcast/BroadcastRemoveItem.h"
#include "Command/Backend/Network/Server/Broadcast/BroadcastMessage.h"
#include "Command/Backend/Common/Quit.h"

ServerGameModel::ServerGameModel(Processor *processor, QObject *parent)
    : LocalGameModel(processor, parent), currentPlayerID(0)
{
}

void ServerGameModel::nextCurrentPlayer()
{
    unsigned int oldPlayerID = this->currentPlayerID;
    QList<unsigned int> keys = this->playerModels.keys();
    unsigned int playerID;
    int currentIndex = keys.indexOf(this->currentPlayerID);

    do
    {
        currentIndex++;

        if(currentIndex == keys.size())
        {
            currentIndex = 0;
        }

        playerID = keys[currentIndex];
    }
    while(!this->playerModels[playerID]->isActivePlayer() && playerID != oldPlayerID);

    if(playerID == oldPlayerID && !this->playerModels[playerID]->isActivePlayer())
    {
        qDebug("No more active players around. Going to quit the game...");

        QMessageBox::information(0,
                                 tr("No more players available..."),
                                 tr("No more players are available, therefore I'll quit the game."),
                                 QMessageBox::Ok);

        this->processor->execute(new Quit());
    }

    this->setCurrentPlayerID(playerID);

    return;
}

void ServerGameModel::showNewMessage(unsigned int playerID, const QString &message, const QString &publicMessage, const QList<unsigned int> &playerIDs)
{
    GameModel::showNewMessage(playerID, message, publicMessage, playerIDs);
    QList<unsigned int> otherPlayerIDs = playerIDs;
    otherPlayerIDs.removeOne(this->playerID);

    for(int i = 0; i < otherPlayerIDs.size(); ++i)
    {
        if(!this->playerModels.value(otherPlayerIDs.at(i))->isActivePlayer())
        {
            otherPlayerIDs.removeAt(i);
        }
    }

    this->processor->execute(new BroadcastMessage(playerID, publicMessage, otherPlayerIDs));

    return;
}

void ServerGameModel::showNewMessage(unsigned int playerID, const QString &message, const QList<unsigned int> &playerIDs)
{
    this->showNewMessage(playerID, message, message, playerIDs);
    return;
}

bool ServerGameModel::nextTurn()
{
    this->nextCurrentPlayer();

    return this->beginTurn();
}

bool ServerGameModel::beginTurn()
{
    if(this->currentPlayerID != this->playerID)
    {
        if(!this->processor->execute(new SendToClientNextTurn(this->currentPlayerID)))
        {
            return false;
        }
    }
    else
    {
        this->processor->execute(new YourTurnInformation());

        if(!this->refPlayerModel(this->currentPlayerID)->hasCharacterSpawned())
        {
            // TODO: If character died, ask if he wants to cotinue or become a dedicated server.
            if(!this->processor->execute(new FirstTurn()))
            {
                return false;
            }
        }
        else
        {
            if(!this->loadMap(this->playerModels[this->playerID]->getLocalMapPath()))
            {
                qDebug() << "The current map could not be loaded for player with ID" << this->playerID << "\nClosing the game.";
                this->processor->execute(new Quit());
                return false;
            }

            if(this->isInitialized())
            {
                ObjectCharacter *player = this->refCurrentMap()->refPlayerStack(this->getCurrentPlayerID())->refCharacter();
                this->GameModel::showNewMessage(tr("{\"%1's turn...\" : []}").arg(player->getObjectName()));
                player->prepareTurn();
                this->checkPlayerChanged(player);
            }
        }
    }

    return true;
}

bool ServerGameModel::firstTurn()
{
    return this->beginTurn();
}

bool ServerGameModel::endTurn()
{
    Player *player = this->refPlayer(this->getPlayerID());
    player->endTurn();
    bool result = this->nextTurn();
    this->checkPlayerChanged(player);

    return result;
}

bool ServerGameModel::spawnCharacter(unsigned int playerID, MapModel *map, const ObjectCharacter &newCharacter)
{
    if(!this->GameModel::spawnCharacter(playerID, map, newCharacter))
    {
        return false;
    }

    return this->processor->execute(new BroadcastSpawnCharacter(playerID, this->playerModels.values(), map->getLocalMapPath(), newCharacter));
}

bool ServerGameModel::updateCharacter(unsigned int playerID, MapModel *map, const ObjectCharacter &oldCharacter, const ObjectCharacter &newCharacter)
{
    if(!this->GameModel::updateCharacter(playerID, map, oldCharacter, newCharacter))
    {
        return false;
    }

    return this->processor->execute(new BroadcastUpdateCharacter(playerID, this->playerModels.values(), map->getLocalMapPath(), oldCharacter, newCharacter));
}

ObjectCharacter *ServerGameModel::removeCharacter(unsigned int playerID, MapModel *map, const ObjectCharacter &character)
{
    ObjectCharacter *removedCharacter = this->GameModel::removeCharacter(playerID, map, character);

    if(!removedCharacter)
    {
        return 0;
    }

    this->processor->execute(new BroadcastRemoveCharacter(playerID, this->playerModels.values(), map->getLocalMapPath(), character));

    return removedCharacter;
}

bool ServerGameModel::spawnItem(unsigned int playerID, MapModel *map, const ObjectItem &newItem)
{
    if(!this->GameModel::spawnItem(playerID, map, newItem))
    {
        return false;
    }

    return this->processor->execute(new BroadcastSpawnItem(playerID, this->playerModels.values(), map->getLocalMapPath(), newItem));
}

bool ServerGameModel::updateItem(unsigned int playerID, MapModel *map, const ObjectItem &oldItem, const ObjectItem &newItem)
{
    if(!this->GameModel::updateItem(playerID, map, oldItem, newItem))
    {
        return false;
    }

    return this->processor->execute(new BroadcastUpdateItem(playerID, this->playerModels.values(), map->getLocalMapPath(), oldItem, newItem));
}

ObjectItem *ServerGameModel::removeItem(unsigned int playerID, MapModel *map, const ObjectItem &item)
{
    ObjectItem *removedItem = this->GameModel::removeItem(playerID, map, item);

    if(!removedItem)
    {
        return 0;
    }

    this->processor->execute(new BroadcastRemoveItem(playerID, this->playerModels.values(), map->getLocalMapPath(), item));

    return removedItem;
}

void ServerGameModel::deavtivatePlayer(unsigned int playerID)
{
    this->playerModels[playerID]->setActivePlayer(false);
    if(this->currentPlayerID == playerID)
    {
        this->nextTurn();
    }
}

void ServerGameModel::convertFromDifferentGameModel(GameModel *gameModel)
{
    if(!gameModel)
    {
        qDebug() << "Given Game Model was 0.";
        return;
    }

    if(gameModel->getGameType() != TEoH::SOLO_HOTSEAT)
    {
        qDebug() << "Not a compatible game model used to update. Type was" << gameModel->getGameType();
        return;
    }

    this->LocalGameModel::convertFromDifferentGameModel(gameModel);

    this->setCurrentPlayerID(0);
    this->setPlayerID(0);

    QMap<unsigned int, PlayerModel *> playerModelMap = this->playerModels;
    playerModelMap.remove(0);
    QList<unsigned int> keyList = playerModelMap.keys();

    for(int i = 0; i < keyList.size(); ++i)
    {
        this->refPlayerModel(keyList[i])->setActivePlayer(false);
    }

    return;
}

bool ServerGameModel::isYourTurn() const
{
    return this->playerID == this->currentPlayerID;
}

TEoH::GameType ServerGameModel::getGameType() const
{
    return TEoH::NETWORK;
}

unsigned int ServerGameModel::getCurrentPlayerID() const
{
    return this->currentPlayerID;
}

MapModel *ServerGameModel::refCurrentMap() const
{
    return this->preloadedMaps.value(this->playerModels.value(this->currentPlayerID)->getLocalMapPath(), 0);
}

void ServerGameModel::setCurrentPlayerID(unsigned int currentPlayerID)
{
    this->currentPlayerID = currentPlayerID;
    qDebug() << QString("Current Player: ").append(QString::number(this->currentPlayerID));
    return;
}

void ServerGameModel::serialize(QIODevice *device) const
{
    QDataStream dataStream(device);

    LocalGameModel::serialize(dataStream);

    dataStream << this->currentPlayerID;
    return;
}

void ServerGameModel::deserialize(QIODevice *device)
{
    QDataStream dataStream(device);

    LocalGameModel::deserialize(dataStream);

    int currentPlayerID;
    dataStream >> currentPlayerID;

    this->setCurrentPlayerID(currentPlayerID);
    return;
}
