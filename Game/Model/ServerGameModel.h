/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SERVER_GAME_MODEL_H
#define SERVER_GAME_MODEL_H

#include "Game/Model/LocalGameModel.h"

/** \addtogroup Game
  * \{
  * \class ServerGameModel
  *
  * \brief The model representing a game that is also a server.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class ServerGameModel : public LocalGameModel
{
    Q_OBJECT

    unsigned int currentPlayerID;

public:
    ServerGameModel(Processor *processor, QObject *parent);

private:
    void nextCurrentPlayer();

public:
    virtual void showNewMessage(unsigned int playerID, const QString &message, const QString &publicMessage, const QList<unsigned int> &playerIDs);
    virtual void showNewMessage(unsigned int playerID, const QString &message, const QList<unsigned int> &playerIDs);

    virtual bool nextTurn();
    virtual bool beginTurn();
    virtual bool firstTurn();
    virtual bool endTurn();

    virtual bool spawnCharacter(unsigned int playerID, MapModel *map, const ObjectCharacter &newCharacter);
    virtual bool updateCharacter(unsigned int playerID, MapModel *map, const ObjectCharacter &oldCharacter, const ObjectCharacter &newCharacter);
    virtual ObjectCharacter *removeCharacter(unsigned int playerID, MapModel *map, const ObjectCharacter &character);

    virtual bool spawnItem(unsigned int playerID, MapModel *map, const ObjectItem &newItem);
    virtual bool updateItem(unsigned int playerID, MapModel *map, const ObjectItem &oldItem, const ObjectItem &newItem);
    virtual ObjectItem *removeItem(unsigned int playerID, MapModel *map, const ObjectItem &item);

    virtual void deavtivatePlayer(unsigned int playerID);

    virtual void convertFromDifferentGameModel(GameModel *gameModel);

    // Get-Methods
    virtual bool isYourTurn() const;
    virtual TEoH::GameType getGameType() const;
    virtual unsigned int getCurrentPlayerID() const;

    // Ref-Methods
    virtual MapModel *refCurrentMap() const;

private:
    // Set-Methods
    void setCurrentPlayerID(unsigned int currentPlayerID);

public:

    // Serialize
    virtual void serialize(QIODevice *device) const;
    virtual void deserialize(QIODevice *device);
};

#endif // SERVER_GAME_MODEL_H
