/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GAME_MODEL_H
#define GAME_MODEL_H

#include <QObject>

#include "Game/Model/PlayerModel.h"
#include "Common/Serialize.h"
#include "MainView/Model/MapModel.h"
#include "Object/Character/Player.h"

namespace TEoH
{
    enum GameType {SOLO_HOTSEAT = 0, NETWORK, EMAIL};
}

/** \addtogroup Game
  * \{
  * \class GameModel
  *
  * \brief The model representing the game preferences and players.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class GameModel : public QObject, public Serialize
{
    Q_OBJECT

signals:
    void setNewMapSignal(MapModel *); ///< Send a MapModel out to be set.
    void newMessage(const QString &message); ///< Send a message out.
    void clearMessagesRequest(); ///< Send to inform everything connected to clear messages.
    void changeGameVisiblityRequest(bool isVisible); ///< Request that parts of the game change visibility.
    void playerCharacterUpdated(ObjectCharacter *player); ///< Sent if the player character is updated.

protected:
    QVariant                requestedData;              ///< Requested Data QVariant. Everything that is not an object, has to be saved here.
    QObject *               requestedObject;            ///< For all the objects which inherit from QObject.

    QMap< unsigned int,
          PlayerModel * >   playerModels;               ///< The players registered in the game. The key is the player's ID.
    bool                    playerChoosesStartLocation; ///< True, if players can choose their own start location.
    unsigned int            playerID;                   ///< Your player ID.
    bool                    initialized;                ///< Is true, if the game model is completely initialized with all requested data.

public:
    explicit GameModel(QObject *parent);

    /**
      * @brief Add a new message for all players it is adressed to.
      * @param playerID The player who initially created the message.
      * @param message The message seen by this player.
      * @param publicMessage The message seen by the other players.
      * @param playerIDs The players this message is meant for.
      */
    virtual void showNewMessage(unsigned int playerID, const QString &message, const QString &publicMessage, const QList<unsigned int> &playerIDs);
    virtual void showNewMessage(unsigned int playerID, const QString &message, const QList<unsigned int> &playerIDs);
    virtual void showNewMessage(const QString &message);
    void showNewMessageToMe(const QString &message);

    virtual void clearMessages();

    /**
      * @brief Add a new player to the game.
      * @param playerID The playerID to be added.
      * @param playerModel The player model representing the player to be added.
      * @return True, if the player has been added. Otherwise, false.
      */
    virtual bool addNewPlayer(unsigned int playerID, PlayerModel *playerModel) = 0;

    /**
      * @brief Add a preloaded map to the preloaded maps.
      * @param localMapPath Local file path.
      * @param map The preloaded map.
      */
    virtual void addtoPreloadedMaps(const QString &localMapPath, MapModel *map) = 0;

    /**
      * @brief Load a map from a local map path.
      * @param localMapPath The local map path from which to load a map.
      * @return True, if the map has been loaded. Otherwise. false.
      */
    virtual bool loadMap(const QString &localMapPath) = 0;

    /**
      * @brief Load a map directly from a map model.
      * @param map The map model to be loaded.
      * @return True, if the map model has been loaded. Otherwise, false.
      */
    virtual bool loadMap(MapModel *map);

    /**
      * @brief Spawn a given character on a given map.
      * @param playerID The playerID of the player that wants to spawn a character.
      * @param map The map where to spawn the character on.
      * @param newCharacter The character to be spawned with given coordinates already set.
      * @return True, if the character has been spawned. Otherwise, false.
      */
    virtual bool spawnCharacter(unsigned int playerID, MapModel *map, const ObjectCharacter &newCharacter);

    /**
     * @brief Update a given character with the information of a new character on a given map.
     * @param playerID The playerID of the player that wants to update a character.
     * @param map The map where to update the character.
     * @param oldCharacter The old character that will be updated.
     * @param newCharacter The new character with the updated information.
     * @return True, if the character has been updated. Otherwise, false.
     */
    virtual bool updateCharacter(unsigned int playerID, MapModel *map, const ObjectCharacter &oldCharacter, const ObjectCharacter &newCharacter);

    /**
      * @brief Remove a character from the map.
      * @param playerID The player ID which remmoves the character.
      * @param map The map where the character will be removed from.
      * @param character The character to be removed.
      * @return True, if the character has been removed. Otherwise, false.
      */
    virtual ObjectCharacter *removeCharacter(unsigned int playerID, MapModel *map, const ObjectCharacter &character);

    /**
      * @brief Spawn an item on a given map.
      * @param playerID The player ID which spawns the item.
      * @param map The map where the item will be spawning.
      * @param item The item to be spawned.
      * @return True, if the item has been spawned. Otherwise, false.
      */
    virtual bool spawnItem(unsigned int playerID, MapModel *map, const ObjectItem &newItem);

    /**
     * @brief Update a given item with the information of a new item on a given map.
     * @param playerID The playerID of the player that wants to update an item.
     * @param map The map where to update the item.
     * @param oldItem The old item that will be updated.
     * @param newItem The new item with the updated information.
     * @return True, if the item has been updated. Otherwise, false.
     */
    virtual bool updateItem(unsigned int playerID, MapModel *map, const ObjectItem &oldItem, const ObjectItem &newItem);

    /**
      * @brief Remove an item from a given map.
      * @param playerID The player ID which removes the item.
      * @param map The map where the item is located.
      * @param item The item to be removed.
      * @return The removed item. Is 0, if it failed.
      */
    virtual ObjectItem *removeItem(unsigned int playerID, MapModel *map, const ObjectItem &item);

    /**
      * @brief Deactivate a player.
      * @param playerID The playerID of which to deactivate the character of.
      */
    virtual void deavtivatePlayer(unsigned int playerID) = 0;

    /**
      * @brief Prepare for the first turn.
      */
    virtual bool firstTurn() = 0;

    /**
      * @brief Prepare for the next turn.
      */
    virtual bool nextTurn() = 0;

    /**
      * @brief Begin the turn.
      */
    virtual bool beginTurn() = 0;

    /**
      * @brief End your turn.
      */
    virtual bool endTurn() = 0;

    /**
      * @brief Delete the current player models.
      */
    void deletePlayerModels();

    /**
      * @brief Change the player ID.
      * @param newID The new ID of the player.
      */
    virtual void changePlayerID(unsigned int oldID) = 0;

    /**
      * @brief Update this game model with a game model of different type.
      * @param gameModel Game Model with a different type than the one being updated.
      */
    virtual void convertFromDifferentGameModel(GameModel *gameModel);

protected:
    void checkPlayerChanged(ObjectCharacter *player);

public:
    // Get-Methods
    virtual TEoH::GameType getGameType() const = 0;
    virtual bool isYourTurn() const = 0;
    bool getPlayerChoosesStartLocation() const;
    unsigned int getPlayerID() const;
    bool isInitialized() const;
    virtual unsigned int getCurrentPlayerID() const;
    virtual QMap <unsigned int, PlayerModel *> getPlayerModels() const;
    virtual QMap <QString, MapModel *> getPreloadedMaps() const = 0;

    // Set-Methods
    void setPlayerChoosesStartLocation(bool playerChoosesStartLocation);
    void setPlayerID(unsigned int playerID);
    virtual void setCurrentMap(MapModel *map) = 0;
    void setPlayerModels(const QMap<unsigned int, PlayerModel *> playerModels);
    void setRequestedData(QVariant data);
    void setRequestedObject(QObject *object);
    void setInitialized(bool initialized);

    // Ref-Methods
    virtual MapModel *refPreloadedMap(const QString &localMapPath) const = 0;
    virtual MapModel *refCurrentMap() const = 0;
    virtual PlayerModel *refPlayerModel(unsigned int playerID) const = 0;
    virtual Player *refPlayer(unsigned int playerID) const = 0;
    virtual Player *refYourPlayer() const;

    // Serialize
    virtual void serialize(QDataStream &dataStream) const;
    virtual void serialize(QIODevice *device) const = 0;
    virtual void deserialize(QDataStream &dataStream);
    virtual void deserialize(QIODevice *device) = 0;
};

#endif // GAME_MODEL_H
