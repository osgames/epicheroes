/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PlayerModel.h"

#include <QDataStream>

PlayerModel::PlayerModel(unsigned int ID, const QString &localMapPath, bool activePlayer, QObject *parent)
    : QObject(parent)
{
    this->init(ID, localMapPath, activePlayer);
}

PlayerModel::PlayerModel(QObject *parent)
    : QObject(parent)
{
    this->init();
}

void PlayerModel::init(unsigned int ID, const QString &localMapPath, bool activePlayer, bool characterSpawned)
{
    this->ID = ID;
    this->setPlayerMapPath(localMapPath);
    this->setActivePlayer(activePlayer);
    this->setCharacterSpwawned(characterSpawned);
    return;
}

void PlayerModel::appendMessage(const QString &message)
{
    this->pendingMessages.append(message);
    return;
}

QList<QString> PlayerModel::takePendingMessages()
{
    QList<QString> pendingMessages = this->pendingMessages;

    this->pendingMessages.clear();

    return pendingMessages;
}

QString PlayerModel::getLocalMapPath() const
{
    return this->playerMapPath;
}

bool PlayerModel::isActivePlayer() const
{
    return this->activePlayer;
}

bool PlayerModel::hasCharacterSpawned() const
{
    return this->characterSpawned;
}

unsigned int PlayerModel::getID() const
{
    return this->ID;
}

void PlayerModel::setID(unsigned int newID)
{
    this->ID = newID;
    return;
}

void PlayerModel::setPlayerMapPath(const QString &currentMapPath)
{
    this->playerMapPath = currentMapPath;
    return;
}

void PlayerModel::setActivePlayer(bool activePlayer)
{
    this->activePlayer = activePlayer;
    return;
}

void PlayerModel::setCharacterSpwawned(bool characterSpawned)
{
    this->characterSpawned = characterSpawned;
    return;
}

void PlayerModel::serialize(QIODevice *device) const
{
    QDataStream dataStream(device);

    this->serialize(dataStream);

    return;
}

void PlayerModel::serialize(QDataStream &dataStream) const
{
    (dataStream) << this->ID;
    (dataStream) << this->playerMapPath;
    (dataStream) << this->activePlayer;
    (dataStream) << this->characterSpawned;

    (dataStream) << this->pendingMessages.size();

    for(int i = 0; i < this->pendingMessages.size(); ++i)
    {
        (dataStream) << this->pendingMessages.at(i);
    }

    return;
}

void PlayerModel::deserialize(QIODevice *device)
{
    QDataStream dataStream(device);

    this->deserialize(dataStream);

    return;
}

void PlayerModel::deserialize(QDataStream &dataStream)
{
    unsigned int ID;
    QString playerMapPath;
    bool activePlayer;
    bool characterSpawned;

    (dataStream) >> ID;
    (dataStream) >> playerMapPath;
    (dataStream) >> activePlayer;
    (dataStream) >> characterSpawned;

    this->init(ID, playerMapPath, activePlayer, characterSpawned);

    int messagesPendingAmount;
    QString message;
    (dataStream) >> messagesPendingAmount;

    for(int i = 0; i < messagesPendingAmount; ++i)
    {
        (dataStream) >> message;
        this->appendMessage(message);
    }
}
