/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MESSAGE_TREE_ITEM_H_
#define _MESSAGE_TREE_ITEM_H_

#include <QObject>

/** \addtogroup GUI
  * \{
  * \class MessageTreeItem
  *
  * \brief The message tree showing game messages.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class MessageTreeItem : public QObject
{
    Q_OBJECT

private:
    QString message;
    QList<MessageTreeItem *> childItems;
    MessageTreeItem *parentItem;

public:
    MessageTreeItem(const QString &message, MessageTreeItem *parentItem, QObject *parent = 0);

    void appendChild(MessageTreeItem *child);
//    bool insertChild(int position, MessageTreeItem *message);

    int childCount() const;
    int row() const;
    QString getMessage() const;
    QVariant data() const;

//    void setMessage(const QString &message);

    MessageTreeItem *refParentItem();
    MessageTreeItem *refChild(int row);
};

#endif // _MESSAGE_TREE_ITEM_H_
