/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SaveGameFile.h"

#include <QDataStream>

#include "Command/Backend/Common/Quit.h"
#include "Game/Model/ServerGameModel.h"
#include "Game/Model/HotseatGameModel.h"

SaveGameFile::SaveGameFile(Processor *processor, WorldModel *worldModel, GameModel *gameModel, QObject *parent)
    : QObject(parent)
{
    this->processor = processor;
    this->worldModel = worldModel;
    this->gameModel = gameModel;
}

void SaveGameFile::serialize(QDataStream &dataStream) const
{
    worldModel->serialize(dataStream);

    dataStream << gameModel->getGameType();

    gameModel->serialize(dataStream);

    return;
}

void SaveGameFile::serialize(QIODevice *device) const
{
    QDataStream dataStream(device);

    this->serialize(dataStream);

    return;
}

void SaveGameFile::deserialize(QDataStream &dataStream)
{
    worldModel->deserialize(dataStream);

    int gameTypeInteger;
    TEoH::GameType gameType;
    dataStream >> gameTypeInteger;
    gameType = TEoH::GameType(gameTypeInteger);

    if(gameType == gameModel->getGameType())
    {
        gameModel->deserialize(dataStream);
    }
    else
    {
        GameModel *differentGameModel = this->fromGameType(gameType);

        if(!differentGameModel)
        {
            qDebug() << "Could not convert the given game type to the actual game type.\nGiven Type:"
                     << gameType
                     << " Actual Type:"
                     << gameModel->getGameType();
            return;
        }

        differentGameModel->deserialize(dataStream);

        qDebug() << "Converting Game Model...";
        this->gameModel->convertFromDifferentGameModel(differentGameModel);
    }

    return;
}

void SaveGameFile::deserialize(QIODevice *device)
{
    QDataStream dataStream(device);

    this->deserialize(dataStream);

    return;
}

GameModel *SaveGameFile::fromGameType(TEoH::GameType type)
{
    GameModel *gameModel = 0;

    switch(type)
    {
    case TEoH::SOLO_HOTSEAT:
    {
        gameModel = new HotseatGameModel(0,this);
        break;
    }
    case TEoH::NETWORK:
    {
        gameModel = new ServerGameModel(0,this);
        break;
    }
    default: qDebug() << "The given game type is not supported to be created. Type was" << type; break;
    }

    return gameModel;
}
