/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "MessageTreeModel.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

#include <QDebug>

MessageTreeModel::MessageTreeModel(QObject *parent)
    : QAbstractItemModel(parent)
{
    this->root = new MessageTreeItem("Root", 0, this);
    //this->addJSONMessage("{\"A\" : [{\"AA\" : []},{\"AB\" : []}]}");

//    MessageTreeItem *a = new MessageTreeItem("A", this->root);
//    this->root->appendChild(a);
//    this->root->appendChild(new MessageTreeItem("B", this->root));
//    this->root->appendChild(new MessageTreeItem("C", this->root));
//    a->appendChild(new MessageTreeItem("Wat", a));
}

MessageTreeModel::~MessageTreeModel()
{
    this->clearItems();
}

void MessageTreeModel::addJSONMessage(const QString &json, const QModelIndex &index)
{
    QJsonDocument message = QJsonDocument::fromJson(json.toUtf8());

    QString mainMessage = message.object().keys().first();

    MessageTreeItem *mainItem = new MessageTreeItem(mainMessage, this->root, this->root);
    this->beginInsertRows(index.parent(), this->rowCount(), this->rowCount());
    this->root->appendChild(mainItem);
    this->items.append(mainItem);

    if(!message.object().value(mainMessage).isArray())
    {
        return;
    }

    QJsonArray array = message.object().value(mainMessage).toArray();
    this->addSubEntries(array, mainItem);
    this->endInsertRows();

    // this->insertRow(0, index.parent());
    //this->setData(this->index(0, 0, index.parent()), QString("Blap :D"), Qt::DisplayRole);
    emit dataChanged(index, index);
    //qDebug() << this->rowCount();

    return;
}

void MessageTreeModel::addSubEntries(const QJsonArray &array, MessageTreeItem *parent)
{
    QJsonObject object;
    QString message;
    MessageTreeItem *current;
    QJsonValue value;

    for(int i = 0; i < array.size(); ++i)
    {
        value = array.at(i);
        if(value.isString())
        {
            message = value.toString();
            current = new MessageTreeItem(message, parent, parent);
            parent->appendChild(current);
            this->items.append(current);
        }
        else if(value.isObject())
        {
            object = value.toObject();
            message = object.keys().first();
            current = new MessageTreeItem(message, parent, parent);
            parent->appendChild(current);
            this->items.append(current);

            if(object.value(message).isArray())
            {
                this->addSubEntries(object.value(message).toArray(), current);
            }
        }
    }
    return;
}

void MessageTreeModel::clearItems()
{
    for(int i = 0; i < this->items.size(); ++i)
    {
        this->items[i]->deleteLater();
    }
    this->items.clear();
    return;
}

QModelIndex MessageTreeModel::index(int row, int column, const QModelIndex &parent) const
{
    if (!this->hasIndex(row, column, parent))
        return QModelIndex();

    MessageTreeItem *parentItem;

    if (!parent.isValid())
        parentItem = this->root;
    else
        parentItem = static_cast<MessageTreeItem *>(parent.internalPointer());

    MessageTreeItem *childItem = parentItem->refChild(row);
    if (childItem)
        return this->createIndex(row, column, childItem);
    else
        return QModelIndex();
}

int MessageTreeModel::rowCount(const QModelIndex &parent) const
{
    MessageTreeItem *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = this->root;
    else
        parentItem = static_cast<MessageTreeItem *>(parent.internalPointer());

    return parentItem->childCount();
}

int MessageTreeModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 1;
}

QModelIndex MessageTreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid() || !index.internalPointer())
        return QModelIndex();

    MessageTreeItem *parentItem = static_cast<MessageTreeItem *>(index.internalPointer())->refParentItem();

    if (parentItem == this->root)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

QVariant MessageTreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() ||
        !index.internalPointer() ||
         role != Qt::DisplayRole)
        return QVariant();

    MessageTreeItem *item = static_cast<MessageTreeItem *>(index.internalPointer());

    return item->data();
}

Qt::ItemFlags MessageTreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return QAbstractItemModel::flags(index);
}

//bool MessageTreeModel::setData(const QModelIndex &index, const QString &value, int role)
//{
//    if (role != Qt::DisplayRole)
//        return false;

//    this->getItem(index)->setMessage(value);
//    emit dataChanged(index, index);

//    return true;
//}

//bool MessageTreeModel::insertRows(int position, int rows, const QModelIndex &parent)
//{
//    MessageTreeItem *parentItem = this->getItem(parent);

//    MessageTreeItem *childItem = new MessageTreeItem("", parentItem);
//    this->items.append(childItem);

//    this->beginInsertRows(parent, position, position + rows - 1);
//    bool success = parentItem->insertChild(position, childItem);
//    this->endInsertRows();

//    return success;
//}

//MessageTreeItem *MessageTreeModel::getItem(const QModelIndex &index) const
//{
//    if (index.isValid()) {
//        MessageTreeItem *item = static_cast<MessageTreeItem *>(index.internalPointer());
//        if (item) return item;
//    }
//    return this->root;
//}
