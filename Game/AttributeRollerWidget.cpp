/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "AttributeRollerWidget.h"

#include <QMimeData>
#include <QDrag>
#include <QPainter>
#include <QMouseEvent>
#include <QPixmap>
#include <algorithm>
#include <QList>

#include "Common/DiceAttribute.h"
#include "Common/Common.h"
#include "Object/Character/Abilities/Abilities.h"

AttributeRollerWidget::AttributeRollerWidget(QWidget *parent, Qt::WindowFlags f)
    : QWidget(parent, f)
{
    this->attributeLayout = new QGridLayout(this);
    this->setLayout(this->attributeLayout);
    this->setAcceptDrops(true);

    this->rollButton = new QPushButton(tr("Roll!"), this);

    for(int i = 0; i < 6; ++i)
    {
        QWidget *attributeWidget = new QWidget(this);
        QGridLayout *awLayout = new QGridLayout(attributeWidget);
        this->attributes.append(-1);

        this->assignedLabel.append(new QLabel("", this));
        QLabel *assignedLabel = this->assignedLabel.last();
        QPixmap assignedPixmap(AttributeRollerWidget::ATTRIBUTE_ENTRY_SQUARE_SIZE, AttributeRollerWidget::ATTRIBUTE_ENTRY_SQUARE_SIZE);
        assignedPixmap.fill();
        assignedLabel->setPixmap(assignedPixmap);
        assignedLabel->setProperty("ATTRIBUTE_INDEX", QVariant(i));
        awLayout->addWidget(assignedLabel, 1,1,1,1, Qt::AlignRight);

        this->modLabel.append(new QLabel("(  )", this));

        QFont font("monospace");
        font.setStyleHint(QFont::TypeWriter);
        this->modLabel.last()->setFont(font);
        awLayout->addWidget(this->modLabel.last(), 1,2,1,1,Qt::AlignLeft);
        this->attributeLayout->addWidget(attributeWidget,i+1,1, 1,1,Qt::AlignRight);
    }



    this->attributeLayout->addWidget(this->rollButton, 0,0,1,2);
    connect(this->rollButton, SIGNAL(clicked()), this, SLOT(rollAttributes()));
    this->attributeLayout->addWidget(new QLabel("Strength", this), 1,0,1,1,Qt::AlignLeft);
    this->attributeLayout->addWidget(new QLabel("Dexterity", this), 2,0,1,1,Qt::AlignLeft);
    this->attributeLayout->addWidget(new QLabel("Constitution", this), 3,0,1,1,Qt::AlignLeft);
    this->attributeLayout->addWidget(new QLabel("Intelligence", this), 4,0,1,1,Qt::AlignLeft);
    this->attributeLayout->addWidget(new QLabel("Wisdom", this), 5,0,1,1,Qt::AlignLeft);
    this->attributeLayout->addWidget(new QLabel("Charisma", this), 6,0,1,1,Qt::AlignLeft);
}

QList<int> AttributeRollerWidget::getAssignedAttributes() const
{
    return this->attributes;
}

QPixmap AttributeRollerWidget::attributeSlot(int value)
{
    QPixmap pixmap(AttributeRollerWidget::ATTRIBUTE_ENTRY_SQUARE_SIZE, AttributeRollerWidget::ATTRIBUTE_ENTRY_SQUARE_SIZE);
    pixmap.fill();

    QPainter painter;
    painter.begin(&pixmap);
    painter.drawText(QRect(0,0,
                     AttributeRollerWidget::ATTRIBUTE_ENTRY_SQUARE_SIZE,
                     AttributeRollerWidget::ATTRIBUTE_ENTRY_SQUARE_SIZE),
                     Qt::AlignCenter,
                     QString::number(value));
    painter.end();

    return pixmap;
}

void AttributeRollerWidget::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasFormat("application/x-attributedata")) {
        if (event->source() == this) {
            event->setDropAction(Qt::MoveAction);
            event->accept();
        } else {
            event->acceptProposedAction();
        }
    } else {
        event->ignore();
    }
}

void AttributeRollerWidget::dragMoveEvent(QDragMoveEvent *event)
{
    if (event->mimeData()->hasFormat("application/x-attributedata")) {
        if (event->source() == this) {
            event->setDropAction(Qt::MoveAction);
            event->accept();
        } else {
            event->acceptProposedAction();
        }
    } else {
        event->ignore();
    }
}

void AttributeRollerWidget::dropEvent(QDropEvent *event)
{
    if (event->mimeData()->hasFormat("application/x-attributedata"))
    {
        QByteArray itemData = event->mimeData()->data("application/x-attributedata");
        QDataStream dataStream(&itemData, QIODevice::ReadOnly);

        int oldIndex;
        int oldValue;
        dataStream >> oldIndex >> oldValue;

        QLabel *child = qobject_cast<QLabel *>(this->childAt(event->pos()));
        if (!child)
        {
            event->ignore();
            return;
        }

        int index = child->property("ATTRIBUTE_INDEX").toInt();
        int value;

        if(index == oldIndex)
        {
            event->ignore();
            return;
        }

        value = this->attributes[index];
        this->attributes[index] = oldValue;
        this->attributes[oldIndex] = value;

        this->updateRoller();

        if (event->source() == this) {
            event->setDropAction(Qt::MoveAction);
            event->accept();
        } else {
            event->acceptProposedAction();
        }
    } else {
        event->ignore();
    }

    return;
}

void AttributeRollerWidget::mousePressEvent(QMouseEvent *event)
{
    QLabel *child = qobject_cast<QLabel *>(this->childAt(event->pos()));
    if (!child || child->property("ATTRIBUTE_INDEX").isNull())
    {
        return;
    }

    int index = child->property("ATTRIBUTE_INDEX").toInt();
    int value;

    value = this->attributes[index];
    this->attributes[index] = -1;

    if(value == -1)
    {
        return;
    }

    QByteArray itemData;
    QDataStream dataStream(&itemData, QIODevice::WriteOnly);
    dataStream << index << value;

    QMimeData *mimeData = new QMimeData;
    mimeData->setData("application/x-attributedata", itemData);

    QDrag *drag = new QDrag(this);
    drag->setMimeData(mimeData);
    drag->setPixmap(this->attributeSlot(value));
    drag->setHotSpot(child->pos());

    this->updateRoller();
    if (drag->exec(Qt::CopyAction | Qt::MoveAction, Qt::CopyAction) != Qt::MoveAction)
    {
        this->attributes[index] = value;
    }
    this->updateRoller();

    return;
}

void AttributeRollerWidget::updateRoller()
{
    QPixmap pixmap(AttributeRollerWidget::ATTRIBUTE_ENTRY_SQUARE_SIZE, AttributeRollerWidget::ATTRIBUTE_ENTRY_SQUARE_SIZE);
    pixmap.fill();
    int value;

    for(int i = 0; i < 6; ++i)
    {
        value = this->attributes[i];
        if(value != -1)
        {
            assignedLabel[i]->setPixmap(this->attributeSlot(value));
            modLabel[i]->setText(tr("(%1)").arg(TEoH::showSign(Abilities::toMod(value))));
        }
        else
        {
            assignedLabel[i]->setPixmap(pixmap);
            modLabel[i]->setText("(  )");
        }
    }
}

void AttributeRollerWidget::rollAttributes()
{
    for(int i = 0; i < 6; ++i)
    {
        this->attributes[i] = DiceAttribute(DiceAttribute::KEEP_HIGHEST_THREE).roll();
    }

    //std::sort(this->attributesRolled.begin(), this->attributesRolled.end(), qGreater<int>());

    this->rollButton->setEnabled(false);
    this->rollButton->setText(tr("Drag'n'drop to customize."));
    this->updateRoller();

    return;
}
