/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "MessageWidget.h"

#include <QStandardItemModel>
#include <QHeaderView>
#include <QScrollBar>

MessageWidget::MessageWidget(GameModel *gameModel, QWidget *parent) :
    QTreeView(parent)
{
    this->gameModel = gameModel;
    this->horizontalSizeAdjust = 421;

    this->setEditTriggers(QAbstractItemView::NoEditTriggers);
    this->setAnimated(true);
    this->setWordWrap(true);
    this->setHeaderHidden(true);
    this->setExpandsOnDoubleClick(true);

    QFont font("monospace");
    font.setStyleHint(QFont::TypeWriter);
    this->setFont(font);
    this->header()->setStretchLastSection(false);
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    this->setFixedHeight(192);

    this->treeModel = new MessageTreeModel(this);

    this->setModel(this->treeModel);

    connect(this->gameModel, SIGNAL(newMessage(const QString &)), this, SLOT(addNewMessage(QString)));
    connect(this->gameModel, SIGNAL(clearMessagesRequest()), this, SLOT(clearMessages()));
}

void MessageWidget::addNewMessage(const QString &JSONMessage)
{
    this->treeModel->addJSONMessage(JSONMessage, this->indexAt(QPoint(0,0)));
    this->resizeColumnToContents(0);

    this->adjustWidth();

    this->scrollToBottom();
    return;
}

void MessageWidget::clearMessages()
{
    this->treeModel->deleteLater();
    this->treeModel = new MessageTreeModel(this);
    this->setModel(this->treeModel);
    //this->reset();
    return;
}

void MessageWidget::adjustWidth()
{
    if(this->verticalScrollBar()->isVisible())
    {
        this->horizontalSizeAdjust = 406;
    }
    else
    {
        this->horizontalSizeAdjust = 421;
    }

    if(columnWidth(0) < this->horizontalSizeAdjust)
    {
        this->setColumnWidth(0, this->horizontalSizeAdjust);
    }
    return;
}

