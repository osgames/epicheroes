/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLAYERWIDGET_H
#define PLAYERWIDGET_H

#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QListWidget>

#include "Game/OverviewListWidget.h"
#include "Object/ObjectCharacter.h"
#include "Command/Processor.h"

/** \addtogroup GUI
  * \{
  * \class PlayerWidget
  *
  * \brief Represents the entire player representation widget.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class PlayerWidget : public QWidget
{
    Q_OBJECT

    Processor *         processor;
    ObjectCharacter *   character;
    QLabel *            attributeLabel;

    QGridLayout *       playerLayout;
    QLabel *            playerPortrait;
    OverviewListWidget *playerOverviewList;
    QPushButton *       characterSheetButton;
    QPushButton *       inventoryButton;
    QPushButton *       endTurnButton;

public:
    PlayerWidget(Processor *processor, bool showEndTurnButton = true, QWidget *parent = 0);

    void updatePlayerWidget(ObjectCharacter *character, bool yourTurn);

private slots:
    void openInventory();
    void endTurn();
    void showItemToolTip(QListWidgetItem *item);
};

#endif // PLAYERWIDGET_H
