/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PlayerWidget.h"

#include <QImage>
#include <QPalette>
#include <QToolTip>
#include <QCursor>

#include "Common/Common.h"
#include "Command/Backend/Object/Character/ShowInventory.h"
#include "Command/Backend/Game/Ingame/EndTurn.h"

PlayerWidget::PlayerWidget(Processor *processor, bool showEndTurnButton, QWidget *parent)
    : QWidget(parent), processor(processor)
{
    this->playerLayout = new QGridLayout(this);
    this->playerPortrait = new QLabel(this);
    this->attributeLabel = new QLabel(this);

    QFont font("monospace");
    font.setStyleHint(QFont::TypeWriter);
    this->attributeLabel->setFont(font);

    this->playerOverviewList = new OverviewListWidget(this);
    this->playerOverviewList->setFont(font);
    this->playerOverviewList->setFrameShape(QFrame::NoFrame);
    this->playerOverviewList->setFrameShadow(QFrame::Plain);
    this->playerOverviewList->setSelectionMode(QAbstractItemView::NoSelection);
    this->playerOverviewList->setToolTipDuration(600000);
    connect(this->playerOverviewList, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(showItemToolTip(QListWidgetItem *)));

    this->characterSheetButton = new QPushButton(tr("Character Sheet"), this);
    this->inventoryButton = new QPushButton(tr("Inventory"), this);
    connect(this->inventoryButton, SIGNAL(clicked()), this, SLOT(openInventory()));

    this->playerLayout->addWidget(this->playerPortrait, 0,0,1,1, Qt::AlignTop|Qt::AlignHCenter);
    this->playerLayout->addWidget(this->attributeLabel, 1,0,1,1, Qt::AlignTop|Qt::AlignHCenter);
    this->playerLayout->addWidget(this->playerOverviewList,0,1,2,2);
    this->playerLayout->addWidget(this->characterSheetButton,2,0);
    this->playerLayout->addWidget(this->inventoryButton,2,1);
    if(showEndTurnButton)
    {
        this->endTurnButton = new QPushButton(tr("End Turn..."), this);
        connect(this->endTurnButton, SIGNAL(clicked()), this, SLOT(endTurn()));
        this->playerLayout->addWidget(this->endTurnButton,2,2);
    }
    else
    {
        this->endTurnButton = 0;
    }
}

void PlayerWidget::updatePlayerWidget(ObjectCharacter *character, bool yourTurn)
{
    const int PORTRAIT_SCALE_FACTOR = 3;

    this->character = character;

    QImage characterImage = this->character->getImage();
    characterImage = characterImage.scaled(characterImage.size().width()*PORTRAIT_SCALE_FACTOR, characterImage.size().height()*PORTRAIT_SCALE_FACTOR);

    QImage portrait(TEoH::STANDARD_SQUARE_SIZE*PORTRAIT_SCALE_FACTOR, TEoH::STANDARD_SQUARE_SIZE*PORTRAIT_SCALE_FACTOR, characterImage.format());
    portrait.fill(TEoH::PORTRAIT_BACKGROUND_COLOR);

    QPainter painter(&portrait);
    painter.drawImage(0,0,characterImage);
    painter.end();

    this->playerPortrait->setPixmap(QPixmap::fromImage(portrait));

    this->attributeLabel->setText(this->character->getAbilities().getAbilityString());

    this->playerOverviewList->clear();

    QList<QPair<QString, QString> > labels = this->character->getOverviewInformation();
    for(int i = 0; i < labels.size(); ++i)
    {
        QListWidgetItem *item = new QListWidgetItem(labels[i].first,this->playerOverviewList);
        if(labels[i].second != "")
        {
            item->setToolTip(labels[i].second);
        }

        this->playerOverviewList->addItem(item);
    }

    this->playerOverviewList->setFixedHeight(this->playerOverviewList->sizeHintForRow(0) * this->playerOverviewList->count() + 2 * this->playerOverviewList->frameWidth());

    if(this->endTurnButton)
    {
        if(yourTurn)
        {
            if(this->character->hasStandardActionLeft() || this->character->hasMoveActionLeft())
            {
                this->endTurnButton->setStyleSheet("QPushButton { background-color: green }");
            }
        //    else if(this->character->hasSwiftAction())
        //    {
        //        endTurnPalette.setColor(QPalette::ButtonText, Qt::yellow);
        //    }
            else
            {
                this->endTurnButton->setStyleSheet("QPushButton { background-color: red }");
            }
        }
        else
        {
            this->endTurnButton->setStyleSheet("");
        }

        this->endTurnButton->setEnabled(yourTurn);
    }

    this->inventoryButton->setEnabled(yourTurn);

    return;
}

void PlayerWidget::openInventory()
{
    this->processor->execute(new ShowInventory(this->character));
    return;
}

void PlayerWidget::endTurn()
{
    this->processor->execute(new EndTurn());
    return;
}

void PlayerWidget::showItemToolTip(QListWidgetItem *item)
{
    QToolTip::showText(QCursor::pos(), item->toolTip());
    return;
}
