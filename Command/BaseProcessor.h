#ifndef BASEPROCESSOR_H
#define BASEPROCESSOR_H

#include <QObject>

#include "Command/Backend/BaseCommand.h"

class BaseProcessor : public QObject
{
public:
    BaseProcessor(QObject *parent = 0);

    /**
      * @brief Check if the command can be executed.
      * @param command The command to be checked.
      * @return True if command is executable. Otherwise, false.
      */
    virtual bool isExecutable(BaseCommand *command) = 0;

    /**
      * @brief Check if the command can be executed. Then delete that command.
      * @param command The command to be checked.
      * @return True if command is executable. Otherwise, false.
      */
    virtual bool isExecutableAndDelete(BaseCommand *command) = 0;

    /**
      * \brief Execute a given command.
      * \param command The command to be executed.
      * \return True if the given command was executed successfully. Otherwise, false.
      * \note The given command will have the processor set as its parent may be deleted when executed.
      */
    virtual bool execute(BaseCommand *command) = 0;
};

#endif // BASEPROCESSOR_H
