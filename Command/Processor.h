/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROCESSOR_H
#define PROCESSOR_H

#include "Object/ObjectBase.h"
#include "Command/UndoRedo.h"
#include "Command/BaseProcessor.h"
#include "Editor/Model/EditorModel.h"
#include "Editor/Model/WorldModel.h"
#include "Game/Model/GameModel.h"
#include "MainView/Model/MapModel.h"
#include "Network/NetworkModel.h"

/** \addtogroup CommandProcessor
  * \{
  * \class Processor
  *
  * \brief Process commands and manages them accordingly.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class Processor : public BaseProcessor
{
    Q_OBJECT

signals:
    void commandProcessed(); ///< Emmited whenever a command is processed.

private:
    UndoRedo *      undoRedo;       ///< The Undo and Redo management object.
    EditorModel *   editorModel;
    WorldModel *    worldModel;
    GameModel *     gameModel;
    MapModel *      mapModel;
    NetworkModel *  networkModel;

public:
    explicit Processor(QObject *parent = 0);

    virtual bool isExecutable(BaseCommand *command);
    virtual bool isExecutableAndDelete(BaseCommand *command);
    virtual bool execute(BaseCommand *command);

    // Set-Methods
    void setEditorModel(EditorModel *editorModel);
    void setWorldModel(WorldModel *worldModel);
    void setGameModel(GameModel *gameModel);
    void setMapModel(MapModel *mapModel);
    void setNetworkModel(NetworkModel *networkModel);

    // Ref-Methods
    UndoRedo *refUndoRedo() const;
};

#endif // PROCESSOR_H
