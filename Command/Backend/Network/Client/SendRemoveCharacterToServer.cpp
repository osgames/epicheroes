/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SendRemoveCharacterToServer.h"

SendRemoveCharacterToServer::SendRemoveCharacterToServer(const ObjectCharacter &character, QObject *parent)
    : ClientCommand(parent), character(character)
{
    this->setObjectName("SendRemoveCharacterToServer");
}

bool SendRemoveCharacterToServer::execute()
{
    this->networkModel->sendData(SEND_REMOVE_PLAYER);

    this->networkModel->sendData(this->character.getObjectID());
    this->networkModel->sendData(this->character);

    return true;
}
