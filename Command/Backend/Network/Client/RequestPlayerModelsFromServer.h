/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REQUEST_PLAYER_MODELS_FROM_SERVER_H
#define REQUEST_PLAYER_MODELS_FROM_SERVER_H

#include "Command/Backend/Network/Client/ClientCommand.h"

/** \addtogroup Commands
  * \{
  * \class RequestPlayerModelsFromServer
  *
  * \brief Request all player models from the server.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class RequestPlayerModelsFromServer : public ClientCommand
{
    Q_OBJECT

public:
    RequestPlayerModelsFromServer(QObject *parent = 0);

    virtual bool execute();
};

#endif // REQUEST_PLAYER_MODELS_FROM_SERVER_H
