/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SendToServerMessage.h"

SendToServerMessage::SendToServerMessage(const QString &message, const QList<unsigned int> &playerList, QObject *parent)
    : ClientCommand(parent)
{
    this->setObjectName("SendToServerMessage");
    this->message = message;
    this->playerList = playerList;
    this->broadcastToEveryone = false;
}

SendToServerMessage::SendToServerMessage(const QString &message, QObject *parent)
    : ClientCommand(parent)
{
    this->setObjectName("SendToServerMessage");
    this->message = message;
    this->broadcastToEveryone = true;
}

bool SendToServerMessage::execute()
{
    this->networkModel->sendData(SEND_MESSAGE);

    this->networkModel->sendData(message);
    this->networkModel->sendData(this->broadcastToEveryone);

    if(!this->broadcastToEveryone)
    {
        this->networkModel->sendData(playerList.size());
        for(int i = 0; i < playerList.size(); ++i)
        {
            this->networkModel->sendData(playerList.at(i));
        }
    }

    return true;
}
