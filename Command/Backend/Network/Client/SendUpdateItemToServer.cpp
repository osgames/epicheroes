/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SendUpdateItemToServer.h"

SendUpdateItemToServer::SendUpdateItemToServer(const ObjectItem &oldItem, const ObjectItem &newItem, QObject *parent)
    : ClientCommand(parent), oldItem(oldItem), newItem(newItem)
{
    this->setObjectName("SendUpdateItemToServer");
}

bool SendUpdateItemToServer::execute()
{
    this->networkModel->sendData(SEND_UPDATE_ITEM);

    this->networkModel->sendData(this->oldItem.getObjectID());
    this->networkModel->sendData(this->oldItem);
    this->networkModel->sendData(this->newItem.getObjectID());
    this->networkModel->sendData(this->newItem);

    return true;
}
