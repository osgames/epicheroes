/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SEND_REMOVE_ITEM_TO_SERVER_H
#define _SEND_REMOVE_ITEM_TO_SERVER_H

#include "Command/Backend/Network/Client/ClientCommand.h"

/** \addtogroup Commands
  * \{
  * \class SendRemoveItemToServer
  *
  * \brief Send the item which has to be removed.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class SendRemoveItemToServer : public ClientCommand
{
    Q_OBJECT

    const ObjectItem &item; ///< The item that will be removed.

public:
    SendRemoveItemToServer(const ObjectItem &item, QObject* parent = 0);

    virtual bool execute();
};

#endif // _SEND_REMOVE_ITEM_TO_SERVER_H
