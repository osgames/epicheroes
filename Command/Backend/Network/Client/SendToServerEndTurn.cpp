/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SendToServerEndTurn.h"

SendToServerEndTurn::SendToServerEndTurn(QObject *parent)
    : ClientCommand(parent)
{
    this->setObjectName("SendToServerNextTurn");
}

bool SendToServerEndTurn::execute()
{
    this->networkModel->sendData(FROM_CLIENT_NEXT_TURN);

    return true;
}
