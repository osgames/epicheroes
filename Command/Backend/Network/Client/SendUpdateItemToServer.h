/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Command/Backend/Network/Client/ClientCommand.h"

#ifndef SENDUPDATEITEMTOSERVER_H
#define SENDUPDATEITEMTOSERVER_H

/** \addtogroup Commands
  * \{
  * \class SendUpdateCharacterToServer
  *
  * \brief Send updated information about a item to the server.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class SendUpdateItemToServer : public ClientCommand
{
    Q_OBJECT

    const ObjectItem &oldItem; ///< The old item that will be updated.
    const ObjectItem &newItem; ///< The new item which has the informations to replace the old item.

public:
    SendUpdateItemToServer(const ObjectItem &oldItem, const ObjectItem &newItem, QObject *parent = 0);

    virtual bool execute();
};

#endif // SENDUPDATEITEMTOSERVER_H

