/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ReceiveRemoveItemFrom.h"

#include "Object/ObjectFromID.h"

ReceiveRemoveItemFrom::ReceiveRemoveItemFrom(unsigned int senderID, QObject *parent)
    : NetworkCommand(parent), senderID(senderID)
{
    this->setObjectName("ReceiveRemoveItemFrom");
}

bool ReceiveRemoveItemFrom::execute()
{
    unsigned int itemID;

    this->networkModel->receiveData(itemID, this->senderID);
    ObjectItem *item = ObjectFromID::objectFrom(ObjectID::ItemID(itemID), this);
    this->networkModel->receiveData(*item, this->senderID);

    ObjectItem *removedItem = this->gameModel->removeItem(this->senderID, this->gameModel->refCurrentMap(), *item);

    item->deleteLater();

    if(!removedItem)
    {
        return false;
    }

    removedItem->deleteLater();

    return true;
}
