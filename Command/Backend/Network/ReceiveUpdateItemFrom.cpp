/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ReceiveUpdateItemFrom.h"

#include "Object/ObjectFromID.h"

ReceiveUpdateItemFrom::ReceiveUpdateItemFrom(unsigned int senderID, QObject *parent)
    : NetworkCommand(parent), senderID(senderID)
{
    this->setObjectName("ReceiveUpdateItemFrom");
}

bool ReceiveUpdateItemFrom::execute()
{
    unsigned int oldItemID;
    unsigned int newItemID;

    this->networkModel->receiveData(oldItemID, this->senderID);
    ObjectItem *oldItem = ObjectFromID::objectFrom(ObjectID::ItemID(oldItemID), this);
    this->networkModel->receiveData(*oldItem, this->senderID);

    this->networkModel->receiveData(newItemID, this->senderID);
    ObjectItem *newItem = ObjectFromID::objectFrom(ObjectID::ItemID(newItemID), this);
    this->networkModel->receiveData(*newItem, this->senderID);

    return this->gameModel->updateItem(this->senderID, this->gameModel->refCurrentMap(), *oldItem, *newItem);
}
