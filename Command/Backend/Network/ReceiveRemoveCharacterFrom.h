/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _RECEIVE_REMOVE_CHARACTER_FROM_H_
#define _RECEIVE_REMOVE_CHARACTER_FROM_H_

#include "Command/Backend/Network/NetworkCommand.h"

/** \addtogroup Commands
  * \{
  * \class ReceiveRemoveCharacterFrom
  *
  * \brief Receive the character that will be removed from any given sender ID.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class ReceiveRemoveCharacterFrom : public NetworkCommand
{
    Q_OBJECT

    unsigned int senderID;

public:
    ReceiveRemoveCharacterFrom(unsigned int senderID, QObject *parent = 0);

    virtual bool execute();
};

#endif // _RECEIVE_REMOVE_CHARACTER_FROM_H_
