/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ReceiveMessage.h"

ReceiveMessage::ReceiveMessage(unsigned int senderID, QObject *parent)
    : NetworkCommand(parent)
{
    this->setObjectName("ReceiveMessage");
    this->senderID = senderID;
}

bool ReceiveMessage::execute()
{
    QString message;
    bool broadcastToEveryone;
    int playerIDsListSize;
    QList<unsigned int> playerIDs;
    unsigned int playerID;

    this->networkModel->receiveData(message, this->senderID);
    this->networkModel->receiveData(broadcastToEveryone, this->senderID);

    if(!broadcastToEveryone)
    {
        this->networkModel->receiveData(playerIDsListSize, this->senderID);
        for(int i = 0; i < playerIDsListSize; ++i)
        {
            this->networkModel->receiveData(playerID, this->senderID);
            playerIDs.append(playerID);
        }

        this->gameModel->showNewMessage(this->senderID, message, playerIDs);
    }
    else
    {
        this->gameModel->showNewMessage(this->senderID, message, this->gameModel->getPlayerModels().keys());
    }

    return true;
}
