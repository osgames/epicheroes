/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "BroadcastSpawnItem.h"

BroadcastSpawnItem::BroadcastSpawnItem(unsigned int receivedByID, const QList<PlayerModel *> &playerList, const QString &localMapPath, const ObjectItem &newItem, QObject *parent)
    : BroadcastCommand(receivedByID, playerList, localMapPath, parent), newItem(newItem)
{
    this->setObjectName("BroadcastSpawnItem");
}

bool BroadcastSpawnItem::execute()
{
    unsigned int playerID;
    for(int i = 0; i < this->broadcastPlayerIDs.size(); ++i)
    {
        playerID = this->broadcastPlayerIDs[i];
        this->networkModel->sendData(BROADCAST_SPAWN_ITEM, playerID);
        this->networkModel->sendData(this->newItem.getObjectID(), playerID);
        this->networkModel->sendData(this->newItem, playerID);
    }

    return true;
}
