/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _BROADCAST_REMOVE_CHARACTER_H_
#define _BROADCAST_REMOVE_CHARACTER_H_

#include "Command/Backend/Network/Server/Broadcast/BroadcastCommand.h"

/** \addtogroup Commands
  * \{
  * \class BroadcastRemoveCharacter
  *
  * \brief Broadcast the character that has been removed.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class BroadcastRemoveCharacter : public BroadcastCommand
{
    Q_OBJECT

    const ObjectCharacter &character; ///< The character that will be removed.

public:
    BroadcastRemoveCharacter(unsigned int receivedByID,
                             const QList<PlayerModel *> &playerList,
                             const QString &localMapPath,
                             const ObjectCharacter &character,
                             QObject *parent = 0);

    virtual bool execute();
};

#endif // _BROADCAST_REMOVE_CHARACTER_H_
