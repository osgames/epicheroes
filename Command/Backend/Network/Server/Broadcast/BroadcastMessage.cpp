/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "BroadcastMessage.h"

BroadcastMessage::BroadcastMessage(unsigned int receivedByID, const QString &publicMessage, const QList<unsigned int> &otherPlayerIDs)
    : BroadcastCommand(receivedByID)
{
    this->setObjectName("BroadcastMessage");

    this->publicMessage = publicMessage;
    this->otherPlayerIDs = otherPlayerIDs;
    this->otherPlayerIDs.removeOne(receivedByID);
}

bool BroadcastMessage::execute()
{
    PlayerModel *currentPlayerModel;
    unsigned int playerID;
    for(int i = 0; i < this->otherPlayerIDs.size(); ++i)
    {
        playerID = this->otherPlayerIDs.at(i);
        currentPlayerModel = this->gameModel->refPlayerModel(playerID);
        if(currentPlayerModel && currentPlayerModel->isActivePlayer())
        {
            this->networkModel->sendData(BROADCAST_MESSAGE, playerID);
            this->networkModel->sendData(this->publicMessage, playerID);
            this->networkModel->sendData(false, playerID);
            this->networkModel->sendData(1, playerID);
            this->networkModel->sendData(playerID, playerID);
        }
    }

    return true;
}
