/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ShowDescription.h"

#include <QMessageBox>

ShowDescription::ShowDescription(const ObjectBase *object, QObject *parent)
    : ObjectCommand(object, parent)
{
    this->setObjectName("ShowDescription");
}

bool ShowDescription::execute()
{
    QMessageBox descriptionDialog;

    descriptionDialog.setWindowTitle(this->object->getObjectName());
    descriptionDialog.setIconPixmap(QPixmap::fromImage(this->object->getImage()));
    descriptionDialog.setText(this->object->getDescription());
    descriptionDialog.setStandardButtons(QMessageBox::Ok);
    descriptionDialog.exec();

    return true;
}
