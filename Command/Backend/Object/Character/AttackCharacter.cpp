/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "AttackCharacter.h"

#include "Common/Dice.h"
#include "Common/DiceAttack.h"
#include "Command/Backend/Game/Map/DealDamageTo.h"

AttackCharacter::AttackCharacter(ObjectCharacter *source, ObjectCharacter *target, int weaponIndex, QObject *parent)
    : ObjectCommand(source, parent)
{
    this->setObjectName("AttackCharacter");
    this->source = source;
    this->target = target;
    this->weaponIndex = weaponIndex;
}

bool AttackCharacter::isExecutable() const
{
    if(!this->source)
    {
        return false;
    }

    Object::EquipmentSlotType slotType = this->source->refInventoryModel()->getEquipment()[this->weaponIndex].getEquipmentSlotType();

    return this->source && this->target && this->source != this->target && this->source->inReachOf(this->target, 5) &&
           (slotType == Object::MAIN_HAND || slotType == Object::OFF_HAND) &&
           this->source->hasStandardActionLeft();
}

bool AttackCharacter::execute()
{
    ObjectCharacter *oldSource = this->source->copy();
    oldSource->setParent(this);
    ObjectCharacter *newSource = this->source->copy();
    newSource->setParent(this);
    newSource->standardActionUsed();

    DiceAttack melee = source->getAttackDice(this->weaponIndex, "AC", this->target->getAC());
    melee.roll();
    Dice damage = source->getDamageDice(this->weaponIndex, melee.isCritical(), melee.getCritMultiplier());
    damage.roll();

    if(!this->gameModel->updateCharacter(this->gameModel->getPlayerID(),
                                         this->mapModel,
                                         *oldSource,
                                         *newSource))
    {
        return false;
    }

    QString verb;

    if(melee.isHit())
    {
        if(melee.isCritical())
        {
            verb = tr("crits");
        }
        else
        {
            verb = tr("hits");
        }
    }
    else
    {
        verb = tr("misses");
    }

    QString mainMessage = tr("%1 %2 %3 with %4.")
            .arg(this->source->getObjectName())
            .arg(verb)
            .arg(this->target->getObjectName())
            .arg(melee.getWeaponName());

    this->gameModel->showNewMessage(this->gameModel->getPlayerID(),
                                    QString("{\"%1\" : [%2]}").arg(mainMessage, melee.getJSONResult(damage)),
                                    QString("{\"%1\" : []}").arg(mainMessage),
                                    this->mapModel->getPlayerIDsOnMap());

    if(melee.isHit())
    {
        bool targetDead = false;
        if(!this->processor->execute(new DealDamageTo(this->target, damage.roll(), targetDead)))
        {
            return false;
        }
    }

    return true;
}
