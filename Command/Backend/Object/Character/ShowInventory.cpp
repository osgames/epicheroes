/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ShowInventory.h"

#include "Game/Character/PlayerInventoryDialog.h"

ShowInventory::ShowInventory(ObjectCharacter *character, QObject *parent)
    : ObjectCommand(character, parent)
{
    this->setObjectName("ShowInventory");
    this->character = character;
}

bool ShowInventory::isExecutable() const
{
    return this->character &&
           ((!this->editorModel->isEditor() &&
             this->character->getObjectID() == ObjectID::PLAYER &&
             this->character->getSpawnID()  == this->gameModel->getCurrentPlayerID()));
}

bool ShowInventory::execute()
{
    if(!this->character)
    {
        return false;
    }

    if(this->character->getSpawnID() == this->gameModel->getCurrentPlayerID())
    {
        this->inventoryDialog = new PlayerInventoryDialog(this->processor, this->character, 0);
        this->inventoryDialog->exec();
        this->inventoryDialog->deleteLater();
    }

    return true;
}
