/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TWOWEAPONATTACK_H
#define TWOWEAPONATTACK_H

#include "Command/Backend/Object/ObjectCommand.h"

/** \addtogroup Commands
  * \{
  * \class TwoWeaponAttack
  *
  * \brief One character attacks another with two weapons.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class TwoWeaponAttack: public ObjectCommand
{
    Q_OBJECT

    ObjectCharacter *source;
    ObjectCharacter *target;
    int mainHandWeaponIndex;
    int offHandWeaponIndex;
    bool targetDead;

public:
    TwoWeaponAttack(ObjectCharacter *source, ObjectCharacter *target, int mainHandWeaponIndex, int offHandWeaponIndex, QObject *parent = 0);

    virtual bool isExecutable() const;
    virtual bool execute();

private:
    bool attack(Object::EquipmentSlotType type, int index);
};

#endif // TWOWEAPONATTACK_H
