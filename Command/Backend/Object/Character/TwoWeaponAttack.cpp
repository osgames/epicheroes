/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "TwoWeaponAttack.h"

#include "Common/Dice.h"
#include "Common/DiceAttack.h"
#include "Command/Backend/Game/Map/DealDamageTo.h"

TwoWeaponAttack::TwoWeaponAttack(ObjectCharacter *source, ObjectCharacter *target, int mainHandWeaponIndex, int offHandWeaponIndex, QObject *parent)
    : ObjectCommand(source, parent)
{
    this->setObjectName("TwoWeaponAttack");
    this->source = source;
    this->target = target;
    this->targetDead = false;
    this->mainHandWeaponIndex = mainHandWeaponIndex;
    this->offHandWeaponIndex = offHandWeaponIndex;
}

bool TwoWeaponAttack::isExecutable() const
{
    if(!this->source)
    {
        return false;
    }

    Object::EquipmentSlotType mainHandType = this->source->refInventoryModel()->getEquipment()[this->mainHandWeaponIndex].getEquipmentSlotType();
    Object::EquipmentSlotType offHandType = this->source->refInventoryModel()->getEquipment()[this->offHandWeaponIndex].getEquipmentSlotType();

    return this->source && this->target && this->source != this->target && this->source->inReachOf(this->target, 5) &&
           mainHandType == Object::MAIN_HAND && offHandType == Object::OFF_HAND &&
           this->source->hasFullRoundActionLeft();
}

bool TwoWeaponAttack::execute()
{
    QString sourceName = this->source->getObjectName();
    QString targetName = this->target->getObjectName();

    if(!this->attack(Object::MAIN_HAND, this->mainHandWeaponIndex))
    {
        return false;
    }

    if(this->targetDead)
    {
        this->gameModel->showNewMessage(this->gameModel->getPlayerID(),
                                        QString("{\"%1's off-hand attack misses, because %2 is already dead.\" : []}")
                                        .arg(sourceName)
                                        .arg(targetName),
                                        this->mapModel->getPlayerIDsOnMap());
    }
    else
    {
        return this->attack(Object::OFF_HAND, this->offHandWeaponIndex);
    }

    return true;
}

bool TwoWeaponAttack::attack(Object::EquipmentSlotType type, int index)
{    
    ObjectCharacter *oldSource = this->source->copy();
    oldSource->setParent(this);
    ObjectCharacter *newSource = this->source->copy();
    newSource->setParent(this);
    newSource->standardActionUsed();

    DiceAttack melee = source->getAttackDice(index, "AC", this->target->getAC());

    if(type == Object::MAIN_HAND)
    {
        melee.addModifier(new NumberMod(4, tr("Light Off-Hand"), false));
    }
    else
    {
        melee.addModifier(new NumberMod(8, tr("Light Off-Hand"), false));
    }

    melee.roll();
    Dice damage = source->getDamageDice(index, melee.isCritical(), melee.getCritMultiplier());
    damage.roll();

    if(!this->gameModel->updateCharacter(this->gameModel->getPlayerID(),
                                         this->mapModel,
                                         *oldSource,
                                         *newSource))
    {
        return false;
    }

    QString verb;

    if(melee.isHit())
    {
        if(melee.isCritical())
        {
            verb = tr("crits");
        }
        else
        {
            verb = tr("hits");
        }
    }
    else
    {
        verb = tr("misses");
    }

    QString mainMessage = tr("%1 %2 %3 with %4.")
            .arg(this->source->getObjectName())
            .arg(verb)
            .arg(this->target->getObjectName())
            .arg(melee.getWeaponName());

    this->gameModel->showNewMessage(this->gameModel->getPlayerID(),
                                    QString("{\"%1\" : [%2]}").arg(mainMessage, melee.getJSONResult(damage)),
                                    QString("{\"%1\" : []}").arg(mainMessage),
                                    this->mapModel->getPlayerIDsOnMap());

    if(melee.isHit())
    {
        bool targetDead = false;
        if(!this->processor->execute(new DealDamageTo(this->target, damage.roll(), targetDead)))
        {
            return false;
        }
    }

    return true;
}
