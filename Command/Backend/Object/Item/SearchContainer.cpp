/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SearchContainer.h"

#include "Game/Item/SearchDialog.h"

SearchContainerItem::SearchContainerItem(ObjectContainerItem *containerItem, QObject *parent)
    : ObjectCommand(containerItem, parent)
{
    this->setObjectName("SearchContainer");
    this->containerItem = containerItem;
}

bool SearchContainerItem::isExecutable() const
{
    if(this->editorModel->isEditor())
    {
        return false;
    }

    ObjectCharacter *player = this->mapModel->refPlayerStack(this->gameModel->getPlayerID())->refCharacter();

    StackModel *stack = this->mapModel->refPlayerStack(this->gameModel->getPlayerID());
    return this->containerItem &&
           (this->containerItem->isInside() || stack->hasItem(this->containerItem)) &&
            player && player->hasStandardActionLeft();
}

bool SearchContainerItem::execute()
{
    ObjectCharacter *oldPlayer = this->mapModel->refPlayerStack(this->gameModel->getPlayerID())->refCharacter()->copy();
    oldPlayer->setParent(this);
    ObjectCharacter *newPlayer = oldPlayer->copy();
    newPlayer->setParent(this);

    ObjectItem *oldContainerItem;
    ObjectItem *newContainerItem;

    oldContainerItem = this->containerItem;
    newContainerItem = this->containerItem->copy();
    newContainerItem->setParent(this);

    this->gameModel->showNewMessage(this->gameModel->getPlayerID(),
                                    QString("{\"%1 searches the %2.\" : []}")
                                    .arg(newPlayer->getObjectName())
                                    .arg(newContainerItem->getObjectName()),
                                    this->mapModel->getPlayerIDsOnMap());

    SearchDialog dialog(this->processor, qobject_cast<ObjectContainerItem *>(newContainerItem)->refContainer(), newPlayer, newContainerItem);

    int result = dialog.exec();

    if(result != QDialog::Accepted)
    {
        return true;
    }

    newPlayer->standardActionUsed();

    if(!this->gameModel->updateItem(this->gameModel->getPlayerID(),
                                        this->gameModel->refCurrentMap(),
                                        *oldContainerItem,
                                        *newContainerItem))
    {
        return false;
    }

    if(!this->gameModel->updateCharacter(this->gameModel->getPlayerID(),
                                        this->gameModel->refCurrentMap(),
                                        *oldPlayer,
                                        *newPlayer))
    {
        this->gameModel->updateItem(this->gameModel->getPlayerID(),
                                    this->gameModel->refCurrentMap(),
                                    *newContainerItem,
                                    *oldContainerItem);
        return false;
    }

    return true;
}
