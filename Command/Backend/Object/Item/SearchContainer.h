/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SEARCH_CONTAINER_H_
#define _SEARCH_CONTAINER_H_

#include "Command/Backend/Object/ObjectCommand.h"
#include "Object/Item/ItemContainer.h"
#include "Object/Item/ObjectContainerItem.h"

/** \addtogroup Commands
  * \{
  * \class SearchContainer
  *
  * \brief Search a container item with the help of a dialog.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class SearchContainerItem : public ObjectCommand
{
    Q_OBJECT

    ObjectContainerItem *containerItem;

public:
    SearchContainerItem(ObjectContainerItem *containerItem, QObject *parent = 0);

    virtual bool isExecutable() const;
    virtual bool execute();
};

#endif // _SEARCH_CONTAINER_H_
