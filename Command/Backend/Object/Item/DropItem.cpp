/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DropItem.h"

DropItem::DropItem(ObjectItem *item, ObjectCharacter *fromCharacter, QObject *parent)
    : ObjectCommand(item, parent)
{
    this->setObjectName("DropItem");
    this->item = item;
    this->fromCharacter = fromCharacter;
}

bool DropItem::isExecutable() const
{
    return this->item &&
           this->fromCharacter &&
           this->item->isInside() &&
           this->fromCharacter->refInventoryModel()->hasItemInInventory(this->item) &&
           this->fromCharacter->hasMoveActionLeft();
}

bool DropItem::execute()
{
    //NOTE: Optimization: Replace with update Item and just change the inside flag.

    ObjectItem *chosenItem = this->item->copy();
    chosenItem->setParent(this);

    ObjectCharacter *oldCharacter = this->fromCharacter->copy();
    oldCharacter->setParent(this);
    ObjectCharacter *newCharacter = this->fromCharacter->copy();
    newCharacter->setParent(this);

    int oldItemIndex = newCharacter->refInventoryModel()->removeFromInventory(this->item);

    if(oldItemIndex < 0)
    {
        return false;
    }

    newCharacter->moveActionUsed();

    if(!this->gameModel->updateCharacter(this->gameModel->getPlayerID(),
                                         this->mapModel,
                                         *oldCharacter,
                                         *newCharacter))
    {
        return false;
    }

    chosenItem->setPosition(newCharacter->getX(), newCharacter->getY(), newCharacter->getZ());
    chosenItem->setInside(false);

    if(!this->gameModel->spawnItem(this->gameModel->getPlayerID(),
                                      this->mapModel,
                                      *chosenItem))
    {
        return false;
    }

    this->gameModel->showNewMessage(this->gameModel->getPlayerID(),
                                    QString("{\"%1 dropped %2 to the ground.\" : []}")
                                    .arg(newCharacter->getObjectName())
                                    .arg(chosenItem->getObjectName()),
                                    this->mapModel->getPlayerIDsOnMap());

    return true;
}
