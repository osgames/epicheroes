/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DropAllEquipmentItems.h"

#include <QDebug>

DropAllEquipmentItems::DropAllEquipmentItems(ObjectCharacter *fromCharacter, QObject *parent)
    : ObjectCommand(fromCharacter, parent)
{
    this->setObjectName("DropAllEquipmentItems");
    this->fromCharacter = fromCharacter;
}

bool DropAllEquipmentItems::isExecutable() const
{
    return this->fromCharacter != 0;
}

bool DropAllEquipmentItems::execute()
{
    //NOTE: Optimization: Replace with update Item and just change the inside flag.

    ObjectCharacter *oldCharcter = this->fromCharacter->copy();
    oldCharcter->setParent(this);
    ObjectCharacter *newCharacter = this->fromCharacter->copy();
    newCharacter->setParent(this);

    QList<ObjectItem *> equipmentItems = newCharacter->refInventoryModel()->removeAllFromEquipment();

    if(equipmentItems.isEmpty())
    {
        qDebug() << this->objectName() << ": No items to be dropped.";
        return true;
    }

    if(!this->gameModel->updateCharacter(this->gameModel->getPlayerID(),
                                         this->mapModel,
                                         *oldCharcter,
                                         *newCharacter))
    {
        return false;
    }

    foreach(ObjectItem *item, equipmentItems)
    {
        item->setParent(this);
        item->setPosition(newCharacter->getX(), newCharacter->getY(), newCharacter->getZ());
        item->setInside(false);

        if(!this->gameModel->spawnItem(this->gameModel->getPlayerID(),
                                          this->mapModel,
                                          *item))
        {
            return false;
        }
    }

    this->gameModel->showNewMessage(this->gameModel->getPlayerID(),
                                    QString("{\"%1 dropped all equipped items.\" : []}").arg(newCharacter->getObjectName()),
                                    this->mapModel->getPlayerIDsOnMap());

    return true;
}
