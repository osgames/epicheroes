/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PickupItem.h"

PickupItem::PickupItem(ObjectItem *item, ObjectCharacter *toCharacter, QObject *parent)
    : ObjectCommand(item, parent), toCharacter(toCharacter)
{
    this->setObjectName("PickupItem");
    this->item = item;
}

bool PickupItem::isExecutable() const
{
    if(this->editorModel->isEditor())
    {
        return false;
    }

    StackModel *stack = this->mapModel->refPlayerStack(this->gameModel->getPlayerID());
    return this->item &&
           !this->item->isInside() &&
           stack->itemListSize() != 0 &&
           stack->hasItem(this->item) &&
           this->toCharacter &&
           this->toCharacter->hasMoveActionLeft();
}

bool PickupItem::execute()
{
    //NOTE: Optimization: Replace with update Item and just change the inside flag.
    ObjectItem *chosenItem = this->item->copy();
    chosenItem->setParent(this);

    if(this->gameModel->removeItem(this->gameModel->getPlayerID(), this->gameModel->refCurrentMap(), *chosenItem) != this->item)
    {
        return false;
    }

    ObjectCharacter *oldPlayer = this->toCharacter->copy();
    oldPlayer->setParent(this);
    ObjectCharacter *newPlayer = this->toCharacter->copy();
    newPlayer->setParent(this);
    newPlayer->refInventoryModel()->addToBackpack(item);
    newPlayer->moveActionUsed();

    if(!this->gameModel->updateCharacter(this->gameModel->getPlayerID(),
                                            this->gameModel->refCurrentMap(),
                                            *oldPlayer,
                                            *newPlayer))
    {
        return false;
    }

    this->gameModel->showNewMessage(this->gameModel->getPlayerID(),
                                    QString("{\"%1 picked up %2 from the ground.\" : []}")
                                    .arg(newPlayer->getObjectName())
                                    .arg(chosenItem->getObjectName()),
                                    this->mapModel->getPlayerIDsOnMap());

    return true;
}
