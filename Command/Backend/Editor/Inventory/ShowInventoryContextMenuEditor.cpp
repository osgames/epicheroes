/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ShowInventoryContextMenuEditor.h"

#include "MainView/ObjectMenu.h"
#include "Command/Backend/Editor/Inventory/CopyItemInventory.h"
#include "Command/Backend/Editor/Inventory/CutItemInventory.h"
#include "Command/Backend/Editor/Inventory/RemoveItemInventory.h"

ShowInventoryContextMenuEditor::ShowInventoryContextMenuEditor(const QPoint &globalPosition, ObjectItem *item, InventoryModel::Type inventoryType, InventoryModel *inventoryModel, QObject *parent)
    : NotUndoable(false, parent)
{
    this->setObjectName("ShowInventoryContextMenuEditor");
    this->globalPosition = globalPosition;
    this->item = item;
    this->inventoryType = inventoryType;
    this->inventoryModel = inventoryModel;

    this->copyItemAction = 0;
    this->cutItemAction = 0;
    this->removeItemAction = 0;
    this->editItemAction = 0;
}

bool ShowInventoryContextMenuEditor::execute()
{
    if(!this->item)
    {
        return false;
    }

    if(this->editorModel->isEditor())
    {
        QMenu menu;

        this->copyItemAction = new QAction(tr("Copy"), this);
        connect(this->copyItemAction, SIGNAL(triggered()), this, SLOT(copyItem()));
        this->cutItemAction = new QAction(tr("Cut"), this);
        connect(this->cutItemAction, SIGNAL(triggered()), this, SLOT(cutItem()));
        this->removeItemAction = new QAction(tr("Remove"), this);
        connect(this->removeItemAction, SIGNAL(triggered()), this, SLOT(removeItem()));
        this->editItemAction = new QAction(tr("Edit..."), this);
        connect(this->editItemAction, SIGNAL(triggered()), this, SLOT(editItem()));

        this->editItemAction->setEnabled(false);

        menu.addAction(this->copyItemAction);
        menu.addAction(this->cutItemAction);
        menu.addAction(this->removeItemAction);
        menu.addSeparator();
        menu.addAction(this->editItemAction);

        menu.exec(this->globalPosition);
    }
    else
    {
        ObjectMenu menu(this->item, this->gameModel->refYourPlayer(), this->processor);
        menu.exec(this->globalPosition);
    }

    return true;
}

void ShowInventoryContextMenuEditor::copyItem()
{
    this->processor->execute(new CopyItemInventory(this->item));
    return;
}

void ShowInventoryContextMenuEditor::cutItem()
{
    this->processor->execute(new CutItemInventory(this->item, this->inventoryModel, this->inventoryType));
    return;
}

void ShowInventoryContextMenuEditor::removeItem()
{
    this->processor->execute(new RemoveItemInventory(this->item, this->inventoryModel, this->inventoryType));
    return;
}

void ShowInventoryContextMenuEditor::editItem()
{
    // Execute Edit Command
    return;
}
