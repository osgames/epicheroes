/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MOVE_ITEM_IN_INVENTORY_EDITOR_
#define _MOVE_ITEM_IN_INVENTORY_EDITOR_

#include "Command/Backend/Undoable.h"

/** \addtogroup Commands
  * \{
  * \class PlaceItemInInventoryEditor
  *
  * \brief Place an item from an inventory in the editor.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class PlaceItemInInventoryEditor : public Undoable
{
    Q_OBJECT

private:    
    int                     movedFrom;
    InventoryModel::Type    inventoryTypeFrom;
    ObjectItem *            item;
    int                     placedTo;
    ObjectCharacter *       character;
    InventoryModel::Type    inventoryTypeTo;

public:
    PlaceItemInInventoryEditor(int movedFrom,
                               InventoryModel::Type inventoryTypeFrom,
                               ObjectItem *item,
                               int placeTo,
                               ObjectCharacter *character,
                               InventoryModel::Type inventoryTypeTo,
                               QObject *parent = 0);

public:
    virtual bool execute();
    virtual void undo();
    virtual void redo();

};

#endif //_MOVE_ITEM_IN_INVENTORY_EDITOR_
