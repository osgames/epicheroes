/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PlaceItemInInventoryEditor.h"

PlaceItemInInventoryEditor::PlaceItemInInventoryEditor(int movedFrom,
                                                       InventoryModel::Type inventoryTypeFrom,
                                                       ObjectItem *item,
                                                       int placeTo,
                                                       ObjectCharacter *character,
                                                       InventoryModel::Type inventoryTypeTo,
                                                       QObject *parent)
    : Undoable(parent)
{
    this->setObjectName("PlaceItemInInventoryEditor");
    this->movedFrom = movedFrom;
    this->inventoryTypeFrom = inventoryTypeFrom;
    this->item = item;
    this->placedTo = placeTo;
    this->character = character;
    this->inventoryTypeTo = inventoryTypeTo;
}

bool PlaceItemInInventoryEditor::execute()
{
    if(!this->character || !this->item)
    {
        return false;
    }

    this->character->refInventoryModel()->insertToInventory(this->placedTo, this->item, this->inventoryTypeTo);

    return true;
}

void PlaceItemInInventoryEditor::undo()
{
    this->character->refInventoryModel()->removeFromInventory(this->placedTo, this->inventoryTypeTo);
    this->character->refInventoryModel()->insertToInventory(this->movedFrom, this->item, this->inventoryTypeFrom);
    return;
}

void PlaceItemInInventoryEditor::redo()
{
    this->character->refInventoryModel()->removeFromInventory(this->movedFrom, this->inventoryTypeFrom);
    this->character->refInventoryModel()->insertToInventory(this->placedTo, this->item, this->inventoryTypeTo);
    return;
}
