/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "RemoveItemInventory.h"

RemoveItemInventory::RemoveItemInventory(ObjectItem *item, InventoryModel *inventoryModel, InventoryModel::Type inventoryType, QObject *parent)
    : Undoable(parent)
{
    this->setObjectName("RemoveItemInventory");
    this->item = item;
    this->inventoryModel = inventoryModel;
    this->inventoryType = inventoryType;
    this->inventoryIndex = -1;
}

bool RemoveItemInventory::execute()
{
    if(!this->inventoryModel || !this->item)
    {
        return false;
    }

    this->inventoryIndex = this->inventoryModel->removeFromInventory(this->item, this->inventoryType);

    return this->inventoryIndex;
}

void RemoveItemInventory::undo()
{
    this->inventoryModel->insertToInventory(this->inventoryIndex, this->item, this->inventoryType);
    return;
}

void RemoveItemInventory::redo()
{
    this->inventoryModel->removeFromInventory(this->inventoryIndex, this->inventoryType);
    return;
}
