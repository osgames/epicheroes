/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CUT_ITEM_INVENTORY_H_
#define _CUT_ITEM_INVENTORY_H_

#include "Command/Backend/NotUndoable.h"

#include "Object/ObjectItem.h"
#include "Object/Item/Inventory/InventoryModel.h"

/** \addtogroup Commands
  * \{
  * \class CutItemInventory
  *
  * \brief Cut an item in the inventory.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class CutItemInventory : public NotUndoable
{
    Q_OBJECT

    ObjectItem *item;
    InventoryModel *inventoryModel;
    InventoryModel::Type inventoryType;

public:
    CutItemInventory(ObjectItem *item, InventoryModel *inventoryModel, InventoryModel::Type inventoryType, QObject *parent = 0);

    virtual bool execute();
};

#endif // _CUT_ITEM_INVENTORY_H_
