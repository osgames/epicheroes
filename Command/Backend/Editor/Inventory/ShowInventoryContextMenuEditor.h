/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SHOW_INVENTORY_CONTEXT_MENU_EDITOR_H_
#define _SHOW_INVENTORY_CONTEXT_MENU_EDITOR_H_

#include <QPoint>
#include <QAction>
#include <QMenu>

#include "Command/Backend/NotUndoable.h"

#include "Object/ObjectItem.h"
#include "Object/Item/Inventory/InventoryModel.h"

/** \addtogroup Commands
  * \{
  * \class ShowInventoryContextMenuEditor
  *
  * \brief Show the inventory context menu for the editor.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class ShowInventoryContextMenuEditor : public NotUndoable
{
    Q_OBJECT

private:
    QPoint                  globalPosition;
    ObjectItem *            item;
    InventoryModel::Type    inventoryType;
    InventoryModel *        inventoryModel;

    QAction *               copyItemAction;
    QAction *               cutItemAction;
    QAction *               removeItemAction;
    QAction *               editItemAction;

public:
    ShowInventoryContextMenuEditor(const QPoint &globalPosition,
                                   ObjectItem *item,
                                   InventoryModel::Type inventoryType,
                                   InventoryModel *inventoryModel,
                                   QObject *parent = 0);

    virtual bool execute();

private slots:
    void copyItem();
    void cutItem();
    void removeItem();
    void editItem();
};

#endif //_SHOW_INVENTORY_CONTEXT_MENU_EDITOR_H_
