/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PlaceFloor.h"

PlaceFloor::PlaceFloor(ObjectTile *floor, StackModel *stackModel, QObject *parent)
    : Undoable(parent)
{
    this->setObjectName("PlaceFloor");
    this->floor = floor;
    this->oldFloor = 0;
    this->stackModel = stackModel;
}

bool PlaceFloor::execute()
{
    if(!this->floor || this->floor->getTileType() != ObjectTile::FLOOR)
    {
        return false;
    }

    this->floor->setSpawnID(this->worldModel->refObjectID()->getNewSpawnIDFor(this->floor->getObjectID()));

    this->oldFloor = this->stackModel->replaceFloor(this->floor);

    if(this->oldFloor)
    {
        this->oldFloor->setParent(this);
    }

    return this->stackModel->refFloor() != 0;
}

void PlaceFloor::undo()
{
    this->stackModel->replaceFloor(this->oldFloor);

    if(this->floor)
    {
        this->floor->setParent(this);
    }

    return;
}

void PlaceFloor::redo()
{
    this->stackModel->replaceFloor(this->floor);

    if(this->oldFloor)
    {
        this->oldFloor->setParent(this);
    }

    return;
}
