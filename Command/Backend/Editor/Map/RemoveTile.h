/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _REMOVE_TILE_H_
#define _REMOVE_TILE_H_

#include "Command/Backend/Undoable.h"
#include "MainView/Model/StackModel.h"
#include "Object/ObjectTile.h"

/** \addtogroup Commands
  * \{
  * \class RemoveTile
  *
  * \brief Remove a tile from a chosen stack.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class RemoveTile : public Undoable
{
    Q_OBJECT

    StackModel *stackModel;         ///< The stack from which to remove the tile from.
    ObjectTile *oldTile;            ///< The tile which has been removed.
    ObjectTile::TileType tileType;

public:
    explicit RemoveTile(StackModel *stackModel, ObjectTile::TileType tileType, QObject *parent = 0);

    virtual bool execute();
    virtual void undo();
    virtual void redo();
};

#endif // _REMOVE_TILE_H_
