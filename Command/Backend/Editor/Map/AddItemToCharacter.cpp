/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "AddItemToCharacter.h"

AddItemToCharacter::AddItemToCharacter(ObjectCharacter *toCharacter, QObject *parent)
    : Undoable(parent)
{
    this->setObjectName("AddItemToCharacter");
    this->toCharacter = toCharacter;
}

bool AddItemToCharacter::isExecutable() const
{
    return this->toCharacter && this->editorModel->chosenObjectType() == ObjectBase::ITEM;
}

bool AddItemToCharacter::execute()
{
    this->addedItem = qobject_cast<ObjectItem *>(this->editorModel->copyChosenObject());
    this->addedItem->setSpawnID(this->worldModel->refObjectID()->getNewSpawnIDFor(this->addedItem->getObjectID()));

    if(!this->addedItem)
    {
        return false;
    }

    this->toCharacter->refInventoryModel()->addToBackpack(this->addedItem);
    return true;
}

void AddItemToCharacter::undo()
{
    this->toCharacter->refInventoryModel()->removeFromInventory(this->addedItem);
    this->addedItem->setParent(this);
    return;
}

void AddItemToCharacter::redo()
{
    this->toCharacter->refInventoryModel()->addToBackpack(this->addedItem);
    return;
}
