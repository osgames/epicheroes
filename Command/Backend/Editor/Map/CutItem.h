﻿/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CUT_ITEM_H_
#define _CUT_ITEM_H_

#include <QList>

#include "Command/Backend/Undoable.h"
#include "MainView/Model/StackModel.h"
#include "Object/ObjectItem.h"

/** \addtogroup Commands
  * \{
  * \class CutItem
  *
  * \brief Cut a chosen item and put it into the clipboard.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class CutItem : public Undoable
{
    Q_OBJECT

private:
    StackModel *        stackModel;         ///< The stack model from which to pick the item to be cut.
    QList<ObjectItem *> oldItems;
    ObjectItem *        oldChosenItem;
    int                 chosenItem;

public:
    CutItem(StackModel *stackModel, QObject *parent = 0);

    virtual bool execute();
    virtual void undo();
    virtual void redo();
};

#endif // _CUT_ITEM_H_
