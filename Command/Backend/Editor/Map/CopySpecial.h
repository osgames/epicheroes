/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _COPY_SPECIAL_H_
#define _COPY_SPECIAL_H_

#include <QList>

#include "Command/Backend/NotUndoable.h"
#include "MainView/Model/StackModel.h"
#include "Object/ObjectSpecial.h"

/** \addtogroup Commands
  * \{
  * \class CopySpecial
  *
  * \brief Copy a chosen special to the clipboard.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class CopySpecial : public NotUndoable
{
    Q_OBJECT

private:
    StackModel *stackModel; ///< The stack model from which to pick the item to be copied.

public:
    CopySpecial(StackModel *stackModel, QObject *parent = 0);

    virtual bool execute();
};

#endif // _COPY_SPECIAL_H_
