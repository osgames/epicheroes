/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CutItem.h"

#include "MainView/ChooseObjectDialog.h"

CutItem::CutItem(StackModel *stackModel, QObject *parent)
    : Undoable(parent)
{
    this->setObjectName("CutItem");
    this->stackModel = stackModel;
}

bool CutItem::execute()
{
    ChooseObjectDialog dialog(this->stackModel, ObjectBase::ITEM);

    if(this->stackModel->itemListSize() == 0)
    {
        return false;
    }

    this->oldItems = this->stackModel->getItemList();

    if(this->oldItems.size() == 1)
    {
        this->chosenItem = 0;
        this->oldChosenItem = this->stackModel->takeItem(this->chosenItem);
        this->mapModel->setClipboardObject(this->oldChosenItem->copyBase());
        this->oldChosenItem->setParent(this);
        return true;
    }

    dialog.setWindowTitle(tr("Select item to cut..."));
    QDialog::DialogCode dialogCode = QDialog::DialogCode(dialog.exec());

    if(dialogCode == QDialog::Rejected)
    {
        return false;
    }

    QList<int> objectList = dialog.getSortedChosenObjects();

    if(objectList.isEmpty())
    {
        return false;
    }

    this->chosenItem = objectList.first();
    this->oldChosenItem = this->stackModel->takeItem(this->chosenItem);
    this->mapModel->setClipboardObject(this->oldChosenItem->copyBase());
    this->oldChosenItem->setParent(this);

    return true;
}

void CutItem::undo()
{
    this->stackModel->setItemList(this->oldItems);

    return;
}

void CutItem::redo()
{
    this->oldItems = this->stackModel->getItemList();

    this->oldChosenItem = this->stackModel->takeItem(this->chosenItem);
    this->mapModel->setClipboardObject(this->oldChosenItem->copyBase());
    this->oldChosenItem->setParent(this);

    return;
}
