/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "RemoveTile.h"

RemoveTile::RemoveTile(StackModel *stackModel, ObjectTile::TileType tileType, QObject *parent)
    : Undoable(parent)
{
    this->setObjectName("RemoveTile");
    this->stackModel = stackModel;
    this->oldTile = 0;
    this->tileType = tileType;
}

bool RemoveTile::execute()
{
    this->oldTile = this->stackModel->replaceTile(0, this->tileType);
    this->oldTile->setParent(this);

    return this->stackModel->refTile(this->tileType) == 0;
}

void RemoveTile::undo()
{
    this->stackModel->setTile(this->oldTile, this->tileType);
    return;
}

void RemoveTile::redo()
{
    this->stackModel->removeTile(this->tileType);
    this->oldTile->setParent(this);

    return;
}
