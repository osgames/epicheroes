/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CopyTile.h"

CopyTile::CopyTile(StackModel *stackModel, ObjectTile::TileType tileType, QObject *parent)
    : NotUndoable(false, parent)
{
    this->setObjectName("CopyTile");
    this->tile = 0;
    this->tileType = tileType;

    if(stackModel->hasTile(this->tileType))
    {
        this->tile = stackModel->refTile(this->tileType)->copy();
        this->tile->setParent(this);
    }
}

bool CopyTile::execute()
{
    if(this->tile)
    {
        this->mapModel->setClipboardObject(this->tile);
        return true;
    }

    return false;
}
