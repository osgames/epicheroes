/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PlaceCharacter.h"

PlaceCharacter::PlaceCharacter(ObjectCharacter *character, StackModel *stackModel, QObject *parent)
    : Undoable(parent)
{
    this->setObjectName("PlaceCharacter");
    this->character = character;
    this->oldCharacter = 0;
    this->stackModel = stackModel;
}

bool PlaceCharacter::execute()
{
    if(!this->character)
    {
        return false;
    }

    if(this->stackModel->refWall() ||
       this->stackModel->specialIDExists(ObjectID::START_POSITION))
    {
        this->setErrorEnabled(false);
        return false;
    }

    this->character->setSpawnID(this->worldModel->refObjectID()->getNewSpawnIDFor(this->character->getObjectID()));

    this->oldCharacter = this->stackModel->replaceCharacter(this->character);

    if(this->oldCharacter)
    {
        this->oldCharacter->setParent(this);
    }

    return this->stackModel->refCharacter() != 0;
}

void PlaceCharacter::undo()
{
    this->stackModel->replaceCharacter(this->oldCharacter);

    if(this->character)
    {
        this->character->setParent(this);
    }

    return;
}

void PlaceCharacter::redo()
{
    this->stackModel->replaceCharacter(this->character);

    if(this->oldCharacter)
    {
        this->oldCharacter->setParent(this);
    }

    return;
}
