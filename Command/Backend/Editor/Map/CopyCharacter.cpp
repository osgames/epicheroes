/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CopyCharacter.h"

CopyCharacter::CopyCharacter(StackModel *stackModel, QObject *parent)
    : NotUndoable(false, parent)
{
    this->setObjectName("CopyCharacter");
    this->character = 0;

    if(stackModel->hasCharacter())
    {
        this->character = stackModel->refCharacter()->copy();
        this->character->setParent(this);
    }
}

bool CopyCharacter::execute()
{
    if(this->character)
    {
        this->mapModel->setClipboardObject(this->character);
        return true;
    }

    return false;
}
