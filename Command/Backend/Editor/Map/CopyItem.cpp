/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CopyItem.h"

#include "MainView/ChooseObjectDialog.h"

CopyItem::CopyItem(StackModel *stackModel, QObject *parent)
    : NotUndoable(false, parent)
{
    this->setObjectName("CopyItem");
    this->stackModel = stackModel;
}

bool CopyItem::execute()
{
    ChooseObjectDialog dialog(this->stackModel, ObjectBase::ITEM);

    if(this->stackModel->itemListSize() == 0)
    {
        return false;
    }

    if(this->stackModel->itemListSize() == 1)
    {
        this->mapModel->setClipboardObject(this->stackModel->refItem(0)->copyBase());
        return true;
    }

    dialog.setWindowTitle(tr("Select item to copy..."));

    QDialog::DialogCode dialogCode = QDialog::DialogCode(dialog.exec());

    if(dialogCode == QDialog::Rejected)
    {
        return false;
    }

    QList<int> objectList = dialog.getSortedChosenObjects();

    if(objectList.isEmpty())
    {
        return false;
    }

    this->mapModel->setClipboardObject(this->stackModel->refItem(objectList.first())->copyBase());

    return true;
}
