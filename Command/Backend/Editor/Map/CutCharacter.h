/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CUT_CHARACTER_H
#define CUT_CHARACTER_H

#include "Command/Backend/NotUndoable.h"
#include "MainView/Model/StackModel.h"

/** \addtogroup Commands
  * \{
  * \class CutCharacter
  *
  * \brief Copy and remove (Cut) the character to the clipboard.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class CutCharacter : public NotUndoable
{
    Q_OBJECT

private:
    StackModel *stackModel; ///< The stack from which to cut the character.

public:
    CutCharacter(StackModel *stackModel, QObject *parent = 0);

    virtual bool execute();
};

#endif // CUT_CHARACTER_H
