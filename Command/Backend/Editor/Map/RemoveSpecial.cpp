/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "RemoveSpecial.h"

#include "MainView/ChooseObjectDialog.h"

RemoveSpecial::RemoveSpecial(StackModel *stackModel, QObject *parent)
    : Undoable(parent)
{
    this->setObjectName("RemoveSpecial");
    this->stackModel = stackModel;
}

bool RemoveSpecial::execute()
{
    if(this->stackModel->specialListSize() == 0)
    {
        return false;
    }

    this->oldSpecials = this->stackModel->getSpecialList();

    if(this->oldSpecials.size() == 1)
    {
        this->stackModel->takeSpecial(0)->setParent(this);
        this->chosenSpecials.append(0);
        return true;
    }

    ChooseObjectDialog dialog(this->stackModel, ObjectBase::SPECIAL, QAbstractItemView::MultiSelection);
    QDialog::DialogCode dialogCode = QDialog::DialogCode(dialog.exec());

    if(dialogCode == QDialog::Rejected)
    {
        return false;
    }

    this->chosenSpecials = dialog.getSortedChosenObjects();

    if(this->chosenSpecials.isEmpty())
    {
        return false;
    }

    ObjectSpecial *special;

    for(int i = this->chosenSpecials.size() - 1; i >= 0; --i)
    {
        special = this->stackModel->takeSpecial(this->chosenSpecials.at(i));
        special->setParent(this);
    }

    return true;
}

void RemoveSpecial::undo()
{
    this->stackModel->setSpecialList(this->oldSpecials);

    return;
}

void RemoveSpecial::redo()
{
    this->oldSpecials = this->stackModel->getSpecialList();

    ObjectSpecial *special;

    for(int i = this->chosenSpecials.size() - 1; i >= 0; --i)
    {
        special = this->stackModel->takeSpecial(this->chosenSpecials.at(i));
        special->setParent(this);
    }

    return;
}
