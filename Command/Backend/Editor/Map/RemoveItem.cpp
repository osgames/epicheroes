/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "RemoveItem.h"

#include "MainView/ChooseObjectDialog.h"

RemoveItem::RemoveItem(StackModel *stackModel, QObject *parent)
    : Undoable(parent)
{
    this->setObjectName("RemoveItem");
    this->stackModel = stackModel;
}

bool RemoveItem::execute()
{
    if(this->stackModel->itemListSize() == 0)
    {
        return false;
    }

    this->oldItems = this->stackModel->getItemList();

    if(this->oldItems.size() == 1)
    {
        this->stackModel->takeItem(0)->setParent(this);
        this->chosenItems.append(0);
        return true;
    }

    ChooseObjectDialog dialog(this->stackModel, ObjectBase::ITEM, QAbstractItemView::MultiSelection);
    dialog.setWindowTitle(tr("Select item(s) to remove..."));
    QDialog::DialogCode dialogCode = QDialog::DialogCode(dialog.exec());

    if(dialogCode == QDialog::Rejected)
    {
        return false;
    }

    this->chosenItems = dialog.getSortedChosenObjects();

    if(this->chosenItems.isEmpty())
    {
        return false;
    }

    ObjectItem *item;

    for(int i = this->chosenItems.size() - 1; i >= 0; --i)
    {
        item = this->stackModel->takeItem(this->chosenItems.at(i));
        item->setParent(this);
    }

    return true;
}

void RemoveItem::undo()
{
    this->stackModel->setItemList(this->oldItems);

    return;
}

void RemoveItem::redo()
{
    this->oldItems = this->stackModel->getItemList();

    ObjectItem *item;

    for(int i = this->chosenItems.size() - 1; i >= 0; --i)
    {
        item = this->stackModel->takeItem(this->chosenItems.at(i));
        item->setParent(this);
    }

    return;
}
