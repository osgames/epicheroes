/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ADD_ITEM_TO_CHARACTER_H_
#define _ADD_ITEM_TO_CHARACTER_H_

#include "Command/Backend/Undoable.h"

#include "Object/ObjectCharacter.h"
#include "Object/ObjectItem.h"

/** \addtogroup Commands
  * \{
  * \class AddItemToCharacter
  *
  * \brief Adds an item to a character.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class AddItemToCharacter : public Undoable
{
    Q_OBJECT

    ObjectCharacter *   toCharacter;
    ObjectItem *        addedItem;

public:
    AddItemToCharacter(ObjectCharacter *toCharacter, QObject *parent = 0);

    virtual bool isExecutable() const;
    virtual bool execute();

    virtual void undo();
    virtual void redo();
};

#endif // _ADD_ITEM_TO_CHARACTER_H_
