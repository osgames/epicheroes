/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PLACE_CHARACTER_H_
#define _PLACE_CHARACTER_H_

#include "Command/Backend/Undoable.h"
#include "MainView/Model/StackModel.h"
#include "Object/ObjectSpecial.h"

/** \addtogroup Commands
  * \{
  * \class PlaceCharacter
  *
  * \brief Place a character on a chosen stack.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class PlaceCharacter : public Undoable
{
    Q_OBJECT

    ObjectCharacter *   character;      ///< The character to be placed.
    ObjectCharacter *   oldCharacter;   ///< The old character.
    StackModel *        stackModel;     ///< The stack where the object will be placed to.

public:
    explicit PlaceCharacter(ObjectCharacter *character, StackModel *stackModel, QObject *parent = 0);

    virtual bool execute();
    virtual void undo();
    virtual void redo();
};

#endif // _PLACE_CHARACTER_H_
