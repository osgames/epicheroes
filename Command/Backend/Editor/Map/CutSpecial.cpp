/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CutSpecial.h"

#include "MainView/ChooseObjectDialog.h"

CutSpecial::CutSpecial(StackModel *stackModel, QObject *parent)
    : Undoable(parent)
{
    this->setObjectName("CutSpecial");
    this->stackModel = stackModel;
}

bool CutSpecial::execute()
{
    ChooseObjectDialog dialog(this->stackModel, ObjectBase::ITEM);

    if(this->stackModel->specialListSize() == 0)
    {
        return false;
    }

    this->oldSpecials = this->stackModel->getSpecialList();

    if(this->oldSpecials.size() == 1)
    {
        this->chosenSpecial = 0;
        this->oldChosenSpecial = this->stackModel->takeSpecial(this->chosenSpecial);
        this->mapModel->setClipboardObject(this->oldChosenSpecial->copyBase());
        this->oldChosenSpecial->setParent(this);
        return true;
    }

    QDialog::DialogCode dialogCode = QDialog::DialogCode(dialog.exec());

    if(dialogCode == QDialog::Rejected)
    {
        return false;
    }

    QList<int> objectList = dialog.getSortedChosenObjects();

    if(objectList.isEmpty())
    {
        return false;
    }

    this->chosenSpecial = objectList.first();
    this->oldChosenSpecial = this->stackModel->takeSpecial(this->chosenSpecial);
    this->mapModel->setClipboardObject(this->oldChosenSpecial->copyBase());
    this->oldChosenSpecial->setParent(this);

    return true;
}

void CutSpecial::undo()
{
    this->stackModel->setSpecialList(this->oldSpecials);

    return;
}

void CutSpecial::redo()
{
    this->oldSpecials = this->stackModel->getSpecialList();

    this->oldChosenSpecial = this->stackModel->takeSpecial(this->chosenSpecial);
    this->mapModel->setClipboardObject(this->oldChosenSpecial->copyBase());
    this->oldChosenSpecial->setParent(this);

    return;
}
