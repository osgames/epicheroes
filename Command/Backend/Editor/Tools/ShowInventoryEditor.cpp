/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ShowInventoryEditor.h"

#include "Game/Character/PlayerInventoryDialog.h"

ShowInventoryEditor::ShowInventoryEditor(ObjectCharacter *character, QObject *parent)
    : NotUndoable(false, parent)
{
    this->setObjectName("ShowInventoryEditor");
    this->character = character;
}

bool ShowInventoryEditor::isExecutable() const
{
    return this->character && this->editorModel->isEditor();
}

bool ShowInventoryEditor::execute()
{
    this->inventoryDialog = new PlayerInventoryDialog(this->processor, this->character, 0);
    this->inventoryDialog->exec();
    this->inventoryDialog->deleteLater();

    return true;
}
