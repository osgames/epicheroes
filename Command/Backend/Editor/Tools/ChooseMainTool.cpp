/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ChooseMainTool.h"

#include "Object/ObjectBase.h"
#include "Editor/Tools/ChooseToolDialog.h"

ChooseMainTool::ChooseMainTool(int button, QObject *parent) :
    NotUndoable(false, parent)
{
    this->setObjectName("ChosenMainTool");
    this->button = button;
}

bool ChooseMainTool::execute()
{
    ObjectBase::ObjectType type;

    switch(ChooseMainTool::ChosenTool(this->button))
    {
    case(ChooseMainTool::CHARACTER_TOOL):
    {
        type = ObjectBase::CHARACTER;
        break;
    }
    case(ChooseMainTool::ITEM_TOOL):
    {
        type = ObjectBase::ITEM;
        break;
    }
    case(ChooseMainTool::TILE_TOOL):
    {
        type = ObjectBase::TILE;
        break;
    }
    case(ChooseMainTool::SPECIAL_TOOL):
    {
        type = ObjectBase::SPECIAL;
        break;
    }
    }

    ChooseToolDialog dialog(type);
    QDialog::DialogCode result = QDialog::DialogCode(dialog.exec());

    if(result == QDialog::Accepted)
    {
        this->editorModel->setChosenObject(type, dialog.getChosenObjectID());
    }
    return true;
}
