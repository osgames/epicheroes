/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMANDNODE_H
#define COMMANDNODE_H

#include <QList>

#include "Command/Backend/CommandTree.h"

/** \addtogroup Commands
  * \{
  * \class CommandNode
  *
  * \brief A node contains a list of command tree elements (More nodes, seperators, leaves).
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class CommandNode : public CommandTree
{
    Q_OBJECT

    QList<CommandTree *> list;
    bool removeIfUnavailable;

public:
    CommandNode(const QString &name, bool removeIfUnavailable = false, QObject *parent = 0);

    void append(const QList<CommandTree *> &commandTrees);
    void append(CommandTree *commandTree);
    virtual CommandTree::Type getCommandTreeType() const;

    QList<CommandTree *> getList() const;
    bool doRemoveIfUnavailable() const;
};

#endif // COMMANDNODE_H
