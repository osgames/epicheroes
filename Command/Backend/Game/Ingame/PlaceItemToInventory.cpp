/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PlaceItemToInventory.h"

#include "Command/Backend/Editor/Inventory/PlaceItemInInventoryEditor.h"

PlaceItemToInventory::PlaceItemToInventory(int indexFrom, InventoryModel::Type inventoryTypeFrom, ObjectItem *item, ObjectCharacter *character, int indexTo, InventoryModel::Type inventoryTypeTo, QObject *parent)
    : GameCommand(parent)
{
    this->setObjectName("PlaceItemToInventory");

    this->indexFrom = indexFrom;
    this->inventoryTypeFrom = inventoryTypeFrom;
    this->item = item;
    this->character = character;
    this->indexTo = indexTo;
    this->inventoryTypeTo = inventoryTypeTo;
}

bool PlaceItemToInventory::execute()
{
    if(!this->item || !this->character)
    {
        return false;
    }

    if(this->editorModel->isEditor())
    {
        return this->processor->execute(new PlaceItemInInventoryEditor(this->indexFrom, this->inventoryTypeFrom, this->item, this->indexTo, this->character, this->inventoryTypeTo));
    }

    ObjectCharacter *oldCharacter = this->character->copy();
    oldCharacter->setParent(this);
    ObjectCharacter *newCharacter = this->character->copy();
    newCharacter->setParent(this);

    newCharacter->refInventoryModel()->insertToInventory(this->indexTo, this->item, this->inventoryTypeTo);

    return this->gameModel->updateCharacter(this->gameModel->getCurrentPlayerID(),
                                            this->mapModel,
                                            *oldCharacter,
                                            *newCharacter);
}
