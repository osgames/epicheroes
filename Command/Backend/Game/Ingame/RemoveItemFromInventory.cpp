/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "RemoveItemFromInventory.h"

RemoveItemFromInventory::RemoveItemFromInventory(ObjectCharacter *character, int index, InventoryModel::Type inventoryType, QObject *parent)
    : GameCommand(parent)
{
    this->setObjectName("RemoveItemFromInventory");

    this->character = character;
    this->index = index;
    this->inventoryType = inventoryType;
}

bool RemoveItemFromInventory::execute()
{
    if(!this->character)
    {
        return false;
    }

    bool success = false;
    ObjectCharacter *oldCharacter = 0;
    ObjectCharacter *newCharacter = 0;

    if(this->editorModel->isEditor())
    {
        newCharacter = this->character;
    }
    else
    {
        oldCharacter = this->character->copy();
        oldCharacter->setParent(this);
        newCharacter = this->character->copy();
        newCharacter->setParent(this);
    }

    success = newCharacter->refInventoryModel()->removeFromInventory(this->index, this->inventoryType) != 0;

    if(this->editorModel->isEditor())
    {
        return success;
    }

    return success && this->gameModel->updateCharacter(this->gameModel->getCurrentPlayerID(),
                                                       this->mapModel,
                                                       *oldCharacter,
                                                       *newCharacter);
}

