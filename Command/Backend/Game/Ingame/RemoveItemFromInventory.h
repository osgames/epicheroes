/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _REMOVE_ITEM_FROM_INVENTORY_H_
#define _REMOVE_ITEM_FROM_INVENTORY_H_

#include "Command/Backend/Game/GameCommand.h"
#include "Object/Item/Inventory/InventoryModel.h"
#include "Object/ObjectCharacter.h"

/** \addtogroup Commands
  * \{
  * \class RemoveItemFromInventory
  *
  * \brief Remove the item from a given inventory.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class RemoveItemFromInventory : public GameCommand
{
    Q_OBJECT

private:
    ObjectCharacter *       character;
    int                     index;
    InventoryModel::Type    inventoryType;

public:
    RemoveItemFromInventory(ObjectCharacter *character, int index, InventoryModel::Type inventoryType, QObject *parent = 0);

    virtual bool execute();
};

#endif // _REMOVE_ITEM_FROM_INVENTORY_H_
