/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PreloadWorld.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QDir>

#include "Common/FileNameConventions.h"
#include "Common/Common.h"

PreloadWorld::PreloadWorld(QObject *parent)
    : GameCommand(parent), fileManager(parent)
{
    this->setObjectName("PreloadWorld");
}

bool PreloadWorld::execute()
{
    QDir worldDir(QDir::current());
    if(!worldDir.cd(TEoH::WORLD_FOLDER_NAME))
    {
        worldDir.mkdir(TEoH::WORLD_FOLDER_NAME);
        worldDir.cd(TEoH::WORLD_FOLDER_NAME);
    }

    QString worldFilePath = QFileDialog::getOpenFileName
            (
                0,
                tr("Choose a New World to play in..."),
                worldDir.absolutePath(),
                tr("TEoH World Files (*").append(TEoH::WORLD_SUFFIX).append(")")
            );

    if(worldFilePath.isEmpty())
    {
        TEoH::writeLogFail("No file was specified. The string where the path should've been was empty.");
        return false;
    }

    this->worldModel->setWorldFileInfo(QFileInfo(worldFilePath));

    if(!this->fileManager.deserialize(this->worldModel, this->worldModel->refWorldFileInfo()->absoluteFilePath()))
    {
        QMessageBox::critical(0, tr("Failed to load a world..."), tr("Loading a world has failed."));
        TEoH::writeLogFail("The loading process of the world failed.");
        return false;
    }

    return true;
}
