/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "FindPath.h"

#include <iostream>

FindPath::FindPath(StackModel *from, StackModel *to, QList<StackModel *> checkpoints, QObject *parent)
    : GameCommand(parent), checkpoints(checkpoints)
{
    this->setObjectName("FindPath");
    this->from = from;
    this->to = to;
}

bool FindPath::execute()
{
    this->setErrorEnabled(false);
    if(!this->from || !this->to || !this->gameModel->isYourTurn() || !this->to->isAccessible() ||
       (this->from == this->to && this->checkpoints.isEmpty()))
    {
        return false;
    }

    this->aStarMap = AStarMap();
    this->aStarMap.fromMapModel(this->mapModel, this->mapModel->refPlayerStack(this->gameModel->getPlayerID())->refCharacter());

    QList<QPoint> checkpointPositions;

    checkpointPositions.append(this->from->getSquarePos());
    for(int i = 0; i < this->checkpoints.size(); ++i)
    {
        checkpointPositions.append(this->checkpoints.at(i)->getSquarePos());
    }
    checkpointPositions.append(this->to->getSquarePos());

    QVector<AStarMapEntry> path, nextPath;

    for(int i = 0; i < checkpointPositions.size()-1; ++i)
    {
        if(!this->aStarMap.calculatePath(checkpointPositions.at(i), checkpointPositions.at(i+1))){
            return false;
        }

        nextPath = this->aStarMap.getPath();

        if(!path.isEmpty())
        {
            path.removeLast();
            if(!nextPath.isEmpty())
            {
                nextPath.first().setCheckpoint(true);
            }
        }

        for(int j = 0; j < nextPath.size(); ++j)
        {
            path.append(nextPath.at(j));
        }
    }

    ObjectCharacter *character = this->from->refCharacter();
    MapModel::ConvertedPath convertedPath(this->mapModel, character, path);

    if(!convertedPath.canMoveDistance(character))
    {
        return false;
    }

    this->mapModel->setConvertedPath(convertedPath);

//    QString output = QString("Path: [%1,%2] -> ").arg(path.first().getPosition().x())
//                                                 .arg(path.first().getPosition().y());
//    for(int i = 1; i < path.size()-1; ++i)
//    {
//        output.append(QString("(%1,%2) -> ").arg(path.at(i).getPosition().x())
//                                            .arg(path.at(i).getPosition().y()));
//    }
//    output.append(QString("[%1,%2]").arg(path.last().getPosition().x())
//                                    .arg(path.last().getPosition().y()));
//    qDebug() << output;

    return true;
}
