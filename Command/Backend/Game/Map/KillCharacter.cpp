/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "KillCharacter.h"

#include "Object/Item/Corpse.h"
#include "Command/Backend/Object/Item/DropAllEquipmentItems.h"

KillCharacter::KillCharacter(ObjectCharacter *target, QObject *parent)
    : GameCommand(parent)
{
    this->setObjectName("KillCharacter");
    this->target = target;
}

bool KillCharacter::execute()
{
    if(!this->target)
    {
        return false;
    }

    if(!this->processor->execute(new DropAllEquipmentItems(this->target)))
    {
        return false;
    }

    ObjectCharacter *oldTarget = this->target->copy();
    oldTarget->setParent(this);

    ItemContainer container(oldTarget->refInventoryModel()->getBackpack());

    ObjectItem *corpse = new Corpse(oldTarget, container);
    corpse->setSpawnID(this->worldModel->refObjectID()->getNewSpawnIDFor(corpse->getObjectID()));

    if(!this->gameModel->removeCharacter(this->gameModel->getPlayerID(),
                                         this->mapModel,
                                         *oldTarget) ||
          !this->gameModel->spawnItem(this->gameModel->getPlayerID(),
                                      this->mapModel,
                                      *corpse))
    {
        return false;
    }

    this->gameModel->showNewMessage(this->gameModel->getPlayerID(),
                                    QString("{\"%1 died.\" : []}").arg(oldTarget->getObjectName()),
                                    this->mapModel->getPlayerIDsOnMap());

    return true;
}


