/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CommandNode.h"

CommandNode::CommandNode(const QString &name, bool removeIfUnavailable, QObject *parent)
    : CommandTree(name, parent), removeIfUnavailable(removeIfUnavailable)
{}

void CommandNode::append(const QList<CommandTree *> &commandTrees)
{
    for(int i = 0; i < commandTrees.size(); ++i)
    {
        this->append(commandTrees[i]);
    }
    return;
}

void CommandNode::append(CommandTree *commandTree)
{
    commandTree->setParent(this);
    this->list.append(commandTree);
    return;
}

CommandTree::Type CommandNode::getCommandTreeType() const
{
    return CommandTree::NODE;
}

QList<CommandTree *> CommandNode::getList() const
{
    return this->list;
}

bool CommandNode::doRemoveIfUnavailable() const
{
    return this->removeIfUnavailable;
}
