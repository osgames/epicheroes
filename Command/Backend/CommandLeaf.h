/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMANDLEAF_H
#define COMMANDLEAF_H

#include "Command/Backend/CommandTree.h"
#include "Command/Backend/BaseCommand.h"

/** \addtogroup Commands
  * \{
  * \class CommandLeaf
  *
  * \brief A leaf containing an actual command in the command tree.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class CommandLeaf : public CommandTree
{
    Q_OBJECT

    BaseCommand *command;
    bool removeIfUnavailable;

public:
    CommandLeaf(BaseCommand *command, const QString &name, bool removeIfUnavailable = false, QObject *parent = 0);

    virtual CommandTree::Type getCommandTreeType() const;
    bool doRemoveIfUnavailable() const;

    BaseCommand *refCommand() const;
};

#endif // COMMANDLEAF_H
