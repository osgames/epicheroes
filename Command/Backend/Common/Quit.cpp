/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Quit.h"

#include <QDir>
#include <QFileInfo>
#include <QCoreApplication>
#include <QMessageBox>

#include "Command/Backend/Editor/AskForSave.h"
#include "Command/Backend/Common/RemoveFolder.h"
#include "Command/Backend/Game/Ingame/SaveGame.h"
#include "Common/FileNameConventions.h"
#include "Common/Common.h"
#include "MainWindow.h"

Quit::Quit(bool askToSaveGame, QObject *parent)
    : CommonCommand(parent), askToSaveGame(askToSaveGame)
{
    this->setObjectName("Quit");
}

bool Quit::execute()
{
    if(this->editorModel->isEditor())
    {
        if(this->processor->execute(new AskForSave()))
        {
            TEoH::done = true;
                TEoH::quit = true;
            QCoreApplication::quit();
        }
    }
    else
    {
        if(this->askToSaveGame && this->gameModel)
        {
            QMessageBox messageBox;
            messageBox.setWindowTitle(tr("Quit the Game?"));
            messageBox.setIcon(QMessageBox::Question);

            QPushButton *saveQuit = 0;
            QPushButton *quit = 0;

            if(this->gameModel->getGameType() == TEoH::SOLO_HOTSEAT ||
               (this->gameModel->getGameType() == TEoH::NETWORK && this->networkModel->isServer()))
            {
                messageBox.setText(tr("Do you want to save the game before quitting?"));
                saveQuit = messageBox.addButton(tr("Save and Quit"), QMessageBox::AcceptRole);
                quit = messageBox.addButton(tr("Quit"), QMessageBox::DestructiveRole);
                messageBox.addButton(tr("Cancel"), QMessageBox::RejectRole);
            }
            else
            {
                messageBox.setText(tr("Do you really want to quit?\nUnsaved progress will be lost."));
                quit = messageBox.addButton(tr("Quit"), QMessageBox::DestructiveRole);
                messageBox.addButton(tr("Cancel"), QMessageBox::RejectRole);
            }

            messageBox.exec();

            if(messageBox.clickedButton() == saveQuit)
            {
                if(!this->processor->execute(new SaveGame()))
                {
                    return false;
                }
            }
            else
            {
                if(messageBox.clickedButton() != quit)
                {
                    return false;
                }
            }
        }

        if(this->gameModel && this->gameModel->getGameType() == TEoH::NETWORK && this->networkModel && this->networkModel->isClient())
        {
            this->gameModel->showNewMessage(tr("{\"Player %1 left.\" : []}").arg(this->gameModel->getPlayerID()));
        }

        TEoH::done = true;
        TEoH::quit = true;
        QCoreApplication::quit();
    }

    return true;
}
