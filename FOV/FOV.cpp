#include "FOV.h"

#include <QDebug>
#include <iostream>

FOV::FOV(const MapModel *mapModel, int x, int y, int z, int reach, double restriction)
    : width(mapModel->getHSquareCount()), height(mapModel->getVSquareCount()),
      sourceSquare(FOVPoint(x,y + 1.0),
                   FOVPoint(x + 1.0, y)),
      z(z),
      reach(reach),
      restriction(0.025*restriction),
      relativeSourceSquare(FOVPoint(0.0,1.0),FOVPoint(1.0,0.0))
{
    for(int x = 0; x < this->width; ++x)
    {
        this->map.append(QList<char>());
        this->visibleMask.append(QList<bool>());
        for(int y = 0; y < this->height; ++y)
        {
            if(mapModel->refStackModel(x,y,this->z) && mapModel->refStackModel(x,y,this->z)->isBlockingView())
            {
                this->map.last().append(FOVMap::WALL);
            }
            else
            {
                this->map.last().append(FOVMap::FREE);
            }
            this->visibleMask.last().append(false);
        }
    }

    this->refSquare(this->getSourcePos()) = FOVMap::SOURCE;
}

void FOV::calculateQuadrantFOV()
{
    int x = 0;
    int y = 0;
    (*this->quadrantVisibleMask[x][y]) = true;

    // View preperation for top-right quadrant.
    this->views.clear();
    this->bumps.clear();
    views.push_back(ViewLines(FOVLine(FOVPoint(x+0.5+this->restriction, y+0.5-this->restriction),
                              FOVPoint(x+1.0, this->quadrantMap[x].size())),
                              FOVLine(FOVPoint(x+0.5-this->restriction, y+0.5+this->restriction),
                              FOVPoint(this->quadrantMap.size(), y+1.0))));

    // Iterate through the top-right quadrant diagonal from left to bottom-right.
    for(int i = 1; i < (qMax(this->quadrantMap.front().size(), this->quadrantMap.back().size()) + this->quadrantMap.size()) && this->views.size() > 0; ++i)
    {
        x = 0;
        y = i;

        while(y >= 0)
        {
            if(y >= 0 && x >= 0 && x < this->quadrantMap.size() && y < this->quadrantMap[x].size() && FOVPoint(0,0).inReachOf(FOVPoint(x,y),this->reach))
            {
                // Checks for algorithm.
                FOVRectangle square = FOVRectangle(FOVPoint(x,y+1.0), FOVPoint(x+1.0,y));
                if((*this->quadrantMap[x][y]) == FOVMap::WALL)
                {
                    for(int v = 0; v < this->views.size(); ++v)
                    {
                        if(square.blocksBoth(this->views[v].steep, this->views[v].shallow))
                        {
                            v = this->blocksBothCase(v, x, y);
                        }
                        else if(square.isIntersectedBy(this->views[v].steep))
                        {
                            v = this->bump(v, x, y, square, ViewLines::STEEP);
                        }
                        else if(square.isIntersectedBy(this->views[v].shallow))
                        {
                            v = this->bump(v, x, y, square, ViewLines::SHALLOW);
                        }
                        else if(square.isInBetween(this->views[v].steep, this->views[v].shallow))
                        {
                            v = this->inBetweenView(v, x, y, square);
                        }
                    }
                }
                else
                {
                    for(int v = 0; v < views.size(); ++v)
                    {
                        if(square.isIntersectedBy(this->views[v].steep) ||
                           square.isIntersectedBy(this->views[v].shallow) ||
                           square.isInBetween(this->views[v].steep, this->views[v].shallow))
                        {
                            (*this->quadrantVisibleMask[x][y]) = true;
                        }
                    }
                }
            }

            x++;
            y--;
        }
    }

    return;
}

void FOV::calculateFOV(FOV::ViewDirection direction)
{
    // NOTE: Threading easy by leting implementing mutex for access and leting those quadrant run in 4 threads.
    // Top-Right quadrant.
    if(((direction & FOV::N) > 0 || (direction & FOV::E) > 0) &&
       !(direction == FOV::SE || (direction == FOV::NW)))
    {
        for(int x = this->sourceSquare.getBLeft().x; x < this->map.size(); ++x)
        {
            this->quadrantMap.append(QList<char *>());
            this->quadrantVisibleMask.append(QList<bool *>());
            for(int y = this->sourceSquare.getBLeft().y; y < this->map[x].size(); ++y)
            {
                int ox = x - this->sourceSquare.getBLeft().x;
                int oy = y - this->sourceSquare.getBLeft().y;
                if(((direction & FOV::N) > 0 && qAbs(ox) <= qAbs(oy)) ||
                   ((direction & FOV::E) > 0 && qAbs(ox) >= qAbs(oy)))
                {
                    this->quadrantMap.last().append(&this->map[x][y]);
                    this->quadrantVisibleMask.last().append(&this->visibleMask[x][y]);
                }
                else
                {
                    this->quadrantMap.last().append(new char(this->map[x][y]));
                    this->quadrantVisibleMask.last().append(new bool(this->visibleMask[x][y]));
                }
            }
        }


        this->calculateQuadrantFOV();

        this->quadrantMap.clear();
        this->quadrantVisibleMask.clear();
    }

    // Bottom-Right Quadrant
    if(((direction & FOV::S) > 0 || (direction & FOV::E) > 0) &&
       !(direction == FOV::NE || (direction == FOV::SW)))
    {
        for(int x = this->sourceSquare.getBLeft().x; x < this->map.size(); ++x)
        {
            this->quadrantMap.append(QList<char *>());
            this->quadrantVisibleMask.append(QList<bool *>());
            for(int y = this->sourceSquare.getBLeft().y; y >= 0; --y)
            {
                int ox = x - this->sourceSquare.getBLeft().x;
                int oy = y - this->sourceSquare.getBLeft().y;
                if(((direction & FOV::S) > 0 && qAbs(ox) <= qAbs(oy)) ||
                   ((direction & FOV::E) > 0 && qAbs(ox) >= qAbs(oy)))
                {
                    this->quadrantMap.last().append(&this->map[x][y]);
                    this->quadrantVisibleMask.last().append(&this->visibleMask[x][y]);
                }
                else
                {
                    this->quadrantMap.last().append(new char(this->map[x][y]));
                    this->quadrantVisibleMask.last().append(new bool(this->visibleMask[x][y]));
                }
            }
        }

        this->calculateQuadrantFOV();

        this->quadrantMap.clear();
        this->quadrantVisibleMask.clear();
    }

    // Top-Left Quadrant
    if(((direction & FOV::N) > 0 || (direction & FOV::W) > 0) &&
       !(direction == FOV::NE || (direction == FOV::SW)))
    {
        for(int x = this->sourceSquare.getBLeft().x; x >= 0; --x)
        {
            this->quadrantMap.append(QList<char *>());
            this->quadrantVisibleMask.append(QList<bool *>());
            for(int y = this->sourceSquare.getBLeft().y; y < this->map[x].size(); ++y)
            {
                int ox = x - this->sourceSquare.getBLeft().x;
                int oy = y - this->sourceSquare.getBLeft().y;
                if(((direction & FOV::N) > 0 && qAbs(ox) <= qAbs(oy)) ||
                   ((direction & FOV::W) > 0 && qAbs(ox) >= qAbs(oy)))
                {
                    this->quadrantMap.last().append(&this->map[x][y]);
                    this->quadrantVisibleMask.last().append(&this->visibleMask[x][y]);
                }
                else
                {
                    this->quadrantMap.last().append(new char(this->map[x][y]));
                    this->quadrantVisibleMask.last().append(new bool(this->visibleMask[x][y]));
                }
            }
        }

        this->calculateQuadrantFOV();

        this->quadrantMap.clear();
        this->quadrantVisibleMask.clear();
    }

    // Bottom-Left Quadrant
    if(((direction & FOV::S) > 0 || (direction & FOV::W)) > 0 &&
       !(direction == FOV::NW || (direction == FOV::SE)))
    {
        for(int x = this->sourceSquare.getBLeft().x; x >= 0; --x)
        {
            this->quadrantMap.append(QList<char *>());
            this->quadrantVisibleMask.append(QList<bool *>());
            for(int y = this->sourceSquare.getBLeft().y; y >= 0; --y)
            {
                int ox = x - this->sourceSquare.getBLeft().x;
                int oy = y - this->sourceSquare.getBLeft().y;
                if(((direction & FOV::S) > 0 && qAbs(ox) <= qAbs(oy)) ||
                   ((direction & FOV::W) > 0 && qAbs(ox) >= qAbs(oy)))
                {
                    this->quadrantMap.last().append(&this->map[x][y]);
                    this->quadrantVisibleMask.last().append(&this->visibleMask[x][y]);
                }
                else
                {
                    this->quadrantMap.last().append(new char(this->map[x][y]));
                    this->quadrantVisibleMask.last().append(new bool(this->visibleMask[x][y]));
                }
            }
        }

        this->calculateQuadrantFOV();
     }

    return;
}

void FOV::printMap() const
{
    QString line;

    if(this->map.isEmpty())
    {
        return;
    }

    for(int y = 0; y < this->map.first().size(); ++y)
    {
        line.clear();
        for(int x = 0; x < this->map.size(); ++x)
        {
            if(this->visibleMask[x][y])
            {
                line.append(map[x][y]);
            }
            else
            {
                line.append('X');
            }
        }
        std::cout << line.toStdString() << std::endl;
    }

    return;
}

void FOV::applyToMap(MapModel *mapModel, ObjectBase *object) const
{
    StackModel *stackModel;
    for(int y = 0; y < this->map.first().size(); ++y)
    {
        for(int x = 0; x < this->map.size(); ++x)
        {
            stackModel = mapModel->refStackModel(x,y,this->z);
            stackModel->notLookedAtBy(object);
            if(this->visibleMask[x][y])
            {
                stackModel->lookedAtBy(object);
            }
        }
    }
    return;
}

int FOV::blocksBothCase(int v, int x, int y)
{
    (*this->quadrantVisibleMask[x][y]) = true;
    this->views.removeAt(v);
    v--;
    return v;
}

int FOV::bump(int v, int x, int y, FOVRectangle square, ViewLines::LineType type)
{
    (*this->quadrantVisibleMask[x][y]) = true;

    if(type == ViewLines::STEEP)
    {
        this->views[v].steep.p2 = square.getBRight();
    }
    else
    {
        this->views[v].shallow.p2 = square.getTLeft();
    }

    for(int b = 0; b < this->bumps.size(); ++b)
    {
        if(type == ViewLines::STEEP)
        {
            if(this->bumps[b].isIntersectedBy(this->views[v].steep))
            {
                this->views[v].steep.p1 = this->bumps[b].getTLeft();
            }
        }
        else
        {
            if(this->bumps[b].isIntersectedBy(this->views[v].shallow))
            {
                this->views[v].shallow.p1 = this->bumps[b].getBRight();
            }
        }
    }

    this->bumps.append(square);

    if((this->views[v].steep.hasPointOn(this->relativeSourceSquare.getTLeft()) &&
        this->views[v].shallow.hasPointOn(this->relativeSourceSquare.getTLeft())) ||
       (this->views[v].steep.hasPointOn(this->relativeSourceSquare.getBRight()) &&
        this->views[v].shallow.hasPointOn(this->relativeSourceSquare.getBRight())))
    {
        this->views.removeAt(v);
        v--;
    }

    return v;
}

int FOV::inBetweenView(int v, int x, int y, FOVRectangle square)
{
    (*this->quadrantVisibleMask[x][y]) = true;
    ViewLines newView(FOVLine(this->views[v].steep.p1, square.getBRight()), this->views[v].shallow);

    if(!(this->views[v].steep.hasPointOn(this->relativeSourceSquare.getTLeft()) &&
         this->views[v].shallow.hasPointOn(this->relativeSourceSquare.getTLeft())) &&
       !(this->views[v].steep.hasPointOn(this->relativeSourceSquare.getBRight()) &&
         this->views[v].shallow.hasPointOn(this->relativeSourceSquare.getBRight())))
    {
        this->views.append(newView);
    }

    this->views[v] = ViewLines(this->views[v].steep, FOVLine(this->views[v].shallow.p1, square.getTLeft()));

    if((this->views[v].steep.hasPointOn(this->relativeSourceSquare.getTLeft()) &&
        this->views[v].shallow.hasPointOn(this->relativeSourceSquare.getTLeft())) ||
       (this->views[v].steep.hasPointOn(this->relativeSourceSquare.getBRight()) &&
        this->views[v].shallow.hasPointOn(this->relativeSourceSquare.getBRight())))
    {
        this->views.removeAt(v);
        v--;
    }

    return v;
}
