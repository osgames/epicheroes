#ifndef _PRFOV_MAP_H_
#define _PRFOV_MAP_H_

#include <QList>
#include <QPointF>
#include <qmath.h>

#include "MainView/Model/MapModel.h"
#include "Object/ObjectBase.h"

struct FOVPoint
{
    qreal x,y;

    FOVPoint(qreal x, qreal y) : x(x), y(y) {}

    qreal directDistance(const FOVPoint &other)
    {
        qreal distance = 0;
        bool secondDiagonal = false;
        qreal x = this->x - other.x;
        qreal y = this->y - other.y;

        while(x != 0 && y != 0)
        {
            x > 0 ? --x : ++x;
            y > 0 ? --y : ++y;

            secondDiagonal ? distance += 10 : distance += 5;

            secondDiagonal = !secondDiagonal;
        }

        distance += 5*qAbs(x);
        distance += 5*qAbs(y);

        return distance;
    }

    bool inReachOf(const FOVPoint &other, qreal reach)
    {
        if(reach <= 0)
        {
            return false;
        }

        return this->directDistance(other) <= reach;
    }
};

struct FOVLine
{
    FOVPoint p1;
    FOVPoint p2;

    FOVLine(FOVPoint p1, FOVPoint p2) : p1(p1), p2(p2)
    {}

    // Return < 0.0, if its left as seen from the line's direction,
    // = 0.0 if it's on the line or > 0.0 if it's right as seen from the line's direction.
    double relativeSlope(const FOVPoint &p) const
    {
        return (((this->p2.x - this->p1.x) * (p.y - this->p1.y)) -
                ((this->p2.y - this->p1.y) * (p.x - this->p1.x)));
    }

    bool hasPointAbove(const FOVPoint &p) const
    {
        return this->relativeSlope(p) > 0;
    }

    bool hasPointOn(const FOVPoint &p) const
    {
        return this->relativeSlope(p) == 0;
    }

    bool hasPointBelow(const FOVPoint &p) const
    {
        return this->relativeSlope(p) < 0;
    }

    bool isColinearTo(const FOVLine &l2) const
    {
        return this->hasPointOn(l2.p1) && this->hasPointOn(l2.p2);
    }
};

class FOVRectangle
{
    FOVPoint tLeft;
    FOVPoint bRight;

public:
    FOVRectangle(FOVPoint tLeft, FOVPoint bRight) : tLeft(tLeft), bRight(bRight) {}

    FOVPoint getTLeft() const { return this->tLeft; }
    FOVPoint getTRight() const { return FOVPoint(this->bRight.x, this->tLeft.y); }
    FOVPoint getBLeft() const { return FOVPoint(this->tLeft.x, this->bRight.y); }
    FOVPoint getBRight() const { return this->bRight; }

    bool isOutsideOf(const FOVLine &l) const
    {
        return l.hasPointBelow(this->getTLeft()) && l.hasPointAbove(this->getBRight());
    }

    bool isIntersectedBy(const FOVLine &l) const
    {
        return l.hasPointAbove(this->getTLeft()) && l.hasPointBelow(this->getBRight());
    }

    bool isInBetween(const FOVLine &l1, const FOVLine &l2) const
    {
        return (l1.hasPointBelow(this->getTLeft()) || l1.hasPointOn(this->getTLeft())) &&
               (l2.hasPointAbove(this->getBRight()) || l2.hasPointOn(this->getBRight()));
    }

    bool blocksBoth(const FOVLine &l1, const FOVLine &l2) const
    {
        return (l1.hasPointAbove(this->getTLeft())) && (l2.hasPointBelow(this->getBRight()));
    }
};

namespace FOVMap
{
    const char SOURCE = '@';
    const char FREE = '.';
    const char WALL = '#';
}

struct ViewLines
{
    enum LineType {STEEP = 0, SHALLOW};

    FOVLine steep;
    FOVLine shallow;

    ViewLines(const FOVLine &steep, const FOVLine &shallow) : steep(steep), shallow(shallow) {}
};

class FOV
{
public:
    //enum ViewDirection {NONE=0, N=1,S=2,NS=3,W=4,NW=5,SW=6,SWN=7,E=8,NE=9,SE=10,SEN=11,WE=12,WEN=13,WES=14,ALL_AROUND=15};
    enum ViewDirection {NONE=0, N=1,S=2,W=4,NW=5,SW=6,E=8,NE=9,SE=10,ALL_AROUND=15};

private:
    static const int STANDARD_RESTRICTION = 5;

    QList<QList<char> > map;
    QList<QList<char *> > quadrantMap;
    QList<QList<bool> > visibleMask;
    QList<QList<bool *> > quadrantVisibleMask;
    QList<ViewLines> views;
    QList<FOVRectangle> bumps;

    int width;
    int height;
    FOVRectangle sourceSquare;
    int z;
    int reach;
    double restriction;
    const FOVRectangle relativeSourceSquare;

public:
    FOV(const MapModel *mapModel, int x, int y, int z, int reach, double restriction = STANDARD_RESTRICTION);

private:
    void calculateQuadrantFOV();

public:
    void calculateFOV(FOV::ViewDirection direction = ALL_AROUND);

    void addWall(int x, int y) {this->map[x][y] = FOVMap::WALL; return;}
    void removeWall(int x, int y) {this->map[x][y] = FOVMap::FREE; return;}

    void printMap() const;

    void applyToMap(MapModel *mapModel, ObjectBase *object) const;

    void setSource(int x, int y)
    {
        this->sourceSquare = FOVRectangle(FOVPoint(x,y + 1.0),FOVPoint(x + 1.0,y));
        return;
    }

private:
    int blocksBothCase(int v, int x, int y);
    int bump(int v, int x, int y, FOVRectangle square, ViewLines::LineType type);
    int inBetweenView(int v, int x, int y, FOVRectangle square);

    static FOVRectangle getSquareRect(int x, int y){return FOVRectangle(FOVPoint(x,y+1.0), FOVPoint(x+1.0,y));}
    static FOVRectangle getSquareRect(const FOVPoint &p){return FOV::getSquareRect(p.x,p.y);}
    FOVPoint getSourcePos() const {return this->sourceSquare.getBLeft();}

    char &refSquare(int x, int y) {return this->map[x][y];}
    char &refSquare(const FOVPoint &p) {return this->refSquare(p.x,p.y);}
};

#endif // _PRFOV_MAP_H_
