/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "WaitingForDataDialog.h"

WaitingForDataDialog::WaitingForDataDialog(NetworkModel *networkModel, QObject *parent)
    : QObject(parent),
      messageBox(QMessageBox::Information,
                 tr("Receving data from Server..."),
                 tr("Waiting for data from the server...\nPress cancel to stop waiting..."),
                 QMessageBox::Cancel)
{
    this->networkModel = networkModel;
    this->messageBox.setModal(true);
    this->messageBox.setCursor(Qt::BusyCursor);
}

WaitingForDataDialog::WaitingForDataDialog(NetworkModel *networkModel, const QString &title, const QString &text, QObject *parent)
    : QObject(parent),
      messageBox(QMessageBox::Information,
                 title,
                 text,
                 QMessageBox::Cancel)
{
    this->networkModel = networkModel;
    this->messageBox.setModal(true);
    this->messageBox.setCursor(Qt::BusyCursor);
}

bool WaitingForDataDialog::waitFor(FromClient fromClient)
{
    if(!this->networkModel || this->networkModel->isClient())
    {
        return false;
    }

    this->fromClient = fromClient;
    this->hasBeenReceived = false;

    connect(networkModel, SIGNAL(allDataReceived(FromClient)), this, SLOT(dataReceived(FromClient)));

    this->messageBox.exec();

    if(!this->hasBeenReceived)
    {
        qDebug("Canceled the waiting dialog.");
    }

    return this->hasBeenReceived;
}

bool WaitingForDataDialog::waitFor(FromServer fromServer)
{
    if(!this->networkModel || this->networkModel->isServer())
    {
        return false;
    }

    this->fromServer = fromServer;
    this->hasBeenReceived = false;

    connect(networkModel, SIGNAL(allDataReceivedFromServer(FromServer)), this, SLOT(dataReceived(FromServer)));

    this->messageBox.exec();

    if(!this->hasBeenReceived)
    {
        qDebug("Canceled the waiting dialog.");
    }

    return this->hasBeenReceived;
}


void WaitingForDataDialog::dataReceived(FromClient fromClient)
{
    this->hasBeenReceived = this->fromClient == fromClient;

    if(this->hasBeenReceived)
    {
        this->messageBox.accept();
    }

    return;
}

void WaitingForDataDialog::dataReceived(FromServer fromServer)
{
    this->hasBeenReceived = this->fromServer == fromServer;

    if(this->hasBeenReceived)
    {
        this->messageBox.accept();
    }

    return;
}

