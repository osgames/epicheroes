/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WAITING_FOR_DATA_DIALOG_H
#define WAITING_FOR_DATA_DIALOG_H

#include <QObject>
#include <QMessageBox>

#include "Network/NetworkModel.h"

/** \addtogroup GUI
  * \{
  * \class WaitingForDataDialog
  *
  * \brief Shown while a given networkmodel waits for data to be received.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class WaitingForDataDialog : public QObject
{
    Q_OBJECT

    NetworkModel *  networkModel;

    FromClient      fromClient;         ///< Wait for a specific fromClient enum from the Client.
    FromServer      fromServer;         ///< Wait for a specific fromServer enum from the Server.

    QMessageBox     messageBox;         ///< The message box informing about what to wait for and giving the possibility to cancel the wait.
    bool            hasBeenReceived;    ///< True, if the waited for enum was received.

public:
    WaitingForDataDialog(NetworkModel *networkModel, QObject *parent = 0);
    WaitingForDataDialog(NetworkModel *networkModel, const QString &title, const QString &text, QObject *parent = 0);

    /**
      * @brief Wait for a given fromClient enum to be received.
      * @param fromClient The fromClient enum to be received.
      * @return True, if the waited for fromClient enum was received. Otherwise, false.
      */
    bool waitFor(FromClient fromClient);

    /**
      * @brief Wait for a given fromServer enum to be received.
      * @param fromServer The fromServer enum to be received.
      * @return True, if the waited for fromServer enum was received. Otherwise, false.
      */
    bool waitFor(FromServer fromServer);

private slots:
    void dataReceived(FromClient fromClient);
    void dataReceived(FromServer fromServer);
};

#endif // WAITING_FOR_REPLY_DIALOG_H
