/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "MainWindow.h"

#include <QCoreApplication>
#include <QFileInfo>
#include <QDockWidget>
#include <QMessageBox>
#include <QPushButton>
#include <QFileDialog>
#include <QString>
#include <QDesktopWidget>
#include <QApplication>
#include <QHeaderView>

#include "Command/Backend/Editor/World/NewWorld.h"
#include "Command/Backend/Editor/World/LoadWorld.h"
#include "Command/Backend/Editor/World/SaveWorld.h"
#include "Command/Backend/Common/Quit.h"
#include "Command/Backend/Game/Startmenu/LoadGame.h"
#include "Command/Backend/Game/Startmenu/SetupNewGame.h"
#include "Command/Backend/Game/Startmenu/ResumeEMailGame.h"
#include "Command/Backend/Game/Startmenu/ConnectTo.h"
#include "Game/Model/ClientGameModel.h"
#include "Game/Model/EMailGameModel.h"
#include "Common/YesNoCancelDialog.h"
#include "Common/Common.h"

#include <iostream>

namespace TEoH
{
    bool done = false;
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    // Initialize MainWindow.
    init();
}

void MainWindow::init()
{
    this->overviewDock = 0;

    QWidget *w = new QWidget(this);
    this->centralWidgetLayout = new QGridLayout(w);
    this->centralWidgetLayout->setAlignment(Qt::AlignHCenter);
    w->setLayout(this->centralWidgetLayout);
    this->setCentralWidget(w);

    this->editorModel = new EditorModel(QCoreApplication::arguments().contains("-e"), this);
    this->setObjectName(QString::fromUtf8("mainWindow"));

    TEoH::writeLogDone();

    this->processor = new Processor(this);
    this->processor->setEditorModel(this->editorModel);
    this->done = false;

    // Initialize the Editor or Game.
    // NOTE: The mapModel will be initialized in here as well as a standard map model.
    if(this->editorModel->isEditor())
    {
        while(!TEoH::done && !this->initializeEditor())
        {
        }
    }
    else
    {
        while(!TEoH::done && !this->initializeGame())
        {
        }
    }

    if(TEoH::done)
    {
        return;
    }

    TEoH::writeLog("Initialize MapView");

    this->mainViewMap = 0;

    TEoH::writeLogDone();

    // Initialize the Editor Toolbox, if the editor flag is set.
    if(this->editorModel->isEditor())
    {
        TEoH::writeLog("Initialize Editor");

        this->editorMenu = new EditorMenu(this->processor, this->editorModel, this->worldModel, this);
        this->setMenuBar(this->editorMenu);

        this->worldView = new WorldView(this->processor, this->worldModel, this);
        this->worldViewDock = new QDockWidget(this);
        this->worldViewDock->setWindowTitle(tr(" World Manager"));
        this->worldViewDock->setFeatures(QDockWidget::DockWidgetFloatable);
        this->worldViewDock->setAllowedAreas(Qt::LeftDockWidgetArea);
        this->worldViewDock->setWidget(this->worldView);
        this->worldViewDock->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        this->addDockWidget(Qt::LeftDockWidgetArea, this->worldViewDock);

        this->editorTools = new EditorTools(this->processor, this->editorModel, this);
        this->editorToolsDock = new QDockWidget(this);
        this->editorToolsDock->setWindowTitle(tr("Editor Tools"));
        this->editorToolsDock->setFeatures(QDockWidget::DockWidgetFloatable);
        this->editorToolsDock->setAllowedAreas(Qt::RightDockWidgetArea);
        this->editorToolsDock->setWidget(this->editorTools);
        this->addDockWidget(Qt::RightDockWidgetArea, this->editorToolsDock);

        connect(this, SIGNAL(windowSizeChanged()), this->worldView, SLOT(resizeWorldView()));
        connect(this->worldViewDock, SIGNAL(topLevelChanged(bool)), this, SLOT(updateEditorSize()));
        connect(this->editorToolsDock, SIGNAL(topLevelChanged(bool)), this, SLOT(updateEditorSize()));
        connect(this->processor, SIGNAL(commandProcessed()), this, SLOT(updateWindowTitle()));

        this->mapModel->deleteLater();
        this->setNewMap(new MapModel(this->processor, this));

        TEoH::writeLogDone();
    }
    else
    {
        this->editorTools = 0;
        this->editorMenu = 0;
        this->worldView = 0;

        connect(this->gameModel, SIGNAL(setNewMapSignal(MapModel *)), this, SLOT(setNewMap(MapModel*)));

        qDebug() << QString("Your ID: %1").arg(QString::number(this->gameModel->getPlayerID()));

        this->overviewWidget = new QWidget(this);
        this->overviewLayout = new QGridLayout(this->overviewWidget);
        this->playerWidget = new PlayerWidget(this->processor, true, this->overviewWidget);
        this->messageWidget = new MessageWidget(this->gameModel, this->overviewWidget);
        this->overviewLayout->addWidget(this->playerWidget,0,0);
        this->overviewLayout->addWidget(this->messageWidget,1,0);
        this->overviewLayout->setSpacing(0);
        this->overviewLayout->setMargin(0);
        this->overviewWidget->setLayout(this->overviewLayout);

        this->overviewDock = new QDockWidget(this);
        this->overviewDock->setWindowTitle(tr("(Character Overview)"));
        this->overviewDock->setFeatures(QDockWidget::DockWidgetFloatable);
        this->overviewDock->setAllowedAreas(Qt::RightDockWidgetArea);
        this->overviewDock->setWidget(this->overviewWidget);
        this->addDockWidget(Qt::RightDockWidgetArea, this->overviewDock);
        this->overviewDock->setFixedWidth(425);

        // Start Game
        if(!this->gameModel->firstTurn())
        {
            return;
        }

        this->gameModel->setInitialized(true);

        connect(this->overviewDock, SIGNAL(topLevelChanged(bool)), this, SLOT(updateGameSize()));
        connect(this->gameModel, SIGNAL(changeGameVisiblityRequest(bool)), this, SLOT(setVisible(bool)));
        connect(this->gameModel, SIGNAL(playerCharacterUpdated(ObjectCharacter *)), this, SLOT(updatePlayerOverview(ObjectCharacter *)));

        this->updateGameSize();
        this->updatePlayerOverview(this->mapModel->refPlayerStack(this->gameModel->getPlayerID())->refCharacter());

        this->setWindowTitle(tr("The Epic of Heroes %1").arg(TEoH::TEOH_VERSION));

        this->gameModel->showNewMessageToMe("{\"### The adventure begins now! ###\" : []}");
    }

    this->setWindowIcon(QIcon(":/objects/characters/hero"));
    this->setFocusPolicy(Qt::WheelFocus);
    this->setContextMenuPolicy(Qt::NoContextMenu);

    return;
}

bool MainWindow::initializeEditor()
{
    TEoH::writeLog("Initialize World");

    QMessageBox initEditor;

    initEditor.setText(tr("Welcome to the TEoH Editor."));
    initEditor.setInformativeText(tr("I'm sorry to interrupt you, but I need to ask you one question before we can begin.\n\nShould I create a new world or load an existing one?"));
    QPushButton *newButton = initEditor.addButton(tr("New World..."), QMessageBox::ActionRole);
    QPushButton *loadButton = initEditor.addButton(tr("Load World..."), QMessageBox::ActionRole);
    QPushButton *quitButton = initEditor.addButton(tr("Quit"), QMessageBox::RejectRole);
    initEditor.setIcon(QMessageBox::Question);
    initEditor.setDefaultButton(newButton);
    int result = initEditor.exec();

    if(initEditor.clickedButton() == newButton)
    {
        TEoH::writeLogDone();

        this->worldModel = new WorldModel(this);
        this->mapModel = new MapModel(this->processor, this);
        this->processor->setWorldModel(this->worldModel);

        if(!this->processor->execute(new NewWorld()))
        {
            return false;
        }
    }
    else if(initEditor.clickedButton() == loadButton)
    {
        TEoH::writeLogDone();

        this->worldModel = new WorldModel(this);
        this->mapModel = new MapModel(this->processor, this);
        this->processor->setWorldModel(this->worldModel);

        if(!this->processor->execute(new LoadWorld(false)))
        {
            return false;
        }
    }
    else if(initEditor.clickedButton() == quitButton || result == QMessageBox::Rejected)
    {
        TEoH::writeLogDone("[EXIT]");
        this->processor->execute(new Quit());
        return true;
    }
    else
    {
        TEoH::writeLogFail("None of the expected functions to initialize the editor was used. Something went wrong.");
        return false;
    }

    this->gameModel = 0;

    return true;
}


bool MainWindow::initializeGame()
{
    TEoH::writeLog("Initialize Game");

    QMessageBox mainMenu;

    this->worldModel = new WorldModel(this);
    this->mapModel = new MapModel(this->processor, this);
    this->processor->setWorldModel(this->worldModel);

    mainMenu.setText(tr("TEoH Main Menu"));
    mainMenu.setInformativeText(tr("What do you want to do?"));
    QPushButton *newWorldButton = mainMenu.addButton(tr("Play a New World..."), QMessageBox::ActionRole);
    QPushButton *loadGameButton = mainMenu.addButton(tr("Load Game..."), QMessageBox::ActionRole);
    QPushButton *connectToButton = mainMenu.addButton(tr("Connect to..."), QMessageBox::ActionRole);
    QPushButton *resumeEMailGameButton = mainMenu.addButton(tr("Resume E-Mail Game..."), QMessageBox::ActionRole);
    QPushButton *quitButton = mainMenu.addButton(tr("Quit"), QMessageBox::RejectRole);
    mainMenu.setIcon(QMessageBox::Question);
    mainMenu.setDefaultButton(newWorldButton);

    int result = QMessageBox::Accepted;
    bool continueEMailGameFlag = QCoreApplication::arguments().contains("-m");

    if(!continueEMailGameFlag)
    {
        result = mainMenu.exec();
    }

    if(mainMenu.clickedButton() == newWorldButton)
    {
        TEoH::writeLogDone();
        if(!this->processor->execute(new SetupNewGame(&this->gameModel)))
        {
            return false;
        }

        this->gameModel->setParent(this);
        return true;
    }
    else if(mainMenu.clickedButton() == loadGameButton)
    {
        if(!this->processor->execute(new LoadGame(&this->gameModel)))
        {
            return false;
        }

        this->gameModel->setParent(this);
        return true;
    }
    else if(mainMenu.clickedButton() == connectToButton)
    {
        TEoH::writeLogDone();
        this->gameModel = new ClientGameModel(this->processor, this);
        this->processor->setGameModel(this->gameModel);
        return this->processor->execute(new ConnectTo());
    }
    else if(continueEMailGameFlag || mainMenu.clickedButton() == resumeEMailGameButton)
    {
        TEoH::writeLogDone();
        this->gameModel = new EMailGameModel(this->processor, this);
        this->processor->setGameModel(this->gameModel);
        return this->processor->execute(new ResumeEMailGame());
    }
    else if(result == QMessageBox::Rejected || mainMenu.clickedButton() == quitButton)
    {
        TEoH::writeLogDone("[EXIT]");
        this->processor->execute(new Quit());
        return true;
    }

    TEoH::writeLogFail("None of the expected functions to initialize the editor was used. Something went wrong.");
    return true;
}

void MainWindow::updateGameSize()
{
    int widthIncrement = 0;
    int heightIncrement = 0;

    if(this->overviewDock && !this->overviewDock->isFloating())
    {
        widthIncrement = widthIncrement + 425;
    }

    QSize mapSize = this->mapModel->getMapSize();

    if(mapSize.width() < TEoH::MIN_CENTRAL_WIDGET_WIDTH)
    {
        mapSize = QSize(TEoH::MIN_CENTRAL_WIDGET_WIDTH,mapSize.height());
    }

    QSize newSize = mapSize + QSize(widthIncrement,heightIncrement);
    int width = newSize.width();
    int height = newSize.height();

    if(height < 512)
    {
        height = 512;
    }

    this->setFixedSize(width+20, height+15);
    emit this->windowSizeChanged();
    return;
}

void MainWindow::updatePlayerOverview(ObjectCharacter *player)
{
    if(player)
    {
        this->overviewDock->setWindowTitle(player->getObjectName().append(tr(" (Character Overview)")));
        this->playerWidget->updatePlayerWidget(player, this->gameModel->isYourTurn());
    }
    return;
}

void MainWindow::updateEditorSize()
{
    int widthIncrement = 0;

    if(!this->worldViewDock->isFloating())
    {
        widthIncrement = widthIncrement + 218;
    }

    if(!this->editorToolsDock->isFloating())
    {
        widthIncrement = widthIncrement + 160;
    }

    QSize mapSize = this->mapModel->getMapSize();

    if(mapSize.width() < TEoH::MIN_CENTRAL_WIDGET_WIDTH)
    {
        mapSize = QSize(TEoH::MIN_CENTRAL_WIDGET_WIDTH,mapSize.height());
    }

    QSize newSize = mapSize + QSize(widthIncrement,this->menuWidget()->size().height());
    int width = newSize.width();
    int height = newSize.height();

    if(height < TEoH::MIN_MAIN_WINDOW_HEIGHT)
    {
        height = TEoH::MIN_MAIN_WINDOW_HEIGHT;
    }

    this->setFixedSize(width+15, height+15);
    //this->setFixedSize(this->mapModel->getMapSize() + QSize(widthIncrement,this->menuWidget()->size().height()));
    this->worldViewDock->setFixedHeight(this->size().height());
    this->worldViewDock->setFixedWidth(218);
    emit this->windowSizeChanged();
    return;
}

void MainWindow::updateWindowTitle()
{
    QString windowTitle;
    QString path = this->mapModel->getFileInfo().absoluteFilePath();

    if(path == "")
    {
        windowTitle = tr("New Map");
    }
    else
    {
        windowTitle = QFileInfo(path).fileName();
    }

    if(!this->processor->refUndoRedo()->hasOnlySavedMapChanges() || !this->processor->refUndoRedo()->hasOnlySavedWorldChanges())
    {
        windowTitle.append("*");
    }

    this->setWindowTitle(windowTitle);
    return;
}

void MainWindow::createMainViewMap()
{
    if(this->mainViewMap)
    {
        this->centralWidgetLayout->removeWidget(this->mainViewMap);
        this->mainViewMap->deleteLater();
        this->mainViewMap = 0;
    }

    this->mainViewMap = new MainViewMap(this->processor, this->mapModel, this->editorModel, this->gameModel, this);
    this->centralWidgetLayout->addWidget(this->mainViewMap);

    if(this->editorModel->isEditor())
    {
        //this->editorMenu->updateSize(this->mapModel->getHSquareCount(), this->mapModel->getVSquareCount(), this->mapModel->getMaxHeight());
        connect(this->mainViewMap, SIGNAL(mapSizeUpdated()) , this, SLOT(updateEditorSize()));
    }

    this->update();
    return;
}

void MainWindow::setNewMap(MapModel *mapModel)
{
    if(!mapModel)
    {
        qDebug("Map coould not be loaded because it was 0.");
        return;
    }

    this->mapModel->setParent(this);
    this->mapModel = mapModel;
    this->processor->setMapModel(mapModel);

    TEoH::writeLog(QString("Load New Map: %1").arg(this->mapModel->getFileInfo().fileName()));

    this->createMainViewMap();
    connect(this->mapModel, SIGNAL(gridChanged()), this, SLOT(createMainViewMap()));

    if(this->editorModel->isEditor())
    {
        this->updateEditorSize();
        connect(this->mapModel, SIGNAL(gridChanged()), this, SLOT(updateEditorSize()));
        this->updateWindowTitle();
        connect(this->mapModel, SIGNAL(mapPathChanged()), this, SLOT(updateWindowTitle()));
    }
    else
    {
        this->updateGameSize();
        connect(this->mapModel, SIGNAL(gridChanged()), this, SLOT(updateGameSize()));
    }

    TEoH::writeLogDone();

    return;
}

void MainWindow::setVisible(bool visible)
{
    this->QMainWindow::setVisible(visible);
    this->move(QApplication::desktop()->screen()->rect().center() - this->rect().center());
    return;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    this->processor->execute(new Quit(true));
    event->ignore();
    return;
}
