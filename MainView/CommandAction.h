/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMAND_ACTION_H
#define COMMAND_ACTION_H

#include <QAction>
#include <QIcon>

#include "Command/Processor.h"
#include "Command/Backend/BaseCommand.h"

/** \addtogroup GUI
  * \{
  * \class CommandAction
  *
  * \brief An action that will trigger a command.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class CommandAction : public QAction
{
    Q_OBJECT
signals:
    void triggerWithName(const QString &);

private:
    Processor *processor;
    BaseCommand *command;

public:
    CommandAction(Processor *processor, BaseCommand *command, const QString &text, QObject *parent);

private slots:
    void commandActionTriggered();
};

#endif // COMMAND_ACTION_H
