/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ChooseObjectDialog.h"

#include <QPainter>
#include <QScrollBar>
#include <QtAlgorithms>

#include <QDebug>

#include "Object/ObjectID.h"
#include "Object/ObjectFromID.h"

ChooseObjectDialog::ChooseObjectDialog(StackModel *fromStack, ObjectBase::ObjectType type, QAbstractItemView::SelectionMode selectionMode, QWidget *parent)
    : QDialog(parent)
{
    this->fromStack = fromStack;
    this->type = type;

    this->gridLayout = new QGridLayout(this);
    this->setLayout(this->gridLayout);

    this->list = new QListWidget(this);

    this->list->setDragEnabled(false);
    this->list->setViewMode(QListView::IconMode);
    this->list->setResizeMode(QListView::Adjust);
    this->list->setMovement(QListView::Static);
    this->list->setDragDropMode(QAbstractItemView::NoDragDrop);
    this->list->setIconSize(QSize(30,30));
    this->list->setSpacing(5);
    this->list->setAcceptDrops(false);
    this->list->setDropIndicatorShown(false);
    this->list->setSelectionMode(selectionMode);

    this->createSelection();

    connect(this->list->verticalScrollBar(), SIGNAL(sliderReleased()), this, SLOT(snapSlider()));
    connect(this->list, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(itemSelected(QListWidgetItem*)));

    this->ok = new QPushButton(tr("Ok"),this);
    connect(this->ok, SIGNAL(clicked()), this, SLOT(accept()));

    this->cancel = new QPushButton(tr("Cancel"),this);
    connect(this->cancel, SIGNAL(clicked()), this, SLOT(reject()));

    this->gridLayout->addWidget(this->list,0,0,1,2);
    this->gridLayout->addWidget(this->ok,1,0);
    this->gridLayout->addWidget(this->cancel,1,1);
}

QList<int> ChooseObjectDialog::getSortedChosenObjects() const
{
    QList<int> sortedList = this->chosenObjects;
    qSort(sortedList);
    return sortedList;
}

void ChooseObjectDialog::createSelection()
{
    switch(this->type)
    {
    case ObjectBase::ITEM:
    {
        QList<ObjectItem *> *itemList = this->fromStack->refItemList();
        for(int i = 0; i < itemList->size(); ++i)
        {
            this->objectList.append(itemList->at(i));
        }
        break;
    }
    case ObjectBase::SPECIAL:
    {
        QList<ObjectSpecial *> *specialList = this->fromStack->refSpecialList();
        for(int i = 0; i < specialList->size(); ++i)
        {
            this->objectList.append(specialList->at(i));
        }
        break;
    }
    default: break;
    }

    QListWidgetItem *item;
    ObjectBase *object;

    for(int i = 0; i < this->objectList.size(); ++i)
    {
        object = this->objectList.at(i);
        item = new QListWidgetItem(this->list);
        item->setSizeHint(QSize(32,32));

        QPixmap itemPixmap(":/misc/inventory/slot");
        QPainter painter(&itemPixmap);
        painter.drawPixmap(0,0,30,30,QPixmap(object->getCurrentImagePath()));
        item->setIcon(QIcon(itemPixmap));
        item->setToolTip(QString("%1 - %2").arg(object->objectName(), object->getDescription()));
        item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
        this->list->insertItem(i, item);
    }

    this->list->verticalScrollBar()->setPageStep(this->list->verticalScrollBar()->singleStep()*
                                                 ((int) this->list->verticalScrollBar()->maximum()/4));

    return;
}

void ChooseObjectDialog::snapSlider()
{
    int value = qRound(this->list->verticalScrollBar()->value()/ (double) this->list->verticalScrollBar()->singleStep());
    this->list->verticalScrollBar()->setValue(this->list->verticalScrollBar()->singleStep()*value);
    return;
}

void ChooseObjectDialog::itemSelected(QListWidgetItem *item)
{
    int chosen = this->list->row(item);

    if(this->chosenObjects.contains(chosen))
    {
        this->chosenObjects.removeOne(chosen);
        item->setSelected(false);
    }
    else
    {
        if(this->list->selectionMode() == QAbstractItemView::SingleSelection)
        {
            this->chosenObjects.clear();
        }

        this->chosenObjects.append(chosen);
        item->setSelected(true);
    }

    return;
}
