/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ObjectMenu.h"

#include <QAction>

#include "Command/Backend/BaseCommand.h"
#include "MainView/CommandAction.h"
#include "Command/Backend/CommandNode.h"
#include "Command/Backend/CommandLeaf.h"
#include "Command/Backend/CommandSeperator.h"

ObjectMenu::ObjectMenu(ObjectBase *object, ObjectCharacter *player, Processor *processor, QWidget *parent)
    : QMenu(parent)
{
    this->processor = processor;
    this->object = object;

    this->setTitle(this->object->getDescriptiveName());
    this->setIcon(QIcon(QPixmap::fromImage(this->object->getImage())));

    CommandNode *node = new CommandNode(object->getDescriptiveName(), true, this);
    node->append(object->getCommandTreeList(player));
    node->setParent(this);

    QList<CommandTree *> list = node->getList();
    for(int i = 0; i < list.size(); ++i)
    {
        ObjectMenu::addCommandTree(list[i], this, processor);
    }

    if(this->actions().isEmpty())
    {
        QAction *action = new QAction(tr("No Interactions..."), this);
        action->setEnabled(false);
        this->addAction(action);
    }
}

void ObjectMenu::addCommandTree(CommandTree *commandTree, QMenu *menu, Processor *processor)
{
    if(!commandTree || !menu)
    {
        return;
    }

    switch(commandTree->getCommandTreeType())
    {
    case CommandTree::LEAF:
    {
        CommandLeaf *commandLeaf = qobject_cast<CommandLeaf *>(commandTree);
        BaseCommand *command = commandLeaf->refCommand();

        if(command)
        {
            if(processor->isExecutable(command))
            {
                CommandAction *action = new CommandAction(processor, command, commandLeaf->getName(), menu);
                menu->addAction(action);
            }
            else if(!commandLeaf->doRemoveIfUnavailable())
            {
                CommandAction *action = new CommandAction(processor, command, commandLeaf->getName(), menu);
                menu->addAction(action);
                action->setEnabled(false);
            }
        }
        break;
    }
    case CommandTree::SEPERATOR:
    {
        menu->addSeparator();
        break;
    }
    case CommandTree::NODE:
    {
        CommandNode *commandNode = qobject_cast<CommandNode *>(commandTree);
        QList<CommandTree *> list = commandNode->getList();

        QMenu *nextMenu = new QMenu(commandNode->getName(), menu);

        for(int i = 0; i < list.size(); ++i)
        {
            ObjectMenu::addCommandTree(list[i], nextMenu, processor);
        }

        bool allActionsDisabled = true;

        QList<QAction *> actions = nextMenu->actions();

        for(int i = 0; i < actions.size() && allActionsDisabled; ++i)
        {
            allActionsDisabled = allActionsDisabled && !actions[i]->isEnabled();
        }

        if(!actions.isEmpty() && !allActionsDisabled)
        {
            menu->addMenu(nextMenu);
        }
        else if(!commandNode->doRemoveIfUnavailable())
        {
            menu->addMenu(nextMenu);
            nextMenu->setEnabled(false);
        }

        break;
    }
    }
}

