/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CommandAction.h"

CommandAction::CommandAction(Processor *processor, BaseCommand *command, const QString &text, QObject *parent)
    : QAction(QIcon(command->getCommandIcon()), text, parent), processor(processor), command(command)
{
    command->setParent(this);
    connect(this, SIGNAL(triggered()), this, SLOT(commandActionTriggered()));
}

void CommandAction::commandActionTriggered()
{
    this->processor->execute(this->command);
    return;
}
