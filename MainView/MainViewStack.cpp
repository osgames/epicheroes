/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "MainViewStack.h"

#include <QPainter>
#include <QPoint>
#include <QApplication>

#include <iostream>

#include "MainView/ObjectMenu.h"
#include "Game/Model/GameModel.h"
#include "Common/Common.h"

// FIXME: Make lines part of this and resize so everything drawing related is managed here.
MainViewStack::MainViewStack(Processor *processor, StackModel *stackModel, MapModel *mapModel, EditorModel *editorModel, GameModel *gameModel, MouseInput *mouseInput, QWidget *parent) :
    QWidget(parent), gridColor(192,192,192), unseenColor(0,0,0)
{
    this->processor = processor;

    this->stackModel = stackModel;
    this->mapModel = mapModel;
    this->editorModel = editorModel;
    this->gameModel = gameModel;

    this->mouseInput = mouseInput;

    this->updateSize();

    connect(this->mapModel, SIGNAL(gridChanged()), this, SLOT(updateSize()));
    connect(this->stackModel, SIGNAL(characterChanged()), this, SLOT(update()));
    connect(this->stackModel, SIGNAL(itemListChanged()), this, SLOT(update()));
    connect(this->stackModel, SIGNAL(wallChanged()), this, SLOT(update()));
    connect(this->stackModel, SIGNAL(floorChanged()), this, SLOT(update()));
    connect(this->stackModel, SIGNAL(specialListChanged()), this, SLOT(update()));

    connect(this->stackModel, SIGNAL(pathIndicationChanged()), this, SLOT(update()));

    this->setAcceptDrops(true);

    this->update();
}

void MainViewStack::drawObject(QPainter *painter, ObjectBase *objectBase)
{
    if(objectBase)
    {
        QImage image = objectBase->getImage();
        if(!image.isNull())
        {
            painter->drawImage(this->getTopLeft(),image.scaledToHeight(this->getSquare().height())); //, Qt::SmoothTransformation));
        }
    }
    return;
}

void MainViewStack::drawDirectionIndicator(QPainter *painter)
{
    if(this->stackModel->getPathIndication() == StackModel::NONE)
    {
        return;
    }

    QImage image;
    QString direction;

    // TODO: Optimize to have the images loaded seperately and only use pointers instead of actual images.
    switch(this->stackModel->getPathIndication())
    {
        case StackModel::N:
        {
            direction = "n";
            break;
        }
        case StackModel::NE:
        {
            direction = "ne";
            break;
        }
        case StackModel::E:
        {
            direction = "e";
            break;
        }
        case StackModel::SE:
        {
            direction = "se";
            break;
        }
        case StackModel::S:
        {
            direction = "s";
            break;
        }
        case StackModel::SW:
        {
            direction = "sw";
            break;
        }
        case StackModel::W:
        {
            direction = "w";
            break;
        }
        case StackModel::NW:
        {
            direction = "nw";
            break;
        }
        case StackModel::DESTINATION:
        {
            direction = "destination";
            break;
        }
        default: return;
    }

    switch(this->stackModel->getPathType())
    {
    case StackModel::HUSTLE: image = QImage(QString(":/misc/directionArrows/%1_hustle").arg(direction)); break;
    case StackModel::RUN: image = QImage(QString(":/misc/directionArrows/%1_run").arg(direction)); break;
    case StackModel::FIRST: image = QImage(QString(":/misc/directionArrows/%1_first").arg(direction)); break;
    default: image = QImage(QString(":/misc/directionArrows/%1").arg(direction)); break;
    }

    if(this->stackModel->getPathIndication() == StackModel::DESTINATION)
    {
        QImage distance(image.size(), image.format());
        distance.fill(qRgba(0,0,0,0));
        QPainter textPainter(&distance);
        textPainter.setPen(QColor(0x10,0x10,0x10));
        QFont font = textPainter.font();
        font.setPixelSize(7);
        textPainter.setFont(font);
        textPainter.drawText(this->getSquare().x()-1, this->getSquare().y()-1, this->getSquare().width(), this->getSquare().height(),
                             Qt::AlignBottom | Qt::AlignHCenter,
                             QString("%1 ft").arg(this->mapModel->getCurrentPathDistance()));
        painter->drawImage(this->getTopLeft(),TEoH::drawGlowOnImage(distance).scaledToHeight(this->getSquare().height()));
    }

    painter->drawImage(this->getTopLeft(),image.scaledToHeight(this->getSquare().height()));

    if(this->stackModel->isCheckpoint())
    {
        painter->drawImage(this->getTopLeft(),QImage(":/misc/checkpoint").scaledToHeight(this->getSquare().height()));
    }
    return;
}

void MainViewStack::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    bool lookedAt = this->editorModel->isEditor() || this->stackModel->isLookedAtBy(this->mapModel->refPlayerStack(this->gameModel->getPlayerID())->refCharacter());
    bool seen = this->editorModel->isEditor() || this->stackModel->hasBeenSeenBy(this->mapModel->refPlayerStack(this->gameModel->getPlayerID())->refCharacter());

    if(!this->editorModel->isEditor() && !lookedAt && !seen)
    {
        painter.fillRect(this->rect(), this->gridColor);

        QRect square = this->rect();

        if(this->stackModel->getSquarePos().x() == 0)
        {
            square.setWidth(square.width()-1);
            square.moveLeft(1);
        }

        if(this->stackModel->getSquarePos().y() == 0)
        {
            square.setHeight(square.height()-1);
            square.moveTop(1);
        }

        if(this->stackModel->getSquarePos().x() == this->mapModel->getHSquareCount()-1)
        {
            square.setWidth(square.width()-1);
        }

        if(this->stackModel->getSquarePos().y() == this->mapModel->getVSquareCount()-1)
        {
            square.setHeight(square.height()-1);
        }

        painter.fillRect(square, this->unseenColor);
    }
    else
    {
        QRect square = this->rect();

        if(!this->editorModel->isEditor())
        {
            int x = this->stackModel->getSquarePos().x();
            int y = this->stackModel->getSquarePos().y();
            bool westSeen = x > 0 && !this->mapModel->refStackModel(x-1,y)->isLookedAtBy(this->mapModel->refPlayerStack(this->gameModel->getPlayerID())->refCharacter());
            bool northSeen = y > 0 && !this->mapModel->refStackModel(x,y-1)->isLookedAtBy(this->mapModel->refPlayerStack(this->gameModel->getPlayerID())->refCharacter());

            if(westSeen)
            {
                square.setWidth(square.width()-1);
                square.moveLeft(1);
            }

            if(northSeen)
            {
                square.setHeight(square.height()-1);
                square.moveTop(1);
            }

            painter.fillRect(square, this->gridColor);

            if(!westSeen && !northSeen && x > 0 && y > 0 && !this->mapModel->refStackModel(x-1,y-1)->isLookedAtBy(this->mapModel->refPlayerStack(this->gameModel->getPlayerID())->refCharacter()))
            {
                painter.fillRect(this->rect().x(), this->rect().y(), 1,1, this->unseenColor);
            }
        }
        else
        {
            painter.fillRect(square, this->gridColor);
            painter.fillRect(this->getSquare(), Qt::white);
        }

        QList<ObjectItem *> *itemList = this->stackModel->refItemList();
        QList<ObjectSpecial *> *specialList = this->stackModel->refSpecialList();

        //std::cout << "Drawing Floor...";
        this->drawObject(&painter, this->stackModel->refFloor());
        //std::cout << "Drawing Wall...";
        this->drawObject(&painter, this->stackModel->refWall());

        if(lookedAt)
        {
            //std::cout << "Drawing Special...";
            for(int i = 0; i < specialList->size(); ++i)
            {
                if(this->editorModel->isEditor() || (!this->editorModel->isEditor() && specialList->at(i)->isVisibleInGame()))
                {
                    this->drawObject(&painter, specialList->at(i));
                }
            }

            //std::cout << "Drawing Items...";
            for(int i = 0; i < itemList->size(); ++i)
            {
                this->drawObject(&painter, itemList->at(i));
            }

            //std::cout << "Drawing Character..." << std::endl;
            this->drawObject(&painter, this->stackModel->refCharacter());
        }
        else if(seen)
        {
            painter.fillRect(this->getTopLeft().x(), this->getTopLeft().y(),TEoH::STANDARD_SQUARE_SIZE, TEoH::STANDARD_SQUARE_SIZE, QColor(0,0,0,80));
        }
    }

    if(!this->editorModel->isEditor())
    {
        this->drawDirectionIndicator(&painter);
    }

    return;
}

void MainViewStack::mousePressEvent(QMouseEvent *event)
{
    this->mouseInput->mousePressed(event, this->stackModel);
    return;
}

void MainViewStack::mouseMoveEvent(QMouseEvent *event)
{
    QWidget *widget = QApplication::widgetAt(this->mapToGlobal(event->pos()));

    if(!widget)
    {
        return;
    }

    if(event->buttons() & Qt::LeftButton)
    {
        QMouseEvent newEvent = QMouseEvent(QEvent::MouseButtonPress, event->pos(),Qt::LeftButton, event->buttons(), event->modifiers());
        QApplication::sendEvent(widget, &newEvent);
    }
    else if(event->buttons() & Qt::MiddleButton)
    {
        QMouseEvent newEvent = QMouseEvent(QEvent::MouseButtonPress, event->pos(),Qt::MiddleButton, event->buttons(), event->modifiers());
        QApplication::sendEvent(widget, &newEvent);
    }

    return;
}

void MainViewStack::mouseReleaseEvent(QMouseEvent *event)
{
    this->mouseInput->mouseReleased(event, this->stackModel);
    return;
}

QPoint MainViewStack::getTopLeft() const
{
    return QPoint(TEoH::STANDARD_GRID_LINE_SIZE, TEoH::STANDARD_GRID_LINE_SIZE);
}

QRect MainViewStack::getSquare() const
{
    return QRect(this->getTopLeft(), QPoint(this->mapModel->getSquareSize(), this->mapModel->getSquareSize()));
}

void MainViewStack::setStackModel(StackModel *stackModel)
{
    this->stackModel = stackModel;
    this->update();
    return;
}

void MainViewStack::updateSize()
{
    int x = this->stackModel->getSquarePos().x();
    int y = this->stackModel->getSquarePos().y();
    int squareSize = this->mapModel->getSquareSize();
    this->resize(squareSize+2*TEoH::STANDARD_GRID_LINE_SIZE, squareSize+2*TEoH::STANDARD_GRID_LINE_SIZE);
    this->move(x*(squareSize+1),
               y*(squareSize+1));
    this->update();
    return;
}

void MainViewStack::updateAll()
{
    this->update();
    return;
}

