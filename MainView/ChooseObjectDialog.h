/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CHOOSE_OBJECT_DIALOG_H_
#define _CHOOSE_OBJECT_DIALOG_H_

#include <QList>
#include <QDialog>
#include <QGridLayout>
#include <QListWidget>
#include <QPushButton>

#include "Object/ObjectBase.h"
#include "MainView/Model/StackModel.h"

/** \addtogroup GUI
  * \{
  * \class ChooseObjectDialog
  *
  * \brief Choose one of the given objects.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class ChooseObjectDialog : public QDialog
{
    Q_OBJECT

private:

    QGridLayout *           gridLayout;
    StackModel *            fromStack;
    ObjectBase::ObjectType  type;

    QListWidget *           list;
    QPushButton *           ok;
    QPushButton *           cancel;

    QList<ObjectBase *>     objectList;
    QList<int>              chosenObjects; ///< Chosen object positions in the given objectList. The order of this list depends on the order of how the elemens have been chosen.

public:
    ChooseObjectDialog(StackModel *fromStack,
                       ObjectBase::ObjectType type,
                       QAbstractItemView::SelectionMode selectionMode = QAbstractItemView::SingleSelection,
                       QWidget *parent = 0);

    /**
      * @brief Order the chosen objects seperately and return them as a ascending ordered list->
      * @return Returns the chosen objects list in ascending order.
      */
    QList<int> getSortedChosenObjects() const;

private:
    void createSelection();

private slots:
    void snapSlider();
    void itemSelected(QListWidgetItem *item);
};

#endif //_CHOOSE_OBJECT_DIALOG_H_

