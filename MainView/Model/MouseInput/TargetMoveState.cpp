/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "TargetMoveState.h"

#include "MainView/Model/MouseInput/IdleGameMouseState.h"
#include "MainView/Model/MouseInput/TargetChosenState.h"
#include "Command/Backend/Game/Map/MoveCharacter.h"
#include "Command/Backend/Game/Map/FindPath.h"

TargetMoveState::TargetMoveState(Processor *processor, StackModel *target)
    : MouseState(processor, target)
{
}

MouseState *TargetMoveState::nextState(MouseState::MouseEventType type, QMouseEvent *event, MapModel *mapModel, StackModel *targetStack, StackModel *fromStack)
{
    Q_UNUSED(mapModel);

    MouseState *nextState = this;

    switch(type)
    {
    case MouseState::RELEASED:
    {
        switch(event->button())
        {
        case Qt::LeftButton:
        {
            if(this->target == targetStack)
            {
                qDebug("TargetMove -> IdleMouse : Move");
                this->processor->execute(new MoveCharacter(fromStack, this->target));
                nextState = new IdleGameMouseState(this->processor, targetStack);
            }
        }
        default: break;
        }
        break;
    }
    case MouseState::PRESSED:
    {
        if(targetStack->hasBeenSeenBy(fromStack->refCharacter()) && this->target != targetStack)
        {
            qDebug("TargetMove -> TargetChosen : Don't Move");
            this->processor->execute(new FindPath(fromStack, targetStack));
            nextState = new TargetChosenState(this->processor, targetStack);
        }
        break;
    }
    default: break;
    }

    if(nextState != this)
    {
        delete this;
    }

    return nextState;
}
