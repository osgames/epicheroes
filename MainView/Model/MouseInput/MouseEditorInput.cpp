/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "MouseEditorInput.h"

#include "MainView/Model/MouseInput/IdleEditorMouseState.h"

#include "Command/Backend/Editor/Map/CopyCharacter.h"
#include "Command/Backend/Editor/Map/CutCharacter.h"
#include "Command/Backend/Editor/Map/RemoveCharacter.h"

#include "Command/Backend/Editor/Map/CopySpecial.h"
#include "Command/Backend/Editor/Map/CutSpecial.h"
#include "Command/Backend/Editor/Map/RemoveSpecial.h"

#include "Command/Backend/Editor/Map/CopyTile.h"
#include "Command/Backend/Editor/Map/CutTile.h"
#include "Command/Backend/Editor/Map/RemoveTile.h"

#include "Command/Backend/Editor/Map/CopyItem.h"
#include "Command/Backend/Editor/Map/CutItem.h"
#include "Command/Backend/Editor/Map/RemoveItem.h"

#include "Command/Backend/Editor/Map/PasteObject.h"

#include "Command/Backend/Editor/Map/RemoveAllObjects.h"

#include "Command/Backend/Editor/Tools/ShowInventoryEditor.h"
#include "Command/Backend/Editor/Map/AddItemToCharacter.h"

MouseEditorInput::MouseEditorInput(GameModel *gameModel, MapModel *mapModel, Processor *processor, QObject *parent)
    : MouseInput(new IdleEditorMouseState(processor, 0), gameModel, mapModel, processor, parent)
{
    this->contextMenuStack = 0;
}

void MouseEditorInput::nextState(MouseState::MouseEventType type, QMouseEvent *event, StackModel *targetStack)
{
    this->currentMouseState = this->currentMouseState->nextState(type, event, this->mapModel, targetStack);

    if(this->currentMouseState->showContextMenu())
    {
        this->showEditorContextMenu(targetStack);
    }
    return;
}

void MouseEditorInput::showEditorContextMenu(StackModel *stackModel)
{
    this->contextMenuStack = stackModel;
    this->contextMenu = new QMenu();

    this->characterMenu = new QMenu(tr("Character"), this->contextMenu);
    this->wallMenu = new QMenu(tr("Wall"), this->contextMenu);
    this->floorMenu = new QMenu(tr("Floor"), this->contextMenu);
    this->itemsMenu = new QMenu(tr("Items"), this->contextMenu);
    this->specialsMenu = new QMenu(tr("Specials"), this->contextMenu);

    this->copyCharacterAction = new QAction(tr("Copy"), this->characterMenu);
    connect(this->copyCharacterAction, SIGNAL(triggered()), this, SLOT(copyCharacter()));
    this->cutCharacterAction = new QAction(tr("Cut"), this->characterMenu);
    connect(this->cutCharacterAction, SIGNAL(triggered()), this, SLOT(cutCharacter()));
    this->removeCharacterAction = new QAction(tr("Remove"), this->characterMenu);
    connect(this->removeCharacterAction, SIGNAL(triggered()), this, SLOT(removeCharacter()));
    this->addItemToCharacterAction = new QAction(tr("Add selected item to Inventory"), this->characterMenu);
    connect(this->addItemToCharacterAction, SIGNAL(triggered()), this, SLOT(addItemToCharacter()));
    this->editCharacterInventoryAction = new QAction(tr("Edit Character Inventory..."), this->characterMenu);
    connect(this->editCharacterInventoryAction, SIGNAL(triggered()), this, SLOT(editCharacterInventory()));
    this->editCharacterAction = new QAction(tr("Edit..."), this->characterMenu);
    connect(this->editCharacterAction, SIGNAL(triggered()), this, SLOT(editCharacter()));

    this->copyWallAction = new QAction(tr("Copy"), this->wallMenu);
    connect(this->copyWallAction, SIGNAL(triggered()), this, SLOT(copyWall()));
    this->cutWallAction = new QAction(tr("Cut"), this->wallMenu);
    connect(this->cutWallAction, SIGNAL(triggered()), this, SLOT(cutWall()));
    this->removeWallAction = new QAction(tr("Remove"), this->wallMenu);
    connect(this->removeWallAction, SIGNAL(triggered()), this, SLOT(removeWall()));
    this->editWallAction = new QAction(tr("Edit..."), this->wallMenu);
    connect(this->editWallAction, SIGNAL(triggered()), this, SLOT(editWall()));

    this->copyFloorAction = new QAction(tr("Copy"), this->floorMenu);
    connect(this->copyFloorAction, SIGNAL(triggered()), this, SLOT(copyFloor()));
    this->cutFloorAction = new QAction(tr("Cut"), this->floorMenu);
    connect(this->cutFloorAction, SIGNAL(triggered()), this, SLOT(cutFloor()));
    this->removeFloorAction = new QAction(tr("Remove"), this->floorMenu);
    connect(this->removeFloorAction, SIGNAL(triggered()), this, SLOT(removeFloor()));
    this->editFloorAction = new QAction(tr("Edit..."), this->floorMenu);
    connect(this->editFloorAction, SIGNAL(triggered()), this, SLOT(editFloor()));

    this->copyItemsAction = new QAction(tr("Copy..."), this->itemsMenu);
    connect(this->copyItemsAction, SIGNAL(triggered()), this, SLOT(copyItem()));
    this->cutItemsAction = new QAction(tr("Cut..."), this->itemsMenu);
    connect(this->cutItemsAction, SIGNAL(triggered()), this, SLOT(cutItems()));
    this->removeItemsAction = new QAction(tr("Remove..."), this->itemsMenu);
    connect(this->removeItemsAction, SIGNAL(triggered()), this, SLOT(removeItems()));
    this->editItemsAction = new QAction(tr("Edit..."), this->itemsMenu);
    connect(this->editItemsAction, SIGNAL(triggered()), this, SLOT(editItems()));

    this->copySpecialsAction = new QAction(tr("Copy..."), this->specialsMenu);
    connect(this->copySpecialsAction, SIGNAL(triggered()), this, SLOT(copySpecials()));
    this->cutSpecialsAction = new QAction(tr("Cut..."), this->specialsMenu);
    connect(this->cutSpecialsAction, SIGNAL(triggered()), this, SLOT(cutSpecials()));
    this->removeSpecialsAction = new QAction(tr("Remove..."), this->specialsMenu);
    connect(this->removeSpecialsAction, SIGNAL(triggered()), this, SLOT(removeSpecials()));
    this->editSpecialsAction = new QAction(tr("Edit..."), this->specialsMenu);
    connect(this->editSpecialsAction, SIGNAL(triggered()), this, SLOT(editSpecials()));

    this->pasteObjectAction = new QAction(tr("Paste"), this);
    connect(this->pasteObjectAction, SIGNAL(triggered()), this, SLOT(pasteObject()));

    this->removeAllAction = new QAction(tr("Remove All"), this);
    connect(this->removeAllAction, SIGNAL(triggered()), this, SLOT(removeAll()));

    this->checkItemsActions();
    this->checkCharacterActions();
    this->checkWallActions();
    this->checkFloorActions();
    this->checkSpecialsActions();

    this->checkPasteAction();
    this->checkRemoveAllAction();

    this->characterMenu->addAction(this->copyCharacterAction);
    this->characterMenu->addAction(this->cutCharacterAction);
    this->characterMenu->addAction(this->removeCharacterAction);
    this->characterMenu->addAction(this->addItemToCharacterAction);
    this->characterMenu->addSeparator();
    this->characterMenu->addAction(this->editCharacterInventoryAction);
    this->characterMenu->addAction(this->editCharacterAction);

    this->wallMenu->addAction(this->copyWallAction);
    this->wallMenu->addAction(this->cutWallAction);
    this->wallMenu->addAction(this->removeWallAction);
    this->wallMenu->addSeparator();
    this->wallMenu->addAction(this->editWallAction);

    this->floorMenu->addAction(this->copyFloorAction);
    this->floorMenu->addAction(this->cutFloorAction);
    this->floorMenu->addAction(this->removeFloorAction);
    this->floorMenu->addSeparator();
    this->floorMenu->addAction(this->editFloorAction);

    this->itemsMenu->addAction(this->copyItemsAction);
    this->itemsMenu->addAction(this->cutItemsAction);
    this->itemsMenu->addAction(this->removeItemsAction);
    this->itemsMenu->addSeparator();
    this->itemsMenu->addAction(this->editItemsAction);

    this->specialsMenu->addAction(this->copySpecialsAction);
    this->specialsMenu->addAction(this->cutSpecialsAction);
    this->specialsMenu->addAction(this->removeSpecialsAction);
    this->specialsMenu->addSeparator();
    this->specialsMenu->addAction(this->editSpecialsAction);

    this->contextMenu->addMenu(this->characterMenu);
    this->contextMenu->addMenu(this->wallMenu);
    this->contextMenu->addMenu(this->floorMenu);
    this->contextMenu->addMenu(this->itemsMenu);
    this->contextMenu->addMenu(this->specialsMenu);
    this->contextMenu->addSeparator();
    this->contextMenu->addAction(this->pasteObjectAction);
    this->contextMenu->addSeparator();
    this->contextMenu->addAction(this->removeAllAction);

    if(!this->contextMenu->isEmpty())
    {
        emit showContextMenu(stackModel);
    }

    return;
}

void MouseEditorInput::checkRemoveAllAction()
{
    this->removeAllAction->setEnabled(!this->contextMenuStack->isEmpty());
    return;
}

void MouseEditorInput::copyCharacter()
{
    this->processor->execute(new CopyCharacter(this->contextMenuStack));

    return;
}

void MouseEditorInput::cutCharacter()
{
    this->processor->execute(new CutCharacter(this->contextMenuStack));

    return;
}

void MouseEditorInput::removeCharacter()
{
    this->processor->execute(new RemoveCharacter(this->contextMenuStack));
    return;
}

void MouseEditorInput::addItemToCharacter()
{
    this->processor->execute(new AddItemToCharacter(this->contextMenuStack->refCharacter()));
    return;
}

void MouseEditorInput::editCharacterInventory()
{
    this->processor->execute(new ShowInventoryEditor(this->contextMenuStack->refCharacter()));
    return;
}

void MouseEditorInput::editCharacter()
{
    // Command Edit Character

    return;
}

void MouseEditorInput::checkCharacterActions()
{
    bool enable = this->contextMenuStack->hasCharacter();

    this->copyCharacterAction->setEnabled(enable);
    this->cutCharacterAction->setEnabled(enable);
    this->removeCharacterAction->setEnabled(enable);
    this->addItemToCharacterAction->setEnabled(enable &&
                                         this->processor->isExecutableAndDelete(new AddItemToCharacter(this->contextMenuStack->refCharacter())));
    this->editCharacterInventoryAction->setEnabled(enable &&
                                             this->processor->isExecutableAndDelete(new ShowInventoryEditor(this->contextMenuStack->refCharacter())));
    this->editCharacterAction->setEnabled(false && enable);


    this->checkRemoveAllAction();

    this->characterMenu->setEnabled(this->copyCharacterAction->isEnabled() ||
                                    this->cutCharacterAction->isEnabled() ||
                                    this->removeCharacterAction->isEnabled() ||
                                    this->addItemToCharacterAction->isEnabled() ||
                                    this->editCharacterInventoryAction->isEnabled() ||
                                    this->editCharacterAction->isEnabled()
                                    );

    return;
}

void MouseEditorInput::copyItem()
{
    this->processor->execute(new CopyItem(this->contextMenuStack));

    return;
}

void MouseEditorInput::cutItems()
{
    this->processor->execute(new CutItem(this->contextMenuStack));

    return;
}

void MouseEditorInput::removeItems()
{
    this->processor->execute(new RemoveItem(this->contextMenuStack));

    return;
}

void MouseEditorInput::editItems()
{
    // Command EditItems

    return;
}

void MouseEditorInput::checkItemsActions()
{
    bool enable = !this->contextMenuStack->refItemList()->isEmpty();

    this->copyItemsAction->setEnabled(enable);
    this->cutItemsAction->setEnabled(enable);
    this->removeItemsAction->setEnabled(enable);
    this->editItemsAction->setEnabled(false && enable);
    this->checkRemoveAllAction();

    this->itemsMenu->setEnabled(this->copyItemsAction->isEnabled() ||
                                this->cutItemsAction->isEnabled() ||
                                this->removeItemsAction->isEnabled() ||
                                this->editItemsAction->isEnabled());

    return;
}

void MouseEditorInput::copySpecials()
{
    this->processor->execute(new CopySpecial(this->contextMenuStack));

    return;
}

void MouseEditorInput::cutSpecials()
{
    this->processor->execute(new CutSpecial(this->contextMenuStack));

    return;
}

void MouseEditorInput::removeSpecials()
{
    this->processor->execute(new RemoveSpecial(this->contextMenuStack));

    return;
}

void MouseEditorInput::editSpecials()
{
    // Command EditSpecials

    return;
}

void MouseEditorInput::checkSpecialsActions()
{
    bool enable = !this->contextMenuStack->refSpecialList()->isEmpty();

    this->copySpecialsAction->setEnabled(enable);
    this->cutSpecialsAction->setEnabled(enable);
    this->removeSpecialsAction->setEnabled(enable);
    this->editSpecialsAction->setEnabled(false && enable);
    this->checkRemoveAllAction();

    this->specialsMenu->setEnabled(this->copySpecialsAction->isEnabled() ||
                                   this->cutSpecialsAction->isEnabled() ||
                                   this->removeSpecialsAction->isEnabled() ||
                                   this->editSpecialsAction->isEnabled());

    return;
}

void MouseEditorInput::copyWall()
{
    this->processor->execute(new CopyTile(this->contextMenuStack, ObjectTile::WALL));

    return;
}

void MouseEditorInput::cutWall()
{
    this->processor->execute(new CutTile(this->contextMenuStack, ObjectTile::WALL));

    return;
}

void MouseEditorInput::removeWall()
{
    this->processor->execute(new RemoveTile(this->contextMenuStack, ObjectTile::WALL));

    return;
}

void MouseEditorInput::editWall()
{
    // Command Edit Wall

    return;
}

void MouseEditorInput::checkWallActions()
{
    bool enable = this->contextMenuStack->refWall();

    this->copyWallAction->setEnabled(enable);
    this->cutWallAction->setEnabled(enable);
    this->removeWallAction->setEnabled(enable);
    this->editWallAction->setEnabled(false && enable);
    this->checkRemoveAllAction();

    this->wallMenu->setEnabled(this->copyWallAction->isEnabled() ||
                               this->cutWallAction->isEnabled() ||
                               this->removeWallAction->isEnabled() ||
                               this->editWallAction->isEnabled());

    return;
}

void MouseEditorInput::copyFloor()
{
    this->processor->execute(new CopyTile(this->contextMenuStack, ObjectTile::FLOOR));

    return;
}

void MouseEditorInput::cutFloor()
{
    this->processor->execute(new CutTile(this->contextMenuStack, ObjectTile::FLOOR));

    return;
}

void MouseEditorInput::removeFloor()
{
    this->processor->execute(new RemoveTile(this->contextMenuStack, ObjectTile::FLOOR));

    return;
}

void MouseEditorInput::editFloor()
{
    // Command Edit Floor

    return;
}

void MouseEditorInput::checkFloorActions()
{
    bool enable = this->contextMenuStack->refFloor();

    this->copyFloorAction->setEnabled(enable);
    this->cutFloorAction->setEnabled(enable);
    this->removeFloorAction->setEnabled(enable);
    this->editFloorAction->setEnabled(false && enable);
    this->checkRemoveAllAction();

    this->floorMenu->setEnabled(this->copyFloorAction->isEnabled() ||
                                this->cutFloorAction->isEnabled() ||
                                this->removeFloorAction->isEnabled() ||
                                this->editFloorAction->isEnabled());

    return;
}

void MouseEditorInput::checkPasteAction()
{
    this->pasteObjectAction->setEnabled(this->mapModel->refClipboardObject());
    return;
}

void MouseEditorInput::pasteObject()
{
    this->processor->execute(new PasteObject(this->contextMenuStack));

    return;
}

void MouseEditorInput::removeAll()
{
    this->processor->execute(new RemoveAllObjects(this->contextMenuStack));
    return;
}
