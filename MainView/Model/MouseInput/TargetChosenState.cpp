/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "TargetChosenState.h"

#include <QWidget>
#include <QDebug>
#include <QApplication>

#include "Command/Backend/Game/Map/FindPath.h"
#include "MainView/Model/MouseInput/TargetConfirmedState.h"

TargetChosenState::TargetChosenState(Processor *processor, StackModel *target)
    : MouseState(processor, target)
{
    this->targetStackIsCheckpoint = false;
}

MouseState *TargetChosenState::nextState(MouseState::MouseEventType type, QMouseEvent *event, MapModel *mapModel, StackModel *targetStack, StackModel *fromStack)
{
    MouseState *nextState = this;

    switch(type)
    {
    case MouseState::PRESSED:
    {
        switch(event->button())
        {
        case Qt::LeftButton:
        {
            if(targetStack->hasBeenSeenBy(fromStack->refCharacter()) && this->target != targetStack && !this->checkpoints.contains(targetStack))
            {
                if(this->processor->execute(new FindPath(fromStack, targetStack, this->checkpoints)))
                {
                    qDebug("TargetChosen -> TargetChosen");
                    if(this->targetStackIsCheckpoint)
                    {
                        this->checkpoints.append(this->target);
                        this->processor->execute(new FindPath(fromStack, targetStack, this->checkpoints));
                    }
                    this->target = targetStack;
                    this->targetStackIsCheckpoint = false;
                }
            }
            break;
        }
        case Qt::RightButton:
        {
            if(this->checkpoints.contains(targetStack))
            {
                this->checkpoints.removeOne(targetStack);
                this->processor->execute(new FindPath(fromStack, targetStack, this->checkpoints));
            }
            else
            {
                if(mapModel->getCurrentPathDistance() < fromStack->refCharacter()->getMovementSpeed()+fromStack->refCharacter()->getHustleSpeed())
                {
                    this->targetStackIsCheckpoint = !this->targetStackIsCheckpoint;
                }
            }
            break;
        }
        default: break;
        }
        break;
    }
    case MouseState::RELEASED:
    {
        switch(event->button())
        {
        case Qt::LeftButton:
        {
            qDebug("TargetChosen -> TargetConfirmed : Target Confirmed");
            nextState = new TargetConfirmedState(this->processor, this->target);
            break;
        }
        default: break;
        }
        break;
    }
    default: break;
    }

//    QString output = QString("Path: [%1,%2] -> ").arg(fromStack->getSquarePos().x())
//                                                 .arg(fromStack->getSquarePos().y());
//    for(int i = 0; i < this->checkpoints.size(); ++i)
//    {
//        output.append(QString("(%1,%2) -> ").arg(this->checkpoints.at(i)->getSquarePos().x())
//                                            .arg(this->checkpoints.at(i)->getSquarePos().y()));
//    }
//    output.append(QString("[%1,%2]").arg(this->target->getSquarePos().x())
//                                    .arg(this->target->getSquarePos().y()));
//    qDebug() << output;

    if(nextState != this)
    {
        delete this;
    }

    return nextState;
}
