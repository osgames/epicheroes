/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "StackModel.h"

#include <iostream>
#include <QDebug>

#include "Object/Character/Player.h"
#include "Object/ObjectFromID.h"
#include "Common/Common.h"

StackModel::StackModel(int xPos, int yPos, int height, QObject *parent) :
    QObject(parent)
{
    this->initStack(QPoint(xPos, yPos), height);
    this->initConnects();
}

StackModel::StackModel(QPoint pos, int height, QObject *parent) :
    QObject(parent)
{
    this->initStack(pos, height);
    this->initConnects();
}

StackModel::StackModel(const StackModel &stackModel)
    : QObject(stackModel.parent())
{
    QPoint point = stackModel.getSquarePos();
    int z = stackModel.getHeight();
    this->setPosition(point.x(), point.y(), z);

    this->resetPathDirection();

    for(int i = stackModel.itemListSize() - 1; i >= 0; --i)
    {
        this->prependItem(stackModel.refItem(i)->copy());
    }

    for(int i = stackModel.specialListSize() - 1; i >= 0; --i)
    {
        this->prependSpecial(stackModel.refSpecial(i)->copy());
    }

    this->setCharacter(stackModel.refCharacter()->copy());
    this->setWall(stackModel.refWall()->copy());
    this->setFloor(stackModel.refFloor()->copy());

    this->initConnects();
}

bool StackModel::specialIDExists(ObjectID::SpecialID id) const
{
    for(int i = 0; i < this->specialList.size(); ++i)
    {
        if(this->specialList[i]->getObjectID() == id)
        {
            return true;
        }
    }

    return false;
}

void StackModel::removeAll()
{
    this->removeAllItems();
    this->removeAllSpecials();
    this->removeCharacter();
    this->removeWall();
    this->removeFloor();
    return;
}

void StackModel::deleteAll()
{
    this->deleteAllItems();
    this->deleteAllSpecials();
    this->deleteCharacter();
    this->deleteWall();
    this->deleteFloor();
}

void StackModel::initStack(const QPoint &squarePos, int height)
{
    this->squarePos = squarePos;
    this->height = height;

    this->character = 0;
    this->wall = 0;
    this->floor = 0;
    this->pathIndication = StackModel::NONE;
    this->pathType = StackModel::MOVE;
    this->checkpoint = false;

    return;
}

void StackModel::initConnects()
{
    connect(this, SIGNAL(itemListChanged()), this, SIGNAL(stackChanged()));
    connect(this, SIGNAL(specialListChanged()), this, SIGNAL(stackChanged()));
    connect(this, SIGNAL(pathIndicationChanged()), this, SIGNAL(stackChanged()));
    connect(this, SIGNAL(characterChanged()), this, SIGNAL(stackChanged()));
    connect(this, SIGNAL(wallChanged()), this, SIGNAL(stackChanged()));
    connect(this, SIGNAL(floorChanged()), this, SIGNAL(stackChanged()));
    return;
}

void StackModel::resetPathDirection()
{
    this->setPathIndication(StackModel::NONE);
    this->setPathType(StackModel::MOVE);
    this->setCheckpoint(false);
    return;
}

void StackModel::prependItem(ObjectItem *item)
{
    if(!item)
    {
        return;
    }

    this->itemList.prepend(item);
    item->setParent(this);
    item->setPosition(this->squarePos.x(), this->squarePos.y(), this->height);
    emit this->itemListChanged();

    return;
}

void StackModel::removeItem(int i)
{
    this->itemList[i]->unsetPosition();
    this->itemList.removeAt(i);
    emit this->itemListChanged();

    return;
}

ObjectItem *StackModel::takeItem(int i)
{
    ObjectItem *item = this->refItem(i);

    this->removeItem(i);

    return item;
}

ObjectItem *StackModel::takeItem(ObjectID::ItemID id, unsigned int spawnID)
{
    ObjectItem *item = 0;
    ObjectItem *foundItem;

    for(int i = 0; i < this->itemList.size() && !item; ++i)
    {
        foundItem = this->itemList.at(i);
        if(foundItem->getObjectID() == id && foundItem->getSpawnID() == spawnID)
        {
            item = foundItem;
            this->removeItem(i);
        }
    }

    return item;
}

void StackModel::deleteItem(int i)
{
    ObjectItem *item = this->takeItem(i);
    item->deleteLater();
    return;
}

void StackModel::removeAllItems()
{
    for(int i = 0; i < this->itemList.size(); ++i)
    {
        this->removeItem(i);
    }
    return;
}

QList<ObjectItem *> StackModel::takeAllItems()
{
    QList<ObjectItem *> itemList = this->itemList;

    this->removeAllItems();

    return itemList;
}

void StackModel::deleteAllItems()
{
    for(int i = 0; i < this->itemList.size(); ++i)
    {
        this->deleteItem(i);
    }

    return;
}

int StackModel::itemListSize() const
{
    return this->itemList.size();
}

bool StackModel::hasItem(ObjectItem *item) const
{
    return this->itemList.contains(item);
}

void StackModel::prependSpecial(ObjectSpecial *special)
{
    if(!special)
    {
        return;
    }

    this->specialList.prepend(special);
    special->setPosition(this->squarePos.x(), this->squarePos.y(), this->height);
    special->setParent(this);
    emit this->specialListChanged();

    if(special->getObjectID() == ObjectID::START_POSITION)
    {
        emit this->startPositionSet(this);
    }

    return;
}

void StackModel::removeSpecial(int i)
{
    if(this->specialList[i]->getObjectID() == ObjectID::START_POSITION)
    {
        emit this->startPositionRemoved(this);
    }

    this->specialList[i]->unsetPosition();
    this->specialList.removeAt(i);
    emit this->specialListChanged();

    return;
}

ObjectSpecial *StackModel::takeSpecial(int i)
{
    ObjectSpecial *special = this->refSpecial(i);

    this->removeSpecial(i);

    return special;
}

void StackModel::deleteSpecial(int i)
{
    ObjectSpecial *special = this->takeSpecial(i);
    special->deleteLater();
    return;
}

void StackModel::removeAllSpecials()
{
    for(int i = 0; i < this->specialList.size(); ++i)
    {
        this->removeSpecial(i);
    }
    return;
}

QList<ObjectSpecial *> StackModel::takeAllSpecial()
{
    QList<ObjectSpecial *> specialList = this->specialList;

    this->removeAllSpecials();

    return specialList;
}

void StackModel::deleteAllSpecials()
{
    for(int i = 0; i < this->specialList.size(); ++i)
    {
        this->deleteSpecial(i);
    }

    return;
}

int StackModel::specialListSize() const
{
    return this->specialList.size();
}

bool StackModel::hasSpaceForSpecial(ObjectSpecial *special) const
{
    int specialCount = 0;

    for(int i = 0; i < this->specialList.size(); ++i)
    {
        if(this->refSpecial(i)->getObjectID() == special->getObjectID())
        {
            specialCount++;
        }
    }

    return specialCount < special->getMaxAmountPerStack();
}

bool StackModel::hasSpecial(ObjectSpecial *special) const
{
    return this->specialList.contains(special);
}

ObjectCharacter *StackModel::replaceCharacter(ObjectCharacter *character)
{
    ObjectCharacter *oldCharacter = this->takeCharacter();
    this->setCharacter(character);
    return oldCharacter;
}

ObjectCharacter *StackModel::takeCharacter()
{
    ObjectCharacter *character = this->refCharacter();
    this->removeCharacter();
    return character;
}

void StackModel::removeCharacter()
{
    if(this->character)
    {
        if(this->character->getObjectID() == ObjectID::PLAYER)
        {
            emit this->playerRemoved(this);
        }

        this->character->unsetPosition();
    }

    this->character = 0;
    emit this->characterChanged();
    return;
}

void StackModel::deleteCharacter()
{
    if(this->character)
    {
        ObjectCharacter *character = this->takeCharacter();
        character->deleteLater();
    }
    return;
}

bool StackModel::hasCharacter() const
{
    return this->character != 0;
}

bool StackModel::characterExists(ObjectCharacter *character) const
{
    return this->characterExists(character->getObjectID(), character->getSpawnID());
}

bool StackModel::characterExists(ObjectID::CharacterID characterID, unsigned int spawnID) const
{
    return this->character != 0 && this->character->getObjectID() == characterID && this->character->getSpawnID() == spawnID;
}

ObjectTile *StackModel::replaceWall(ObjectTile *wall)
{
    ObjectTile *oldWall = this->takeWall();
    this->setWall(wall);
    return oldWall;
}

ObjectTile *StackModel::takeWall()
{
    ObjectTile *wall = this->refWall();
    this->removeWall();
    return wall;
}

void StackModel::removeWall()
{
    if(this->wall)
    {
        this->wall->unsetPosition();
    }

    this->wall = 0;
    emit this->wallChanged();
    return;
}

void StackModel::deleteWall()
{
    if(this->wall)
    {
        ObjectTile *wall = this->takeWall();
        wall->deleteLater();
    }
    return;
}

bool StackModel::hasWall() const
{
    return this->wall != 0;
}

ObjectTile *StackModel::replaceFloor(ObjectTile *floor)
{
    ObjectTile *oldFloor = this->takeFloor();
    this->setFloor(floor);
    return oldFloor;
}

ObjectTile *StackModel::takeFloor()
{
    ObjectTile *floor = this->refFloor();
    this->removeFloor();
    return floor;
}

void StackModel::removeFloor()
{
    if(this->floor)
    {
        this->floor->unsetPosition();
    }
    this->floor = 0;
    emit this->floorChanged();
    return;
}

void StackModel::deleteFloor()
{
    if(this->floor)
    {
        ObjectTile *floor = this->takeFloor();
        floor->deleteLater();
    }

    return;
}

bool StackModel::hasFloor() const
{
    return this->floor != 0;
}

bool StackModel::hasTile(ObjectTile::TileType tileType) const
{
    return (this->hasWall() && tileType == ObjectTile::WALL) ||
            (this->hasFloor() && tileType == ObjectTile::FLOOR);
}

ObjectTile *StackModel::replaceTile(ObjectTile *tile, ObjectTile::TileType tileType)
{
    ObjectTile *oldTile;

    switch(tileType)
    {
    case ObjectTile::WALL:
    {
        oldTile = this->takeWall();
        this->setWall(tile);
        break;
    }
    case ObjectTile::FLOOR:
    {
        oldTile = this->takeFloor();
        this->setFloor(tile);
        break;
    }
    }

    return oldTile;
}

void StackModel::removeTile(ObjectTile::TileType tileType)
{
    switch(tileType)
    {
    case ObjectTile::WALL:
    {
        this->removeWall();
        break;
    }
    case ObjectTile::FLOOR:
    {
        this->removeFloor();
        break;
    }
    }

    return;
}

bool StackModel::isAdjacentTo(const StackModel *other) const
{
    int xDist = this->getSquarePos().x() - other->getSquarePos().x();
    int yDist = this->getSquarePos().y() - other->getSquarePos().y();
    int zDist = this->getHeight() - other->getHeight();

    return xDist >= -1 && xDist <= 1 &&
           yDist >= -1 && yDist <= 1 &&
           (xDist != 0 || yDist != 0) &&
            zDist == 0;
}

bool StackModel::isAccessible() const
{
    return this->isPassable() && !this->hasCharacter();
}

bool StackModel::isPassable() const
{
    return this->hasFloor() && !this->hasWall() &&
            (!this->hasCharacter() || (this->hasCharacter() && this->character->isUnconcious()));
}

void StackModel::notLookedAtBy(const ObjectBase *object)
{
    this->lookedAt[object->getType()][object->getObjectIDNumber()].remove(object->getSpawnID());
    return;
}

void StackModel::lookedAtBy(const ObjectBase *object)
{
    this->lookedAt[object->getType()][object->getObjectIDNumber()].insert(object->getSpawnID());
    this->seen[object->getType()][object->getObjectIDNumber()].insert(object->getSpawnID());
    return;
}

bool StackModel::isLookedAtBy(const ObjectBase *object) const
{
    return this->lookedAt[object->getType()][object->getObjectIDNumber()].contains(object->getSpawnID());
}

bool StackModel::hasBeenSeenBy(const ObjectBase *object) const
{
    return this->seen[object->getType()][object->getObjectIDNumber()].contains(object->getSpawnID());
}

QPoint StackModel::getSquarePos() const
{
    return this->squarePos;
}

int StackModel::getHeight() const
{
    return this->height;
}

bool StackModel::isEmpty() const
{
    return !this->wall && !this->floor && !this->character &&
            this->itemList.isEmpty() && this->specialList.isEmpty();
}

int StackModel::getSpeedRequired() const
{
    return TEoH::DISTANCE_BETWEEN_SQUARES;
}

StackModel::PathIndication StackModel::getPathIndication() const
{
    return this->pathIndication;
}

StackModel::PathType StackModel::getPathType() const
{
    return this->pathType;
}

bool StackModel::isCheckpoint() const
{
    return this->checkpoint;
}

bool StackModel::isBlockingView() const
{
    return this->hasWall();
}

QList<ObjectItem *> StackModel::getItemList() const
{
    return this->itemList;
}

QList<ObjectSpecial *> StackModel::getSpecialList() const
{
    return this->specialList;
}

QList<ObjectBase *> StackModel::getObjects() const
{
    QList<ObjectBase *> objects;
    if(this->floor)
    {
        objects.append(this->floor);
    }
    if(this->wall)
    {
        objects.append(this->wall);
    }
    foreach(ObjectSpecial *special, this->specialList)
    {
        objects.append(special);
    }
    foreach(ObjectItem *item, this->itemList)
    {
        objects.append(item);
    }
    if(this->character)
    {
        objects.append(this->character);
    }
    return objects;
}

void StackModel::setPosition(int x, int y, int z)
{
    if(x < 0)
    {
        std::cout << "Error in setPosition: x was < 0." << std::endl;
        return;
    }

    if(y < 0)
    {
        std::cout << "Error in setPosition: y was < 0." << std::endl;
        return;
    }

    if(z < 0)
    {
        std::cout << "Error in setPosition: z was < 0." << std::endl;
        return;
    }

    this->squarePos = QPoint(x,y);
    this->height = z;
    return;
}

void StackModel::setItemList(const QList<ObjectItem *> &itemList)
{
    this->itemList.clear();

    for(int i = itemList.size() - 1; i >= 0; --i)
    {
        this->prependItem(itemList[i]);
    }

    return;
}

void StackModel::setSpecialList(const QList<ObjectSpecial *> &specialList)
{
    this->specialList.clear();

    for(int i = specialList.size() - 1; i >= 0; --i)
    {
        this->prependSpecial(specialList[i]);
    }

    return;
}

void StackModel::setPathIndication(StackModel::PathIndication pathIndication)
{
    this->pathIndication = pathIndication;
    emit this->pathIndicationChanged();
    return;
}

void StackModel::setPathType(StackModel::PathType pathType)
{
    this->pathType = pathType;
    return;
}

void StackModel::setCheckpoint(bool isCheckpoint)
{
    this->checkpoint = isCheckpoint;
    return;
}

void StackModel::setCharacter(ObjectCharacter *character)
{
    if(this->hasCharacter())
    {
        qDebug("Cannot set a character on an occupied stack with another character.");
        return;
    }

    this->character = character;

    if(this->character)
    {
        this->character->setPosition(this->squarePos.x(), this->squarePos.y(), this->height);
        this->character->setParent(this);

        if(this->character->getObjectID() == ObjectID::PLAYER)
        {
            emit this->playerSet(this);
        }
    }

    emit this->characterChanged();

    return;
}

void StackModel::setWall(ObjectTile *wall)
{
    if(this->hasWall())
    {
        qDebug("Cannot set a wall on an occupied stack with another wall.");
        return;
    }

    this->wall = wall;

    if(this->wall)
    {
        this->wall->setPosition(this->squarePos.x(), this->squarePos.y(), this->height);
        this->wall->setParent(this);
    }

    emit this->wallChanged();

    return;
}

void StackModel::setFloor(ObjectTile *floor)
{
    if(this->hasFloor())
    {
        qDebug("Cannot set a floor on an occupied stack with another floor.");
        return;
    }

    this->floor = floor;

    if(this->floor)
    {
        this->floor->setPosition(this->squarePos.x(), this->squarePos.y(), this->height);
        this->floor->setParent(this);
        emit this->floorChanged();
    }

    emit this->floorChanged();

    return;
}

void StackModel::setTile(ObjectTile *tile, ObjectTile::TileType tileType)
{
    switch(tileType)
    {
    case ObjectTile::WALL:
    {
        this->setWall(tile);
        break;
    }
    case ObjectTile::FLOOR:
    {
        this->setFloor(tile);
        break;
    }
    }

    return;
}

ObjectCharacter *StackModel::refCharacter() const
{
    return this->character;
}

QList<ObjectItem *> *StackModel::refItemList()
{
    return &this->itemList;
}

ObjectItem *StackModel::refItem(int i) const
{
    if(i < 0 || i >= this->itemList.size())
    {
        qDebug() << QString("Error in refItem(): i was %1").arg(i);
        i = 0;
    }

    if(this->itemList.empty())
    {
        return 0;
    }

    return this->itemList[i];
}

QList<ObjectSpecial *> *StackModel::refSpecialList()
{
    return &this->specialList;
}

ObjectSpecial *StackModel::refSpecial(int i) const
{
    if(i < 0 || i >= this->specialList.size())
    {
        qDebug() << QString("Error in refSpecial(): i was %1").arg(i);
        i = 0;
    }

    if(this->specialList.empty())
    {
        return 0;
    }

    return this->specialList[i];
}

ObjectTile *StackModel::refTile(ObjectTile::TileType tileType) const
{
    ObjectTile *tile;

    switch(tileType)
    {
    case ObjectTile::WALL:
    {
        tile = this->refWall();
        break;
    }
    case ObjectTile::FLOOR:
    {
        tile = this->refFloor();
        break;
    }
    }

    return tile;
}

ObjectTile *StackModel::refWall() const
{
    return this->wall;
}
ObjectTile *StackModel::refFloor() const
{
    return this->floor;
}

void StackModel::serialize(QDataStream &dataStream) const
{
    dataStream << this->itemList.size();
    for(int i = this->itemList.size()-1; i >= 0; --i)
    {
        dataStream << this->itemList[i]->getObjectID();
        this->itemList[i]->serialize(dataStream);
    }

    dataStream << this->specialList.size();
    for(int i = this->specialList.size()-1; i >= 0; --i)
    {
        dataStream << this->specialList[i]->getObjectID();
        this->specialList[i]->serialize(dataStream);
    }

    dataStream << (this->character != 0);
    if(this->character)
    {
        dataStream << this->character->getObjectID();
        this->refCharacter()->serialize(dataStream);
    }

    dataStream << (this->wall != 0);
    if(this->wall)
    {
        dataStream << this->wall->getObjectID();
        this->refWall()->serialize(dataStream);
    }

    dataStream << (this->floor != 0);
    if(this->floor)
    {
        dataStream << this->floor->getObjectID();
        this->refFloor()->serialize(dataStream);
    }

    dataStream << this->seen.size();
    foreach(ObjectBase::ObjectType type, this->seen.keys())
    {
        dataStream << type;
        dataStream << this->seen[type];
    }

    return;
}

void StackModel::deserialize(QDataStream &dataStream)
{
    this->deleteAll();

    this->resetPathDirection();

    int itemListSize;
    dataStream >> itemListSize;

    ObjectItem *item;
    int itemID;

    for(int i = 0; i < itemListSize; ++i)
    {
        dataStream >> itemID;
        item = ObjectFromID::objectFrom(ObjectID::ItemID(itemID));
        item->deserialize(dataStream);
        this->prependItem(item);
    }

    int specialListSize;
    dataStream >> specialListSize;

    ObjectSpecial *special;
    int specialID;

    for(int i = 0; i < specialListSize; ++i)
    {
        dataStream >> specialID;
        special = ObjectFromID::objectFrom(ObjectID::SpecialID(specialID));
        special->deserialize(dataStream);
        this->prependSpecial(special);
    }

    bool characterNotNull;
    bool wallNotNull;
    bool floorNotNull;

    dataStream >> characterNotNull;

    if(characterNotNull)
    {
        int characterID;

        dataStream >> characterID;
        ObjectCharacter *character = ObjectFromID::objectFrom(ObjectID::CharacterID(characterID));
        character->deserialize(dataStream);
        this->setCharacter(character);
    }

    dataStream >> wallNotNull;
    if(wallNotNull)
    {
        int wallID;

        dataStream >> wallID;
        ObjectTile *wall = ObjectFromID::objectFrom(ObjectID::TileID(wallID));
        wall->deserialize(dataStream);
        this->setWall(wall);
    }

    dataStream >> floorNotNull;
    if(floorNotNull)
    {
        int floorID;

        dataStream >> floorID;
        ObjectTile *floor = ObjectFromID::objectFrom(ObjectID::TileID(floorID));
        floor->deserialize(dataStream);
        this->setFloor(floor);
    }

    int seenSize;
    dataStream >> seenSize;
    for(int i = 0; i < seenSize; ++i)
    {
        int typeNumber;
        dataStream >> typeNumber;
        ObjectBase::ObjectType type = ObjectBase::ObjectType(typeNumber);
        dataStream >> this->seen[type];
    }

    return;
}
