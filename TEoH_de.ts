<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>AddToChosenList</name>
    <message>
        <location filename="Command/Backend/Editor/World/AddToChosenList.cpp" line="41"/>
        <location filename="Command/Backend/Editor/World/AddToChosenList.cpp" line="53"/>
        <source>Couldn&apos;t add a map to the start maps...</source>
        <oldsource>Couldn&apos;t adding a map to the start maps...</oldsource>
        <translation>Die Map konnt enicht zu den Start Maps hinzugefügt werden...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/AddToChosenList.cpp" line="42"/>
        <source>The chosen entry %1 was a directory and
cannot be added as a start map.</source>
        <translation>Der gewählte Eintrag %1 ist ein Verzeichniss und
kann deshalb nicht zu den Start Maps hinzugefügt werden.</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/AddToChosenList.cpp" line="54"/>
        <source>The map %1 cannot be added to the start maps
because it doesn&apos;t have any start positions on the map.</source>
        <oldsource>The map %1 cannot be added to the start maps
 because it doesn&apos;t have any start positions on the map.</oldsource>
        <translation>Die Map %1 kann nicht zu den Start Maps hinzugefügt werden,
weil die Karte keine Start Position besitzt.
</translation>
    </message>
</context>
<context>
    <name>ApplyMapGeometryChanges</name>
    <message>
        <location filename="Command/Backend/Editor/Map/ApplyMapGeometryChanges.cpp" line="41"/>
        <source>Applying property changes to map...</source>
        <translation>Änderungen auf Map anwenden...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/Map/ApplyMapGeometryChanges.cpp" line="49"/>
        <source>Couldn&apos;t apply changes to map geometry.</source>
        <translation>Änderungen auf die Map Geometrie konnten 
nicht angewendet werden.</translation>
    </message>
</context>
<context>
    <name>ApplyProjectPropertiesChanges</name>
    <message>
        <source>Applying project changes failed...</source>
        <translation type="obsolete">Änderungen auf Projekt anwenden...</translation>
    </message>
    <message>
        <source>Couldn&apos;t apply changes to starter maps list.</source>
        <translation type="obsolete">Änderungen der Starter Maps konnten nicht angewendet werden.</translation>
    </message>
</context>
<context>
    <name>ApplyWorldPropertiesChanges</name>
    <message>
        <location filename="Command/Backend/Editor/World/ApplyWorldPropertiesChanges.cpp" line="36"/>
        <source>Applying world changes failed...</source>
        <translation>Änderungen an der World fehlgeschlagen...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/ApplyWorldPropertiesChanges.cpp" line="44"/>
        <source>Couldn&apos;t apply changes to starter maps list.</source>
        <translation>Änderungen der Starter Maps konnten nicht angewendet werden.</translation>
    </message>
</context>
<context>
    <name>AskForSave</name>
    <message>
        <source>Save map changes?</source>
        <translation type="obsolete">Änderungen an der Map speichern?</translation>
    </message>
    <message>
        <source>There are unsaved changes for this map.
Do you want to save?</source>
        <translation type="obsolete">Es gibt ungesicherte Änderungen an der Map.
Möchten Sie speichern?</translation>
    </message>
    <message>
        <source>Save project changes?</source>
        <translation type="obsolete">Änderungen am Project speichern?</translation>
    </message>
    <message>
        <source>There are unsaved changes for this project.
Do you want to save?</source>
        <translation type="obsolete">Es gibt ungesicherte Änderungen am Projekt.
Möchten Sie speichern?</translation>
    </message>
    <message>
        <source>There are unsaved changes for this project. Do you want to save?</source>
        <translation type="obsolete">Es gibt ungesicherte Änderungen am Projekt. Möchten Sie speichern?</translation>
    </message>
</context>
<context>
    <name>AskForSaveMap</name>
    <message>
        <location filename="Command/Backend/Editor/Map/AskForSaveMap.cpp" line="33"/>
        <source>Save map changes?</source>
        <translation>Änderungen an der Map speichern?</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/Map/AskForSaveMap.cpp" line="33"/>
        <source>There are unsaved changes for this map.
Do you want to save?</source>
        <translation>Es gibt ungesicherte Änderungen an der Map.
Möchten Sie speichern?</translation>
    </message>
</context>
<context>
    <name>AskForSaveProject</name>
    <message>
        <source>Save project changes?</source>
        <translation type="obsolete">Änderungen am Project speichern?</translation>
    </message>
    <message>
        <source>There are unsaved changes for this project.
Do you want to save?</source>
        <translation type="obsolete">Es gibt ungesicherte Änderungen am Projekt.
Möchten Sie speichern?</translation>
    </message>
</context>
<context>
    <name>AskForSaveWorld</name>
    <message>
        <location filename="Command/Backend/Editor/World/AskForSaveWorld.cpp" line="33"/>
        <source>Save world changes?</source>
        <translation>World änderungen speichern?</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/AskForSaveWorld.cpp" line="33"/>
        <source>There are unsaved changes for this world.
Do you want to save?</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AttackCharacter</name>
    <message>
        <location filename="Command/Backend/Object/Character/AttackCharacter.cpp" line="75"/>
        <source>crits</source>
        <translation>crits</translation>
    </message>
    <message>
        <location filename="Command/Backend/Object/Character/AttackCharacter.cpp" line="79"/>
        <source>hits</source>
        <translation>trifft</translation>
    </message>
    <message>
        <location filename="Command/Backend/Object/Character/AttackCharacter.cpp" line="84"/>
        <source>misses</source>
        <translation>verfehlt</translation>
    </message>
    <message>
        <location filename="Command/Backend/Object/Character/AttackCharacter.cpp" line="87"/>
        <source>%1 %2 %3 with %4.</source>
        <translation>%1 %2 %3 mit %4.</translation>
    </message>
</context>
<context>
    <name>AttributeRollerWidget</name>
    <message>
        <location filename="Game/AttributeRollerWidget.cpp" line="40"/>
        <source>Roll!</source>
        <translation>Würfeln!</translation>
    </message>
    <message>
        <location filename="Game/AttributeRollerWidget.cpp" line="223"/>
        <source>(%1)</source>
        <translation>(%1)</translation>
    </message>
    <message>
        <location filename="Game/AttributeRollerWidget.cpp" line="243"/>
        <source>Drag&apos;n&apos;drop to customize.</source>
        <translation>Drag&apos;n&apos;drop um Werte anzupassen.</translation>
    </message>
</context>
<context>
    <name>BrickWall</name>
    <message>
        <location filename="Object/Tile/BrickWall.cpp" line="27"/>
        <source>Brick Wall</source>
        <translation>Ziegel Wand</translation>
    </message>
    <message>
        <location filename="Object/Tile/BrickWall.cpp" line="28"/>
        <source>A very sturdy wall of bricks.</source>
        <translation>Eine sehr stabile Ziegelsteinwand.</translation>
    </message>
    <message>
        <source>Description...</source>
        <translation>Beschreibung...</translation>
    </message>
</context>
<context>
    <name>ChooseMainTool</name>
    <message>
        <source>Player</source>
        <translation type="obsolete">Spieler</translation>
    </message>
    <message>
        <source>Primary Character</source>
        <oldsource>The player character</oldsource>
        <translation type="obsolete">Primärer Charakter</translation>
    </message>
    <message>
        <source>The primary character</source>
        <translation type="obsolete">Der primäre Charakter</translation>
    </message>
    <message>
        <source>Brick</source>
        <translation type="obsolete">Ziegel</translation>
    </message>
</context>
<context>
    <name>ChooseObjectDialog</name>
    <message>
        <location filename="MainView/ChooseObjectDialog.cpp" line="57"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="MainView/ChooseObjectDialog.cpp" line="60"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>ChooseToolDialog</name>
    <message>
        <location filename="Editor/Tools/ChooseToolDialog.cpp" line="55"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="Editor/Tools/ChooseToolDialog.cpp" line="58"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>ClientGameModel</name>
    <message>
        <location filename="Game/Model/ClientGameModel.cpp" line="103"/>
        <source>{&quot;%1&apos;s turn...&quot; : []}</source>
        <translation>{&quot;%1&apos;s Runde...&quot; : []}</translation>
    </message>
</context>
<context>
    <name>ClientModel</name>
    <message>
        <location filename="Network/ClientModel.cpp" line="98"/>
        <location filename="Network/ClientModel.cpp" line="394"/>
        <location filename="Network/ClientModel.cpp" line="399"/>
        <location filename="Network/ClientModel.cpp" line="406"/>
        <location filename="Network/ClientModel.cpp" line="416"/>
        <source>TEoH Client</source>
        <translation>TEoH Client</translation>
    </message>
    <message>
        <source>When waiting for a ready to read, he following error occurred: %1.</source>
        <translation type="obsolete">Wärend auf zu lesende Daten gewartet wurde, ist der folgende Fehler aufgetreten: %1.</translation>
    </message>
    <message>
        <location filename="Network/ClientModel.cpp" line="99"/>
        <source>When waiting for a ready to read, the following error occurred: %1.</source>
        <translation>Ein Fehler ist aufgetreten, als auf ein Ready to Read gewartet wurde: %1.</translation>
    </message>
    <message>
        <location filename="Network/ClientModel.cpp" line="395"/>
        <source>The host was not found. Please check the host name and port settings.</source>
        <translation>Der Host wurde nicht gefunden. Überprüfe den Hostnamen und den Port.</translation>
    </message>
    <message>
        <location filename="Network/ClientModel.cpp" line="400"/>
        <source>The connection was refused by the peer. Make sure a TEoH server is running, and check that the host name and port settings are correct.</source>
        <translation>Die Verbindung wurde von außen abgelehnt. Stelle sicher, dass der TEoH Server gestartet ist und überprüfe ob der Hostname und der Port korrekt sind.</translation>
    </message>
    <message>
        <location filename="Network/ClientModel.cpp" line="407"/>
        <source>The following error occurred: %1.</source>
        <translation>Der folgende Fehler ist aufgetreten: %1.</translation>
    </message>
    <message>
        <location filename="Network/ClientModel.cpp" line="417"/>
        <source>The connection to the server closed.</source>
        <translation>Die Verbindung mit dem Server wurde geschlossen.</translation>
    </message>
</context>
<context>
    <name>CommandSeperator</name>
    <message>
        <location filename="Command/Backend/CommandSeperator.cpp" line="22"/>
        <source>Seperator</source>
        <translation>Trennzeichen</translation>
    </message>
</context>
<context>
    <name>ConnectTo</name>
    <message>
        <location filename="Command/Backend/Game/Startmenu/ConnectTo.cpp" line="33"/>
        <source>Connect To...</source>
        <translation>Verbinde mit...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Startmenu/ConnectTo.cpp" line="48"/>
        <source>Start Game</source>
        <translation>Spiel starten</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Startmenu/ConnectTo.cpp" line="64"/>
        <source>Waiting for your turn...</source>
        <translation>Auf deine Runde warten...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Startmenu/ConnectTo.cpp" line="64"/>
        <source>Game Starts when it&apos;s your turn...</source>
        <translation>Spiel beginnt wenn deine Runde anfängt...</translation>
    </message>
</context>
<context>
    <name>CopyItem</name>
    <message>
        <location filename="Command/Backend/Editor/Map/CopyItem.cpp" line="45"/>
        <source>Select item to copy...</source>
        <translation>Item kopieren...</translation>
    </message>
</context>
<context>
    <name>Corpse</name>
    <message>
        <location filename="Object/Item/Corpse.cpp" line="28"/>
        <source>Corpse</source>
        <translation>Leiche</translation>
    </message>
    <message>
        <source>A lifeless body of a once alive being.</source>
        <translation>Ein jetzt lebloser Körper.</translation>
    </message>
    <message>
        <location filename="Object/Item/Corpse.cpp" line="28"/>
        <source>A lifeless body of an unknown being.</source>
        <translation>Ein lebloser Körper eines unbekannten Wesens.</translation>
    </message>
    <message>
        <location filename="Object/Item/Corpse.cpp" line="34"/>
        <source>%1&apos;s Corpse</source>
        <translation>%1&apos;s Leiche</translation>
    </message>
    <message>
        <location filename="Object/Item/Corpse.cpp" line="35"/>
        <source>The lifeless body of the deceased %1.</source>
        <translation>Der leblose Körper des %1.</translation>
    </message>
</context>
<context>
    <name>CreateCharacter</name>
    <message>
        <location filename="Command/Backend/Game/Ingame/CreateCharacter.cpp" line="42"/>
        <source>Character Creation...</source>
        <translation>Character erstellen...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/CreateCharacter.cpp" line="43"/>
        <source>Done</source>
        <translation>Fertig</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/CreateCharacter.cpp" line="56"/>
        <source>Player Name?</source>
        <translation>Spieler Name?</translation>
    </message>
    <message>
        <source>Waiting for your turn...</source>
        <translation type="obsolete">Auf deine Runde warten...</translation>
    </message>
    <message>
        <source>Game Starts when it&apos;s your turn...</source>
        <translation type="obsolete">Spiel beginnt wenn deine Runde anfängt...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/CreateCharacter.cpp" line="89"/>
        <source>Loading the starting map has failed...</source>
        <translation>Laden der Startkarte ist fehlgeschlagen...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/CreateCharacter.cpp" line="90"/>
        <source>The map couldn&apos;t be loaded.</source>
        <translation>Die Karte konnte nicht geladen werden.</translation>
    </message>
    <message>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/CreateCharacter.cpp" line="105"/>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/CreateCharacter.cpp" line="106"/>
        <source>Attributes</source>
        <translation>Attribute</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/CreateCharacter.cpp" line="117"/>
        <source>Starting Map</source>
        <translation>Startkarte</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/CreateCharacter.cpp" line="126"/>
        <source>&lt; Random &gt;</source>
        <translation>&lt; Zufall &gt;</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/CreateCharacter.cpp" line="157"/>
        <source>Connections (Count: 0):</source>
        <translation>Verbindungen (Anzahl: 0):</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/CreateCharacter.cpp" line="168"/>
        <source>Server Overview</source>
        <translation>Serverübersicht</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/CreateCharacter.cpp" line="204"/>
        <source>Attributes not rolled up yet.
</source>
        <translation>Attribute noch nicht ausgewürfelt.</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/CreateCharacter.cpp" line="207"/>
        <source>
Randomly assign them?</source>
        <translation>Zufällig zuwisen?</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/CreateCharacter.cpp" line="211"/>
        <source>Character creation...</source>
        <translation>Charakter Erstellung...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/CreateCharacter.cpp" line="229"/>
        <source>Player name has to be at least %1 characters long.</source>
        <translation>Spielername muss mindestens %1 Zeichen lang sein.</translation>
    </message>
    <message>
        <source>Player Name has to be at least %1 characters long.</source>
        <translation>Spielername muss mindestens %1 Zeichen lang sein.</translation>
    </message>
    <message>
        <source>Choose maps that can be used as start maps. This can be done in the editor. (TODO: Look up random maps later, if no maps were chosen.)</source>
        <translation type="obsolete">Wähle Karten als Startkarten aus. Dies kann im Editor gemacht werden. (TODO: Wenn keine Karten gewählt wurden im Editor, wähle eine Karte zufällig aus.)</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/CreateCharacter.cpp" line="233"/>
        <source>Character not done yet...</source>
        <translation>Charakter ist nocht nicht fertig...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/CreateCharacter.cpp" line="267"/>
        <source>Connections (Count: %1):</source>
        <translation>Verbindungen (Anzahl: %1):</translation>
    </message>
</context>
<context>
    <name>CutItem</name>
    <message>
        <location filename="Command/Backend/Editor/Map/CutItem.cpp" line="50"/>
        <source>Select item to cut...</source>
        <translation>Item ausscheniden...</translation>
    </message>
</context>
<context>
    <name>Dagger</name>
    <message>
        <location filename="Object/Item/Dagger.cpp" line="45"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="Object/Item/Dagger.cpp" line="55"/>
        <source>Dagger Base Damage</source>
        <translation>Dolch Basisschaden</translation>
    </message>
    <message>
        <location filename="Object/Item/Dagger.h" line="42"/>
        <source>Dagger</source>
        <translation>Dolch</translation>
    </message>
    <message>
        <location filename="Object/Item/Dagger.h" line="42"/>
        <source>A short pointy blade to cut and stab with.</source>
        <translation>Mit dieser kurzen Klinge kann leicht gestochen und geschnlitzt werden.</translation>
    </message>
</context>
<context>
    <name>EMailGameModel</name>
    <message>
        <location filename="Game/Model/EMailGameModel.cpp" line="59"/>
        <source>No more players available...</source>
        <translation>Keine Spieler verfügbar...</translation>
    </message>
    <message>
        <location filename="Game/Model/EMailGameModel.cpp" line="60"/>
        <source>No more players are available, therefore I&apos;ll quit the game.</source>
        <translation>Keine Spieler waren verfügbar, darum schließe ich das Spiel.</translation>
    </message>
</context>
<context>
    <name>EMailSetupWidget</name>
    <message>
        <location filename="Game/EMailSetupWidget.cpp" line="30"/>
        <source>Player Count:</source>
        <translation>Spieleranzahl:</translation>
    </message>
    <message>
        <location filename="Game/EMailSetupWidget.cpp" line="38"/>
        <source>This is only the amount of players you can start with with the maximum being %1.
Even more players than that can be later added in-game.</source>
        <translation>Diese Anzahl beschreibtdie Anzahl der Spieler mit denen ein Spiel begonnen werden kann.
Die maximale Anzahl ist %1. Mehr Spieler können später im Spiel hinzugefügt werden.</translation>
    </message>
    <message>
        <location filename="Game/EMailSetupWidget.cpp" line="41"/>
        <source>Start Game</source>
        <translation>Spiel starten</translation>
    </message>
</context>
<context>
    <name>EditorMenu</name>
    <message>
        <source>Editor Menu</source>
        <translation type="obsolete">Editor Menü</translation>
    </message>
    <message>
        <location filename="Editor/EditorMenu.cpp" line="97"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <source>New Project</source>
        <translation type="obsolete">Neues Projekt</translation>
    </message>
    <message>
        <location filename="Editor/EditorMenu.cpp" line="48"/>
        <source>Save Map...</source>
        <oldsource>Save...</oldsource>
        <translation>Map Speichern...</translation>
    </message>
    <message>
        <location filename="Editor/EditorMenu.cpp" line="51"/>
        <source>Save Map As...</source>
        <oldsource>Save As...</oldsource>
        <translation>Map Speichern unter...</translation>
    </message>
    <message>
        <source>Load...</source>
        <oldsource>Load</oldsource>
        <translation type="obsolete">Laden...</translation>
    </message>
    <message>
        <source>Import...</source>
        <oldsource>Import</oldsource>
        <translation type="obsolete">Importieren...</translation>
    </message>
    <message>
        <source>Export...</source>
        <oldsource>Export</oldsource>
        <translation type="obsolete">Exportieren...</translation>
    </message>
    <message>
        <location filename="Editor/EditorMenu.cpp" line="54"/>
        <source>Save World...</source>
        <oldsource>Save Project...</oldsource>
        <translation>World Speichern...</translation>
    </message>
    <message>
        <location filename="Editor/EditorMenu.cpp" line="56"/>
        <source>Save World As...</source>
        <oldsource>Save Project As...</oldsource>
        <translation>World Speichern Unter...</translation>
    </message>
    <message>
        <location filename="Editor/EditorMenu.cpp" line="58"/>
        <source>Load World...</source>
        <oldsource>Load Project...</oldsource>
        <translation>World Laden...</translation>
    </message>
    <message>
        <location filename="Editor/EditorMenu.cpp" line="61"/>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="Editor/EditorMenu.cpp" line="76"/>
        <source>Map Properties...</source>
        <translation>Map Eigenschaften...</translation>
    </message>
    <message>
        <location filename="Editor/EditorMenu.cpp" line="78"/>
        <source>World Properties...</source>
        <oldsource>Project Properties...</oldsource>
        <translation>World Eigenschaten...</translation>
    </message>
    <message>
        <location filename="Editor/EditorMenu.cpp" line="110"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="Editor/EditorMenu.cpp" line="118"/>
        <source>View</source>
        <translation>Ansicht</translation>
    </message>
    <message>
        <location filename="Editor/EditorMenu.cpp" line="119"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="Editor/EditorMenu.cpp" line="65"/>
        <source>Undo</source>
        <translation>Rückgängig</translation>
    </message>
    <message>
        <location filename="Editor/EditorMenu.cpp" line="45"/>
        <source>New World...</source>
        <oldsource>New Project...</oldsource>
        <translation>Neue World...</translation>
    </message>
    <message>
        <location filename="Editor/EditorMenu.cpp" line="70"/>
        <source>Redo</source>
        <translation>Wiederherstellen</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="obsolete">Kopieren</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation type="obsolete">Ausschneiden</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="obsolete">Einfügen</translation>
    </message>
    <message>
        <location filename="Editor/EditorMenu.cpp" line="122"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="Editor/EditorMenu.cpp" line="92"/>
        <source>About...</source>
        <oldsource>About</oldsource>
        <translation>Über...</translation>
    </message>
</context>
<context>
    <name>EditorMenuModel</name>
    <message>
        <source>New Project...</source>
        <translation type="obsolete">Neues Project...</translation>
    </message>
    <message>
        <source>Save Project...</source>
        <translation type="obsolete">Projekt Speichern...</translation>
    </message>
    <message>
        <source>Save Project As...</source>
        <translation type="obsolete">Projekt Speichern Unter...</translation>
    </message>
    <message>
        <source>Save Map...</source>
        <translation type="obsolete">Map Speichern...</translation>
    </message>
    <message>
        <source>Save Map As...</source>
        <translation type="obsolete">Map Speichern unter...</translation>
    </message>
    <message>
        <source>Load Project...</source>
        <translation type="obsolete">Projekt Laden...</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="obsolete">Beenden</translation>
    </message>
    <message>
        <source>Undo</source>
        <translation type="obsolete">Rückgängig</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation type="obsolete">Wiederherstellen</translation>
    </message>
    <message>
        <source>About...</source>
        <translation type="obsolete">Über...</translation>
    </message>
</context>
<context>
    <name>EditorModel</name>
    <message>
        <source>Player</source>
        <oldsource>player</oldsource>
        <translation type="obsolete">Spieler</translation>
    </message>
    <message>
        <source>The player character</source>
        <translation type="obsolete">Der Charakter des Spielers</translation>
    </message>
    <message>
        <source>Brick</source>
        <oldsource>brick</oldsource>
        <translation type="obsolete">Ziegel</translation>
    </message>
</context>
<context>
    <name>EditorTools</name>
    <message>
        <source>Editor Tools</source>
        <translation type="obsolete">Editor Werkzeug</translation>
    </message>
    <message>
        <location filename="Editor/EditorTools.cpp" line="38"/>
        <source>Characters</source>
        <translation>Charakter</translation>
    </message>
    <message>
        <location filename="Editor/EditorTools.cpp" line="40"/>
        <source>Items</source>
        <translation>Items</translation>
    </message>
    <message>
        <location filename="Editor/EditorTools.cpp" line="42"/>
        <source>Tiles</source>
        <translation>Tiles</translation>
    </message>
    <message>
        <location filename="Editor/EditorTools.cpp" line="44"/>
        <source>Special</source>
        <translation>Spezial</translation>
    </message>
</context>
<context>
    <name>EndTurn</name>
    <message>
        <location filename="Command/Backend/Game/Ingame/EndTurn.cpp" line="33"/>
        <source>{&quot;%1&apos;s turn ends...&quot; : []}</source>
        <translation>{&quot;%1&apos;s Runde endet...&quot; : []}</translation>
    </message>
</context>
<context>
    <name>FirstTurn</name>
    <message>
        <location filename="Command/Backend/Game/Ingame/FirstTurn.cpp" line="35"/>
        <source>Choose a character...</source>
        <translation>Wähle einen Charakter...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/FirstTurn.cpp" line="69"/>
        <source>{&quot;Player %1 joined.&quot; : []}</source>
        <translation>{&quot;Player %1 ist beigetreten.&quot; : []}</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/FirstTurn.cpp" line="83"/>
        <source>Done</source>
        <translation>Fertig</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/FirstTurn.cpp" line="109"/>
        <source>Select</source>
        <translation>Wählen</translation>
    </message>
</context>
<context>
    <name>GameModel</name>
    <message>
        <location filename="Game/Model/GameModel.cpp" line="161"/>
        <source>%1 is no more...</source>
        <translation>%1 ist gestorben...</translation>
    </message>
    <message>
        <location filename="Game/Model/GameModel.cpp" line="161"/>
        <source>Your character died.</source>
        <translation>Dein Charakter ist gestorben.</translation>
    </message>
</context>
<context>
    <name>GeneralSetupWidget</name>
    <message>
        <location filename="Game/GeneralSetupWidget.cpp" line="28"/>
        <source>Players can choose their starting location?</source>
        <translation>Spieler können ihren Startpunkt wählen?</translation>
    </message>
</context>
<context>
    <name>Goblin</name>
    <message>
        <location filename="Object/Character/Race/Humanoid/Goblin.cpp" line="37"/>
        <source>Goblin</source>
        <translation>Goblin</translation>
    </message>
</context>
<context>
    <name>GoblinCharacter</name>
    <message>
        <location filename="Object/Character/GoblinCharacter.h" line="42"/>
        <source>Goblin</source>
        <translation>Goblin</translation>
    </message>
    <message>
        <location filename="Object/Character/GoblinCharacter.h" line="43"/>
        <source>Fiendish little beasts, with mishapen heads and sharp teeth.</source>
        <translation>Teuflische kleine Biester mit verformten Köpfen und scharfen Zähnen.</translation>
    </message>
</context>
<context>
    <name>HotseatGameModel</name>
    <message>
        <location filename="Game/Model/HotseatGameModel.cpp" line="59"/>
        <source>No more players available...</source>
        <translation>Keine Spieler mehr verfügbar...</translation>
    </message>
    <message>
        <location filename="Game/Model/HotseatGameModel.cpp" line="60"/>
        <source>No more players are available, therefore I&apos;ll quit the game.</source>
        <translation>Keine Spieler waren verfügbar, darum schließe ich das Spiel.</translation>
    </message>
    <message>
        <location filename="Game/Model/HotseatGameModel.cpp" line="115"/>
        <source>The map couldn&apos;t be loaded...</source>
        <translation>Die Karte konnte nicht geladen werden...</translation>
    </message>
    <message>
        <location filename="Game/Model/HotseatGameModel.cpp" line="116"/>
        <source>The map for the current player %1 could not be loaded so something went wrong.
I&apos;ll remove the player for now.</source>
        <translation>Die Karte für den aktuellen Spieler %1 konnte nicht gladen werden da etwas schief gegangen ist.
Der oben genannte Spieler wird entfernt.</translation>
    </message>
    <message>
        <location filename="Game/Model/HotseatGameModel.cpp" line="169"/>
        <source>{&quot;~ %1&apos;s turn ended ~&quot; : []}</source>
        <translation>{&quot;~ %1&apos;s Runde endet ~&quot; : []}</translation>
    </message>
</context>
<context>
    <name>Human</name>
    <message>
        <location filename="Object/Character/Race/Humanoid/Human.cpp" line="37"/>
        <source>Human</source>
        <translation>Mensch</translation>
    </message>
</context>
<context>
    <name>LoadGame</name>
    <message>
        <location filename="Command/Backend/Game/Startmenu/LoadGame.cpp" line="39"/>
        <source>New Game...</source>
        <translation>Neues Spiel...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Startmenu/LoadGame.cpp" line="46"/>
        <source>Solo / Hotseat</source>
        <translation>Solo / Hotseat</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Startmenu/LoadGame.cpp" line="50"/>
        <source>Network</source>
        <translation>Netzwerk</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Startmenu/LoadGame.cpp" line="80"/>
        <source>Choose a Save File to load the game from...</source>
        <translation>Wähle eine Datei um ein Spiel zu laden...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Startmenu/LoadGame.cpp" line="82"/>
        <source>TEoH Save Files (*</source>
        <translation>TEoH Speicher Dateien (*</translation>
    </message>
</context>
<context>
    <name>LoadMap</name>
    <message>
        <location filename="Command/Backend/Editor/Map/LoadMap.cpp" line="52"/>
        <source>Failed to load a map...</source>
        <translation>Laden der Karte fehlgeschlagen...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/Map/LoadMap.cpp" line="52"/>
        <source>Loading a map has failed.</source>
        <translation>Laden der Map ist fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>LoadProject</name>
    <message>
        <source>Load Project...</source>
        <oldsource>Create New Project...</oldsource>
        <translation type="obsolete">Neues Projekt laden...</translation>
    </message>
    <message>
        <source>TEoH Project Files (*</source>
        <oldsource>TEoH Project Files (*.teohproject)</oldsource>
        <translation type="obsolete">TEoH Projekt Dateien (*</translation>
    </message>
    <message>
        <source>Failed to load a project...</source>
        <oldsource>Failed to create a new project...</oldsource>
        <translation type="obsolete">Laden eines Projektes fehlgeschlagen...</translation>
    </message>
    <message>
        <source>Loading a project has failed.</source>
        <translation type="obsolete">Das Laden eines Projektes ist fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>LoadStartMapFromIndex</name>
    <message>
        <location filename="Command/Backend/Game/Ingame/LoadStartMapFromIndex.cpp" line="35"/>
        <source>No starting maps found...</source>
        <translation>Keine Startmaps gefunden...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/LoadStartMapFromIndex.cpp" line="35"/>
        <source>The world you selected did not provide any start maps.
Add starting maps in the editor first.

Aboring game...</source>
        <translation>Die Welt die du ausgewählt hast hat keine Startkarten bereit gestellt.
Füge Startkarten im Editor hinzu.

Spiel wird abgebrochen...</translation>
    </message>
</context>
<context>
    <name>LoadTempProject</name>
    <message>
        <source>Load Project...</source>
        <translation type="obsolete">Lade Projekt...</translation>
    </message>
    <message>
        <source>TEoH Project Files (*</source>
        <translation type="obsolete">TEoH Projekt Dateien (*</translation>
    </message>
    <message>
        <source>Failed to load a project...</source>
        <translation type="obsolete">Laden eines Projektes fehlgeschlagen...</translation>
    </message>
    <message>
        <source>Loading a project has failed.</source>
        <translation type="obsolete">Das Laden eines Projektes ist fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>LoadWorld</name>
    <message>
        <location filename="Command/Backend/Editor/World/LoadWorld.cpp" line="55"/>
        <source>Load World...</source>
        <translation>World Laden...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/LoadWorld.cpp" line="57"/>
        <source>TEoH World Files (*</source>
        <translation>TEoH World Dateien (*</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/LoadWorld.cpp" line="70"/>
        <source>Failed to load a world...</source>
        <translation>Laden einer World fehlgeschlagen...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/LoadWorld.cpp" line="70"/>
        <source>Loading a world has failed.</source>
        <translation>Laden einer Welt ist fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Project Manager</source>
        <translation type="obsolete">Projekt Verwalter</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="123"/>
        <source>Editor Tools</source>
        <translation>Editor Werkzeug</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="200"/>
        <source>Welcome to the TEoH Editor.</source>
        <translation>Willkommen im TEoH Editor.</translation>
    </message>
    <message>
        <source>I&apos;m sorry to interrupt you, but I need to ask you one question before we can begin.

Should I create a new project or load an existing one?</source>
        <translation type="obsolete">Es tut mir leid sie unterbrechen zu müssen, aber ich muss ihnen noch eine Frage stellen bevor wir anfangen.

Soll ich ein neues Projekt erstellen oder ein existierendes Laden?</translation>
    </message>
    <message>
        <source>New...</source>
        <translation type="obsolete">Neu...</translation>
    </message>
    <message>
        <source>Load...</source>
        <translation type="obsolete">Laden...</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="114"/>
        <source> World Manager</source>
        <translation> World Verwaltung</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="160"/>
        <source>(Character Overview)</source>
        <translation>(Charakter Übersicht)</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="182"/>
        <source>The Epic of Heroes %1</source>
        <oldsource>The Epic of Heroes</oldsource>
        <translation>The Epic of Heroes %1</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="201"/>
        <source>I&apos;m sorry to interrupt you, but I need to ask you one question before we can begin.

Should I create a new world or load an existing one?</source>
        <translation>Es tut mir leid sie unterbrechen zu müssen, aber ich muss ihnen noch eine Frage stellen bevor wir anfangen.

Soll ich eine neue World erstellen oder ein existierendes Laden?</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="202"/>
        <source>New World...</source>
        <translation>Neues Welt...</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="203"/>
        <source>Load World...</source>
        <translation>Welt Laden...</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="204"/>
        <location filename="MainWindow.cpp" line="269"/>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="263"/>
        <source>TEoH Main Menu</source>
        <translation>TEoH Hauptmenü</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="264"/>
        <source>What do you want to do?</source>
        <translation>Was möchten Sie machen?</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="265"/>
        <source>Play a New World...</source>
        <translation>Starte eine neue Welt...</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="266"/>
        <source>Load Game...</source>
        <oldsource>New Game...</oldsource>
        <translation>Spiel Laden...</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="267"/>
        <source>Connect to...</source>
        <translation>Verbinden...</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="268"/>
        <source>Resume E-Mail Game...</source>
        <translation>E-Mail Spiel weiterspielen...</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="362"/>
        <source> (Character Overview)</source>
        <translation> (Charakter Übersicht)</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="413"/>
        <source>New Map</source>
        <translation>Neue Map</translation>
    </message>
</context>
<context>
    <name>MapPropertiesDialog</name>
    <message>
        <location filename="Editor/MapPropertiesDialog.cpp" line="42"/>
        <source>Right</source>
        <translation>Rechts</translation>
    </message>
    <message>
        <location filename="Editor/MapPropertiesDialog.cpp" line="41"/>
        <source>Left</source>
        <translation>Links</translation>
    </message>
    <message>
        <location filename="Editor/MapPropertiesDialog.cpp" line="48"/>
        <source>Up</source>
        <translation>Oben</translation>
    </message>
    <message>
        <location filename="Editor/MapPropertiesDialog.cpp" line="49"/>
        <source>Down</source>
        <translation>Unten</translation>
    </message>
    <message>
        <location filename="Editor/MapPropertiesDialog.cpp" line="55"/>
        <source>Top</source>
        <translation>Tiefer</translation>
    </message>
    <message>
        <location filename="Editor/MapPropertiesDialog.cpp" line="56"/>
        <source>Bottom</source>
        <translation>Höher</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="obsolete">Ok</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="obsolete">Anwenden</translation>
    </message>
    <message>
        <location filename="Editor/MapPropertiesDialog.cpp" line="77"/>
        <source>Geometry</source>
        <translation>Geometrie</translation>
    </message>
    <message>
        <location filename="Editor/MapPropertiesDialog.cpp" line="65"/>
        <source>Horizontal Square Count: </source>
        <translation>Horizontale Felderanzahl: </translation>
    </message>
    <message>
        <location filename="Editor/MapPropertiesDialog.cpp" line="68"/>
        <source>Vertical Square Count: </source>
        <translation>Vertikale Felderanzahl: </translation>
    </message>
    <message>
        <location filename="Editor/MapPropertiesDialog.cpp" line="71"/>
        <source>Max Height: </source>
        <translation>Maximale Höhe: </translation>
    </message>
</context>
<context>
    <name>MouseEditorInput</name>
    <message>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="68"/>
        <source>Character</source>
        <translation>Charakter</translation>
    </message>
    <message>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="69"/>
        <source>Wall</source>
        <translation>Wand</translation>
    </message>
    <message>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="70"/>
        <source>Floor</source>
        <translation>Boden</translation>
    </message>
    <message>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="71"/>
        <source>Items</source>
        <translation>Items</translation>
    </message>
    <message>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="72"/>
        <source>Specials</source>
        <translation>Specials</translation>
    </message>
    <message>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="74"/>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="87"/>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="96"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="76"/>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="89"/>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="98"/>
        <source>Cut</source>
        <translation>Ausschneiden</translation>
    </message>
    <message>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="78"/>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="91"/>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="100"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="80"/>
        <source>Add selected item to Inventory</source>
        <translation>Ausgewähltes Item ins Inventar legen</translation>
    </message>
    <message>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="82"/>
        <source>Edit Character Inventory...</source>
        <translation>Charakterinventar editiere...</translation>
    </message>
    <message>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="84"/>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="93"/>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="102"/>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="111"/>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="120"/>
        <source>Edit...</source>
        <translation>Editieren...</translation>
    </message>
    <message>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="105"/>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="114"/>
        <source>Copy...</source>
        <translation>Kopieren...</translation>
    </message>
    <message>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="107"/>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="116"/>
        <source>Cut...</source>
        <translation>Ausschneiden...</translation>
    </message>
    <message>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="109"/>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="118"/>
        <source>Remove...</source>
        <translation>Entfernen...</translation>
    </message>
    <message>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="123"/>
        <source>Paste</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location filename="MainView/Model/MouseInput/MouseEditorInput.cpp" line="126"/>
        <source>Remove All</source>
        <translation>Alles entfernen</translation>
    </message>
</context>
<context>
    <name>MouseGameInput</name>
    <message>
        <location filename="MainView/Model/MouseInput/MouseGameInput.cpp" line="61"/>
        <source> (YOU)</source>
        <translation> (DU)</translation>
    </message>
    <message>
        <location filename="MainView/Model/MouseInput/MouseGameInput.cpp" line="69"/>
        <source>Items</source>
        <translation>Items</translation>
    </message>
    <message>
        <location filename="MainView/Model/MouseInput/MouseGameInput.cpp" line="93"/>
        <source>Specials</source>
        <translation>Specials</translation>
    </message>
    <message>
        <location filename="MainView/Model/MouseInput/MouseGameInput.cpp" line="118"/>
        <source>End Turn</source>
        <translation>Runde beenden</translation>
    </message>
    <message>
        <location filename="MainView/Model/MouseInput/MouseGameInput.cpp" line="120"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
</context>
<context>
    <name>MoveCharacter</name>
    <message>
        <location filename="Command/Backend/Game/Map/MoveCharacter.cpp" line="42"/>
        <source>moved</source>
        <translation>lief</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Map/MoveCharacter.cpp" line="47"/>
        <source>hustled</source>
        <translation>sprintet</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Map/MoveCharacter.cpp" line="51"/>
        <source>ran</source>
        <translation>rannte</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Map/MoveCharacter.cpp" line="68"/>
        <source>{&quot;%1 %2 %3ft.&quot; : []}</source>
        <translation>{&quot;%1 %2 %3ft.&quot; : []}</translation>
    </message>
</context>
<context>
    <name>MudFloor</name>
    <message>
        <location filename="Object/Tile/MudFloor.cpp" line="25"/>
        <source>Mud Floor</source>
        <translation>Dreckboden</translation>
    </message>
    <message>
        <location filename="Object/Tile/MudFloor.cpp" line="26"/>
        <source>Muddy, dry and stable looking ground.</source>
        <translation>Dreckig, trocken aber stabil aussehender Grund.</translation>
    </message>
</context>
<context>
    <name>NetworkSetupWidget</name>
    <message>
        <location filename="Game/NetworkSetupWidget.cpp" line="32"/>
        <source>Start Game</source>
        <translation>Spiel starten</translation>
    </message>
</context>
<context>
    <name>NewFolder</name>
    <message>
        <location filename="Command/Backend/Editor/World/NewFolder.cpp" line="46"/>
        <source>New Folder...</source>
        <translation>Neuer Ordner...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/NewFolder.cpp" line="47"/>
        <source>Enter the name of the new folder.</source>
        <translation>Gebe den Namen des neuen Ordners ein.</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/NewFolder.cpp" line="49"/>
        <source>New Folder</source>
        <oldsource>Folder Name</oldsource>
        <translation>Neuer Ordner</translation>
    </message>
</context>
<context>
    <name>NewMapFile</name>
    <message>
        <location filename="Command/Backend/Editor/World/NewMapFile.cpp" line="52"/>
        <source>New Map...</source>
        <translation>Neue Karte...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/NewMapFile.cpp" line="53"/>
        <source>Enter the name of the new map.</source>
        <translation>Gebe den Namen der neuen Karte ein.</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/NewMapFile.cpp" line="55"/>
        <source>New Map</source>
        <oldsource>Map Name</oldsource>
        <translation>Neue Map</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/NewMapFile.cpp" line="91"/>
        <source>Failed to create a map file...</source>
        <translation>Karte erstellen ist fehlgeschlagen...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/NewMapFile.cpp" line="91"/>
        <source>Creating a map file has failed.</source>
        <translation>Erstellen der Karte ist fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>NewProject</name>
    <message>
        <source>Create New Project...</source>
        <translation type="obsolete">Neues Project erstellen...</translation>
    </message>
    <message>
        <source>TEoH Project Files (*.teohproject)</source>
        <translation type="obsolete">TEoH Projekt Dateien (*.teohproject)</translation>
    </message>
    <message>
        <source>TEoH Project Files (*</source>
        <translation type="obsolete">TEoH Projekt Dateien (*</translation>
    </message>
    <message>
        <source>Failed to create a new project...</source>
        <translation type="obsolete">Erstellen eines Projektes fehlgeschlagen...</translation>
    </message>
    <message>
        <source>Creating a new project has failed.</source>
        <translation type="obsolete">Das Erstellen eines neuen Projektes ist fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>NewWorld</name>
    <message>
        <location filename="Command/Backend/Editor/World/NewWorld.cpp" line="53"/>
        <source>Create New World...</source>
        <translation>Neue World erstellen...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/NewWorld.cpp" line="55"/>
        <source>TEoH World Files (*</source>
        <translation>TEoH World Dateien (*</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/NewWorld.cpp" line="90"/>
        <source>Failed to create a new world...</source>
        <translation>Erstellen einer neuen World fehlgeschlagen...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/NewWorld.cpp" line="90"/>
        <source>Creating a new world has failed.</source>
        <translation>Erstellen einer neuen Welt ist fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>ObjectBase</name>
    <message>
        <location filename="Object/ObjectBase.cpp" line="324"/>
        <source>Description...</source>
        <translation>Beschreibung...</translation>
    </message>
</context>
<context>
    <name>ObjectCharacter</name>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="79"/>
        <source>Melee</source>
        <translation>Nahkampf</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="83"/>
        <location filename="Object/ObjectCharacter.cpp" line="440"/>
        <source>Unarmed</source>
        <translation>Unbewaffnet</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="88"/>
        <source>Improvised (%1)</source>
        <translation>Improvisiert (%1)</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="103"/>
        <location filename="Object/ObjectCharacter.cpp" line="452"/>
        <source>Off-Hand</source>
        <translation>Off-Hand</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="116"/>
        <source>Damage Roll</source>
        <translation>SchadenWurf</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="141"/>
        <source>Critical Damage Roll</source>
        <translation>Kritischer Schadenwurf</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="427"/>
        <source>Attack (Standard)</source>
        <translation>Angriff (Standard)</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="447"/>
        <source> (Improvised)</source>
        <translation> (Improvisiert)</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="451"/>
        <source>%1: %2</source>
        <translation>%1: %2</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="452"/>
        <source>Main-Hand</source>
        <translation>Main-Hand</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="486"/>
        <source>Two-Weapon Attack (Full-Round)</source>
        <translation>Zwei-Waffen Angriff (Full-Round)</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation>Bewusstlos</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="501"/>
        <source>Unconcious</source>
        <translation>Bewusstlos</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="518"/>
        <source>COMBAT</source>
        <translation>KAMPF</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="519"/>
        <source>Hit Points   : %1/%2</source>
        <translation>Hit Points   : %1/%2</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="519"/>
        <source>Non-Lethal Damage: 0</source>
        <translation>Nicht-Tödlicher Schaden: 0</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="540"/>
        <source>%1    : %2</source>
        <translation>%1    : %2</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="543"/>
        <source>%1 (%2) [%3 | %4/%5 | S]</source>
        <oldsource>%1 (%2) [%3 | %4/%5 | B]</oldsource>
        <translation>%1 (%2) [%3 | %4/%5 | S]</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="553"/>
        <source>Armor Class  : %1</source>
        <translation>Armor Class  : %1</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="554"/>
        <source>Touch AC      : %1
Flat-Footed AC: %2</source>
        <translation>Touch AC      : %1
Flat-Footed AC: %2</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="556"/>
        <source>ACTIONS</source>
        <translation>AKTIONEN</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="557"/>
        <source>Standard Action  : %1</source>
        <translation>Standard Action  : %1</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="557"/>
        <location filename="Object/ObjectCharacter.cpp" line="558"/>
        <location filename="Object/ObjectCharacter.cpp" line="559"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="557"/>
        <location filename="Object/ObjectCharacter.cpp" line="558"/>
        <location filename="Object/ObjectCharacter.cpp" line="559"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="558"/>
        <source>Move Action      : %1</source>
        <translation>Move Action      : %1</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="559"/>
        <source>Full-Round Action: %1</source>
        <translation>Full-Round Action: %1</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="561"/>
        <source>SPEED</source>
        <translation>GESCHWINDIGKEIT</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="562"/>
        <source>Movement: %1ft.</source>
        <translation>Laufen: %1ft.</translation>
    </message>
    <message>
        <location filename="Object/ObjectCharacter.cpp" line="563"/>
        <source>Hustle: %1ft.
Run   : %2ft.</source>
        <translation>Sprinten: %1ft.
Rennen   : %2ft.</translation>
    </message>
</context>
<context>
    <name>ObjectContainerItem</name>
    <message>
        <location filename="Object/Item/ObjectContainerItem.cpp" line="52"/>
        <source>Search (Standard)</source>
        <translation>Durchsuchen (Standard)</translation>
    </message>
</context>
<context>
    <name>ObjectItem</name>
    <message>
        <location filename="Object/ObjectItem.cpp" line="49"/>
        <source>Improvised(%1)</source>
        <translation>Improvisiert(%1)</translation>
    </message>
    <message>
        <location filename="Object/ObjectItem.cpp" line="127"/>
        <source>Drop Item (Move)</source>
        <translation>Item hinlegen (Move)</translation>
    </message>
    <message>
        <location filename="Object/ObjectItem.cpp" line="131"/>
        <source>Pick Up (Move)</source>
        <translation>Aufheben (Move)</translation>
    </message>
</context>
<context>
    <name>ObjectMenu</name>
    <message>
        <location filename="MainView/ObjectMenu.cpp" line="50"/>
        <source>No Interactions...</source>
        <translation>Keine Interaktionen...</translation>
    </message>
</context>
<context>
    <name>OptionsLayout</name>
    <message>
        <location filename="Common/OptionsLayout.cpp" line="44"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="Common/OptionsLayout.cpp" line="45"/>
        <source>Apply</source>
        <translation>Anwenden</translation>
    </message>
</context>
<context>
    <name>OverheadMap</name>
    <message>
        <source>Empty Map</source>
        <oldsource>Blank Map</oldsource>
        <translation type="obsolete">Leere Karte</translation>
    </message>
</context>
<context>
    <name>OverheadStack</name>
    <message>
        <source>Remove Player</source>
        <translation type="obsolete">Spieler entfernen</translation>
    </message>
    <message>
        <source>Remove Ceiling</source>
        <translation type="obsolete">Decke entfernen</translation>
    </message>
    <message>
        <source>Remove Character</source>
        <translation type="obsolete">Charakter entfernen</translation>
    </message>
    <message>
        <source>Remove Wall</source>
        <translation type="obsolete">Wand entfernen</translation>
    </message>
    <message>
        <source>Remove Floor</source>
        <translation type="obsolete">Boden entfernen</translation>
    </message>
    <message>
        <source>Remove Characters...</source>
        <translation type="obsolete">Charakter entfernen...</translation>
    </message>
    <message>
        <source>Remove Items...</source>
        <translation type="obsolete">Gegenstände entfernen...</translation>
    </message>
    <message>
        <source>Character</source>
        <translation type="obsolete">Charakter</translation>
    </message>
    <message>
        <source>Wall</source>
        <translation type="obsolete">Wand</translation>
    </message>
    <message>
        <source>Floor</source>
        <translation type="obsolete">Boden</translation>
    </message>
    <message>
        <source>Items</source>
        <translation type="obsolete">Items</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="obsolete">Entfernen</translation>
    </message>
    <message>
        <source>Edit...</source>
        <translation type="obsolete">Editieren...</translation>
    </message>
    <message>
        <source>Remove...</source>
        <translation type="obsolete">Entfernen...</translation>
    </message>
    <message>
        <source>Remove All</source>
        <translation type="obsolete">Alles entfernen</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="obsolete">Kopieren</translation>
    </message>
    <message>
        <source>Place Object</source>
        <translation type="obsolete">Platziere Objekt</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation type="obsolete">Ausschneiden</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="obsolete">Einfügen</translation>
    </message>
    <message>
        <source>Copy...</source>
        <translation type="obsolete">Kopieren...</translation>
    </message>
    <message>
        <source>Cut...</source>
        <translation type="obsolete">Ausschneiden...</translation>
    </message>
    <message>
        <source>Paste...</source>
        <translation type="obsolete">Einfügen...</translation>
    </message>
</context>
<context>
    <name>Player</name>
    <message>
        <source>Description...</source>
        <translation>Beschreibung...</translation>
    </message>
    <message>
        <source>Hero</source>
        <translation>Held</translation>
    </message>
    <message>
        <source>Just another hero.</source>
        <translation>Nur ein weiterer Held.</translation>
    </message>
    <message>
        <location filename="Object/Character/Player.cpp" line="80"/>
        <source>Inventory...</source>
        <translation>Inventar...</translation>
    </message>
    <message>
        <location filename="Object/Character/Player.h" line="42"/>
        <source>Player</source>
        <translation>Spieler</translation>
    </message>
    <message>
        <location filename="Object/Character/Player.h" line="42"/>
        <source>Just another player.</source>
        <translation>Nur ein weiterer Spieler.</translation>
    </message>
</context>
<context>
    <name>PlayerInventoryDialog</name>
    <message>
        <location filename="Game/Character/PlayerInventoryDialog.cpp" line="26"/>
        <source>Inventory</source>
        <translation>Inventar</translation>
    </message>
    <message>
        <location filename="Game/Character/PlayerInventoryDialog.cpp" line="42"/>
        <source>Weight: 0/150.00 lbs.   Space: 0/512</source>
        <translation>Gewicht: 0/150.00 lbs.   Platz: 0/512</translation>
    </message>
    <message>
        <location filename="Game/Character/PlayerInventoryDialog.cpp" line="43"/>
        <source>Currency: 0 CP 0 SP 0 GP 0 PP</source>
        <translation>Geld: 0 CP 0 SP 0 GP 0 PP</translation>
    </message>
</context>
<context>
    <name>PlayerWidget</name>
    <message>
        <location filename="Game/PlayerWidget.cpp" line="46"/>
        <source>Character Sheet</source>
        <translation>Charakter Blatt</translation>
    </message>
    <message>
        <location filename="Game/PlayerWidget.cpp" line="47"/>
        <source>Inventory</source>
        <translation>Inventar</translation>
    </message>
    <message>
        <location filename="Game/PlayerWidget.cpp" line="57"/>
        <source>End Turn...</source>
        <translation>Runde beenden...</translation>
    </message>
</context>
<context>
    <name>PreloadWorld</name>
    <message>
        <location filename="Command/Backend/Game/Startmenu/PreloadWorld.cpp" line="46"/>
        <source>Choose a New World to play in...</source>
        <translation>Wähle eine neue World in der gespielt werden soll...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Startmenu/PreloadWorld.cpp" line="48"/>
        <source>TEoH World Files (*</source>
        <translation>TEoH World Dateien (*</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Startmenu/PreloadWorld.cpp" line="61"/>
        <source>Failed to load a world...</source>
        <translation>Laden einer World fehlgeschlagen...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Startmenu/PreloadWorld.cpp" line="61"/>
        <source>Loading a world has failed.</source>
        <translation>Laden einer World ist fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>ProjectFileTree</name>
    <message>
        <source>New Map</source>
        <translation type="obsolete">Neue Karte</translation>
    </message>
    <message>
        <source>New Folder</source>
        <translation type="obsolete">Neuer Ordner</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="obsolete">Kopieren</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation type="obsolete">Ausschneiden</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="obsolete">Einfügen</translation>
    </message>
    <message>
        <source>New Map...</source>
        <translation type="obsolete">Neue Karte...</translation>
    </message>
    <message>
        <source>New Folder...</source>
        <translation type="obsolete">Neuer Ordner...</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="obsolete">Umbenennen</translation>
    </message>
    <message>
        <source>Duplicate</source>
        <translation type="obsolete">Duplizieren</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="obsolete">Entfernen</translation>
    </message>
</context>
<context>
    <name>ProjectPropertiesDialog</name>
    <message>
        <source>Add &gt;&gt;</source>
        <translation type="obsolete">Hinzufügen &gt;&gt;</translation>
    </message>
    <message>
        <source>&lt;&lt; Remove</source>
        <translation type="obsolete">&lt;&lt; Entfernen</translation>
    </message>
    <message>
        <source>Available Maps:</source>
        <translation type="obsolete">Verfügbare Maps:</translation>
    </message>
    <message>
        <source>Starting Maps:</source>
        <translation type="obsolete">Starter Maps:</translation>
    </message>
    <message>
        <source>Starting Maps</source>
        <translation type="obsolete">Starter Maps</translation>
    </message>
</context>
<context>
    <name>ProjectView</name>
    <message>
        <source>Project Manager</source>
        <translation type="obsolete">Projekt Verwalter</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="Common/DiceAttack.cpp" line="25"/>
        <source>Threat Confirm</source>
        <translation>Threat bestätigen</translation>
    </message>
    <message>
        <location filename="Common/DiceAttack.cpp" line="50"/>
        <location filename="Common/DiceAttack.cpp" line="126"/>
        <location filename="Common/DiceAttack.cpp" line="153"/>
        <source>Attack not rolled yet.</source>
        <translation>Angriff noch nicht gewürfelt.</translation>
    </message>
    <message>
        <location filename="Common/DiceAttack.cpp" line="59"/>
        <source>Hit!</source>
        <translation>Treffer!</translation>
    </message>
    <message>
        <location filename="Common/DiceAttack.cpp" line="63"/>
        <location filename="Common/DiceAttack.cpp" line="143"/>
        <source>Miss!</source>
        <translation>Verfehlt!</translation>
    </message>
    <message>
        <location filename="Common/DiceAttack.cpp" line="71"/>
        <source>%1: Rolled a %2 %3</source>
        <translation>%1: würfelt eine %2 %3</translation>
    </message>
    <message>
        <location filename="Common/DiceAttack.cpp" line="88"/>
        <source>Confirm not rolled yet.</source>
        <translation>Bestätigung noch nicht gewürfelt.</translation>
    </message>
    <message>
        <location filename="Common/DiceAttack.cpp" line="93"/>
        <source>No critical threat.</source>
        <translation>Kein kritischer threat.</translation>
    </message>
    <message>
        <location filename="Common/DiceAttack.cpp" line="102"/>
        <source>Critical!</source>
        <translation>Kritisch!</translation>
    </message>
    <message>
        <location filename="Common/DiceAttack.cpp" line="106"/>
        <source>Failed!</source>
        <translation>Fehlgeschlagen!</translation>
    </message>
    <message>
        <location filename="Common/DiceAttack.cpp" line="114"/>
        <source>%1: Rolled %2 %3</source>
        <translation>%1: Würfelt %2 %3</translation>
    </message>
    <message>
        <location filename="Common/DiceAttack.cpp" line="136"/>
        <source>Crit. </source>
        <translation>Crit. </translation>
    </message>
    <message>
        <location filename="Common/DiceAttack.cpp" line="139"/>
        <source>%2 %1Dmg!</source>
        <translation>%2 %1Dmg!</translation>
    </message>
    <message>
        <location filename="Common/DiceAttack.cpp" line="163"/>
        <source>Critical </source>
        <translation>Kritisch </translation>
    </message>
    <message>
        <location filename="Common/DiceAttack.cpp" line="166"/>
        <source>Rolled %2 %1Damage!</source>
        <translation>%2 %1Schaden gewürfelt!</translation>
    </message>
    <message>
        <location filename="Common/DiceAttack.cpp" line="170"/>
        <source>Missed!</source>
        <translation>Verfehlt!</translation>
    </message>
    <message>
        <location filename="Object/Character/Abilities/Abilities.cpp" line="70"/>
        <source>STR</source>
        <translation>STR</translation>
    </message>
    <message>
        <location filename="Object/Character/Abilities/Abilities.cpp" line="80"/>
        <source>DEX</source>
        <translation>DEX</translation>
    </message>
    <message>
        <location filename="Object/Character/Abilities/Abilities.cpp" line="90"/>
        <source>CON</source>
        <translation>CON</translation>
    </message>
    <message>
        <location filename="Object/Character/Abilities/Abilities.cpp" line="100"/>
        <source>INT</source>
        <translation>INT</translation>
    </message>
    <message>
        <location filename="Object/Character/Abilities/Abilities.cpp" line="110"/>
        <source>WIS</source>
        <translation>WIS</translation>
    </message>
    <message>
        <location filename="Object/Character/Abilities/Abilities.cpp" line="120"/>
        <source>CHA</source>
        <translation>CHA</translation>
    </message>
    <message>
        <location filename="Object/Character/Abilities/Abilities.cpp" line="127"/>
        <source>STR: %1 (%2)
</source>
        <translation>STR: %1 (%2</translation>
    </message>
    <message>
        <location filename="Object/Character/Abilities/Abilities.cpp" line="128"/>
        <source>DEX: %1 (%2)
</source>
        <translation>DEX: %1 (%2)</translation>
    </message>
    <message>
        <location filename="Object/Character/Abilities/Abilities.cpp" line="129"/>
        <source>CON: %1 (%2)
</source>
        <translation>CON: %1 (%2)</translation>
    </message>
    <message>
        <location filename="Object/Character/Abilities/Abilities.cpp" line="130"/>
        <source>INT: %1 (%2)
</source>
        <translation>INT: %1 (%2)</translation>
    </message>
    <message>
        <location filename="Object/Character/Abilities/Abilities.cpp" line="131"/>
        <source>WIS: %1 (%2)
</source>
        <translation>WIS: %1 (%2)</translation>
    </message>
    <message>
        <location filename="Object/Character/Abilities/Abilities.cpp" line="132"/>
        <source>CHA: %1 (%2)</source>
        <translation>CHA: %1 (%2)</translation>
    </message>
    <message>
        <location filename="Object/ObjectCommon.h" line="69"/>
        <source>Not a slot.</source>
        <translation>Nicht ein Slot.</translation>
    </message>
    <message>
        <location filename="Object/ObjectCommon.h" line="73"/>
        <source>Main-Hand</source>
        <oldsource>Main Hand</oldsource>
        <translation>Main-Hand</translation>
    </message>
    <message>
        <location filename="Object/ObjectCommon.h" line="74"/>
        <source>Off-Hand</source>
        <oldsource>Off Hand</oldsource>
        <translation>Off-Hand</translation>
    </message>
    <message>
        <location filename="Object/ObjectCommon.h" line="75"/>
        <source>Head</source>
        <translation>Kopf</translation>
    </message>
    <message>
        <location filename="Object/ObjectCommon.h" line="76"/>
        <source>Headband</source>
        <translation>Stirn</translation>
    </message>
    <message>
        <location filename="Object/ObjectCommon.h" line="77"/>
        <source>Eyes</source>
        <translation>Augen</translation>
    </message>
    <message>
        <location filename="Object/ObjectCommon.h" line="78"/>
        <source>Shoulders</source>
        <translation>Schultern</translation>
    </message>
    <message>
        <location filename="Object/ObjectCommon.h" line="79"/>
        <source>Neck</source>
        <translation>Nacken</translation>
    </message>
    <message>
        <location filename="Object/ObjectCommon.h" line="80"/>
        <source>Chest</source>
        <translation>Brust</translation>
    </message>
    <message>
        <location filename="Object/ObjectCommon.h" line="81"/>
        <source>Body</source>
        <translation>Körper</translation>
    </message>
    <message>
        <location filename="Object/ObjectCommon.h" line="82"/>
        <source>Armor</source>
        <translation>Rüstung</translation>
    </message>
    <message>
        <location filename="Object/ObjectCommon.h" line="83"/>
        <source>Belt</source>
        <translation>Gürtel</translation>
    </message>
    <message>
        <location filename="Object/ObjectCommon.h" line="84"/>
        <source>Hands</source>
        <translation>Hände</translation>
    </message>
    <message>
        <location filename="Object/ObjectCommon.h" line="85"/>
        <source>Ring</source>
        <translation>Ring</translation>
    </message>
    <message>
        <location filename="Object/ObjectCommon.h" line="86"/>
        <source>Feet</source>
        <translation>Füße</translation>
    </message>
</context>
<context>
    <name>Quit</name>
    <message>
        <location filename="Command/Backend/Common/Quit.cpp" line="55"/>
        <source>Quit the Game?</source>
        <translation>Spiel beenden?</translation>
    </message>
    <message>
        <location filename="Command/Backend/Common/Quit.cpp" line="64"/>
        <source>Do you want to save the game before quitting?</source>
        <translation>Möchtest du das Spiel speichern, bevor du es beendest?</translation>
    </message>
    <message>
        <location filename="Command/Backend/Common/Quit.cpp" line="65"/>
        <source>Save and Quit</source>
        <translation>Speichern und Beenden</translation>
    </message>
    <message>
        <location filename="Command/Backend/Common/Quit.cpp" line="66"/>
        <location filename="Command/Backend/Common/Quit.cpp" line="72"/>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="Command/Backend/Common/Quit.cpp" line="67"/>
        <location filename="Command/Backend/Common/Quit.cpp" line="73"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="Command/Backend/Common/Quit.cpp" line="71"/>
        <source>Do you really want to quit?
Unsaved progress will be lost.</source>
        <translation>Möchtest du wirklich das Spiel beenden?
Ungespeicherte Änderungen werden verloren gehen.</translation>
    </message>
    <message>
        <location filename="Command/Backend/Common/Quit.cpp" line="96"/>
        <source>{&quot;Player %1 left.&quot; : []}</source>
        <translation>{&quot;Spieler %1 verlässt das Spiel.&quot; : []}</translation>
    </message>
</context>
<context>
    <name>RemoveItem</name>
    <message>
        <location filename="Command/Backend/Editor/Map/RemoveItem.cpp" line="47"/>
        <source>Select item(s) to remove...</source>
        <translation>Zu entfernende Item(s) auswählen...</translation>
    </message>
</context>
<context>
    <name>RemoveProjectEntry</name>
    <message>
        <source>Folder</source>
        <translation type="obsolete">Ordner</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="obsolete">Datei</translation>
    </message>
    <message>
        <source>Removing </source>
        <oldsource>Removing</oldsource>
        <translation type="obsolete">Entfernen </translation>
    </message>
    <message>
        <source>?</source>
        <translation type="obsolete"> wirklich entfernen?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Ja</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Nein</translation>
    </message>
</context>
<context>
    <name>RemoveWorldEntry</name>
    <message>
        <location filename="Command/Backend/Editor/World/RemoveWorldEntry.cpp" line="51"/>
        <source>Folder</source>
        <translation>Ordner</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/RemoveWorldEntry.cpp" line="55"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/RemoveWorldEntry.cpp" line="61"/>
        <source>Removing </source>
        <translation>Entfernen </translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/RemoveWorldEntry.cpp" line="62"/>
        <source>Do you really want to remove this </source>
        <translation>Wills du  </translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/RemoveWorldEntry.cpp" line="62"/>
        <source>?</source>
        <translation> wirklich entfernen?</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/RemoveWorldEntry.cpp" line="63"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/RemoveWorldEntry.cpp" line="64"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
</context>
<context>
    <name>RenameProjectEntry</name>
    <message>
        <source>Rename...</source>
        <translation type="obsolete">Umbenennen...</translation>
    </message>
    <message>
        <source>Rename the entry.</source>
        <translation type="obsolete">Eintrag umbenennen.</translation>
    </message>
</context>
<context>
    <name>RenameWorldEntry</name>
    <message>
        <location filename="Command/Backend/Editor/World/RenameWorldEntry.cpp" line="49"/>
        <source>Rename...</source>
        <translation>Umbenennen...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/RenameWorldEntry.cpp" line="50"/>
        <source>Rename the entry.</source>
        <translation>Eintrag umbenennen.</translation>
    </message>
</context>
<context>
    <name>ResumeEMailGame</name>
    <message>
        <location filename="Command/Backend/Game/Startmenu/ResumeEMailGame.cpp" line="66"/>
        <source>Open a saved E-Mail Game...</source>
        <translation>Ein gespeichertes E-Mail spiel öffnen...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Startmenu/ResumeEMailGame.cpp" line="68"/>
        <source>TEoH E-Mail Save Files (*</source>
        <translation>TEoH E-Mail Save Dateien (*</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Startmenu/ResumeEMailGame.cpp" line="91"/>
        <source>It&apos;s not your turn.</source>
        <translation>Es ist nicht deine Runde.</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Startmenu/ResumeEMailGame.cpp" line="92"/>
        <source>Sorry, but it is not your turn. It&apos;s %1&apos;s turn.
If you want to join, ask anyone playing to add a new player to the game.</source>
        <translation>Sorry, aber du bist nicht dran. %1 ist dran.
Wenn du dem Spiel beitreten willst, frage einen Mitspieler ob er einen neuen Spieler hinzufügt.</translation>
    </message>
</context>
<context>
    <name>SaveEMailGame</name>
    <message>
        <location filename="Command/Backend/Game/Ingame/SaveEMailGame.cpp" line="70"/>
        <source>Failed to save the E-Mail game...</source>
        <translation>Speichern eines E-Mail Spiels fehlgeschlagen...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/SaveEMailGame.cpp" line="70"/>
        <source>Saving the E-Mail game has failed.</source>
        <translation>Speichern des E-Mail Spiels ist fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/SaveEMailGame.cpp" line="77"/>
        <location filename="Command/Backend/Game/Ingame/SaveEMailGame.cpp" line="80"/>
        <source>E-Mail Game saved...</source>
        <translation>E-Mail Spiel gespeichert...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/SaveEMailGame.cpp" line="77"/>
        <source>The Game has been successfully saved.</source>
        <translation>Das Spiel wurde erfolgriech gespeichert.</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/SaveEMailGame.cpp" line="81"/>
        <source>The E-Mail Game for the next turn has been saved to

%1

Next Turn: %2</source>
        <translation>Das E-Mail Spiel für die nächste Runde wurde an folgendem Ort gespeichert:

%1

Nächster: %2</translation>
    </message>
</context>
<context>
    <name>SaveGame</name>
    <message>
        <location filename="Command/Backend/Game/Ingame/SaveGame.cpp" line="51"/>
        <source>Choose a File to Save the game in...</source>
        <translation>Wähle eine Datei um das Spiel zu speichern...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/SaveGame.cpp" line="53"/>
        <source>TEoH Save Files (*</source>
        <translation>TEoH Save Dateien (*</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/SaveGame.cpp" line="71"/>
        <source>Failed to save the game...</source>
        <translation>Fehlgeschlagen das Spiel zu speichern...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/SaveGame.cpp" line="71"/>
        <source>Saving the game has failed.</source>
        <translation>Speichern des Spiels ist fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>SaveMap</name>
    <message>
        <location filename="Command/Backend/Editor/World/SaveMap.cpp" line="45"/>
        <source>Save Map As...</source>
        <translation>Map Speichern unter...</translation>
    </message>
    <message>
        <source>TEoH Map Files (*.tmap)</source>
        <translation type="obsolete">TEoH Map Dateien (*.tmap)</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/SaveMap.cpp" line="47"/>
        <source>TEoH Map Files (*</source>
        <translation>TEoH Karten Dateien (*</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/SaveMap.cpp" line="67"/>
        <source>Failed to save the map...</source>
        <translation>Speichern der Map fehlgeschlagen...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/SaveMap.cpp" line="67"/>
        <source>Saving the map has failed.</source>
        <translation>Das Speichern der Karte ist fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>SaveMapAs</name>
    <message>
        <location filename="Command/Backend/Editor/World/SaveMapAs.cpp" line="41"/>
        <source>Save Map As...</source>
        <translation>Speichern als...</translation>
    </message>
    <message>
        <source>TEoH Map Files (*.tmap)</source>
        <translation type="obsolete">TEoH Map Dateien (*.tmap)</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/SaveMapAs.cpp" line="43"/>
        <source>TEoH Map Files (*</source>
        <translation>TEoH Karten Dateien (*</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/SaveMapAs.cpp" line="61"/>
        <source>Failed to save the map...</source>
        <translation>Speichern der Map fehlgeschlagen...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/SaveMapAs.cpp" line="61"/>
        <source>Saving the map has failed.</source>
        <translation>Das Speichern der Karte ist fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>SaveProject</name>
    <message>
        <source>Failed to save the project...</source>
        <translation type="obsolete">Speichern des Projektes fehlgeschlagen...</translation>
    </message>
    <message>
        <source>Saving the project has failed.</source>
        <translation type="obsolete">Das Speichern des Projektes ist fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>SaveProjectAs</name>
    <message>
        <source>Save Project As...</source>
        <translation type="obsolete">Projekt Speichern Unter...</translation>
    </message>
    <message>
        <source>TEoH Project Files (*.teohproject)</source>
        <translation type="obsolete">TEoH Projekt Dateien (*.teohproject)</translation>
    </message>
    <message>
        <source>TEoH Project Files (*</source>
        <translation type="obsolete">TEoH Projekt Dateien (*</translation>
    </message>
    <message>
        <source>Failed to save the project...</source>
        <translation type="obsolete">Speichern des Projektes fehlgeschlagen...</translation>
    </message>
    <message>
        <source>Saving the project has failed.</source>
        <translation type="obsolete">Das Speichern des Projektes ist fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>SaveWorld</name>
    <message>
        <location filename="Command/Backend/Editor/World/SaveWorld.cpp" line="39"/>
        <location filename="Command/Backend/Editor/World/SaveWorld.cpp" line="47"/>
        <source>Couldn&apos;t save the world...</source>
        <translation>Die World konnte nicht gespeichert werden...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/SaveWorld.cpp" line="40"/>
        <source>The world can only be saved, if it has start maps.
Otherwise no player can be spawned.</source>
        <translation>Die World kann nur gespeichert werden, wenn diese auch Start Maps beinhaltet.
Anderenfalls können keine Spieler gesetzt werden.</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/SaveWorld.cpp" line="47"/>
        <source>Saving the world has failed.</source>
        <translation>Speichern der World ist fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>SaveWorldAs</name>
    <message>
        <location filename="Command/Backend/Editor/World/SaveWorldAs.cpp" line="45"/>
        <source>Save World As...</source>
        <translation>Projekt Speichern Unter...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/SaveWorldAs.cpp" line="47"/>
        <source>TEoH World Files (*</source>
        <translation>TEoH World Dateien (*</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/SaveWorldAs.cpp" line="65"/>
        <location filename="Command/Backend/Editor/World/SaveWorldAs.cpp" line="73"/>
        <source>Couldn&apos;t save the world...</source>
        <translation>Die World konnte nicht gespeichert werden...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/SaveWorldAs.cpp" line="66"/>
        <source>The world can only be saved, if it has start maps.
Otherwise no player can be spawned.</source>
        <translation>Die World kann nur gespeichert werden, wenn diese auch Start Maps beinhaltet.
Anderenfalls können keine Spieler gesetzt werden.</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/World/SaveWorldAs.cpp" line="73"/>
        <source>Saving the world has failed.</source>
        <translation>Speichern der World ist fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="Game/Item/SearchDialog.cpp" line="61"/>
        <source>Take &gt;&gt;</source>
        <translation>Nehmen &gt;&gt;</translation>
    </message>
    <message>
        <location filename="Game/Item/SearchDialog.cpp" line="63"/>
        <source>Take All &gt;&gt;</source>
        <translation>Alles nehmen &gt;&gt;</translation>
    </message>
    <message>
        <location filename="Game/Item/SearchDialog.cpp" line="65"/>
        <source>&lt;&lt; Put</source>
        <translation>&lt;&lt; Ablegen</translation>
    </message>
    <message>
        <location filename="Game/Item/SearchDialog.cpp" line="68"/>
        <source>Done</source>
        <translation>Fertig</translation>
    </message>
    <message>
        <location filename="Game/Item/SearchDialog.cpp" line="70"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>ServerGameModel</name>
    <message>
        <location filename="Game/Model/ServerGameModel.cpp" line="65"/>
        <source>No more players available...</source>
        <translation>Keine Spieler mehr verfügbar...</translation>
    </message>
    <message>
        <location filename="Game/Model/ServerGameModel.cpp" line="66"/>
        <source>No more players are available, therefore I&apos;ll quit the game.</source>
        <translation>Keine Spieler waren verfügbar, darum schließe ich das Spiel.</translation>
    </message>
    <message>
        <location filename="Game/Model/ServerGameModel.cpp" line="142"/>
        <source>{&quot;%1&apos;s turn...&quot; : []}</source>
        <translation>{&quot;%1&apos;s Runde...&quot; : []}</translation>
    </message>
</context>
<context>
    <name>ServerModel</name>
    <message>
        <location filename="Network/ServerModel.cpp" line="130"/>
        <location filename="Network/ServerModel.cpp" line="477"/>
        <location filename="Network/ServerModel.cpp" line="514"/>
        <location filename="Network/ServerModel.cpp" line="521"/>
        <source>TEoH Server</source>
        <translation>TEoH Server</translation>
    </message>
    <message>
        <location filename="Network/ServerModel.cpp" line="131"/>
        <source>Unable to start the server: %1.</source>
        <translation>Server konnte nicht gestartet werden: %1.</translation>
    </message>
    <message>
        <location filename="Network/ServerModel.cpp" line="478"/>
        <source>Server is full. The server has %1 players, which is the most that this server can hold.</source>
        <translation>Server ist voll. Der Server hat %1 Spieler, was der höchsten Anzahl an Spielern entspricht die der Server halten kann.</translation>
    </message>
    <message>
        <location filename="Network/ServerModel.cpp" line="515"/>
        <source>The connection was refused by the peer. Make sure the Clients are all connected in order, and check that the host name and port settings are correct.</source>
        <translation>Die Verbindung wurde von außen abgelehnt. Stelle sicher, dass der TEoH Server gestartet ist und überprüfe ob der Hostname und der Port korrekt sind.</translation>
    </message>
    <message>
        <location filename="Network/ServerModel.cpp" line="522"/>
        <source>The following error occurred: %1.</source>
        <translation>Der folgende Fehler ist aufgetreten: %1.</translation>
    </message>
</context>
<context>
    <name>SetupNewGame</name>
    <message>
        <source>Players choose their starting location?</source>
        <translation type="obsolete">Spieler wählen ihre Startposition?</translation>
    </message>
    <message>
        <source>Players can choose their starting location?</source>
        <translation type="obsolete">Spieler können ihren Startpunkt wählen?</translation>
    </message>
    <message>
        <source>Start Game</source>
        <translation type="obsolete">Spiel starten</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Startmenu/SetupNewGame.cpp" line="43"/>
        <source>New Game...</source>
        <translation>Neues Spiel...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Startmenu/SetupNewGame.cpp" line="48"/>
        <source>General Options</source>
        <translation>Allgemeine Optionen</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Startmenu/SetupNewGame.cpp" line="52"/>
        <source>Solo / Hotseat</source>
        <translation>Solo / Hotseat</translation>
    </message>
    <message>
        <source>Single Player</source>
        <translation type="obsolete">Einzelspieler</translation>
    </message>
    <message>
        <source>Hotseat</source>
        <translation type="obsolete">Hotseat</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Startmenu/SetupNewGame.cpp" line="56"/>
        <source>Network</source>
        <translation>Netzwerk</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Startmenu/SetupNewGame.cpp" line="60"/>
        <source>Play By E-Mail</source>
        <translation>Play By E-Mail</translation>
    </message>
</context>
<context>
    <name>ShowAbout</name>
    <message>
        <location filename="Command/Backend/ShowAbout.cpp" line="33"/>
        <source>About TEoH </source>
        <translation>Über TEoH </translation>
    </message>
    <message>
        <location filename="Command/Backend/ShowAbout.cpp" line="34"/>
        <source>Author: Ryoga Unryu
License: GPLv3</source>
        <translation>Autor: Ryoga Unryu
Lizenz: GPLv3</translation>
    </message>
</context>
<context>
    <name>ShowGameOptions</name>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="73"/>
        <source>Overview</source>
        <translation>Übersicht</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="75"/>
        <source>Save Game</source>
        <translation>Spiel speichern</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="76"/>
        <source>Save the current game.</source>
        <translation>Das aktuelle Spiel speichern.</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="102"/>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="170"/>
        <source>Active Players: %1
New Players Joining: %2
Player Count Overall: %3</source>
        <translation>Aktive Spieler: %1
Neue Spieler: %2
Gesammte Spieleranzahl: %3</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="104"/>
        <source>Solo / Hotseat</source>
        <translation>Solo / Hotseat</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="111"/>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="190"/>
        <source>Add New Player</source>
        <translation>Neuen Spieler hinzufügen</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="112"/>
        <source>Add a new player to the game.</source>
        <translation>Füge einen neuen Spieler zu dem Spiel hinzu.</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="116"/>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="195"/>
        <source>Leave Game</source>
        <translation>Spiel verlassen</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="117"/>
        <source>You can leave the the game. Your turn will be skipped until you join again.
This will not close the game, if there are still players left to play.</source>
        <translation>Du kannst das Spiel verlassen. Deine Runde wird übersprungen bis du wieder dem Spiel betrittst.
Dies wird das Spiel nicht schließen, solange andere Spieler noch im Spiel sind.</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="124"/>
        <source>Network</source>
        <translation>Netzwerk</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="128"/>
        <source>Connections (Player Count: 0):</source>
        <translation>Verbindungen (Spieler Anzahl: 0):</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="175"/>
        <source>Play By E-Mail</source>
        <translation>Play By E-Mail</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="177"/>
        <source>Save Location:</source>
        <translation>Speicherort:</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="181"/>
        <source>&lt;Default Directory&gt;</source>
        <translation>&lt;Vorgabeverzeichnis&gt;</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="191"/>
        <source>Add a new player to the game. His turn will be last.</source>
        <translation>Füge einen neuen Spieler zum Spiel hinzu. Seine Runde ist zuletzt.</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="196"/>
        <source>You can leave the the game. Your turn will end and the game will close.</source>
        <oldsource>You can leave the the game. Your turn will end and the game will close.
Any progress will be lost and the next player&apos;s turn will start.</oldsource>
        <translation>Du kannst das Spiel verlassen. Deine Runde wird beendet und das Spiel wird schließen.</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="230"/>
        <source>New Player?</source>
        <translation>Neuer Spieler?</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="231"/>
        <source>Do you really want to add a new player?</source>
        <translation>Willst du wirklich einen neuen Spieler hinzufügen?</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="239"/>
        <source>New player added.</source>
        <translation>Neuer Spieler hinzugefügt.</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="240"/>
        <source>The new player will create a new character when it&apos;s her turn.
His turn starts after everyone took their turn.</source>
        <translation>Der neue Spieler erstellt einen neuen Charakter, wenn er dran ist.
Seine Runde beginnt nachdem alle ihre Runde beendet haben.</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="249"/>
        <source>Quit Playing?</source>
        <translation>Spiel beenden?</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="250"/>
        <source>Do you really want to quit the game? Your character will not be removed.
Your turn will end and the next player&apos;s turn will start.</source>
        <oldsource>Do you really want to quit the game, while still keeping your character?
Your turn will end and the next player&apos;s turn will start.</oldsource>
        <translation>Willst du wirklich das Spiel verlassen? Dein Charakter wird dabei nicht entfernt.
Deine Runde wird beendet und der nächste Spieler ist dran.</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="265"/>
        <source>Choose a E-Mail Save Location...</source>
        <oldsource>Choose a EMail Save Location...</oldsource>
        <translation>Wähle einen E-Mail Save Ort...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/ShowGameOptions.cpp" line="292"/>
        <source>Connections (Player Count: %1):</source>
        <translation>Verbindungen (Spieler Anzahl: %1):</translation>
    </message>
</context>
<context>
    <name>ShowInventoryContextMenuEditor</name>
    <message>
        <location filename="Command/Backend/Editor/Inventory/ShowInventoryContextMenuEditor.cpp" line="52"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/Inventory/ShowInventoryContextMenuEditor.cpp" line="54"/>
        <source>Cut</source>
        <translation>Ausschneiden</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/Inventory/ShowInventoryContextMenuEditor.cpp" line="56"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="Command/Backend/Editor/Inventory/ShowInventoryContextMenuEditor.cpp" line="58"/>
        <source>Edit...</source>
        <translation>Editieren...</translation>
    </message>
</context>
<context>
    <name>SoloHotseatSetupWidget</name>
    <message>
        <location filename="Game/SoloHotseatSetupWidget.cpp" line="28"/>
        <source>Start Game</source>
        <translation>Spiel starten</translation>
    </message>
</context>
<context>
    <name>SpawnPlayerOnCurrentMap</name>
    <message>
        <location filename="Command/Backend/Game/Ingame/SpawnPlayerOnCurrentMap.cpp" line="38"/>
        <source>Spawning the character has failed...</source>
        <translation>Charaktererzeugung ist fehlgeschlagen...</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/SpawnPlayerOnCurrentMap.cpp" line="39"/>
        <source>The character couldn&apos;t be spwaned on a map because none was slected.</source>
        <translation>Der Charakter konnte nicht erzeugt werden, da keine Karte ausgewählt wurde.</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/SpawnPlayerOnCurrentMap.cpp" line="44"/>
        <source>The Player Character</source>
        <oldsource>Player Character</oldsource>
        <translation>Der Spieler Charakter</translation>
    </message>
</context>
<context>
    <name>StackModel</name>
    <message>
        <source>Remove Player</source>
        <translation type="obsolete">Spieler entfernen</translation>
    </message>
    <message>
        <source>Remove Ceiling</source>
        <translation type="obsolete">Decke entfernen</translation>
    </message>
    <message>
        <source>Remove Wall</source>
        <translation type="obsolete">Wand entfernen</translation>
    </message>
    <message>
        <source>Remove Floor</source>
        <translation type="obsolete">Boden entfernen</translation>
    </message>
    <message>
        <source>Remove Characters...</source>
        <translation type="obsolete">Charakter entfernen...</translation>
    </message>
    <message>
        <source>Remove Items...</source>
        <translation type="obsolete">Gegenstände entfernen...</translation>
    </message>
    <message>
        <source>Remove All</source>
        <translation type="obsolete">Alles entfernen</translation>
    </message>
</context>
<context>
    <name>StartPosition</name>
    <message>
        <location filename="Object/Special/StartPosition.cpp" line="28"/>
        <source>Start Position</source>
        <translation>Startposition</translation>
    </message>
    <message>
        <location filename="Object/Special/StartPosition.cpp" line="29"/>
        <source>This indicates where a player can start from.</source>
        <translation>Dies weist darauf hin, wo der Spieler das Spiel beginnen kann.</translation>
    </message>
    <message>
        <source>Description...</source>
        <translation>Beschreibung...</translation>
    </message>
</context>
<context>
    <name>TwoWeaponAttack</name>
    <message>
        <location filename="Command/Backend/Object/Character/TwoWeaponAttack.cpp" line="89"/>
        <location filename="Command/Backend/Object/Character/TwoWeaponAttack.cpp" line="93"/>
        <source>Light Off-Hand</source>
        <translation>Leichte Off-Hand</translation>
    </message>
    <message>
        <location filename="Command/Backend/Object/Character/TwoWeaponAttack.cpp" line="114"/>
        <source>crits</source>
        <translation>crits</translation>
    </message>
    <message>
        <location filename="Command/Backend/Object/Character/TwoWeaponAttack.cpp" line="118"/>
        <source>hits</source>
        <translation>trifft</translation>
    </message>
    <message>
        <location filename="Command/Backend/Object/Character/TwoWeaponAttack.cpp" line="123"/>
        <source>misses</source>
        <translation>verfehlt</translation>
    </message>
    <message>
        <location filename="Command/Backend/Object/Character/TwoWeaponAttack.cpp" line="126"/>
        <source>%1 %2 %3 with %4.</source>
        <translation>%1 %2 %3 mit %4.</translation>
    </message>
</context>
<context>
    <name>WaitingForDataDialog</name>
    <message>
        <location filename="Network/WaitingForDataDialog.cpp" line="24"/>
        <source>Receving data from Server...</source>
        <translation>Erhalte Daten vom Server...</translation>
    </message>
    <message>
        <location filename="Network/WaitingForDataDialog.cpp" line="25"/>
        <source>Waiting for data from the server...
Press cancel to stop waiting...</source>
        <translation>Warte auf Daten vom Server...
Klick Abbrechen um aufzuhören zu warten...</translation>
    </message>
</context>
<context>
    <name>WorldFileTree</name>
    <message>
        <location filename="Editor/WorldFileTree.cpp" line="59"/>
        <source>New Map...</source>
        <translation>Neue Karte...</translation>
    </message>
    <message>
        <location filename="Editor/WorldFileTree.cpp" line="62"/>
        <source>New Folder...</source>
        <translation>Neuer Ordner...</translation>
    </message>
    <message>
        <location filename="Editor/WorldFileTree.cpp" line="66"/>
        <source>Rename</source>
        <translation>Umbenennen</translation>
    </message>
    <message>
        <location filename="Editor/WorldFileTree.cpp" line="69"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="Editor/WorldFileTree.cpp" line="72"/>
        <source>Duplicate</source>
        <translation>Duplizieren</translation>
    </message>
</context>
<context>
    <name>WorldPropertiesDialog</name>
    <message>
        <location filename="Editor/WorldPropertiesDialog.cpp" line="61"/>
        <source>Add &gt;&gt;</source>
        <translation>Hinzufügen &gt;&gt;</translation>
    </message>
    <message>
        <location filename="Editor/WorldPropertiesDialog.cpp" line="63"/>
        <source>&lt;&lt; Remove</source>
        <translation>&lt;&lt; Entfernen</translation>
    </message>
    <message>
        <location filename="Editor/WorldPropertiesDialog.cpp" line="66"/>
        <source>Available Maps:</source>
        <translation>Verfügbare Maps:</translation>
    </message>
    <message>
        <location filename="Editor/WorldPropertiesDialog.cpp" line="67"/>
        <source>Starting Maps:</source>
        <translation>Starter Maps:</translation>
    </message>
    <message>
        <location filename="Editor/WorldPropertiesDialog.cpp" line="81"/>
        <source>Starting Maps</source>
        <translation>Starter Maps</translation>
    </message>
</context>
<context>
    <name>YesNoCancelDialog</name>
    <message>
        <location filename="Common/YesNoCancelDialog.cpp" line="25"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="Common/YesNoCancelDialog.cpp" line="26"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="Common/YesNoCancelDialog.cpp" line="27"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>YourTurnInformation</name>
    <message>
        <location filename="Command/Backend/Game/Ingame/YourTurnInformation.cpp" line="32"/>
        <source>Your Turn</source>
        <translation>Deine Runde</translation>
    </message>
    <message>
        <location filename="Command/Backend/Game/Ingame/YourTurnInformation.cpp" line="33"/>
        <source>It&apos;s your turn now.</source>
        <translation>Du bist dran.</translation>
    </message>
</context>
</TS>
