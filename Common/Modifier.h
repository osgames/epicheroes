/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MODIFIER_H
#define MODIFIER_H

#include <QObject>
#include <QString>

/** \addtogroup Common
  * \{
  * \class Modifier
  *
  * \brief The modifier base class.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class Modifier : public QObject
{
    Q_OBJECT

protected:
    QString modifierName;
    bool    bonus;

public:
    Modifier(const QString &modifierName = QString(), bool bonus = true, QObject *parent = 0);
    Modifier(const Modifier &other);

    Modifier &operator=(const Modifier &other);

protected:
    void init(const Modifier &other);

public:
    /**
     * @brief Has to be called before it is ensured, that the modifier is correct.
     * @return
     */
    virtual int applyMod() = 0;
    virtual QString showMod(bool withName = true) const = 0;

public:

    // Get-Methods
    virtual int getMod() const = 0;
    bool isBonus() const;
    QString getModifierName() const;

protected:
    QString getModifierOutput() const;
    QString showSign() const;
    int getSign() const;
};

#endif // MODIFIER_H
