/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Modifier.h"

Modifier::Modifier(const QString &modifierName, bool bonus, QObject *parent)
    : QObject(parent), modifierName(modifierName)
{
    this->bonus = bonus;
}

Modifier::Modifier(const Modifier &other)
    : QObject(other.parent())
{
    this->init(other);
}

Modifier &Modifier::operator=(const Modifier &other)
{
    this->init(other);
    return *this;
}

void Modifier::init(const Modifier &other)
{
    this->setParent(other.parent());
    this->modifierName = other.getModifierName();
    this->bonus = other.isBonus();
}

QString Modifier::showSign() const
{
    QString sign;

    if(this->bonus)
    {
        sign = QString("+");
    }
    else
    {
        sign = QString("-");
    }

    return sign;
}

bool Modifier::isBonus() const
{
    return this->bonus;
}

QString Modifier::getModifierName() const
{
    return this->modifierName;
}

QString Modifier::getModifierOutput() const
{
    if(this->modifierName.isEmpty())
    {
        return QString();
    }

    return QString("(%1)").arg(this->modifierName);
}

int Modifier::getSign() const
{
    int sign = 1;

    if(!bonus)
    {
        sign = -1;
    }

    return sign;
}
