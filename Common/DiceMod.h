/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DICE_MOD_H
#define DICE_MOD_H

#include "Common/DiceBase.h"
#include "Common/Modifier.h"

/** \addtogroup Common
  * \{
  * \class DiceMod
  *
  * \brief A dice modifier.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class DiceMod : public Modifier, public DiceBase
{
public:
    DiceMod(int amount = 0, int sides = 0, const QString &modifierName = QString(), bool bonus = true, QObject *parent = 0);
    DiceMod(const DiceMod &other);

    DiceMod &operator=(const DiceMod &other);

private:
    void init(const DiceMod &other);

public:
    virtual int applyMod();
    virtual QString showMod(bool showName = true) const;

    // Get-Methods
    virtual int getMod() const;
};

#endif // DICE_MOD_H
