/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DICE_ATTACK_H
#define DICE_ATTACK_H

#include "Common/Dice.h"

/** \addtogroup Common
  * \{
  * \class DiceAttack
  *
  * \brief Attack dice.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class DiceAttack
{
    Dice attackRoll;
    Dice confirmRoll;
    QString WeaponName;
    QString ACName;
    int AC;
    int critRoll;
    int critMultiplier;

public:
    DiceAttack(const QString &attackName, const QString &weaponName, const QString &ACName = "AC", int AC = 10, int critRoll = 20, int critMultiplier = 2);

    int roll();
    void addModifier(Modifier *modifier);

    // Get-Methods
    QString showAttackResult() const;
    QString showAttackRoll() const;
    QString showConfirmResult() const;
    QString showConfirmRoll() const;
    QString showShortDamageResult(const Dice &damage) const;
    QString showLongDamageResult(const Dice &damage) const;
    QString showCritRange() const;
    QString showCritMultiplier() const;

    bool isHit() const;
    bool isThreat() const;
    bool isCritical() const;
    QString getAttackName() const;
    QString getWeaponName() const;
    int getAttackModsTotal() const;
    int getCritRoll() const;
    int getCritMultiplier() const;

    QString getJSONAttackResult() const;
    QString getJSONConfirmResult() const;
    QString getJSONDamageResult(const Dice &damage) const;
    QString getJSONResult(const Dice &damage) const;

    // Set-Methods
    void setWeaponName(const QString &weaponName);
    void setCritRoll(int critRoll);
    void setCritMultiplier(int critMultiplier);
};

#endif // DICE_ATTACK_H
