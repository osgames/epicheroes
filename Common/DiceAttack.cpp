/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DiceAttack.h"

#include <QObject>

DiceAttack::DiceAttack(const QString &attackName, const QString &weaponName, const QString &ACName, int AC, int critRoll, int critMultiplier)
    : attackRoll(attackName, 1, 20),
      confirmRoll(QObject::tr("Threat Confirm"), 1, 20)
{
    this->ACName = ACName;
    this->WeaponName = weaponName;
    this->AC = AC;
    this->critRoll = critRoll;
    this->critMultiplier = critMultiplier;
}

int DiceAttack::roll()
{
    int result = this->attackRoll.roll();

    if(this->isThreat())
    {
        this->confirmRoll.roll();
    }

    return result;
}

QString DiceAttack::showAttackResult() const
{
    if(!this->attackRoll.isRolled())
    {
        return QObject::tr("Attack not rolled yet.");
    }

    QString result;

    if(this->attackRoll.isRolled())
    {
        if(this->isHit())
        {
            result = QObject::tr("Hit!");
        }
        else
        {
            result = QObject::tr("Miss!");
        }

        result = QString(" : %1").arg(result);
    }

    QString showAC = QString("(vs. %1%2)").arg(this->ACName, result);

    return QObject::tr("%1: Rolled a %2 %3").arg(this->attackRoll.getRollName()).arg(this->attackRoll.getRoll()).arg(showAC);
}

QString DiceAttack::showAttackRoll() const
{
    return this->attackRoll.showJSONRoll();
}

int DiceAttack::getAttackModsTotal() const
{
    return this->attackRoll.getModifierTotal();
}

QString DiceAttack::showConfirmResult() const
{
    if(!this->confirmRoll.isRolled())
    {
        return QObject::tr("Confirm not rolled yet.");
    }

    if(!this->isThreat())
    {
        return QObject::tr("No critical threat.");
    }

    QString result;

    if(this->confirmRoll.isRolled())
    {
        if(this->isCritical())
        {
            result = QObject::tr("Critical!");
        }
        else
        {
            result = QObject::tr("Failed!");
        }

        result = QString(" : %1").arg(result);
    }

    QString showAC = QString("(vs. %1%2)").arg(this->ACName, result);

    return QObject::tr("%1: Rolled %2 %3").arg(this->confirmRoll.getRollName()).arg(this->confirmRoll.getRoll()).arg(showAC);
}

QString DiceAttack::showConfirmRoll() const
{
    return this->confirmRoll.showJSONRoll();
}

QString DiceAttack::showShortDamageResult(const Dice &damage) const
{
    if(!this->attackRoll.isRolled())
    {
        return QObject::tr("Attack not rolled yet.");
    }

    QString shortResult;
    QString critical = "";

    if(this->isHit())
    {
        if(this->isCritical())
        {
           critical = QObject::tr("Crit. ");
        }

        shortResult = QObject::tr("%2 %1Dmg!").arg(critical).arg(damage.getRoll());
    }
    else
    {
        shortResult = QObject::tr("Miss!");
    }

    return shortResult;
}

QString DiceAttack::showLongDamageResult(const Dice &damage) const
{
    if(!this->attackRoll.isRolled())
    {
        return QObject::tr("Attack not rolled yet.");
    }

    QString shortResult;
    QString critical = "";

    if(this->isHit())
    {
        if(this->isCritical())
        {
           critical = QObject::tr("Critical ");
        }

        shortResult = QObject::tr("Rolled %2 %1Damage!").arg(critical).arg(damage.getRoll());
    }
    else
    {
        shortResult = QObject::tr("Missed!");
    }

    return shortResult;
}

QString DiceAttack::showCritRange() const
{
    if(this->critRoll == 20)
    {
        return QString("20");
    }

    return QString("%1-20").arg(this->critRoll);
}

QString DiceAttack::showCritMultiplier() const
{
    return QString("x%1").arg(this->critMultiplier);
}

bool DiceAttack::isHit() const
{
    return this->isThreat() || (this->attackRoll.getRoll() >= this->AC && this->attackRoll.getRoll() > 1);
}

bool DiceAttack::isThreat() const
{
    return this->attackRoll.getNaturalRoll() >= this->critRoll;
}

bool DiceAttack::isCritical() const
{
    return this->isThreat() && this->confirmRoll.getRoll() >= this->AC && this->confirmRoll.getRoll() > 1;
}

void DiceAttack::addModifier(Modifier *modifier)
{
    this->confirmRoll.addModifier(modifier);
    this->attackRoll.addModifier(modifier);
    return;
}

QString DiceAttack::getAttackName() const
{
    return this->attackRoll.getRollName();
}

QString DiceAttack::getWeaponName() const
{
    return this->WeaponName;
}

int DiceAttack::getCritRoll() const
{
    return this->critRoll;
}

int DiceAttack::getCritMultiplier() const
{
    return this->critMultiplier;
}

QString DiceAttack::getJSONAttackResult() const
{
    return QString("{\"%1\" : [%2]}").arg(this->showAttackResult(), this->showAttackRoll());
}

QString DiceAttack::getJSONConfirmResult() const
{
    return QString(",{\"%1\" : [%2]}").arg(this->showConfirmResult()).arg(this->showConfirmRoll());;
}

QString DiceAttack::getJSONDamageResult(const Dice &damage) const
{
    return QString(",{\"%1\" : [%2]}").arg(this->showLongDamageResult(damage)).arg(damage.showJSONRoll());
}

QString DiceAttack::getJSONResult(const Dice &damage) const
{
    QString attackRoll = this->getJSONAttackResult();
    QString confirmRoll = "";
    QString damageRoll = "";

    if(this->isHit())
    {
        if(this->isThreat())
        {
            confirmRoll = this->getJSONConfirmResult();
        }

        damageRoll = this->getJSONDamageResult(damage);
    }

    return attackRoll.append(confirmRoll).append(damageRoll);
}

void DiceAttack::setWeaponName(const QString &weaponName)
{
    this->WeaponName = weaponName;
    return;
}

void DiceAttack::setCritRoll(int critRoll)
{
    this->critRoll = critRoll;
    return;
}

void DiceAttack::setCritMultiplier(int critMultiplier)
{
    this->critMultiplier = critMultiplier;
    return;
}
