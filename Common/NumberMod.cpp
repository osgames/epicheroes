/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "NumberMod.h"

NumberMod::NumberMod(int number, const QString &modifierName, bool bonus, QObject *parent)
    : Modifier(modifierName, bonus, parent)
{
    this->number = number;
}

NumberMod::NumberMod(const NumberMod &other)
    : Modifier(other)
{
    this->init(other);
}

NumberMod &NumberMod::operator=(const NumberMod &other)
{
    this->init(other);
    return *this;
}

void NumberMod::init(const NumberMod &other)
{
    this->Modifier::init(other);
    this->number = other.getNumber();
    return;
}

int NumberMod::applyMod()
{
    return this->number;
}

QString NumberMod::showMod(bool withName) const
{
    QString sign;

    if(this->number >= 0)
    {
        if(this->bonus)
        {
            sign = QString("+");
        }
        else
        {
            sign = QString("-");
        }
    }

    return QString("%1%2%3").arg(sign, QString::number(this->number), withName ? this->getModifierOutput() : "");
}

int NumberMod::getMod() const
{
    int sign = 1;

    if(!bonus)
    {
        sign = -1;
    }

    return sign*this->number;
}

int NumberMod::getNumber() const
{
    return this->number;
}
