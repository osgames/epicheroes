/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DICEATTRIBUTE_H
#define DICEATTRIBUTE_H

#include "Common/Dice.h"

/** \addtogroup Common
  * \{
  * \class DiceAttribute
  *
  * \brief Dice to determine attributes.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class DiceAttribute
{
public:
    enum AttributeRollType {KEEP_HIGHEST_THREE = 0, NORMAL, KEEP_LOWEST_THREE};

private:
    int result;
    AttributeRollType type;
    Dice die1;
    Dice die2;
    Dice die3;
    Dice die4;

public:
    DiceAttribute(AttributeRollType type);

    int roll();
};

#endif // DICEATTRIBUTE_H
