/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DiceAttribute.h"

DiceAttribute::DiceAttribute(DiceAttribute::AttributeRollType type)
    : result(0), type(type), die1("die", 1,6), die2("die",1,6), die3("die",1,6), die4("die",1,6)
{}


int DiceAttribute::roll()
{
    int d1 = this->die1.roll();
    int d2 = this->die2.roll();
    int d3 = this->die3.roll();

    if(this->type != DiceAttribute::NORMAL)
    {
        this->die4.roll();
    }

    int d4 = this->die4.getNaturalRoll();

    this->result = d1+d2+d3;

    if(this->type == DiceAttribute::KEEP_HIGHEST_THREE)
    {
        int lowest = qMin(qMin(d1,d2),qMin(d3,d4));
        this->result += d4-lowest;
    }
    else if(this->type == DiceAttribute::KEEP_LOWEST_THREE)
    {
        int highest = qMax(qMax(d1,d2),qMax(d3,d4));
        this->result += d4-highest;
    }

    return this->result;
}
