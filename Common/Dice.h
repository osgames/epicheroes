/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DICE_H
#define DICE_H

#include <QList>

#include "Common/DiceBase.h"
#include "Common/DiceMod.h"

/** \addtogroup Common
  * \{
  * \class Dice
  *
  * \brief Dice with modifiers.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class Dice : public QObject, public DiceBase
{
    Q_OBJECT

protected:
    QString rollName;
    QList<Modifier *> modifiers;

public:
    Dice(const QString &rollName, int amount, int sides, QObject *parent = 0);
    Dice(const Dice &other);

    virtual int roll();
    virtual QString showDiceRoll() const;
    virtual QString showJSONRoll() const;

    void addModifier(Modifier *modifier);

    int getRoll() const;
    QString getRollName() const;
    int getModifierTotal() const;

    void setRollName(const QString &rollName);
};

#endif // DICE_H
