/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMON_H
#define COMMON_H

#include <QCursor>
#include <QColor>
#include <QSize>
#include <QString>
#include <QtGlobal>
#include <QTime>
#include <QImage>
#include <QPainter>
#include <QDebug>

#include <iostream>
#include <climits>

/**
  * \brief The Epic of Heroes namespace.
  */
namespace TEoH
{
    /**
      * \brief Set if the program is shutting down. No more commands will be processed after this is set. Only set with the Quit Command.
      **/
    extern bool quit;

    /**
      * \brief Version Number.
      */
    const QString TEOH_VERSION = "1.1 Alpha";

    /**
      * \brief Specifies the milliseconds until a file access should be retried. 1 sec = 1 millisecond.
      */
    const int SECONDS_UNTIL_RETRYING_FILE_ACCESS = 1;

    /**
      * \brief Specifies how many times a fileaccess should be retried until it's canceled.
      */
    const int MAX_FILE_ACCESS_RETRIES = 5;

    /**
      * \brief The minimum horizontal square count.
      */
    const int MIN_HORIZONTAL_SQUARE_COUNT = 3;

    /**
      * \brief The maximum horizontal square count.
      */
    const int MAX_HORIZONTAL_SQUARE_COUNT =30;

    /**
      * \brief The standard horizontal square count.
      */
    const int STANDARD_HORIZONTAL_SQUARE_COUNT = 10;

    /**
      * \brief The minimum vertical square count.
      */
    const int MIN_VERTICAL_SQUARE_COUNT = 3;

    /**
      * \brief The maximum vertical square count.
      */
    const int MAX_VERTICAL_SQUARE_COUNT = 20;

    /**
      * \brief The standard vertical square count.
      */
    const int STANDARD_VERTICAL_SQUARE_COUNT = 10;

    /**
      * \brief The minimum height.
      */
    const int MIN_HEIGHT = 1;

    /**
      * \brief The maximum height.
      */
    const int MAX_HEIGHT = 30;

    /**
      * \brief The standard height.
      */
    const int STANDARD_HEIGHT = 1;

    /**
      * \brief The minimum central widget width.
      */
    const int MIN_CENTRAL_WIDGET_WIDTH = 312;

    /**
      * \brief The minimum main window height.
      */
    const int MIN_MAIN_WINDOW_HEIGHT = 312;

    /**
      * \brief The minimum zoomfactor for the square size.
      */
    const int MIN_ZOOM_FACTOR = 1;

    /**
      * \brief The maximum zoomfactor for the square size.
      */
    const int MAX_ZOOM_FACTOR = 3;

    /**
      * \brief The standard zoomfactor for the square size.
      */
    const int STANDARD_ZOOM_FACTOR = 1;

    /**
      * \brief The minimum size of a square in SIZE x SIZE pixel.
      */
    const int MIN_SQUARE_SIZE = MIN_ZOOM_FACTOR*30;

    /**
      * \brief The maximum size of a square in SIZE x SIZE pixel.
      */
    const int MAX_SQUARE_SIZE = MAX_ZOOM_FACTOR*MIN_SQUARE_SIZE;

    /**
      * \brief The standard size of a square in SIZE x SIZE pixel.
      */
    const int STANDARD_SQUARE_SIZE = MIN_SQUARE_SIZE;

    /**
      * \brief The minimum size of a grid line.
      */
    const int MIN_GRID_LINE_SIZE = 1;

    /**
      * \brief The maximum size of a grid line.
      */
    const int MAX_GRID_LINE_SIZE = 8;

    /**
      * \brief The standard size of a grid line.
      */
    const int STANDARD_GRID_LINE_SIZE = 1;

    /**
      * \brief The minimum of characters that has to be used when naming a character.
      */
    const int MIN_PLAYER_NAME_LENGTH = 4;

    /**
      * \brief The maximum of characters that can be used when naming a character.
      */
    const int MAX_PLAYER_NAME_LENGTH = 30;

    /**
      * \brief Regular expression for names the player can enter.
      */
    const QString NAME_REG_EXP_STRING = QString("(\\s|\\w){%1,%2}").arg(TEoH::MIN_PLAYER_NAME_LENGTH).arg(TEoH::MAX_PLAYER_NAME_LENGTH);

    /**
      * \brief Regular experssion for IP Addresses. According to RFC952 and RFC1123.
      */
    const QString IP_ADDRESS_REG_EXP_STRING = QString("([0-9]|[a-z]|[A-Z])([0-9]|-|.|[a-z]|[A-Z])*");

    /**
      * \brief The amount of retries before canceling the sending process.
      */
    const int SEND_DATA_RETRIES = 3;

    /**
      * @brief Represents the player ID used by the server.
      */
    const unsigned int SERVER_PLAYER_ID = 0;

    /**
      * @brief The initial maximum amount of players in an E-Mail Game.
      */
    const int MAX_EMAIL_PLAYER_COUNT = 128;

    /**
     * @brief Represents the decimal places put into integer numbers that represent decimal numbers.
     */
    const int DECIMAL_PLACES = 2;

    /**
      * @brief The standard glow color surrounding objects.
      */
    const QColor STANDARD_OBJECT_GLOW_COLOR = QColor(0xffdbdbdb);

    /**
     * @brief The standard background color of portraits.
     */
    const QColor PORTRAIT_BACKGROUND_COLOR = QColor(0xab,0xab,0xab);

    /**
      * @brief Distance between squares.
      */
    const int DISTANCE_BETWEEN_SQUARES = 5;

    /**
      * @brief The width and height of a condition marker.
      */
    const int CONDITION_SIZE = 8;

    /**
      * @brief The x position on a square to position a condition marker.
      */
    const int CONDITION_X_POSITION_ON_SQUARE = STANDARD_SQUARE_SIZE-CONDITION_SIZE-1;

    /**
     * @brief The standard base speed of most races in the game.
     */
    const int STANDARD_BASE_SPEED = 30;

    /**
      * @brief Show the usage of TEoH.
      */
    inline void showUsage()
    {
        std::cout << "Usage:" << std::endl;
        std::cout << "TEoH [Options] " << std::endl;
        std::cout << "Options: " << std::endl;
        std::cout << "  -m <e-mail game>    Continue an E-Mail Game." << std::endl;
        std::cout << "  -e                  Starts the editor." << std::endl;
        std::cout << "  -l <locale>         Choose a language with giving a locale corseponding" << std::endl;
        std::cout << "                      with the TEoH_XX.qm file, where XX is the locale." << std::endl;
        std::cout << "  -h, --help          Show this message." << std::endl;
        std::cout << "I.e.: 'TEoH -e -l de' for the TEoH editor to be translated in german.\n" << std::endl;
        return;
    }

    /**
      * \brief Write a log output in the outputbuffer, so 6 characters are left to be written.
      * \param s
      */
    inline void writeLog(const QString &s)
    {
        std::cout << s.toStdString() << "..." << std::endl;
        return;
    }

    /**
      * \brief Write a certain string.
      * \param s Is [DONE] if nothing is given.
      */
    inline void writeLogDone(const QString &s = "[DONE]")
    {
        std::cout << s.toStdString() << std::endl;
        return;
    }

    /**
      * \brief Something failed and a given error is written after [FAIL] is written.
      * \param error The error to be written out.
      */
    inline void writeLogFail(const QString &error = "")
    {
        writeLogDone("[FAIL]");
        if(!error.isEmpty())
        {
            qDebug() << error;
        }
        return;
    }

    /**
      * \brief Create a new seed for random numbers.
      */
    inline void newSeed()
    {
        QTime time = QTime::currentTime();
        QPoint pos = QCursor::pos();
        qsrand((time.msec() * time.second() * time.minute() * time.hour()) -
               (pos.x() * pos.y()));
        return;
    }

    /**
      * \brief Get the next higher possible ID.
      */
    inline unsigned int nextID(unsigned int id, const unsigned int lowestID = 0)
    {
        if(id < UINT_MAX)
        {
            id++;
        }
        else
        {
            id = lowestID;
        }

        return id;
    }

    inline QImage drawGlowOnImage(const QImage &image, const QColor &glowColor = TEoH::STANDARD_OBJECT_GLOW_COLOR)
    {
        QImage glowImage = image;
        QPainter painter(&glowImage);
        painter.setPen(glowColor);

        for(int x = 1; x < glowImage.size().width()-1; ++x)
        {
            for(int y = 1; y < glowImage.size().height()-1; ++y)
            {
                if(qAlpha(image.pixel(x,y)) > 0)
                {
                    painter.drawPoint(x-1,y);
                    painter.drawPoint(x,y-1);
                    painter.drawPoint(x+1,y);
                    painter.drawPoint(x,y+1);
                }
            }
        }

        painter.drawImage(0,0,image);

        return glowImage;
    }

    inline QString showWithWhitespacePrefix(int number, int minCharacterCount)
    {
        int absoluteNumber = qAbs(number);
        QString result;
        int i = 10;
        int whitespaceCount = minCharacterCount-1;

        if(number < 0)
        {
            whitespaceCount--;
        }

        while(absoluteNumber >= i)
        {
            whitespaceCount--;
            i *= 10;
        }

        for(int j = 0; j < whitespaceCount; ++j)
        {
            result.append(" ");
        }

        return result.append(QString::number(number));
    }

    inline QString showSign(int number)
    {
        QString result;

        if(number >= 0)
        {
            result = QString("+");
        }

        return result.append(QString::number(number));
    }
}

#endif // COMMON_H
