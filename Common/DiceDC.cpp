/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DiceDC.h"

DiceDC::DiceDC(const QString &rollName, int amount, int sides, const QString &DCName, int DC, const QString &success, const QString &failure, QObject *parent)
    : Dice(rollName, amount, sides, parent)
{
    this->DCName = DCName;
    this->success = success;
    this->failure = failure;
    this->DC = DC;
}

bool DiceDC::isSuccessful() const
{
    return this->getRoll() >= this->DC;
}

QString DiceDC::showDiceRoll() const
{
    QString diceRoll = this->Dice::showDiceRoll();
    QString result;

    if(this->rolled)
    {
        if(this->isSuccessful())
        {
            result = this->success;
        }
        else
        {
            result = this->failure;
        }

        result = QString(" : %1").arg(result);
    }

    QString showDC = QString("(vs. %1%2)").arg(this->DCName, result);

    return QString("%1 %2").arg(diceRoll, showDC);
}

void DiceDC::setDC(int DC)
{
    this->DC = DC;
    return;
}
