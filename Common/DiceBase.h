/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DICE_BASE_H
#define DICE_BASE_H

#include <QString>

/** \addtogroup Common
  * \{
  * \class DiceBase
  *
  * \brief The base class for all dice related classes.
  *
  * \author RyogaU
  *
  * \version 1.1 Alpha
  *
  * Contact: RyogaU@googlemail.com
  * \}
  */
class DiceBase
{
protected:
    int     amount;
    int     sides;
    int     naturalRoll;
    bool    rolled;

public:
    DiceBase(int amount, int sides);
    DiceBase(const DiceBase &other);

protected:
    void init(int amount, int sides, int naturalRoll = 0, bool rolled = false);

public:

    /**
      * @brief Roll the given dice and add the modifiers.
      * @return The added up dice result.
      */
    virtual int roll();

    /**
      * @brief Roll the given dice if not already rolled and add the modifiers.
      * @return The added up dice result in detail.
      */
    virtual QString showDiceRoll() const;

public:
    int getAmount() const;
    int getSides() const;
    int getNaturalRoll() const;
    bool isRolled() const;

    void setRoll(int amount, int sides);

private:
    int rollOne() const;
};

#endif // DICE_BASE_H
