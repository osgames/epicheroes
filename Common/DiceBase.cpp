/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DiceBase.h"

#include "Common/Common.h"

#include <QtGlobal>

DiceBase::DiceBase(int amount, int sides)
{
    this->init(amount, sides);
}

DiceBase::DiceBase(const DiceBase &other)
{
    this->init(other.getAmount(), other.getSides(), other.getNaturalRoll(), other.isRolled());
}

void DiceBase::init(int amount, int sides, int result, bool rolled)
{
    this->setRoll(amount, sides);
    this->naturalRoll = result;
    this->rolled = rolled;
    return;
}

int DiceBase::roll()
{
    for(int i = 0; i < this->amount; ++i)
    {
        this->naturalRoll += this->rollOne();
    }

    this->rolled = true;

    return this->naturalRoll;
}

QString DiceBase::showDiceRoll() const
{
    QString rolledNumber;

    if(this->isRolled())
    {
        rolledNumber = QString("[=%1]").arg(QString::number(this->getNaturalRoll()));
    }

    return QString("%1d%2%3").arg(QString::number(this->amount), QString::number(this->sides), rolledNumber);
}

int DiceBase::getAmount() const
{
    return this->amount;
}

int DiceBase::getSides() const
{
    return this->sides;
}

int DiceBase::getNaturalRoll() const
{
    return this->naturalRoll;
}

bool DiceBase::isRolled() const
{
    return this->rolled;
}

void DiceBase::setRoll(int amount, int sides)
{
    if(amount < 0)
    {
        amount = 0;
    }

    if(sides < 1)
    {
        sides = 1;
    }
    this->amount = amount;
    this->sides = sides;
    return;
}

int DiceBase::rollOne() const
{
    return (qrand()%this->sides)+1;
}
