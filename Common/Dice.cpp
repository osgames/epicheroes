/*
 * A game about cooperative gameplay and defeating the evil empire.
 * Copyright (C) 2015  Ryoga Unryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Dice.h"

#include <QtGlobal>

Dice::Dice(const QString &rollName, int amount, int sides, QObject *parent)
    : QObject(parent),
      DiceBase(amount, sides)
{
    this->rollName = rollName;

}

Dice::Dice(const Dice &other)
    : QObject(other.parent()),
      DiceBase(other.getAmount(), other.getSides())
{
    this->rollName = other.getRollName();
}

int Dice::roll()
{
    if(this->DiceBase::isRolled())
    {
        return this->getRoll();
    }

    this->DiceBase::roll();

    for(int i = 0; i < this->modifiers.size(); ++i)
    {
        this->modifiers[i]->applyMod();
    }

    return this->getRoll();
}

QString Dice::showDiceRoll() const
{
    QString roll = this->DiceBase::showDiceRoll();

    for(int i = 0; i < this->modifiers.size(); ++i)
    {
        roll = roll.append(this->modifiers[i]->showMod());
    }

    if(this->rolled)
    {
        roll = roll.append(QString(" = %1").arg(QString::number(this->getRoll())));
    }

    return roll;
}

QString Dice::showJSONRoll() const
{
    QString json(QString("{\"%1\" : []}").arg(this->DiceBase::showDiceRoll()));

    for(int i = 0; i < this->modifiers.size(); ++i)
    {
        json.append(QString(",{\" %1 (%2)\" : []}").arg(this->modifiers[i]->showMod(false)).arg(this->modifiers[i]->getModifierName()));
    }

    return json;
}

void Dice::addModifier(Modifier *modifier)
{
    if(!modifier)
    {
        return;
    }

    modifier->setParent(this);
    this->modifiers.append(modifier);
    this->rolled = false;
    return;
}

int Dice::getRoll() const
{
    int result = 0;
    result += this->naturalRoll;

    for(int i = 0; i < this->modifiers.size(); ++i)
    {
        result += this->modifiers[i]->getMod();
    }

    return result;
}

QString Dice::getRollName() const
{
    return this->rollName;
}

int Dice::getModifierTotal() const
{
    int result = 0;

    for(int i = 0; i < this->modifiers.size(); ++i)
    {
        result += this->modifiers[i]->getMod();
    }

    return result;
}

void Dice::setRollName(const QString &rollName)
{
    this->rollName = rollName;
    return;
}
